trigger SurveyResult on Survey_Result__c (after delete, after insert, after undelete, after update, 
		before delete, before insert, before update) {
	
	if (SurveyResultHandler.enablesTrigger) {
		if (Trigger.isAfter && Trigger.isInsert) {
			//Recalculate Completeness score
			SurveyResultHandler.recalculateContact(Trigger.new);
		}
		
		if (Trigger.isAfter && Trigger.isUpdate) {
			//Recalculate assigned Contacts
			SurveyResultHandler.recalculateContact(Trigger.oldMap, Trigger.new);
			
			//Recalculate Survey Deisgn
			SurveyResultHandler.recalculateSurvey(Trigger.oldMap, Trigger.new);
		}
	}
}