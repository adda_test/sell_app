trigger Question on Question__c (before insert, after insert, before update, after update, before delete, 
		after delete, after undelete) {
	
	if (QuestionHandler.enableTrigger) {
		if (Trigger.isBefore && Trigger.isInsert) {
			QuestionHandler.setLastActivityDate(Trigger.new);
			
			//Set count of Contacts for each question
			QuestionHandler.calculateCountContacts(Trigger.new);
		}
		
		if (Trigger.isAfter && Trigger.isInsert) {
			//Create Question History
			QuestionHandler.addQuestionHistory(Trigger.new);
			
			//Recalculate Surveys
			QuestionHandler.recalculateSurveyDesign(Trigger.new);
			
			//Recalculate Contacts (Insight Index and Completeness)
			QuestionHandler.recalculateContacts(Trigger.new, false);
			
			//Recalculate Company SumMaxResponse
			QuestionHandler.updateCompanies(Trigger.new);
		}
		
		if (Trigger.isBefore && Trigger.isUpdate) {
			QuestionHandler.setLastActivityDate(Trigger.new);
		}
		
		if (Trigger.isAfter && Trigger.isUpdate) {
			//Create Question History
			QuestionHandler.addQuestionHistory(Trigger.oldMap, Trigger.new);
			
			//Recalculate Surveys
			QuestionHandler.recalculateSurveyDesign(Trigger.oldMap, Trigger.new);
			
			//Recalculate Contacts (Insight Index and Completeness)
			QuestionHandler.recalculateContacts(Trigger.oldMap, Trigger.new, false);
			
			//Change insight panel and recalculate contacts
			QuestionHandler.changeInsightPanel(Trigger.oldMap, Trigger.new);
			
			//Recalculate Company SumMaxResponse
			QuestionHandler.updateCompanies(Trigger.oldMap, Trigger.new);
		}
		
		if (Trigger.isBefore && Trigger.isDelete) {
			//Remove related answers with results
			QuestionHandler.removeAnswers(Trigger.oldMap);
			
			QuestionHandler.deleteSurveyResuls(Trigger.oldMap);
			
			QuestionHandler.deleteQuestionHistory(Trigger.oldMap);
		}
		
		if (Trigger.isAfter && Trigger.isDelete) {
			//Recalculate Contacts (Insight Index and Completeness)
			QuestionHandler.recalculateContacts(Trigger.old, true);
			
			//Recalculate Company SumMaxResponse
			QuestionHandler.updateCompanies(Trigger.old);
		}
	}
}