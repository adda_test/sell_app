trigger AssignContact on AssignContact__c (after delete, after insert, after undelete, after update, 
		before delete, before insert, before update) {
	
	if (AssignContactHandler.enablesTrigger) {
		if (Trigger.isAfter && Trigger.isInsert) {
			AssignContactHandler.recalculateJunctions(Trigger.new);
			
			AssignContactHandler.addHistory(Trigger.new, true);
		}
		
		if (Trigger.isAfter && Trigger.isDelete) {
			AssignContactHandler.recalculateJunctions(Trigger.old);
			
			AssignContactHandler.addHistory(Trigger.old, false);
		}
	}
}