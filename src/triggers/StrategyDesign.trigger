trigger StrategyDesign on Strategy_Design__c (after delete, after insert, after undelete, after update, 
	before delete, before insert, before update) {
	
	if (StrategyDesignHandler.enablesTrigger) {
		if (Trigger.isBefore && Trigger.isInsert) {
			StrategyDesignHandler.updateLastActivity(Trigger.new);
		}
		
		if (Trigger.isAfter && Trigger.isInsert) {
			//Create StrategyDesign History
			StrategyDesignHandler.addStrategyHistory(Trigger.new);
		}
		
		if (Trigger.isBefore && Trigger.isUpdate) {
			StrategyDesignHandler.updateLastActivity(Trigger.new);
		}
		
		if (Trigger.isAfter && Trigger.isUpdate) {
			//Create StrategyDesign History
			StrategyDesignHandler.addStrategyHistory(Trigger.new);
			
			//recalculate next strategy
			StrategyDesignHandler.recalculateContacts(Trigger.oldMap, Trigger.new);
		}
		
		if (Trigger.isBefore && Trigger.isDelete) {
			StrategyDesignHandler.removeAssignStrategy(Trigger.old);
			
			StrategyDesignHandler.deleteStrategyResults(Trigger.oldMap);
			
			StrategyDesignHandler.deleteStrategyQuestions(Trigger.oldMap);
			
			StrategyDesignHandler.deleteStrategyHistory(Trigger.oldMap);
		}
	}
}