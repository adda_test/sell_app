trigger AnswerTrigger on Answer__c (after delete, after insert, after undelete, after update, before delete, 
		before insert, before update) {
	
	if (AnswerHandler.enablesTrigger) {
		if (Trigger.isAfter && Trigger.isInsert) {
			//Recalculate Result that are attached to the same Surveys.
			AnswerHandler.recalculateResults(Trigger.new);
			
			//Recalculate Insight
			//AnswerHandler.recalculateSurvey(Trigger.new, true);
			
			//Recalculate Question
			AnswerHandler.recalculateQuestion(Trigger.new);
		}
		
		if (Trigger.isAfter && Trigger.isUpdate) {
			//Recalculate Result that are attached to the same Surveys.
			AnswerHandler.recalculateResults(Trigger.oldMap, Trigger.new);
			
			//Recalculate Insight
			//AnswerHandler.recalculateSurvey(Trigger.oldMap, Trigger.new, true);
			
			//Recalculate Question
			AnswerHandler.recalculateQuestion(Trigger.oldMap, Trigger.new);
		}
		
		if (Trigger.isAfter && Trigger.isDelete) {
			//Recalculate Result that are attached to the same Surveys.
			//AnswerHandler.recalculateResults(Trigger.old);
			
			//Recalculate Insight
			//AnswerHandler.recalculateSurvey(Trigger.new, false);
			
			//Recalculate Question
			AnswerHandler.recalculateQuestion(Trigger.old);
			
			//Recaluclate contacts
			AnswerHandler.recalculateContacts(Trigger.old);
		}
	}
}