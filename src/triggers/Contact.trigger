trigger Contact on Contact__c (after delete, after insert, after undelete, after update, before delete, 
        before insert, before update) {
    
    if (ContactHandler.enablesTrigger) {
        if (Trigger.isBefore && Trigger.isInsert) {
            //Populate name field
            ContactHandler.populateName(Trigger.new);
        }
        
        if (Trigger.isAfter && Trigger.isInsert) {
            //
            ContactHandler.addAccountHistory(Trigger.new, false);
            
            //Create Contact History
            ContactHandler.addContactHistory(Trigger.new);
            
            ContactHandler.setDefaultInfluence(Trigger.new);
            
            //Recalculate count of Contacts
            ContactHandler.addContactsToAccount(Trigger.new);
            
            //Recalculate Insight
            ContactHandler.recalculateInsights(Trigger.new);
            
            ContactHandler.recalculateStrategyResults(Trigger.new, true);
            
            //Recalculate Strategy
            ContactHandler.recalculateStrategy(Trigger.new);
            
            //Recalculate Users
            ContactHandler.updateUsers(Trigger.new);
        }
        
        if (Trigger.isBefore && Trigger.isUpdate) {
            //Update Last activity date
            ContactHandler.updateLastActivity(Trigger.new);
            
            //Populate name field
            ContactHandler.populateName(Trigger.new);
        }
        
        if (Trigger.isAfter && Trigger.isUpdate) {
            //
            ContactHandler.addAccountHistory(Trigger.oldMap, Trigger.new, false);
            
            //
            ContactHandler.addContactHistory(Trigger.oldMap, Trigger.new);
            
            ContactHandler.setDefaultInfluence(Trigger.oldMap, Trigger.new);
            
            //Recalculate Completeness score and Insight index information
            ContactHandler.recalculateAccount(Trigger.oldMap, Trigger.new);
            
            //Recalculate Insight
            ContactHandler.recalculateInsights(Trigger.oldMap, Trigger.new);
            
            //Recalculate Strategy
            ContactHandler.recalculateStrategy(Trigger.oldMap, Trigger.new);
            
            //Recalculate Users
            ContactHandler.updateUsers(Trigger.oldMap, Trigger.new);
        }
        
        if (Trigger.isBefore && Trigger.isDelete) {
        	ContactHandler.removeAssignContact(Trigger.old);
        	
        	ContactHandler.deleteSurveyResults(Trigger.oldMap);
        	
        	ContactHandler.deleteSurveyAnswers(Trigger.oldMap);
        	
        	ContactHandler.deleteStrategyResults(Trigger.oldMap);
        	
        	ContactHandler.deleteSurveyAnswers(Trigger.oldMap);
        	
        	ContactHandler.deleteContactHistory(Trigger.oldMap);
        }
        
        if (Trigger.isAfter && Trigger.isDelete) {
            //
            ContactHandler.addAccountHistory(Trigger.old, true);
            
            ContactHandler.setDefaultInfluence(Trigger.old);
            
            ContactHandler.recalculateStrategyResults(Trigger.old, false);
            
            //Recalculate count of Contacts
            ContactHandler.removeContactsFromAccount(Trigger.old);
            
            //Recalculate Strategy
            ContactHandler.recalculateStrategy(Trigger.old);
            
            //Recalculate Insight
            ContactHandler.recalculateInsights(Trigger.old);
        }
    }
}