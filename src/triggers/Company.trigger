trigger Company on Company__c (after delete, after insert, after undelete, after update, before delete, 
		before insert, before update) {
	
	if (CompanyHandler.enablesTrigger) {
		if (Trigger.isAfter  && Trigger.isInsert) {
			//Create default Insight Panel
			CompanyHandler.createDefaultPanel(Trigger.new);
			
			CompanyHandler.createCompanyHistory(Trigger.new);
		}
		
		if (Trigger.isAfter && Trigger.isUpdate) {
			//Update Expiration date for related Users
			CompanyHandler.updateExpirationDateForUsers(Trigger.oldMap, Trigger.new);
			
			//Update active status for related users.
			CompanyHandler.updateActiveUsers(Trigger.oldMap, Trigger.new);
			
			CompanyHandler.createCompanyHistory(Trigger.oldMap, Trigger.new);
		}
		
		if (Trigger.isBefore && Trigger.isDelete) {
			//Delete Users
			CompanyHandler.deleteUsers(Trigger.oldMap);
			
			//Delete Accounts
			CompanyHandler.deleteAccounts(Trigger.oldMap);
			
			//Delete Survey Designs
			Companyhandler.deleteSurveyDesigns(Trigger.oldMap);
			
			//Delete Strategy Designs
			Companyhandler.deleteStrategyDesigns(Trigger.oldMap);
			
			//Delete Company History
			CompanyHandler.deleteCompanyHistory(Trigger.oldMap);
		}
	}
}