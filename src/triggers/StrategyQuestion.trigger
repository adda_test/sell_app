trigger StrategyQuestion on Strategy_Question__c (after delete, after insert, after undelete, 
		after update, before delete, before insert, before update) {
	
	if (StrategyQuestionHandler.enablesTrigger) {
		if (Trigger.isAfter && Trigger.isInsert) {
			StrategyQuestionHandler.recalculateResults(Trigger.new);
			
			//StrategyQuestionHandler.updateContacts(Trigger.new);
		}
		
		if (Trigger.isBefore && Trigger.isDelete) {
			StrategyQuestionHandler.removeAnswers(Trigger.old);
		}
		
		if (Trigger.isAfter && Trigger.isDelete) {
			//StrategyQuestionHandler.updateContacts(Trigger.old);
			
			StrategyQuestionHandler.recalculateResults(Trigger.old);
		}
	}
}