trigger SurveyDesign on Survey_Design__c (after delete, after insert, after undelete, after update, 
		before delete, before insert, before update) {
	
	if (SurveyDesignHandler.enablesTrigger) {
		if (Trigger.isAfter && Trigger.isInsert) {
			//Create Survey Result
			//SurveyDesignHandler.createSurveyResult(Trigger.new);
		}
		
		if (Trigger.isBefore && Trigger.isUpdate) {
			//Update Last Activity
			SurveyDesignHandler.populateLastActivity(Trigger.new);
		}
		
		if (Trigger.isBefore && Trigger.isDelete) {
			SurveyDesignHandler.deleteInsights(Trigger.oldMap);
		}
	}
}