trigger Account on Account__c (after delete, after insert, after undelete, after update, before delete, 
        before insert, before update) {
    
    if (AccountHandler.enablesTrigger) {
        if (Trigger.isBefore && Trigger.isInsert) {
            //Update Last Activity
            AccountHandler.populateLastActivity(Trigger.new);
        }
        
        if (Trigger.isAfter && Trigger.isInsert) {
            //Create Account History
            AccountHandler.addAccountHistory(Trigger.new);
            
            //Update Insight information
            AccountHandler.recalculateSurveyDesign(Trigger.new);
        }
        
        if (Trigger.isBefore && Trigger.isUpdate) {
            //Recalculate Completeness score and Insight index information
            AccountHandler.recalculateCompany(Trigger.oldMap, Trigger.new);
            
            //Update Insight information
            AccountHandler.recalculateSurveyDesign(Trigger.oldMap, Trigger.new);
            
            //Update Last Activity
            AccountHandler.populateLastActivity(Trigger.new);
        }
        
        if (Trigger.isAfter && Trigger.isUpdate) {
            //Create Account History
            AccountHandler.addAccountHistory(Trigger.oldMap, Trigger.new);
        }
        
        if (Trigger.isBefore && Trigger.isDelete) {
            //AccountHandler.checkContacts(Trigger.old);
            
            AccountHandler.deleteContacts(Trigger.oldMap);
        }
        
        if (Trigger.isAfter && Trigger.isDelete) {
            AccountHandler.removeAssignStrategies(Trigger.old);
        }
    }
}