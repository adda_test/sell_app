trigger StrategyAnswer on Strategy_Answer__c (after delete, after insert, after undelete, after update, 
	before delete, before insert, before update) {
	
	if (StrategyAnswerHandler.enablesTrigger) {
		if (Trigger.isAfter && Trigger.isInsert) {
			StrategyAnswerHandler.recalculateStrategyResults(Trigger.new, true);
			
			StrategyAnswerHandler.recalculateContacts(Trigger.new);
		}
		
		if (Trigger.isAfter && Trigger.isDelete) {
			StrategyAnswerHandler.recalculateStrategyResults(Trigger.old, false);
			
			StrategyAnswerHandler.recalculateContacts(Trigger.old);
		}
	}
}