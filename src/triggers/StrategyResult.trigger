trigger StrategyResult on Strategy_Result__c (after delete, after insert, after undelete, after update, 
	before delete, before insert, before update) {
	
	if (StrategyResultHandler.enablesTrigger) {
		if (Trigger.isAfter && Trigger.isInsert) {
			StrategyResultHandler.recalculateContacts(Trigger.new);
		}
		
		if (Trigger.isAfter && Trigger.isUpdate) {
			StrategyResultHandler.recalculateContacts(Trigger.oldMap, Trigger.new);
			
			StrategyResultHandler.recalculateStrategies(Trigger.oldMap, Trigger.new);
			
			StrategyResultHandler.recalculateShowResults(Trigger.oldMap, Trigger.new);
			
			StrategyResultHandler.addStrategyHistory(Trigger.oldMap, Trigger.new);
			
			StrategyResultHandler.addContactHistory(Trigger.oldMap, Trigger.new);
		}
		
		if (Trigger.isAfter && Trigger.isDelete) {
			StrategyResultHandler.recalculateContacts(Trigger.old);
			
			StrategyResultHandler.removeAnswers(Trigger.old);
		}
	}
}