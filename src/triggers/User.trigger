trigger User on User__c (after delete, after insert, after undelete, after update, before delete, 
		before insert, before update) {
	
	if (UserHandler.enablesTrigger) {
		if (Trigger.isBefore && Trigger.isInsert) {
			//Set Expiration date
			UserHandler.setExpirationDate(Trigger.new);
			
			//Populate name field
			UserHandler.populateName(Trigger.new);
			
			//Update Last Activity
			UserHandler.updateLastActivity(Trigger.new);
		}
		
		if (Trigger.isAfter && Trigger.isInsert) {
			//Create User History
			UserHandler.addUserHistory(Trigger.new);
			
			UserHandler.updateCompany(Trigger.new);
		}
		
		if (Trigger.isBefore && Trigger.isUpdate) {
			//Update Last Activity
			UserHandler.updateLastActivity(Trigger.new);
			
			//Populate name field
			UserHandler.populateName(Trigger.new);
		}
		
		if (Trigger.isAfter && Trigger.isUpdate) {
			//Create User History
			UserHandler.addUserHistory(Trigger.oldMap, Trigger.new);
		}
		
		if (Trigger.isBefore && Trigger.isDelete) {
			UserHandler.deleteAssigns(Trigger.oldMap);
		}
		
		if (Trigger.isAfter && Trigger.isDelete) {
			UserHandler.updateCompany(Trigger.old);
		}
	}
}