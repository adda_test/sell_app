trigger AssignStrategy on AssignStrategy__c (after delete, after insert, after undelete, after update, 
		before delete, before insert, before update) {
	
	if (AssignStrategyHandler.enablesTrigger) {
		if (Trigger.isAfter && Trigger.isInsert) {
			//Create Account History
			AssignStrategyHandler.createAccountHistory(Trigger.new, true);
			
			//Create Strategy History
			AssignStrategyHandler.createStrategyHistory(Trigger.new, true);
			
			AssignStrategyHandler.recalculateStrategyContacts(Trigger.new, true);
			
			//Create Strategy Results
			AssignStrategyHandler.recalculateStrategyResults(Trigger.new, true);
			
			AssignStrategyHandler.recalculateContacts(Trigger.new);
		}
		
		if (Trigger.isAfter && Trigger.isDelete) {
			//Create Account History
			AssignStrategyHandler.createAccountHistory(Trigger.old, false);
			
			//Create Strategy History
			AssignStrategyHandler.createStrategyHistory(Trigger.old, false);
			
			AssignStrategyHandler.recalculateStrategyContacts(Trigger.old, false);
			
			//Create Strategy Results
			AssignStrategyHandler.recalculateStrategyResults(Trigger.old, false);
			
			AssignStrategyHandler.recalculateContacts(Trigger.old);
		}
	}
}