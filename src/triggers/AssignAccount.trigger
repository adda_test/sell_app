trigger AssignAccount on AssignAccount__c (after delete, after insert, after undelete, after update, 
		before delete, before insert, before update) {
	
	if (AssignAccountHandler.enablesTrigger) {
		if (Trigger.isAfter && Trigger.isInsert) {
			//AssignAccountHandler.updateUsers(Trigger.new);
		}
		
		if (Trigger.isAfter && Trigger.isDelete) {
			//AssignAccountHandler.updateUsers(Trigger.old);
		}
	}
}