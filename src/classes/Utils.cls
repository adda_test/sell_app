public with sharing class Utils {
	
	public static Integer checkStrengthPassword(String passwordInput) {
		String password = String.valueOf(passwordInput);
		Integer strength = 0;
		Integer passwordLength = password.length(); 		
		//1 Rule(check length)
		if (passwordLength == 0) {
			strength -= 100;
		} else if (passwordLength >= 1 && passwordLength <= 3) {
			strength -= 50;
		} else if (passwordLength >= 4 && passwordLength <= 5) {
			strength -= 25;
		} else if (passwordLength >= 6 && passwordLength <= 7) {
			strength -= 12;
		} else if (passwordLength >= 8 && passwordLength <= 9) {
			strength -= 5;
		} else if (passwordLength >= 10 && passwordLength <= 12) {
			strength += 0;
		} else if (passwordLength >= 13 && passwordLength <= 15) {
			strength += 5;
		} else if (passwordLength >= 16 && passwordLength <= 19) {
			strength += 10;
		} else if (passwordLength >= 20 && passwordLength <= 24) {
			strength += 20;
		} else if (passwordLength >= 25 && passwordLength <= 29) {
			strength += 30;
		} else if (passwordLength >= 30) {
			strength += 40;
		}
		
		Boolean matchedResult = Pattern.compile('[a-z]').matcher(password).find();
		if (matchedResult) {
			strength += 6;
		} else {
			strength -= 6;
		}
						
		//4 Rule
		matchedResult = Pattern.compile('[A-Z]').matcher(password).find();
		if (matchedResult) {
			strength += 3;
			
		} else {
			strength -= 3;
		}
		
		//5 Rule
		matchedResult = Pattern.compile('[0-9]').matcher(password).find();
		if (matchedResult) {
			strength += 3;
			
		} else {
			strength -= 3;
		}
		
		//6 Rule
		matchedResult = Pattern.compile('\\W').matcher(password).find();
		if (matchedResult) {
			strength += 3;
			
		} else {
			strength -= 3;
		}
						
		//7 Rule
		Integer countWord = 0;
		Integer countDigit = 0;
		Integer countSymbol = 0;
				
		while (password.length() > 0) {
			Boolean indexWord = Pattern.compile('([a-z]|[A-Z])').matcher(password).find();
			Boolean indexDigit = Pattern.compile('[0-9]').matcher(password).find();
			Boolean indexSymbol = Pattern.compile('\\W').matcher(password).find();
			
			if (indexWord) {
				++countWord;
			} else if (indexDigit) {
				++countDigit;
			} else if (indexSymbol) {
				++countSymbol;
			}
			
			password = password.substring(1);
		}
						
		Integer percentWord = countWord / passwordLength;
		if (percentWord > 0.6 && percentWord < 0.8) {
			strength += 3;
		} else {
			strength -= 3;
		}
		
		Integer percentDigit = countDigit / passwordLength;
		if (percentDigit > 0.2 && percentDigit < 0.3) {
			strength += 3;
		} else {
			strength -= 3;
		}
		
		Integer percentSymbol = countSymbol / passwordLength;
		if (percentSymbol > 0.2 && percentSymbol < 0.3) {
			strength += 3;
		} else {
			strength -= 3;
		}
		
		System.debug('checkStrengthPassword:' + strength);
		return strength;
	}


	public static Boolean isWeakPassword(String password) {
		return checkStrengthPassword(password) < -44;
	}
	
	public static Boolean isTooSmallPassword(String password) {
		return password.length() < 6;
	} 
						
					
}