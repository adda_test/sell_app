public with sharing class AUTH_SignInController {
	public String uName {get; set;}
	public String pass {get; set;}
	public String fpURL {get; set;}
	
	public AUTH_SignInController () {
		uName = '';
		pass = '';
		fpURL = AUTH_StaticVariables.forgotPassURL;
	}
	
	public PageReference init() {
		// check cookie
		if (!String.isEmpty(AUTH_Utils.checkSession()) ) { // all valid (logged), redirect user to home page
			return new PageReference('/apex/Home');
		} else {
			return null;
		}
	}
	
	public Pagereference signIn() {
		// validate parameters
		if (uName == '' || pass == '') {
			if (uName == '') {
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please enter a username.'));
			}
			if (pass == '') {
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please enter a password.'));
			}
			return null;
		}
		
		// get hash
		String passCoded = AUTH_Utils.getMd5(pass);
		
		try {
			User__c v_currentUser = [
				SELECT Username__c, SessionId__c, Password__c 
				FROM User__c
				WHERE (Username__c = :uName OR Email__c = :uName) 
				  AND Password__c = :passCoded 
				  AND Active__c = true 
				LIMIT 1];
			
			update AUTH_Utils.generateSession(v_currentUser);
			AUTH_Utils.updateSessionCookie(v_currentUser);
			
			return new Pagereference(AUTH_StaticVariables.prefixSite + '/Home');
		} catch(Exception e) {
			// AUTH_Utils.clearCookie();
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Invalid username or password.'));
			return null;
		}
	}
}