public abstract class AbstractController {
	//Authorization property
	public Boolean isLoggedCorrect;
	public String userRole {get; private set;}
	public String userCompany;
	public String username {get; set;}
	protected User__c user {get; set;}
	
	//Common property
	public Map<String, String> sObjectNameMap {get; set;}
	
	public String sObjectType {get; set;}
	public String sObjectLabel {get; set;}
	
	public Integer pageIndex {get; set;}
	public Integer countOfPage {get; set;}
	public Integer countRecords {get; set;}
	public List<SelectOption> countRecordOptions {get; set;}
	public String selectedCountRecords {get; set;}
	
	public String sortField {get; set;}
	public String sortOrder {get; set;}
	
	public List<ISA_SObjectHelper.sObjectWrapper> records {get; set;}
	public Map<String, String> fieldSetMap {get; set;}
	public Map<String, Boolean> sortableMap {get; set;}
	public List<String> sortFields {get; set;}
	
	public Map<String, SObjectType> globalDescribe;
	// chart variabels
	public List<WrapperCollection.BubbleWrapper> chartList {get;set;}
	public Map<String, List<WrapperCollection.BubbleWrapper>> seriesMap {get;set;}
	public Map<String, String> seriesColorMap {get;set;}
	public Decimal avgX {
			set;
			get {
				return avgX.setScale(2);
			}}
			
	public Decimal avgY {
			set;
			get {
				return avgY.setScale(2);
			}}
	
	public AbstractController() {
		this.isLoggedCorrect = false;
		this.userRole = '';
		this.userCompany = '';
		this.user = new User__c();
		this.username = 'noname';
		
		globalDescribe = Schema.getGlobalDescribe();
		
		sortField = 'Name';
		sortOrder = 'ASC';
		
		pageIndex = 1;
		countOfPage = 1;
		countRecords = 10;
		
		countRecordOptions = new List<SelectOption> {
			new SelectOption('10','10'), 
			new SelectOption('20','20'), 
			new SelectOption('50','50'), 
			new SelectOption('100','100'), 
			new SelectOption('200','200')
		};
		selectedCountRecords = '10';
		
		this.records = new List<ISA_SObjectHelper.sObjectWrapper>();
		this.fieldSetMap = new Map<String, String>();
		this.sortableMap = new Map<String, Boolean> {'Name' => true};
		this.sortFields = new List<String>();
		
		this.globalDescribe = Schema.getGlobalDescribe();
		this.chartList = new List<WrapperCollection.BubbleWrapper>();
	}
	
	public virtual PageReference init() {
		user = AUTH_Utils.getCheckedUser();
		
		if (user == null) {
			PageReference pr = new PageReference('/apex/signin');
			pr.setRedirect(true);
			
			return pr;
		}
		
		// init chart data storage
		this.seriesMap = new Map<String, List<WrapperCollection.BubbleWrapper>>();
		this.seriesColorMap = new Map<String, String>();
		
		this.isLoggedCorrect = true;
		this.userRole = user.Role__c;
		this.userCompany = user.Company__c;
		this.username = this.user.First_Name__c + ' ' + this.user.Last_Name__c;
		
		this.sObjectNameMap = getObjectByUserRole();
		
		return null;
	}
	
	/**
	* @description 
	*/
	public virtual void fillRecords() {
		records.clear();
		fieldSetMap.clear();
		this.sortFields.clear();
		sortableMap = new Map<String, Boolean> {'Name' => true};
		
		if ( ! String.isEmpty(this.sObjectType) && globalDescribe.containsKey(this.sObjectType)) {
			doFillFields();
			
			String orderClause = doGenerateOrderClause();
			
			String whereClause = doGenerateWhereClause();
			
			List<sObject> selectedRecords = getSelectedRecords(fieldSetMap.values(), whereClause, orderClause);
			
			doCalculatePaging(selectedRecords.size());
			
			getViewRecord(selectedRecords);
		}
	}
	
	//First method
	public virtual void doFillFields() {
		List<Schema.FieldSetMember> fieldSetMembers = globalDescribe.get(this.sObjectType).getDescribe().FieldSets.getMap()
				.get('MainView').getFields();
		
		Map<String, Schema.SObjectField> sObjectFieldMap = globalDescribe.get(this.sObjectType).getDescribe().fields.getMap();
		
		//Collect fields that can be viewed.
		for (Schema.FieldSetMember field : fieldSetMembers) {
			sortFields.add(field.getLabel());
			fieldSetMap.put(field.getLabel(), field.getFieldPath());
			
			sortableMap.put(field.getFieldPath(), sObjectFieldMap.get(field.getFieldPath()).getDescribe().isSortable());
		}
	}
	
	//Second Method
	public virtual String doGenerateOrderClause() {
		return ( ! String.isEmpty(sortField) ? 
				' ORDER BY ' + sortField + ' ' + sortOrder + ' NULLS LAST ' : 
				' ORDER BY Name ASC NULLS LAST ');
	}
	
	//Third method
	public virtual String doGenerateWhereClause() {
		String whereClause = '';
		if (this.userRole == 'Company Admin' || this.userRole == 'Company User') {
			if (this.sObjectLabel == 'User' || this.sObjectLabel == 'Account' || 
				this.sObjectLabel == 'Contact' || this.sObjectLabel == 'Insight' || 
				this.sObjectLabel == 'Strategy') {
				
				whereClause = ' WHERE Company__c = \'' + this.userCompany + '\' ';
			}
		}
		
		return whereClause;
	}
	
	//Fourth method
	public virtual List<sObject> getSelectedRecords(List<String> p_fields, String p_whereClause, String p_orderClause) {
		List<sObject> selectedRecords = new List<sObject>();
		try {
			selectedRecords = Database.query('SELECT Name, ' + String.join(p_fields, ', ') + 
					' FROM ' + this.sObjectType + p_whereClause + p_orderClause + ' LIMIT 50000');
			System.debug('SELECT Name, ' + String.join(p_fields, ', ') + 
					' FROM ' + this.sObjectType + p_whereClause + p_orderClause + ' LIMIT 50000');
		} catch(Exception ex) {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
		}
		
		return selectedRecords;
	}
	
	//Fivth method
	public virtual void doCalculatePaging(Integer p_countRecords) {
		pageIndex = (pageIndex <= 0) ? 1 : pageIndex;
		
		if ( ! String.isEmpty(this.selectedCountRecords) && this.selectedCountRecords.isNumeric()) {
			countRecords = Integer.valueOf(this.selectedCountRecords);
		} else {
			countRecords = 10;
		}
		
		countOfPage = getCountOfPage(p_countRecords, countRecords);
	}
	
	public virtual void getViewRecord(List<sObject> p_records) {
		for (Integer i = countRecords * (pageIndex - 1); i < p_records.size() && i < countRecords * pageIndex; i++) {
			records.add(new ISA_SObjectHelper.sObjectWrapper(p_records.get(i)));
		}
	}
	
	/**
	* @description 
	*/
	public virtual void doSort() {
		sortField = ApexPages.currentPage().getParameters().get('sortField');
		sortOrder = ApexPages.currentPage().getParameters().get('sortOrder');
		
		if (sortableMap.get(sortField)) {
			fillRecords();
		}
	}
	
	/**
	* @description 
	*/
	public virtual void doFirstPage() {
		pageIndex = 1;
		
		fillRecords();
	}
	
	/**
	* @description 
	*/
	public virtual void doPreviousPage() {
		pageIndex = (pageIndex > 1) ? --pageIndex : 1;
		
		fillRecords();
	}
	
	/**
	* @description 
	*/
	public virtual void doNextPage() {
		pageIndex = (pageIndex < countOfPage) ? ++pageIndex : countOfPage;
		
		fillRecords();
	}
	
	/**
	* @description 
	*/
	public virtual void doLastPage() {
		pageIndex = countOfPage;
		
		fillRecords();
	}
	
	/**
	* @description 
	*/
	public virtual void doChange() {
		if ( ! String.isEmpty(selectedCountRecords) && selectedCountRecords.isNumeric()) {
			countRecords = Integer.valueOf(selectedCountRecords);
		}
		
		fillRecords();
	}
	
	/**
	* @description 
	*/
	public virtual PageReference doEditRecord() {
		String v_recordId = ApexPages.currentPage().getParameters().get('editRecordId');
		
		if ( ! String.isEmpty(v_recordId)) {
			PageReference pr = new PageReference(AUTH_StaticVariables.prefixSite + '/ISA_New' + this.sObjectLabel);
			pr.getParameters().put('sId', v_recordId);
			addRetUrl(pr);
			return pr;
		}
		
		return null;
	}
	
	/**
	* @description 
	*/
	public virtual void doRemoveRecord() {
		String v_recordId = ApexPages.currentPage().getParameters().get('removeRecordId');
		
		if ( ! String.isEmpty(v_recordId) && 
				v_recordId.left(3) == globalDescribe.get(this.sObjectType).getDescribe().getKeyPrefix()) {
			
			sObject v_deletedObject = globalDescribe.get(this.sObjectType).newSObject();
			v_deletedObject.Id = v_recordId;
			delete v_deletedObject;
			
			doChange();
		}
	}
	
	/**
	* @description 
	*/
	public virtual Integer getCountOfPage(Integer p_sizeOfRecords, Integer p_countRecords) {
		if (p_sizeOfRecords >= p_countRecords) {
			if (Math.mod(p_sizeOfRecords, p_countRecords) > 0) {
				return Math.round(p_sizeOfRecords / p_countRecords) + 1;
			} else {
				return Math.round(p_sizeOfRecords / p_countRecords);
			}
		}
		return 1;
	}
	
	/**
	* @description 
	*/
	public virtual PageReference doCreateObject() {
		if ( ! String.isEmpty(sObjectLabel)) {
			PageReference pr = new PageReference(AUTH_StaticVariables.prefixSite + '/ISA_New' + this.sObjectLabel);
			addRetUrl(pr);
			pr.setRedirect(true);
			
			return pr;
		}
		
		return null;
	}
	
	/**
	* @description 
	*/
	public virtual void doRemove() {
		List<sObject> deleteRecords = new List<sObject>();
		
		for (ISA_SObjectHelper.sObjectWrapper wrapperObj : records) {
			if (wrapperObj.checked) {
				deleteRecords.add(wrapperObj.record);
			}
		}
		
		if ( ! deleteRecords.isEmpty()) {
			Database.deleteResult[] resultList = Database.delete(deleteRecords, false);
			
			fillRecords();
		}
	}
	
	public PageReference doUploadSObjects() {
		PageReference pr = new PageReference('/apex/ISA_TransportData');
		pr.getParameters().put('sobjectlabel', this.sObjectLabel);
		pr.getParameters().put('tranfertype', 'Import');
		pr.setRedirect(true);
		
		return pr;
	}
	
	public virtual PageReference doViewRecord() {
		String recordId = ApexPages.currentPage().getParameters().get('viewId');
		PageReference pr;
		
		if ( ! String.isEmpty(recordId)) {
			if (this.userRole == 'Company User' && sObjectLabel == 'Contact') {
				pr = new PageReference('/apex/ISA_ViewContactUser');
				pr.getParameters().put('cId', recordId);
				addRetUrl(pr);
			} else if (this.userRole == 'Company Admin' && sObjectLabel == 'Account') {
				pr = new PageReference('/apex/ISA_AccountSummary');
				pr.getParameters().put('Id', recordId);
				addRetUrl(pr);				
			} else {
				pr = new PageReference('/apex/ISA_View' + sObjectLabel);
				pr.getParameters().put('sId', recordId);
				addRetUrl(pr);
			}
		}
		
		return pr;
	}
	
	private Map<String, String> getObjectByUserRole() {
		if (this.userRole == 'Admin') {
			return new Map<String, String> {
				'Company' => 'Company__c', 
				'User' => 'User__c', 
				'Insight' => 'Question__c', 
				'Strategy' => 'Strategy_Design__c'
			};
		} else if (this.userRole == 'Company Admin') {
			return new Map<String, String> {
				'Account' => 'Account__c', 
				'Contact' => 'Contact__c', 
				'User' => 'User__c', 
				'Insight' => 'Question__c', 
				'Strategy' => 'Strategy_Design__c'
			};
		} else if (this.userRole == 'Company User') {
			return new Map<String, String> {
				'Account' => 'Account__c', 
				'Contact' => 'Contact__c', 
				'Insight' => 'Question__c',
				'Strategy' => 'Strategy_Design__c'
			};
		}
		
		return new Map<String, String>();
	}
	
	protected void addRetUrl(PageReference pr) {
		PageReference currPage = ApexPages.currentPage();
		if (currPage.getParameters() != null && currPage.getParameters().get('returnUrl') != null) {
			currPage.getParameters().remove('returnUrl');		   
		}
		// set return url	   
		pr.getParameters().put('returnUrl', URL.getCurrentRequestUrl().getPath());	  
	}
	
	protected virtual void collectRetParams(PageReference pr) {
		// nothing
	}
	
	protected virtual PageReference getPreviousPage() {
		PageReference currPage = ApexPages.currentPage();
		if (currPage.getParameters() != null && currPage.getParameters().get('returnUrl') != null) {
			PageReference pr = new PageReference(currPage.getParameters().get('returnUrl'));
			collectRetParams(pr);
			pr.setRedirect(true);   
			return pr;
		}
		return null;
	}
	
	public virtual PageReference doCancel() {
		// check returnURL parameter
		PageReference prevPage = getPreviousPage();
		if (prevPage != null) {
			return prevPage;
		}
		
		PageReference pr = new PageReference(AUTH_StaticVariables.successLoginURL + this.sObjectLabel);
		pr.setRedirect(true);
		
		return pr;
	}
	
	
}