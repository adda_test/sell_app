public class ISA_TriggerHelper {
	public static void recalculateAccounts(Set<Id> p_accountIds) {
		if ( ! p_accountIds.isEmpty()) {
			List<Account__c> accounts = [
				SELECT Count_Answers__c, Count_Questions__c, Sum_Response__c, Sum_Max_Response__c, 
					Contacts__c, No_Data__c, No_Strategy__c, 
					(SELECT Count_Answers__c, Count_Questions__c, Sum_Response__c, Sum_Max_Response__c, 
						Influence__c, Is_No_Data__c, Is_No_Strategy__c, Insight_Index__c 
					FROM Contacts__r) 
				FROM Account__c 
				WHERE Id IN :p_accountIds 
				LIMIT 10000];
			
			for (Account__c account : accounts) {
				Decimal countAnswers = 0.0;
				Decimal countQuestions = 0.0;
				Decimal sumAccountInsightIndex = 0.0;
				Decimal sumMaxResponses = 0.0;
				Integer countNoData = 0;
				Integer countNoStrategy = 0;
				
				for (Contact__c contact : account.Contacts__r) {
					if (contact.Count_Answers__c != null) {
						countAnswers += contact.Count_Answers__c;
					}
					
					if (contact.Count_Questions__c != null) {
						countQuestions += contact.Count_Questions__c;
					}
					
					if (contact.Insight_Index__c != null && contact.Influence__c != null) {
						sumAccountInsightIndex += (contact.Insight_Index__c * 1.0 * contact.Influence__c) / 100;
					}
					
					if (contact.Is_No_Data__c) {
						++countNoData;
					}
					
					if (contact.Is_No_Strategy__c) {
						++countNoStrategy;
					}
				}
				
				account.Count_Answers__c = countAnswers;
				account.Count_Questions__c = countQuestions;
				account.Insight_Index__c = sumAccountInsightIndex;
				account.No_Data__c = countNoData;
				account.No_Strategy__c = countNoStrategy;
				account.Contacts__c = account.Contacts__r.size();
			}
			
			update accounts;
		}
	}
	
	/**
	* @description 
	*/
	public static void calculateNextStrategy(Set<Id> p_accountIds) {
		if ( ! p_accountIds.isEmpty()) {
			List<AssignStrategy__c> junctions = [
				SELECT Account__c, Strategy__c 
				FROM AssignStrategy__c 
				WHERE Account__c IN :p_accountIds 
				  AND Strategy__c != null 
				ORDER BY Strategy__r.Order__c ASC 
				LIMIT 10000];
			
			//account to startegy ids
			Map<Id, List<Id>> accountToStrategyMap = new Map<Id, List<Id>>();
			Set<Id> strategyIds = new Set<Id>();
			for (AssignStrategy__c junction : junctions) {
				strategyIds.add(junction.Strategy__c);
				
				Id key = junction.Account__c;
				if ( ! accountToStrategyMap.containsKey(key)) {
					accountToStrategyMap.put(key, new List<Id>());
				}
				accountToStrategyMap.get(key).add(junction.Strategy__c);
			}
			
			List<Contact__c> contacts = [
				SELECT Account__c, Next_Strategy__c 
				FROM Contact__c 
				WHERE Account__c IN :p_accountIds
				LIMIT 10000];
			
			Map<Id, List<Id>> accountToContactsMap = new Map<Id, List<Id>>();
			for (Contact__c contact : contacts) {
				Id key = contact.Account__c;
				
				if ( ! accountToContactsMap.containsKey(key)) {
					accountToContactsMap.put(key, new List<Id>());
				}
				accountToContactsMap.get(key).add(contact.Id);
			}
			
			
			Map<Id, Strategy_Design__c> strategyMap = new Map<Id, Strategy_Design__c>([
				SELECT Order__c, 
					(SELECT Contact__c, Is_Completeness__c 
					FROM Strategy_Resuts__r 
					WHERE Contact__c IN :new Map<Id, Contact__c>(contacts).keySet()) 
				FROM Strategy_Design__c 
				WHERE Id IN :strategyIds 
				ORDER BY Order__c ASC 
				LIMIT 10000]);
			
			List<Strategy_Answer__c> answers = [
				SELECT Strategy_Question__r.Strategy_Design__c, Contact__c 
				FROM Strategy_Answer__c 
				WHERE Contact__c IN :new Map<Id, Contact__c>(contacts).keySet() 
				  AND Strategy_Question__r.Strategy_Design__c IN :strategyMap.keySet() 
				ORDER BY CreatedDate DESC 
				LIMIT 10000];
			
			Map<Id, List<Strategy_Answer__c>> contactToAnswerMap = new Map<Id, List<Strategy_Answer__c>>();
			for (Strategy_Answer__c answer : answers) {
				Id key = answer.Contact__c;
				
				if ( ! contactToAnswerMap.containsKey(key)) {
					contactToAnswerMap.put(key, new List<Strategy_Answer__c>());
				}
				contactToAnswerMap.get(key).add(answer);
			}
			
			for (Contact__c contact : contacts) {
				contact.Next_Strategy__c = null;
				
				//Set Current Strategy
				if (contactToAnswerMap.containsKey(contact.Id)) {
					for (Strategy_Answer__c answer : contactToAnswerMap.get(contact.Id)) {
						if (strategyMap.containsKey(answer.Strategy_Question__r.Strategy_Design__c)) {
							Strategy_Design__c strategy = strategyMap.get(answer.Strategy_Question__r.Strategy_Design__c);
							
							for (Strategy_Result__c result : strategy.Strategy_Resuts__r) {
								if (result.Contact__c == contact.Id && ! result.Is_Completeness__c) {
									
									contact.Next_Strategy__c = strategy.Id;
									break;
								}
							}
						}
						
						if (contact.Next_Strategy__c != null) {
							break;
						}
					}
				}
				
				if (accountToStrategyMap.containsKey(contact.Account__c) && contact.Next_Strategy__c == null) {
					for (Id strategyId : accountToStrategyMap.get(contact.Account__c)) {
						Strategy_Design__c nextStrategy = strategyMap.get(strategyId);
						
						for (Strategy_Result__c result : nextStrategy.Strategy_Resuts__r) {
							if (result.Contact__c == contact.Id && ! result.Is_Completeness__c) {
								
								contact.Next_Strategy__c = nextStrategy.Id;
								break;
							}
						}
						
						if (contact.Next_Strategy__c != null) {
							break;
						}
					}
				}
			}
			
			
			update contacts;
		}
	}
	
	/**
	* @description 
	*/
	public static void recalculateStrategyResults(List<AssignStrategy__c> p_newRecords, Set<Id> p_accountIds, 
			Set<Id> p_strategyIds, Boolean p_isInsert) {
		
		if ( ! p_accountIds.isEmpty() && ! p_strategyIds.isEmpty()) {
			if (p_isInsert) {
				List<Contact__c> contacts = [
					SELECT Account__c 
					FROM Contact__c 
					WHERE Account__c IN :p_accountIds
					LIMIT 10000];
				
				Map<Id, Set<Id>> accountToContactsMap = new Map<Id, Set<Id>>();
				for (Contact__c contact : contacts) {
					Id key = contact.Account__c;
					
					if ( ! accountToContactsMap.containsKey(key)) {
						accountToContactsMap.put(key, new Set<Id>());
					}
					accountToContactsMap.get(key).add(contact.Id);
				}
				
				Map<Id, Strategy_Design__c> strategyMap = new Map<Id, Strategy_Design__c>([
					SELECT ID, 
						(SELECT Id 
						FROM Strategy_Questions__r)
					FROM Strategy_Design__c 
					WHERE Id IN :p_strategyIds 
					LIMIT 10000]);
				
				List<Strategy_Result__c> insertResults = new List<Strategy_Result__c>();
				for (AssignStrategy__c junction : p_newRecords) {
					if (accountToContactsMap.containsKey(junction.Account__c)) {
						for (Id contactId : accountToContactsMap.get(junction.Account__c)) {
							insertResults.add(
								new Strategy_Result__c(
									Contact__c = contactId, 
									Strategy_Design__c = junction.Strategy__c, 
									Count_Questions__c = strategyMap.get(junction.Strategy__c).Strategy_Questions__r.size(), 
									Count_Answers__c = 0
								));
						}
					}
				}
				
				insert insertResults;
			} else {
				delete [
					SELECT ID 
					FROM Strategy_Result__c 
					WHERE Strategy_Design__c IN :p_strategyIds 
					LIMIT 10000];
			}
		}
	}
	
	public static void recalculateUsers(Set<Id> p_userIds) {
		if ( ! p_userIds.isEmpty()) {
			List<AssignContact__c> junctions = [
				SELECT User__c, Contact__r.Insight_Index__c, Contact__r.Completeness_Score__c, 
					Contact__r.Account__c, Contact__r.Is_No_Data__c, Contact__r.Is_No_Strategy__c 
				FROM AssignContact__c 
				WHERE User__c IN :p_userIds 
				LIMIT 10000];
			
			Map<Id, List<Contact__c>> userToContactsMap = new Map<Id, List<Contact__c>>();
			Map<Id, Set<Id>> userToAccountsMap = new Map<Id, Set<Id>>();
			
			for (AssignContact__c junction : junctions) {
				Id key = junction.User__c; 
				
				if ( ! userToContactsMap.containsKey(key)) {
					userToContactsMap.put(key, new List<Contact__c>());
				}
				userToContactsMap.get(key).add(junction.Contact__r);
				
				if ( ! userToAccountsMap.containsKey(key)) {
					userToAccountsMap.put(key, new Set<Id>());
				}
				userToAccountsMap.get(key).add(junction.Contact__r.Account__c);
			}
			
			List<User__c> updateUsers = new List<User__c>();
			for (Id userId : p_userIds) {
				if (userToContactsMap.containsKey(userId)) {
					Decimal insightIndexValue = 0.0;
					Decimal completenessValue = 0.0;
					Set<String> noDataAccountValue = new Set<String>();
					Decimal noDataValue = 0.0;
					Decimal noStrategyValue = 0.0;
					
					for (Contact__c contact : userToContactsMap.get(userId)) {
						insightIndexValue += (contact.Insight_Index__c != null) ? contact.Insight_Index__c : 0.0;
						completenessValue += (contact.Completeness_Score__c != null) ? contact.Completeness_Score__c : 0.0;
						noDataValue += (contact.Is_No_Data__c) ? 1 : 0;
						noStrategyValue += (contact.Is_No_Strategy__c) ? 1 : 0;
						
						if (contact.Is_No_Data__c && contact.Account__c != null) {
							noDataAccountValue.add(contact.Account__c);
						}
					}
					
					User__c user = new User__c(
						Id = userId, 
						Contacts__c = userToContactsMap.get(userId).size(), 
						Accounts__c = userToAccountsMap.get(userId).size(), 
						Insight_Index__c = insightIndexValue / userToContactsMap.get(userId).size(), 
						Completeness__c = completenessValue / userToContactsMap.get(userId).size(), 
						Contact_No_Data__c = noDataValue, 
						Account_No_Data__c = noDataAccountValue.size(), 
						No_Strategy__c = noStrategyValue
					);
					
					updateUsers.add(user);
				} else {//User haven't anyone Contact
					User__c user = new User__c(
						Id = userId, 
						Contacts__c = 0, 
						Accounts__c = 0, 
						Insight_Index__c = 0, 
						Completeness__c = 0, 
						Contact_No_Data__c = 0, 
						No_Strategy__c = 0
					);
					
					updateUsers.add(user);
				}
			}
			
			update updateUsers;
		}
	}
	
}