public with sharing class ISA_TransportDataCtrl extends AbstractController {
    //CONSTANT
    private final Map<String, String> OBJECT_NAME_MAP = new Map<String, String> {
        'Account' => 'Account__c', 
        'Contact' => 'Contact__c', 
        'Company' => 'Company__c', 
        'User' => 'User__c', 
        'Question' => 'Question__c', 
        'Insight' => 'Survey_Design__c'
    };
    private final Map<String, String> TRANSFER_FIELD_SET_MAP = new Map<String, String> {
        'Export' => 'ExportFields', 
        'Import' => 'ImportFields'
    };
    
    //PROERTY
    public String recordsInfo {get; set;}
    public Attachment attachment {get; set;}
    public String transferType {get; set;}
    
    //PRIVATE
    private String sObjectLabel;
    private List<Schema.FieldSetMember> fieldSetMembers;
    private List<sObject> selectedRecords;
    private List<String> objectFields;
    
    /**
    * @description Consrtructor
    */
    public ISA_TransportDataCtrl() {
        super();
        
        this.recordsInfo = '';
        this.sObjectLabel = ApexPages.currentPage().getParameters().get('sobjectlabel');
        this.transferType = ApexPages.currentPage().getParameters().get('tranfertype');
        this.selectedRecords = new List<sObject>();
        this.objectFields = new List<String>();
        this.attachment = new Attachment();
    }
    
    /**
    * @description Initialize mehtod for selecting records.
    */
    public void initPage() {
        if (OBJECT_NAME_MAP.containsKey(this.sObjectLabel) && 
            TRANSFER_FIELD_SET_MAP.containsKey(this.transferType)) {
            
            if (this.transferType == 'Export') {
                //Export data to file
                Schema.FieldSet fieldSet = Schema.getGlobalDescribe()
                        .get(OBJECT_NAME_MAP.get(this.sObjectLabel)).getDescribe().FieldSets.getMap()
                        .get(TRANSFER_FIELD_SET_MAP.get(this.transferType));
                
                if (fieldSet != null) {
                    List<Schema.FieldSetMember> fieldSetMembers = fieldSet.getFields();
                    
                    this.objectFields = new List<String>();
                    for (Schema.FieldSetMember fieldMember : fieldSetMembers) {
                        this.objectFields.add(fieldMember.getFieldPath());
                    }
                    
                    try {
                        this.selectedRecords = Database.query('SELECT Name, ' + String.join(this.objectFields, ', ') + 
                                ' FROM ' + OBJECT_NAME_MAP.get(this.sObjectLabel) + ' LIMIT 50000');
                        
                        this.recordsInfo = this.selectedRecords.size() + ' records are available for ' + this.transferType;
                    } catch(Exception ex) {
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
                    }
                } else {
                    ApexPages.addMessage(
                        new ApexPages.Message(
                            ApexPages.Severity.ERROR, 
                            'Please, check FieldSet settings for ' + this.sObjectLabel + ' object.'));
                }
            } else if (this.transferType == 'Import') {
                
            }
        } else {
            ApexPages.addMessage(
                new ApexPages.Message(
                    ApexPages.Severity.ERROR, 'Object is not defined.'));
        }
    }
    
    /**
    * @description Export/Import records
    */
    public void doRun() {
        if (this.transferType == 'Export') {
            List<Folder> folders = [
                SELECT Id 
                FROM Folder 
                WHERE Name = 'ExportFolder' 
                LIMIT 1];
            
            Document doc = new Document(
                Name = this.user.Id + this.sObjectLabel + '.csv', 
                FolderId = folders.get(0).Id
            );
            
            //System.assert(false, '---objectFields=' + objectFields + '----selectedRecords=' + selectedRecords);
            String bodyDocument = String.join(this.objectFields, ',') + ',\n';
            for (sObject obj : this.selectedRecords) {
                for (String fieldName : this.objectFields) {
                    bodyDocument += obj.get(fieldName) + ',';
                }
                bodyDocument += ',\n';
            }
            
            doc.Body = Blob.valueOf(bodyDocument);
            insert doc;
            
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Export <a href="' + 
                    'https://c.ap1.content.force.com/servlet/servlet.FileDownload?file=' + doc.Id + '">data</a>'));
        } else if (this.transferType == 'Import') {
            //Import data to Salesforce from file
            Schema.FieldSet fieldSet = Schema.getGlobalDescribe()
                    .get(OBJECT_NAME_MAP.get(this.sObjectLabel)).getDescribe().FieldSets.getMap()
                    .get(TRANSFER_FIELD_SET_MAP.get(this.transferType));
            
            if (fieldSet != null) {
                List<Schema.FieldSetMember> fieldSetMembers = fieldSet.getFields();
                
                Set<String> objectFieldSet = new Set<String>();
                List<String> objectFields = new List<String>();
                for (Schema.FieldSetMember fieldMember : fieldSetMembers) {
                    objectFieldSet.add(fieldMember.getFieldPath());
                    objectFields.add(fieldMember.getFieldPath());
                }
                
                //Check fields
                //System.assert(false, '----attachment.Body.toString()=' + attachment.Body.toString());
                List<String> dataList = attachment.Body.toString().split(',');
                //System.assert(false, '---dataList=' + dataList.get(0) + '|' + dataList.get(1) + '|' + dataList.get(2) + '|' + dataList.get(3) + '----objectFieldSet=' + objectFieldSet);
                for (Integer i = 0; i < dataList.size() && i < objectFieldSet.size(); i++) {
                    if ( ! objectFieldSet.contains(dataList.get(i))) {
                        ApexPages.addMessage(
                            new ApexPages.Message(
                                ApexPages.Severity.ERROR, 
                                'Please, check your fields in document. They should be ' + 
                                String.join(new List<String>(objectFieldSet), ', ')));
                        
                        return;
                    }
                }
                //System.assert(false, '---dataList=' + dataList);
                //Remove field names from data table.
                Integer index = 0;
                while (index != objectFieldSet.size()) {
                    dataList.remove(0);
                    
                    ++index;
                }
                //System.assert(false, '---dataList=' + dataList);
                //Create a list of sObjects
                List<sObject> insertList = new List<sObject>();
                index = 0;
                sObject obj = getNewSobject(OBJECT_NAME_MAP.get(this.sObjectLabel));
                for (Integer i = 0; i < dataList.size(); i += objectFields.size()) {
                    for (Integer j = 0; j < objectFields.size(); j++) {
                        obj.put(objectFields.get(j), datalist.get(i + j).trim());
                    }
                    //System.assert(false, '----obj=' + obj);
                    insertList.add(obj);
                }
                
                try {
                    //System.assert(false, '----insertList=' + insertList);
                    insert insertList;
                    
                    ApexPages.addMessage(
                        new ApexPages.Message(
                            ApexPages.Severity.INFO, insertList.size() + ' were inserted to database.'));
                } catch(Exception ex) {
                    ApexPages.addMessage(
                            new ApexPages.Message(
                                ApexPages.Severity.ERROR, 
                                ex.getMessage()));
                }
            }
        }
    }
    
    public SObject getNewSobject(String t) {
        /* Call global describe to get the map of string to token. */
        Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe(); 
        
        /* Get the token for the sobject based on the type. */
        Schema.SObjectType st = gd.get(t);
        System.assert(st != null,'Type provided: "' + t + '" doesnt map to an sobject token in this org.');
        
        /* Instantiate the sobject from the token. */
        Sobject s = st.newSobject();
        
        return s;
    }
    
    public PageReference doBack() {
        PageReference pr = new PageReference('/apex/ISA_ListView');
        pr.setRedirect(true);
        
        return pr;
    }
}