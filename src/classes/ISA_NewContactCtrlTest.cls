/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class ISA_NewContactCtrlTest {

    static User__c passUser(id id) {
        User__c testUser = new User__c(
            First_Name__c = 'First', 
            Last_Name__c = 'Last', 
            Phone__c = '123455678', 
            Username__c = 'testLogin', 
            Password__c = 'testPassword', 
            Role__c = 'Admin', 
            Company__c = id,
            Expiration_Date__c = Date.today(), 
            Create_Date__c = Date.today(),
            SessionId__c = '1',
            Active__c = true,
            Email__c = 'test@mail.com');
        return testUser;
    }
    
    static Company__c passCompany() {
        Company__c company = new Company__c(
            Name = 'test name',
            Admin_First_Name__c = 'firstname',
            Admin_Last_Name__c = 'lastname',
            Admin_Email__c = 'test@mail.com',
            Admin_Login__c = 'testLogin',
            Admin_Password__c = 'testPassword',
            Address1__c = 'addr1',
            Address2__c = 'addr2',
            Trial_Start__c = Date.today(),
            Maximum_Users__c = 10);
        return company;
    }
        
	static Account__c passAccount(id companyId,id userId) {
    	Account__c account = new Account__c(
    		Name = 'test contact',
    		First_Name__c = 'first name',
    		Last_Name__c = 'last name', 
    		Phone__c = '123456', 
    		Email__C = 'test@mail.com', 
    		Address1__c = 'addr1', 
    		Address2__c = 'addr2', 
			City__c = 'City', 
			State__c = 'State',
			Zipcode__c = '1234', 
			Company__c = companyId,
			User__c = userId,
			Web_Site__c = 'web-site',
			Country__c = 'country');
		return account;
    } 
    
	static Contact__c passContact(id companyId,id userId,id accountId) {
    	Contact__c contact = new Contact__c(
    		Name = 'test contact',
    		First_Name__c = 'first name',
    		Last_Name__c = 'last name', 
    		Phone_No__c = '123456', 
    		Email__C = 'test@mail.com', 
    		Address1__c = 'addr1', 
    		Address2__c = 'addr2', 
			City__c = 'City', 
			State__c = 'State',
			Pincode__c = '1234', 
			Company__c = companyId,
			User__c = userId,
			Account__c = accountId,
			Country__c = 'country');
		return contact;
    }
    
    @isTest
    private static void testInit() {
        Test.startTest();
        
        Company__c testCompany = passCompany();           
        insert testCompany;      
        User__c testUser = passUser(testCompany.Id);    
        insert testUser; 
        Account__c testAccount = passAccount(testCompany.Id,testUser.Id);    
        insert testAccount; 
        Contact__c testContact = passContact(testCompany.Id,testUser.Id,testAccount.Id);    
        insert testContact; 
                              
        ApexPages.currentPage().getParameters().put('sId', testContact.Id);
        
        ISA_NewContactCtrl controller = new ISA_NewContactCtrl();
        controller.initPage();
        
        System.assertNotEquals(null, controller.init());       
        
        Test.stopTest();
    }  
    
    @isTest
    private static void testInitPageWithContactId() {
        Test.startTest();
        
        Company__c testCompany = passCompany();           
        insert testCompany;      
        User__c testUser = passUser(testCompany.Id);    
        insert testUser; 
        Account__c testAccount = passAccount(testCompany.Id,testUser.Id);    
        insert testAccount; 
        Contact__c testContact = passContact(testCompany.Id,testUser.Id,testAccount.Id);    
        insert testContact; 
                              
        ApexPages.currentPage().getParameters().put('sId', testContact.Id);
        Cookie testCookie = new Cookie('SessionId','1',null,-1,false);
        ApexPages.currentPage().setCookies(new Cookie[] {testCookie});
        
        ISA_NewContactCtrl controller = new ISA_NewContactCtrl();
        controller.initPage();
        System.assertEquals(testContact.Id, controller.contact.Id); 
        System.assertNotEquals(null, controller.companyOptions.size()); 
        System.assertNotEquals(null, controller.accountOptions.size()); 
        System.assertNotEquals(null, controller.userOptions.size());  
        
        controller.doSave();
        System.assertNotEquals(null, controller.doSave());    
        
        controller.doSelectUser();
        System.assertNotEquals(null, controller.userOptions.size());
        
        controller.doSelectAccount();
        System.assertNotEquals(null, controller.accountOptions.size());
        
        Test.stopTest();
    } 
    
    @isTest
    private static void testInitPageWithoutContactId() {
        Test.startTest();
        
        Company__c testCompany = passCompany();           
        insert testCompany;      
        User__c testUser = passUser(testCompany.Id);    
        insert testUser; 
        Account__c testAccount = passAccount(testCompany.Id,testUser.Id);    
        insert testAccount; 
        Contact__c testContact = passContact(testCompany.Id,testUser.Id,testAccount.Id);    
        insert testContact; 
                              
        Cookie testCookie = new Cookie('SessionId','1',null,-1,false);
        ApexPages.currentPage().setCookies(new Cookie[] {testCookie});
        
        ISA_NewContactCtrl controller = new ISA_NewContactCtrl();
        controller.initPage();
        
        System.assertEquals(null, controller.contact.Id);      
        
        Test.stopTest();
    } 
    
    @isTest
    private static void testCollectRetParams() {
        Test.startTest();
        
        PageReference pageRef = ApexPages.currentPage();       
        Company__c testCompany = passCompany();           
        insert testCompany;      
        User__c testUser = passUser(testCompany.Id);    
        insert testUser; 
        Account__c testAccount = passAccount(testCompany.Id,testUser.Id);    
        insert testAccount; 
        Contact__c testContact = passContact(testCompany.Id,testUser.Id,testAccount.Id);    
        insert testContact;     
                              
        ApexPages.currentPage().getParameters().put('sId', testContact.Id);
         
        ISA_NewContactCtrl controller = new ISA_NewContactCtrl();
        controller.collectRetParams(pageRef);
        
        System.assertEquals(ApexPages.currentPage().getParameters().get('sId'), pageRef.getParameters().get('sId'));
        
        Test.stopTest();
    }
    
}