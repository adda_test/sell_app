public with sharing class AnswerHandler {
	public static Boolean enablesTrigger = true;
	
	public static void recalculateResults(List<Answer__c> p_newRecords) {
		recalculateResults(new Map<Id, Answer__c>(), p_newRecords);
	}
	
	public static void recalculateResults(Map<Id, Answer__c> p_oldRecordsMap, List<Answer__c> p_newRecords) {
		Set<Id> contactIds = new Set<Id>();
		Set<Id> questionIds = new Set<Id>();
		
		for (Answer__c answer : p_newRecords) {
			Answer__c oldAnswer = p_oldRecordsMap.get(answer.Id);
			
			if ((oldAnswer != null && 
				(answer.Value__c != oldAnswer.Value__c || answer.Point__c != oldAnswer.Point__c)) || 
					oldAnswer == null) {
				
				if (answer.Contact__c != null) {
					contactIds.add(answer.Contact__c);
				}
				
				if (answer.Question__c != null) {
					questionIds.add(answer.Question__c);
				}
			}
		}
		
		List<Survey_Result__c> results = [
			SELECT Question__c, Contact__c 
			FROM Survey_Result__c 
			WHERE Question__c IN :questionIds 
			  AND Contact__c IN :contactIds 
			LIMIT 10000];
		
		if ( ! results.isEmpty()) {
			List<Survey_Result__c> udpateResults = new List<Survey_Result__c>();
			List<Contact_History__c> contactHistories = new List<Contact_History__c>();
			List<Insight_History__c> insightHistories = new List<Insight_History__c>();
			
			for (Survey_Result__c result : results) {
				for (Answer__c answer : p_newRecords) {
					if (result.Question__c == answer.Question__c && result.Contact__c == answer.Contact__c) {
						Answer__c oldAnswer = p_oldRecordsMap.get(answer.Id);
						
						if (oldAnswer == null) {
							result.IsCompleteness__c = true;
							
							PageReference prUser = new PageReference(AUTH_StaticVariables.prefixSite + '/ISA_ViewUser');
							prUser.getParameters().put('sId', answer.Owner__c);
							
							PageReference prInsight = new PageReference(AUTH_StaticVariables.prefixSite + '/ISA_ViewQuestion');
							prInsight.getParameters().put('sId', answer.Insight__c);
							
							PageReference prContact = new PageReference(AUTH_StaticVariables.prefixSite + '/ISA_ViewContact');
							prContact.getParameters().put('sId', answer.Contact__c);
							
							contactHistories.add(
								new Contact_History__c(
									Contact__c = result.Contact__c, 
									Message__c = '<b><a href="' + prUSer.getUrl() + '">' + answer.Owner_Name__c + 
										'</a></b> completed Insight(<b><a href="' + prInsight.getUrl() + '">' + 
										answer.Insight_Name__c + '</a></b>) for Contact(<b><a href="' + 
										prContact.getUrl() + '">' + answer.Contact_Name__c + '</a></b>)'
								));
							
							insightHistories.add(
								new Insight_History__c(
									Insight__c = result.Question__c, 
									Message__c = '<b><a href="' + prUser.getUrl() + '">' + answer.Owner_Name__c + 
										'</a></b> completed Insight(<b><a href="' + prInsight.getUrl() + '">' + 
										answer.Insight_Name__c + '</a></b>) for Contact(<b><a href="' + 
										prContact.getUrl() + '">' + answer.Contact_Name__c + '</a></b>)'
								));
						}
						
						if (Trigger.isDelete) {
							result.IsCompleteness__c = false;
						}
						
						if ( ! String.isEmpty(answer.Point__c) && answer.Point__c.contains(',')) {
							result.Sum_Response__c = 0;
							
							for (String val : answer.Point__c.split(',')) {
								result.Sum_Response__c += (val.isNumeric()) ? Decimal.valueOf(val) : 0;
							}
						} else if ( ! String.isEmpty(answer.Point__c) && ! answer.Point__c.contains(',')) {
							result.Sum_Response__c = (answer.Point__c.isNumeric()) ? Decimal.valueOf(answer.Point__c) : 0;
						} else {
							result.Sum_Response__c = 0;
						}
					}
				}
				
				udpateResults.add(
					new Survey_Result__c(
						Id = result.Id, 
						Sum_Response__c = result.Sum_Response__c, 
						IsCompleteness__c = result.IsCompleteness__c
					));
			}
			
			update udpateResults;
			
			insert contactHistories;
			insert insightHistories;
		}
	}
	
	public static void recalculateSurvey(List<Answer__c> p_newRecords, Boolean p_isInsert) {
		recalculateSurvey(new Map<Id, Answer__c>(), p_newRecords, p_isInsert);
	}
	
	public static void recalculateSurvey(Map<Id, Answer__c> p_oldRecordMap, List<Answer__c> p_newRecords, Boolean p_insert) {
		Map<Id, Answer__c> answerMap = new Map<Id, Answer__c>();
		for (Answer__c answer : p_newRecords) {
			Answer__c oldAnswer = p_oldRecordMap.get(answer.Id);
			
			if (oldAnswer == null) {
				if (answer.Question__c != null) {
					answerMap.put(answer.Question__c, answer);
				}
			}
		}
		
		if ( ! answerMap.keySet().isEmpty()) {
			List<Question__c> questions = [
				SELECT Survey_Design__c 
				FROM Question__c 
				WHERE Id IN :answerMap.keySet() 
				  AND Survey_Design__c != null 
				LIMIT 10000];
			
			Map<Id, Question__c> questionMap = new Map<Id, Question__c>();
			for (Question__c question : questions) {
				questionMap.put(question.Survey_Design__c, question);
			}
			
			List<Survey_Design__c> surveys = [
				SELECT Count_Answers__c 
				FROM Survey_Design__c 
				WHERE Id IN :questionMap.keySet() 
				LIMIT 10000];
			
			for (Survey_Design__c survey : surveys) {
				Question__c question = questionMap.get(survey.Id);
				Answer__c answer = answerMap.get(question.Id);
				
				if (p_insert && p_oldRecordMap.get(answer.Id) != null) {
					survey.Count_Answers__c = (survey.Count_Answers__c != null) ? survey.Count_Answers__c + 1 : 1;
				} else if ( ! p_insert) {
					survey.Count_Answers__c = (survey.Count_Answers__c != null) ? survey.Count_Answers__c - 1 : 0;
				}
			}
			
			update surveys;
		}
	}
	
	public static void recalculateQuestion(List<Answer__c> p_newRecords) {
		recalculateQuestion(new Map<Id, Answer__c>(), p_newRecords);
	}
	
	public static void recalculateQuestion(Map<Id, Answer__c> p_oldRecordMap, List<Answer__c> p_newRecords) {
		Set<Id> questionIds = new Set<Id>();
		for (Answer__c answer : p_newRecords) {
			Answer__c oldAnswer = p_oldRecordMap.get(answer.Id);
			if ((oldAnswer != null && oldAnswer.Point__c != answer.Point__c) || 
					oldAnswer == null) {
				
				if (answer.Question__c != null) {
					questionIds.add(answer.Question__c);
				}
			}
		}
		
		if ( ! questionIds.isEmpty()) {
			List<Question__c> questions = new List<Question__c>([
				SELECT Id, Survey_Design__r.Company__c,
					(SELECT Point__c 
					FROM Asnwers__r 
					WHERE Point__c != null) 
				FROM Question__c 
				WHERE Id IN :questionIds 
				LIMIT 10000]);
			
			/*Id companyId = (questions.get(0).Survey_Design__r.Company__c != null) 
					? questions.get(0).Survey_Design__r.Company__c 
					: null;
			
			List<Company__c> companies = [
				SELECT Sum_Max_Response__c 
				FROM Company__c 
				WHERE Id = :companyId 
				LIMIT 1];
			
			Decimal sumMaxResponse = 1.0;
			if ( ! companies.isEmpty() && (companies.get(0).Sum_Max_Response__c != null && companies.get(0).Sum_Max_Response__c != 0)) {
				sumMaxResponse = companies.get(0).Sum_Max_Response__c;
			}*/
			
			for (Question__c question : questions) {
				/*Decimal maxResponse = 0.0;
				
				for (Answer__c answer : question.Asnwers__r) {
					if (Decimal.valueOf(answer.Point__c) > maxResponse) {
						maxResponse = Decimal.valueOf(answer.Point__c);
					}
				}
				
				question.Weight__c = ((maxResponse * 1.0) / sumMaxResponse) * 100;*/
				question.Count_Answers__c = question.Asnwers__r.size();
			}
			
			update questions;
		}
	}
	
	public static void recalculateContacts(List<Answer__c> p_oldRecords) {
		Set<Id> questionIds = new Set<Id>();
		Set<Id> contactIds = new Set<Id>();
		for (Answer__c answer : p_oldRecords) {
			if (answer.Question__c != null) {
				questionIds.add(answer.Question__c);
			}
			
			if (answer.Contact__c != null) {
				contactIds.add(answer.Contact__c);
			}
		}
		
		if ( ! questionIds.isEmpty()) {
			delete [
				SELECT Contact__c 
				FROM Survey_Result__c 
				WHERE Question__c IN :questionIds 
				  AND Contact__c IN :contactIds 
				LIMIT 10000];
			
			List<Contact__c> contacts = [
				SELECT Id, 
					(SELECT Point__c 
					FROM Asnwers__r) 
				FROM Contact__c 
				WHERE Id IN :contactIds 
				LIMIT 10000];
			
			if ( ! contacts.isEmpty()) {
				for (Contact__c contact : contacts) {
					Decimal sumResponse = 0.0;
					
					for (Answer__c answer : contact.Asnwers__r) {
						if (answer.Point__c != null && answer.Point__c.isNumeric()) {
							sumResponse += Decimal.valueOf(answer.Point__c);
						}
					}
					contact.Count_Answers__c = contact.Asnwers__r.size();
					contact.Sum_Response__c = sumResponse;
				}
				
				update contacts;
			}
		}
	}
}