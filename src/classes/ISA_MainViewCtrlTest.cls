@isTest
private class ISA_MainViewCtrlTest {
	
	static User__c tempUser(id id) {
        User__c testUser = new User__c(
            First_Name__c = 'First', 
            Last_Name__c = 'Last', 
            Phone__c = '123455678', 
            Username__c = 'testLogin', 
            Password__c = 'testPassword', 
            Role__c = 'Company Admin', 
            Company__c = id,
            Expiration_Date__c = Date.today(), 
            Create_Date__c = Date.today(),
            SessionId__c = '1',
            Active__c = true,
            Email__c = 'test@mail.com');
        return testUser;
    }
    
	static Company__c tempCompany() {
        Company__c company = new Company__c(
            Name = 'test name',
            Admin_First_Name__c = 'firstname',
            Admin_Last_Name__c = 'lastname',
            Admin_Email__c = 'test@mail.com',
            Admin_Login__c = 'testLogin',
            Admin_Password__c = 'testPassword',
            Address1__c = 'addr1',
            Address2__c = 'addr2',
            Trial_Start__c = Date.today(),
            Maximum_Users__c = 10);
        return company;
    }
      
    static Account__c tempAccount(id companyId,id userId) {
    	Account__c account = new Account__c(
    		Name = 'test contact',
    		First_Name__c = 'first name',
    		Last_Name__c = 'last name', 
    		Phone__c = '123456', 
    		Email__C = 'test@mail.com', 
    		Address1__c = 'addr1', 
    		Address2__c = 'addr2', 
			City__c = 'City', 
			Contacts__c = 12,
			Sum_Response__c = 5,
			Sum_Max_Response__c = 12,
			Count_Answers__c = 5,
			Count_Questions__c = 13,
			No_Data__c = 10,
			No_Strategy__c = 10,
			Last_Activity__c = date.today(),
			State__c = 'State',
			Zipcode__c = '1234', 
			Company__c = companyId,
			User__c = userId,
			Web_Site__c = 'web-site',
			Country__c = 'country');
		return account;
    } 
    
	@isTest
    private static void testInit() {
    	
        Test.startTest();
        
        Company__c testCompany = tempCompany();           
        insert testCompany;      
        User__c testUser = tempUser(testCompany.Id);    
        insert testUser; 
        
        ApexPages.currentPage().getParameters().put('sObjectLabel','');
        ApexPages.currentPage().getParameters().put('viewId', testUser.Id);
        ApexPages.currentPage().getParameters().put('nameObject', 'Survey_Design__c');
        ApexPages.currentPage().getParameters().put('labelObject', 'Survey Design');
        ApexPages.currentPage().getParameters().put('sortField','Name');
        ApexPages.currentPage().getParameters().put('sortOrder','ASC');
        ApexPages.currentPage().getParameters().put('removeRecordId', testUser.Id);
        Cookie testCookie = new Cookie('SessionId','1',null,-1,false);
        ApexPages.currentPage().setCookies(new Cookie[] {testCookie});

        ISA_MainViewCtrl controller = new ISA_MainViewCtrl();

        controller.fillRecords();
        
        controller.doSelect();           
        System.assertNotEquals(null, controller.doSelect());   
        
        ApexPages.currentPage().getParameters().put('nameObject', 'Contact__c');
        ApexPages.currentPage().getParameters().put('labelObject', 'Contact');
        
        controller.doSelect();     
        System.assertEquals(null, controller.doSelect());        
        
        controller.doViewRecord();
        System.assertNotEquals(null, controller.doViewRecord());
        
        controller.doSort();
        controller.doFirstPage();
        controller.doPreviousPage();
        controller.doNextPage();
        controller.doLastPage();
        controller.doChange();
        
        controller.doEditRecord();     
        System.assertEquals(null, controller.doEditRecord());  
        
        ApexPages.currentPage().getParameters().put('editRecordId', testUser.Id);
        
        controller.doEditRecord();     
        System.assertNotEquals(null, controller.doEditRecord()); 
        
        controller.doCreateObject();
        System.assertNotEquals(null, controller.doCreateObject());
        
        controller.sObjectLabel = '';
        
        controller.doCreateObject();
        System.assertEquals(null, controller.doCreateObject());        
                
        controller.doRemoveRecord();
        controller.doRemove();    
        
        System.assertEquals(3,controller.getCountOfPage(12, 5));       
        System.assertEquals(2,controller.getCountOfPage(10, 5));     
        System.assertEquals(1,controller.getCountOfPage(5, 10));  
         
        Test.stopTest();
    } 
    
    @isTest
    private static void testGetObjectsByUserRoleCompanyUser() {
    	
    	Test.startTest();
        
        Company__c testCompany = tempCompany();           
        insert testCompany;      
        User__c testUser = tempUser(testCompany.Id);
        testUser.Role__c = 'Company User';    
        insert testUser; 
   
        ApexPages.currentPage().getParameters().put('sObjectLabel','Contact');
        ApexPages.currentPage().getParameters().put('viewId', testUser.Id);
        Cookie testCookie = new Cookie('SessionId','1',null,-1,false);
        ApexPages.currentPage().setCookies(new Cookie[] {testCookie});
	
        ISA_MainViewCtrl controller = new ISA_MainViewCtrl();
        
        controller.selectedCountRecords = '';
        controller.fillRecords();
        
        controller.doViewRecord();
        System.assertNotEquals(null,controller.doViewRecord()); 
        
         Test.stopTest(); 
    }
    
    @isTest
    private static void testGetObjectsByUserRoleAdmin() {
    	
    	Test.startTest();
        
        Company__c testCompany = tempCompany();           
        insert testCompany;      
        User__c testUser = tempUser(testCompany.Id);
        testUser.Role__c = 'Admin';    
        insert testUser; 
        Account__c testAccount = tempAccount(testCompany.Id,testUser.Id);    
        insert testAccount; 
   
        ApexPages.currentPage().getParameters().put('sObjectLabel','Account');
        ApexPages.currentPage().getParameters().put('viewId', testUser.Id);
        ApexPages.currentPage().getParameters().put('removeRecordId', testAccount.Id);
        Cookie testCookie = new Cookie('SessionId','1',null,-1,false);
        ApexPages.currentPage().setCookies(new Cookie[] {testCookie});
	
        ISA_MainViewCtrl controller = new ISA_MainViewCtrl();
        
        controller.selectedCountRecords = '';
        controller.fillRecords();
        
        controller.doViewRecord();
        System.assertNotEquals(null,controller.doViewRecord());  
        
        controller.doRemoveRecord();
    	
    	Test.stopTest();
    }

}