public with sharing class ISA_ViewContactResultCtrl extends AbstractController {
	public Decimal insightIndex {get; set;}
	public Decimal insightCompletenessScore {get; set;}
	public Decimal strategyCompletenessScore {get; set;}
	
	private String contactId;
	
	public ISA_ViewContactResultCtrl() {
		super();
		
		this.insightIndex = 0;
		this.insightCompletenessScore = 0;
		this.strategyCompletenessScore = 0;
		
		this.contactId = '';
	}
	
	public PageReference initPage() {
		PageReference afterInit = init();
		if (afterInit != null) {
			return afterInit;
		}
		
		this.contactId = ApexPages.currentPage().getParameters().get('cId');
		
		if (String.isEmpty(this.contactId)) {
			return null;
		}
		
		List<Contact__c> contacts = [
			SELECT Account__r.Company__c, Insight_Index__c, Completeness_Score__c, 
				Startegy_Completeness_Score__c 
			FROM Contact__c 
			WHERE Id = :this.contactId 
			  AND Account__r.Company__c != null 
			LIMIT 1];
		
		if ( ! contacts.isEmpty()) {
			this.insightCompletenessScore = contacts.get(0).Completeness_Score__c;
			this.insightIndex = contacts.get(0).Insight_Index__c;
			this.strategyCompletenessScore = contacts.get(0).Startegy_Completeness_Score__c;
		}
		
		return null;
	}
	
	public static Decimal getMaxValue(List<String> p_values) {
		Decimal maxValue = -10000.0;
		
		if ( ! p_values.isEmpty()) {
			for (String value : p_values) {
				if (value.isNumeric()) {
					Decimal convertValue = Decimal.valueOf(value);
					
					if (maxValue <= convertValue) {
						maxValue = convertValue;
					}
				}
			}
			return maxValue;
		}
		
		return 0.0;
	}
}