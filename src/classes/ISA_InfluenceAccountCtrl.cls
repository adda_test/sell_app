public with sharing class ISA_InfluenceAccountCtrl extends AbstractController {
	public List<ContactWrapper> contacts {get; set;}
	public String accountName {get; set;}
	
	@TestVisible private String accountId;
	
	public ISA_InfluenceAccountCtrl() {
		super();
		
		this.contacts = new List<ContactWrapper>();
		this.accountName = '';
		
		this.accountId = '';
	}
	
	public PageReference initPage() {
		PageReference pr = super.init();
		if (pr != null) {
			return pr;
		}
		
		this.accountId = ApexPages.currentPage().getParameters().get('aId');
		
		List<Account__c> accounts = [
			SELECT Name, 
				(SELECT Name, Influence__c 
				FROM Contacts__r)
			FROM Account__c 
			WHERE Id = :this.accountId 
			LIMIT 1];
		
		if ( ! accounts.isEmpty()) {
			this.accountName = accounts.get(0).Name;
			
			for (Contact__c contact : accounts.get(0).Contacts__r) {
				this.contacts.add(new ContactWrapper(contact.Id, contact.Name, (Integer)contact.Influence__c));
			}
		}
		
		return null;
	}
	
	public void doSave() {
		String influences = ApexPages.currentPage().getParameters().get('influence');
		
		if ( ! String.isEmpty(influences)) {
			List<String> influenceList = influences.split(',');
			
			for (Integer i = 0; i < this.contacts.size(); i++) {
				this.contacts.get(i).influence = Integer.valueOf(influenceList.get(i));
			}
			
			List<Contact__c> updateContacts = new List<Contact__c>();
			for (ContactWrapper contactW : this.contacts) {
				updateContacts.add(
					new Contact__c(
						Id = contactW.recordId, 
						Influence__c = contactW.influence
				));
			}
			
			update updateContacts;
		}
	}
	
	public override PageReference doCancel() {
		if (accountId != null) {
			PageReference pr = new PageReference('/apex/ISA_ViewAccount');
			pr.getParameters().put('sId', accountId);
			pr.setRedirect(true);
			
			return pr;
		}
		
		return null;
	} 
	
	public class ContactWrapper {
		public String recordId {get; set;}
		public String name {get; set;}
		public Integer influence {get; set;}
		public Boolean locked {get; set;}
		
		public ContactWrapper() {
			this.recordId = '';
			this.name = '';
			this.influence = 0;
		}
		
		public ContactWrapper(Id p_recordId, String p_name, Integer p_influence) {
			this();
			
			this.recordId = p_recordId;
			this.name = p_name;
			this.influence = (p_influence != null) ? p_influence : 0;
		}
	}
}