public with sharing class ISA_GlobalSearchCtrl extends AbstractController {
	public List<String> searchResult {get; set;}
	
	public ISA_GlobalSearchCtrl() {
		super();
		
		searchResult = new List<String>();
	}
	
	public PageReference doSearch() {
		PageReference pr = new PageReference('/apex/ISA_Search');
		pr.getParameters().put('searchString', ApexPages.currentPage().getParameters().get('searchValue'));
		
		return pr;
	}
	
	public void doQuickSearch() {
		String findClause = ApexPages.currentPage().getParameters().get('searchString') + '*';
		
		List<List<sObject>> selectedRecords = [
				FIND :findClause IN Name FIELDS 
				RETURNING Account__c (Name), Contact__c(Name), Company__c(Name), User__c(Name), Question__c(Name) ];
		
		this.searchResult.clear();
		for (List<sObject> records : selectedRecords) {
			
			for (sObject obj : records) {
				searchResult.add(obj.getSObjectType().getDescribe().getLabel() + ' ' + String.valueOf(obj.get('Name')));
			}
		}
	}
}