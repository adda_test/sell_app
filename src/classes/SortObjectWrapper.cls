global class SortObjectWrapper {
	
	public Integer listSize{get;set;}
	public Id objectId{get;set;}
	public String sortOrder{get;set;}
	public String label{get;set;} 
	
	public SortObjectWrapper(Integer s, Id oId, String lbl, String sortOrd) {
		this.listSize = s;
		this.objectId = oId;
		this.sortOrder = sortOrd;
		this.label = lbl;
		
	}
	
	global Integer compareTo(Object compareToObj) { 		
		SortObjectWrapper compareTo = (SortObjectWrapper)compareToObj;
		if (sortOrder == 'ASC') { // ASC
			if(compareTo.listSize > listSize)
				return -1;
			if(compareTo.listSize < listSize)
				return 1;
    		return 0;
		} else { // DESC
			if(compareTo.listSize > listSize)
				return 1;
			if(compareTo.listSize < listSize)
				return -1;
    		return 0;
		}
	}

}