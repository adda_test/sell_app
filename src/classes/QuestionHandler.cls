public with sharing class QuestionHandler {
	public static Boolean enableTrigger = true;
	
	public static void calculateSumMaxResponse(Map<Id, Question__c> p_oldRecordMap, List<Question__c> p_newRecords) {
		Set<Id> surveyIds = new Set<Id>();
		for (Question__c question : p_newRecords) {
			Question__c oldQuestion = p_oldRecordMap.get(question.Id);
			
			if (oldQuestion != null && oldQuestion.Point_Options__c != question.Point_Options__c) {
				if (question.Survey_Design__c != null) {
					surveyIds.add(question.Survey_Design__c);
				}
			}
		}
		
		if ( ! surveyIds.isEmpty()) {
			List<Survey_Design__c> surveys = [
				SELECT Company__c 
				FROM Survey_Design__c 
				WHERE Id IN :surveyIds 
				LIMIT 10000];
			
			Id companyId = surveys.get(0).Company__c;
			
			List<Contact__c> contacts = [
				SELECT Account__r.Company__c 
				FROM Contact__c 
				WHERE Account__r.Company__c = :companyId 
				LIMIT 10000];
			
			List<Survey_Result__c> results = [
				SELECT Question__c 
				FROM Survey_Result__c 
				WHERE Question__c IN :new Map<Id, Question__c>(p_newRecords).keySet() 
				  AND Contact__c IN :new Map<Id, Contact__c>(contacts).keySet() 
				LIMIT 10000];
			
			Map<Id, List<Survey_Result__c>> questionToResultsMap = new Map<Id, List<Survey_Result__c>>();
			for (Survey_Result__c result : results) {
				if ( ! questionToResultsMap.containsKey(result.Question__c)) {
					questionToResultsMap.put(result.Question__c, new List<Survey_Result__c>());
				}
				questionToResultsMap.get(result.Question__c).add(result);
			}
			
			for (Question__c question : p_newRecords) {
				Decimal maxPoint = 0.0;
				if (question.Point_Options__c != null) {
					maxPoint = ISA_ViewContactResultCtrl.getMaxValue(question.Point_Options__c.split(','));
				}
				
				for (Survey_Result__c result : questionToResultsMap.get(question.Id)) {
					result.Sum_Max_Response__c = maxPoint;
				}
			}
			
			update results;
		}
	}
	
	public static void recalculateResult(List<Question__c> p_newRecords) {
		/*List<Survey_Result__c> results = [
			SELECT Survey_Design__c, Count_Questions__c, Sum_Max_Response__c 
			FROM Survey_Result__c 
			WHERE Question__c IN :new Map<Id, Question__c>(p_newRecords).keySet() 
			LIMIT 10000];
		
		if ( ! results.isEmpty()) {
			
			
			List<Survey_Result__c> updateResults = new List<Survey_Result__c>();
			for (Question__c question : p_newRecords) {
				if (surveyToResultMap.containsKey(question.Survey_Design__c)) {
					for (Survey_Result__c result : surveyToResultMap.get(question.Survey_Design__c)) {
						
						result.Count_Questions__c = (result.Count_Questions__c != null) ? result.Count_Questions__c + 1 : 1;
						
						if (question.Point_Options__c != null) {
							Decimal maxPoint = ISA_ViewContactResultCtrl.getMaxValue(question.Point_Options__c.split(','));
							
							if (result.Sum_Max_Response__c != null && maxPoint > result.Sum_Max_Response__c) {
								result.Sum_Max_Response__c = maxPoint;
							}
						}
						
						updateResults.add(result);
					}
				}
			}
			
			if ( ! updateResults.isEmpty()) {
				update updateResults;
			}
		}*/
	}
	
	public static void recalculateSurveyDesign(List<Question__c> p_newRecords) {
		recalculateSurveyDesign(new Map<Id, Question__c>(), p_newRecords);
	}
	
	public static void recalculateSurveyDesign(Map<Id, Question__c> p_oldRecordMap, List<Question__c> p_newRecords) {
		Set<Id> newSurveyIds = new Set<Id>();
		Set<Id> oldSurveyIds = new Set<Id>();
		for (Question__c question : p_newRecords) {
			Question__c oldQuestion = p_oldRecordMap.get(question.Id);
			
			if (oldQuestion != null && (oldQuestion.Survey_Design__c != question.Survey_Design__c)) {
				newSurveyIds.add(question.Survey_Design__c);
				oldSurveyIds.add(oldQuestion.Survey_Design__c);
			} else if (oldQuestion == null) {
				newSurveyIds.add(question.Survey_Design__c);
			}
		}
		
		if ( ! newSurveyIds.isEmpty() && ! oldSurveyIds.isEmpty()) {
			List<Survey_Design__c> surveys = [
				SELECT Count_Questions__c 
				FROM Survey_Design__c 
				WHERE Id IN :newSurveyIds 
				   OR Id IN :oldSurveyIds 
				LIMIT 10000];
			
			for (Survey_Design__c survey : surveys) {
				if (newSurveyIds.contains(survey.Id)) {
					survey.Count_Questions__c = (survey.Count_Questions__c != null) ? survey.Count_Questions__c + 1 : 1;
				}
				
				if (oldSurveyIds.contains(survey.Id)) {
					survey.Count_Questions__c = (survey.Count_Questions__c != null) ? survey.Count_Questions__c - 1 : 0;
				}
			}
			
			update surveys;
		}
	}
	
	public static void setLastActivityDate(List<Question__c> p_newRecords) {
		for (Question__c question : p_newRecords) {
			question.Last_Activity__c = DateTime.now();
		}
	}
	
	public static void calculateCountContacts(List<Question__c> p_newRecords) {
		Set<Id> surveyIds = new Set<Id>();
		for (Question__c question : p_newRecords) {
			if (question.Survey_Design__c != null) {
				surveyIds.add(question.Survey_Design__c);
			}
		}
		
		if ( ! surveyIds.isEmpty()) {
			Map<Id, Survey_Design__c> surveys = new Map<Id, Survey_Design__c>([
				SELECT Company__c 
				FROM Survey_Design__c 
				WHERE Id IN :surveyIds 
				  AND Name != :AUTH_StaticVariables.DEFAULT_INSIGHT_PANEL_NAME 
				LIMIT 10000]);
			
			Map<Id, List<Question__c>> companyToQuestionMap = new Map<Id, List<Question__c>>();
			Set<Id> companyIds = new Set<Id>();
			
			for (Question__c question : p_newRecords) {
				if (surveys.containsKey(question.Survey_Design__c)) {
					Survey_Design__c survey = surveys.get(question.Survey_Design__c);
					Id key = survey.Company__c;
					
					if ( ! companyToQuestionMap.containsKey(key)) {
						companyToQuestionMap.put(key, new List<Question__c>());
						companyIds.add(key);
					}
					companyToQuestionMap.get(key).add(question);
				}
			}
			
			List<Contact__c> contacts = [
				SELECT Company__c, Account__c 
				FROM Contact__c 
				WHERE Company__c IN :companyIds 
				LIMIT 10000];
			
			Map<Id, Integer> companyToContactsMap = new Map<Id, Integer>();
			Map<Id, Set<Id>> companyToAccountsMap = new Map<Id, Set<Id>>();
			
			for (Contact__c contact : contacts) {
				Id key = contact.Company__c;
				
				if ( ! companyToContactsMap.containsKey(key)) {
					companyToContactsMap.put(key, 0);
					companyToAccountsMap.put(key, new Set<Id>());
				}
				companyToContactsMap.put(key, companyToContactsMap.get(key) + 1);
				
				if (contact.Account__c != null) {
					if ( ! companyToAccountsMap.containsKey(key)) {
						companyToAccountsMap.put(key, new Set<Id>());
					}
					companyToAccountsMap.get(key).add(contact.Account__c);
				}
			}
			
			for (Id companyId : companyToQuestionMap.keySet()) {
				for (Question__c question : companyToQuestionMap.get(companyId)) {
					question.Accounts__c = (companyToAccountsMap.containsKey(companyId)) 
						? companyToAccountsMap.get(companyId).size() 
						: 0;
					question.Contacts__c = (companyToContactsMap.containsKey(companyId)) 
						? companyToContactsMap.get(companyId) 
						: 0;
				}
			}
		}
	}
	
	public static void recalculateContacts(List<Question__c> p_newRecords, Boolean p_isDelete) {
		recalculateContacts(new Map<Id, Question__c>(), p_newRecords, p_isDelete);
	}
	
	/*TODO: May be update many questions for different company when will be upload contacts 
			which connected with different Companyies*/
	public static void recalculateContacts(Map<Id, Question__c> p_oldrecordMap, List<Question__c> p_newRecords, 
			Boolean p_isDelete) {
		
		Set<Id> surveyIds = new Set<Id>();
		for (Question__c question : p_newRecords) {
			Question__c oldQuestion = p_oldRecordMap.get(question.Id);
			
			if ((oldQuestion != null && 
				(oldQuestion.Point_Options__c != question.Point_Options__c || 
				oldQuestion.Contacts__c != question.Contacts__c)) || 
					oldQuestion == null) {
				
				if (question.Survey_Design__c != null) {
					surveyIds.add(question.Survey_Design__c);
				}
			}
		}
		
		if ( ! surveyIds.isEmpty()) {
			List<Survey_Design__c> surveys = [
				SELECT Company__c 
				FROM Survey_Design__c 
				WHERE Id IN :surveyIds 
				  AND Name != :AUTH_StaticVariables.DEFAULT_INSIGHT_PANEL_NAME 
				LIMIT 10000];
			
			Set<Id> companyIds = new Set<Id>();
			for (Survey_Design__c survey : surveys) {
				companyIds.add(survey.Company__c);
			}
			
			if ( ! companyIds.isEmpty()) {
				List<Question__c> questions = [
					SELECT Survey_Design__r.Company__c, Point_Options__c, Type__c 
					FROM Question__c 
					WHERE Survey_Design__r.Company__c IN : companyIds 
					  AND Survey_Design__r.Name != :AUTH_StaticVariables.DEFAULT_INSIGHT_PANEL_NAME 
					LIMIT 10000];
				
				Map<Id, List<Question__c>> companyToQuestionsMap = new Map<Id, List<Question__c>>();
				for (Question__c question : questions) {
					Id key = question.Survey_Design__r.Company__c;
					
					if ( ! companyToQuestionsMap.containsKey(key)) {
						companyToQuestionsMap.put(key, new List<Question__c>());
					}
					companyToQuestionsMap.get(key).add(question);
				}
				
				List<Contact__c> contacts = [
					SELECT Company__c, Sum_Max_Response__c, Count_Questions__c 
					FROM Contact__c 
					WHERE Company__c IN :companyIds 
					LIMIT 10000];
				
				Map<Id, List<Contact__c>> companyToContactsMap = new Map<Id, List<Contact__c>>();
				for (Contact__c contact : contacts) {
					if ( ! companyToContactsMap.containsKey(contact.Company__c)) {
						companyToContactsMap.put(contact.Company__c, new List<Contact__c>());
					}
					companyToContactsMap.get(contact.Company__c).add(contact);
				}
				
				List<Contact__c> updateContacts = new List<Contact__c>();
				for (Id companyId : companyIds) {
					Decimal sumMaxPoint = 0.0;
					Integer countQuestions = 0;
					
					if (companyToQuestionsMap.containsKey(companyId)) {
						for (Question__c question : companyToQuestionsMap.get(companyId)) {
							if (question.Point_Options__c != null) {
								++countQuestions;
								
								if (question.Type__c == 'Multi select drop-down') {
									for (String value : question.Point_Options__c.split(',')) {
										sumMaxPoint += Integer.valueOf(value);
									}
								} else {
									sumMaxPoint += ISA_ViewContactResultCtrl.getMaxValue(question.Point_Options__c.split(','));
								}
							}
						}
					}
					
					if (companyToContactsMap.containsKey(companyId)) {
						for (Contact__c contact : companyToContactsMap.get(companyId)) {
							contact.Sum_Max_Response__c = sumMaxPoint;
							contact.Count_Questions__c = countQuestions;
						}
						updateContacts.addAll(companyToContactsMap.get(companyId));
					}
				}
				
				update updateContacts;
			}
		}
	}
	
	public static void changeInsightPanel(Map<Id, Question__c> p_oldRecordMap, List<Question__c> p_newRecords) {
		Set<Id> surveyIds = new Set<Id>();
		for (Question__c question : p_newRecords) {
			Question__c oldQuestion = p_oldRecordMap.get(question.Id);
			
			if (oldQuestion != null && oldQuestion.Survey_Design__c != question.Survey_Design__c) {
				if (question.Survey_Design__c != null) {
					surveyIds.add(question.Survey_Design__c);
				}
				
				if (oldQuestion.Survey_Design__c != null) {
					surveyIds.add(oldQuestion.Survey_Design__c);
				}
			}
		}
		
		if ( ! surveyIds.isEmpty()) {
			Map<Id, Survey_Design__c> surveys = new Map<Id, Survey_Design__c>([
				SELECT Company__c, Name 
				FROM Survey_Design__c 
				WHERE Id IN :surveyIds 
				LIMIT 10000]);
			
			Set<Id> companyIds = new Set<Id>();
			for (Survey_Design__c survey : surveys.values()) {
				companyIds.add(survey.Company__c);
			}
			
			if ( ! companyIds.isEmpty()) {
				List<Question__c> questions = [
					SELECT Survey_Design__r.Company__c, Point_Options__c, Type__c 
					FROM Question__c 
					WHERE Survey_Design__r.Company__c IN : companyIds 
					  AND Survey_Design__r.Name != :AUTH_StaticVariables.DEFAULT_INSIGHT_PANEL_NAME 
					LIMIT 10000];
				
				Map<Id, List<Question__c>> companyToQuestionsMap = new Map<Id, List<Question__c>>();
				for (Question__c question : questions) {
					Id key = question.Survey_Design__r.Company__c;
					
					if ( ! companyToQuestionsMap.containsKey(key)) {
						companyToQuestionsMap.put(key, new List<Question__c>());
					}
					companyToQuestionsMap.get(key).add(question);
				}
				
				List<Contact__c> contacts = [
					SELECT Company__c, Sum_Max_Response__c, Count_Questions__c, Account__c 
					FROM Contact__c 
					WHERE Company__c IN :companyIds 
					LIMIT 10000];
				
				Map<Id, List<Contact__c>> companyToContactsMap = new Map<Id, List<Contact__c>>();
				Map<Id, Set<Id>> companyToAccountsMap = new Map<Id, Set<Id>>();
				for (Contact__c contact : contacts) {
					if ( ! companyToContactsMap.containsKey(contact.Company__c)) {
						companyToContactsMap.put(contact.Company__c, new List<Contact__c>());
						companyToAccountsMap.put(contact.Company__c, new Set<Id>());
					}
					companyToContactsMap.get(contact.Company__c).add(contact);
					companyToAccountsMap.get(contact.Company__c).add(contact.Account__c);
				}
				
				List<Contact__c> updateContacts = new List<Contact__c>();
				List<Question__c> updateQuestions = new List<Question__c>();
				for (Id companyId : companyIds) {
					Decimal sumMaxPoint = 0.0;
					Integer countQuestions = 0;
					
					if (companyToQuestionsMap.containsKey(companyId)) {
						for (Question__c question : companyToQuestionsMap.get(companyId)) {
							if (question.Point_Options__c != null) {
								++countQuestions;
								
								if (question.Type__c == 'Multi select drop-down') {
									for (String value : question.Point_Options__c.split(',')) {
										sumMaxPoint += Integer.valueOf(value);
									}
								} else {
									sumMaxPoint += ISA_ViewContactResultCtrl.getMaxValue(question.Point_Options__c.split(','));
								}
							}
							
							if (companyToContactsMap.containsKey(companyId)) {
								question.Contacts__c = companyToContactsMap.get(companyId).size();
								question.Accounts__c = companyToAccountsMap.get(companyId).size();
								updateQuestions.add(question);
							}
						}
					}
					
					if (companyToContactsMap.containsKey(companyId)) {
						for (Contact__c contact : companyToContactsMap.get(companyId)) {
							contact.Sum_Max_Response__c = sumMaxPoint;
							contact.Count_Questions__c = countQuestions;
						}
						updateContacts.addAll(companyToContactsMap.get(companyId));
					}
				}
				
				update updateContacts;
				
				//Delete answers for unassigned insights
				List<Id> questionIds = new List<Id>();
				
				for (Question__c question : p_newRecords) {
					Survey_Design__c survey = surveys.get(question.Survey_Design__c);
					
					if (survey != null && survey.Name == AUTH_StaticVariables.DEFAULT_INSIGHT_PANEL_NAME) {
						questionIds.add(question.Id);
						
						updateQuestions.add(new Question__c(
							Id = question.Id, 
							Contacts__c = 0,
							Accounts__c = 0
						));
					}
				}
				
				if ( ! questionIds.isEmpty()) {
					delete [
						SELECT Id 
						FROM Answer__c 
						WHERE Question__c IN :questionIds 
						LIMIT 10000];
				}
				
				update updateQuestions;
			}
		}
	}
	
	public static void removeAnswers(Map<Id, Question__c> p_oldRecordMap) {
		List<Answer__c> answers = [
			SELECT Id 
			FROM Answer__c 
			WHERE Question__c IN :p_oldRecordMap.keySet() 
			LIMIT 10000];
		
		delete answers;
	}
	
	public static void updateCompanies(List<Question__c> p_newRecords) {
		updateCompanies(new Map<Id, Question__c>(), p_newRecords);
	}
	
	public static void updateCompanies(Map<Id, Question__c> p_oldRecordMap, List<Question__c> p_newRecords) {
		Set<Id> surveyIds = new Set<Id>();
		for (Question__c question : p_newRecords) {
			Question__c oldQuestion = p_oldRecordMap.get(question.Id);
			
			if ((oldQuestion != null && (oldQuestion.Point_Options__c != question.Point_Options__c)) || 
					oldQuestion == null) {
				
				if (question.Survey_Design__c != null) {
					surveyIds.add(question.Survey_Design__c);
				}
			}
		}
		
		if ( ! surveyIds.isEmpty()) {
			List<Survey_Design__c> surveys = [
				SELECT Company__c 
				FROM Survey_Design__c 
				WHERE ID IN :surveyIds 
				LIMIT 10000];
			
			Set<Id> companyIds = new Set<Id>();
			for (Survey_Design__c survey : surveys) {
				if (survey.Company__c != null) {
					companyIds.add(survey.Company__c);
				}
			}
			
			List<Question__c> questions = [
				SELECT Survey_Design__r.Company__c, Point_Options__c 
				FROM Question__c 
				WHERE Survey_Design__r.Company__c IN :companyIds 
				LIMIT 10000];
			
			Map<Id, Integer> companyToResponseMap = new Map<Id, Integer>();
			for (Question__c question : questions) {
				Id key = question.Survey_Design__r.Company__c;
				
				if (question.Point_Options__c != null) {
					Integer maxPoint = (Integer)ISA_ViewContactResultCtrl.getMaxValue(question.Point_Options__c.split(','));
					
					if ( ! companyToResponseMap.containsKey(key)) {
						companyToResponseMap.put(key, maxPoint);
					} else {
						companyToResponseMap.put(key, companyToResponseMap.get(key) + maxPoint);
					}
				}
			}
			
			if ( ! companyToResponseMap.keySet().isEmpty()) {
				List<Company__c> updateCompanies = new List<Company__c>();
				
				for (Id companyId : companyToResponseMap.keySet()) {
					updateCompanies.add(
						new Company__c(
							Id = companyId, 
							Sum_Max_Response__c = companyToResponseMap.get(companyId)
						));
				}
				
				update updateCompanies;
				
				for (Question__c question : questions) {
					Integer maxResponses = companyToResponseMap.get(question.Survey_Design__r.Company__c);
					
					if (question.Point_Options__c != null && maxResponses != null && maxResponses != 0) {
						Decimal maxPoint = ISA_ViewContactResultCtrl.getMaxValue(question.Point_Options__c.split(','));
						
						question.Weight__c = (maxPoint / maxResponses) * 100.0;
					}
				}
				
				update questions;
			}
		}
	}
	
	public static void addQuestionHistory(List<Question__c> p_newRecords) {
		addQuestionHistory(new Map<Id, Question__c>(), p_newRecords);
	}
	
	public static void addQuestionHistory(Map<Id, Question__c> p_oldRecordMap, List<Question__c> p_newRecords) {
		List<Insight_History__c> createHistories = new List<Insight_History__c>();
		
		for (Question__c question : p_newRecords) {
			Question__c oldQuestion = p_oldRecordMap.get(question.Id);
			
			PageReference prUser = new PageReference(AUTH_StaticVariables.prefixSite + '/ISA_ViewUser');
			prUser.getParameters().put('sId', question.Owner__c);
			
			if (oldQuestion != null) {
				List<Schema.FieldSetMember> fieldSetMembers = Schema.getGlobalDescribe().get('Question__c')
					.getDescribe().FieldSets.getMap().get('TrackFields').getFields();
				
				for (Schema.FieldSetMember field : fieldSetMembers) {
					if (oldQuestion.get(field.getFieldPath()) != question.get(field.getFieldPath())) {
						String oldValue = '';
						String newValue = '';
						
						if (field.getFieldPath() == 'Survey_Design__c') {
							oldValue = String.valueOf(oldQuestion.get('Insight_Panel_Name__c'));
							newValue = String.valueOf(question.get('Insight_Panel_Name__c'));
						} else {
							oldValue = String.valueOf(oldQuestion.get(field.getFieldPath()));
							newValue = String.valueOf(question.get(field.getFieldPath()));
						}
						
						createHistories.add(
							new Insight_History__c(
								Insight__c = question.Id, 
								OldValue__c = oldValue, 
								NewValue__c = newValue, 
								Message__c = '<b><a href="' + prUser.getUrl() + '">' + question.Owner_Name__c + 
										 '</a></b> updated <b>' + field.getLabel() + '</b>' + 
										' from ' + ( ! String.isEmpty(oldValue) ? '<b>' + oldValue + '</b>' : '') + 
										' to ' + ( ! String.isEmpty(newValue) ? '<b>' + newValue + '</b>' : '') + '.'
							));
					}
				}
			} else {
				createHistories.add(
					new Insight_History__c(
						Insight__c = question.Id, 
						Message__c = '<b><a href="' + prUser.getUrl() + '">' + question.Owner_Name__c + 
								'</a></b> created Question.'
					));
			}
		}
		
		insert createHistories;
	}
	
	public static void deleteSurveyResuls(Map<Id, Question__c> p_oldRecordMap) {
		delete [SELECT Id FROM Survey_Result__c WHERE Question__c IN :p_oldRecordMap.keySet() LIMIT 10000];
	}
	
	public static void deleteQuestionHistory(Map<Id, Question__c> p_oldRecordMap) {
		delete [SELECT Id FROM Insight_History__c WHERE Insight__c IN :p_oldRecordMap.keySet() LIMIT 10000];
	}
}