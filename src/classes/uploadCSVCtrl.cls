public with sharing class uploadCSVCtrl {
	public Attachment document {get; set;}
	
	public uploadCSVCtrl() {
		document = new Attachment();
	}
	
	public void doSave() {
		System.assert(false, '---' + this.document.body.toString());
	}
}