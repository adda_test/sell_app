@isTest
private class ISA_GlobalSearchCtrlTest {
	
	static User__c tempUser(id id, string role) {
        User__c testUser = new User__c(
            First_Name__c = 'test', 
            Last_Name__c = 'test', 
            Phone__c = '123455678', 
            Username__c = 'test', 
            Password__c = 'testPassword', 
            Role__c = role, 
            Company__c = id,
            Expiration_Date__c = Date.today(), 
            Create_Date__c = Date.today(),
            SessionId__c = '1',
            Active__c = true,
            Email__c = 'test@mail.com');
        return testUser;
    }
    
    static Company__c tempCompany() {
        Company__c company = new Company__c(
            Name = 'test',
            Admin_First_Name__c = 'test',
            Admin_Last_Name__c = 'test',
            Admin_Email__c = 'test@mail.com',
            Admin_Login__c = 'testLogin',
            Admin_Password__c = 'testPassword',
            Address1__c = 'addr1',
            Address2__c = 'addr2',
            Trial_Start__c = Date.today(),
            Maximum_Users__c = 10);
        return company;
    }
    
    static Account__c tempAccount(id companyId,id userId) {
    	Account__c account = new Account__c(
    		Name = 'test',
    		First_Name__c = 'test',
    		Last_Name__c = 'test', 
    		Phone__c = '123456', 
    		Email__C = 'test@mail.com', 
    		Address1__c = 'addr1', 
    		Address2__c = 'addr2', 
			City__c = 'City', 
			State__c = 'State',
			Zipcode__c = '1234', 
			Company__c = companyId,
			User__c = userId,
			Web_Site__c = 'web-site',
			Country__c = 'country');
		return account;
    } 
    
    static Contact__c tempContact(id companyId,id userId,id accountId) {
    	Contact__c contact = new Contact__c(
    		Name = 'test',
    		First_Name__c = 'test',
    		Last_Name__c = 'test', 
    		Phone_No__c = '123456', 
    		Email__C = 'test@mail.com', 
    		Address1__c = 'addr1', 
    		Address2__c = 'addr2', 
			City__c = 'City', 
			State__c = 'State',
			Pincode__c = '1234', 
			Company__c = companyId,
			User__c = userId,
			Account__c = accountId,
			Country__c = 'country');
		return contact;
    }
    
    @isTest
    private static void testDoSearch() {
        Test.startTest();
        
        Company__c testCompany = tempCompany();           
        insert testCompany;      
        User__c testUser = tempUser(testCompany.Id,'Admin');    
        insert testUser; 
     
        PageReference pr = new PageReference('/apex/ISA_ListViewUser');     
        ApexPages.currentPage().getParameters().put('sId', testUser.Id);
        ApexPages.currentPage().getParameters().put('searchValue','test');
        
        ISA_GlobalSearchCtrl controller = new ISA_GlobalSearchCtrl();
        controller.doSearch(); 
        System.assertEquals(ApexPages.currentPage().getParameters().get('searchValue'), controller.doSearch().getParameters().get('searchString'));         
        
        Test.stopTest();
    } 
    
    @isTest
    private static void testDoQuickSearch() {
    	
    	Test.startTest();
        
        Company__c testCompany = tempCompany();           
        insert testCompany;      
        User__c testUser = tempUser(testCompany.Id,'Company Admin');    
        insert testUser;      
        Account__c testAccount = tempAccount(testCompany.Id,testUser.Id);    
        insert testAccount; 
        Contact__c testContact = tempContact(testCompany.Id,testUser.Id,testAccount.Id);    
        insert testContact;
        
        ApexPages.currentPage().getParameters().put('searchString','test');
        
        ISA_GlobalSearchCtrl controller = new ISA_GLobalSearchCtrl();    
        controller.doQuickSearch(); 
        System.assertNotEquals(null,controller.searchResult.size());
        
        Test.stopTest();
    }  
}