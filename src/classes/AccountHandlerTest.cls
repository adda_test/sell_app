@isTest
private class AccountHandlerTest {
	
	static Company__c testCompanyFirst;
	static Company__c testCompanySecond;
	static User__c testUser;
	static Account__c testAccount;
	static Survey_Design__c testSurveyDesignFirst;
    static Survey_Design__c testSurveyDesignSecond;
    
    /*
    * @description: Default object creation
    */
    static void init() {    
    	
    	testCompanyFirst = new Company__c(
            Name = 'test name',
            Admin_First_Name__c = 'firstname',
            Admin_Last_Name__c = 'lastname',
            Admin_Email__c = 'test@mail.com',
            Admin_Login__c = 'testLogin',
            Admin_Password__c = 'testPassword',
            Address1__c = 'addr1',
            Address2__c = 'addr2',
            Count_Answers__c = 10,
			Count_Questions__c = 14,
			Sum_Response__c = 8,
			Sum_Max_Response__c = 20,
            Trial_Start__c = Date.today(),
            Maximum_Users__c = 10);
            
        insert testCompanyFirst;
        
        testCompanySecond = new Company__c(
            Name = 'test name',
            Admin_First_Name__c = 'firstname',
            Admin_Last_Name__c = 'lastname',
            Admin_Email__c = 'test@mail.com',
            Admin_Login__c = 'testLogin',
            Admin_Password__c = 'testPassword',
            Address1__c = 'addr1',
            Address2__c = 'addr2',
            Count_Answers__c = 10,
			Count_Questions__c = 14,
			Sum_Response__c = 8,
			Sum_Max_Response__c = 20,
            Trial_Start__c = Date.today(),
            Maximum_Users__c = 10);
            
        insert testCompanySecond;
        
        testUser = new User__c(
            First_Name__c = 'First', 
            Last_Name__c = 'Last', 
            Username__c = 'testLogin', 
            Password__c = 'testPassword', 
            Role__c = 'Admin', 
            Company__c = testCompanyFirst.Id,
            Expiration_Date__c = Date.today(), 
            Create_Date__c = Date.today(),
            Email__c = 'test@mail.com');
            
        insert testUser;
            
     	testAccount = new Account__c(
    		Name = 'test contact',
    		First_Name__c = 'first name',
    		Last_Name__c = 'last name', 
    		Phone__c = '123456', 
    		Email__C = 'test@mail.com', 
    		Address1__c = 'addr1', 
    		Address2__c = 'addr2', 
			Company__c = testCompanyFirst.Id,
			User__c = testUser.Id,
			Count_Answers__c = 10,
			Count_Questions__c = 14,
			Sum_Response__c = 8,
			Sum_Max_Response__c = 20,
			Web_Site__c = 'web-site',
			Country__c = 'country');
        	
        insert testAccount;
        
           
    	testSurveyDesignFirst = new Survey_Design__c(
    		Name = 'Name',
    		Accounts__c = 10,
    		Company__c = testCompanyFirst.Id);
    		
    	insert testSurveyDesignFirst;
    	
    	testSurveyDesignSecond = new Survey_Design__c(
    		Name = 'Test',
    		Accounts__c = 15,
    		Company__c = testCompanySecond.Id);
    		
    	insert testSurveyDesignSecond;
                
    }
    
    static testMethod void testUpdateAccount() {
    	
    	Test.startTest();
    	
    	init();
    	
    	testAccount.Count_Answers__c = 12;
    	update testAccount;
    	
    	testAccount.Company__c = testCompanySecond.Id;
    	update testAccount;
    	
    	Test.stopTest();
    	
    }
}