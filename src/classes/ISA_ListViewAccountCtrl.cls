public with sharing class ISA_ListViewAccountCtrl extends AbstractController {
	//Assign Users for Account
	public List<SelectOption> assignedUserOptions {get; set;}
	public List<SelectOption> accountOptions {get; set;}
	public String selectedUser {get; set;}
	public List<AssignedUserWrapper> selectedUsers {get; set;}
	public List<WrapperCollection.AccountBubbleWrapper> chartList {get; set;}
	public Decimal allNoData {get; set;}
	
	public ISA_ListViewAccountCtrl() {
		super();
		
		this.sObjectLabel = 'Account';
		this.selectedUser = null;
		this.assignedUserOptions = new List<SelectOption> {new SelectOption('', '-- None --')};
		this.accountOptions = new List<SelectOption> {new SelectOption('', '-- None --')};
		this.selectedUsers = new List<AssignedUserWrapper>();
		
		this.chartList = new List<WrapperCollection.AccountBubbleWrapper>();
		
		this.allNoData = 0;
	}
	
	public PageReference initPage() {
		PageReference pr = super.init();
		
		if (pr != null) {
			return pr;
		}
		
		this.sObjectType = sObjectNameMap.get(this.sObjectLabel);
		
		fillRecords();
		
		doAssignTeam();
		
		// bubble chart
		fillBubbleChartData();
		calcAverageValue();
		splitData();
		return null;
	}
	
	public void fillBubbleChartData() {
		this.chartList = ChartUtils.getAccountBubbleChart('Contacts__c', 'Completeness_Score__c', 
				'Insight_Index__c', 'No_Data__c', 'No_Strategy__c', 
				' WHERE Company__c = ' + '\'' + userCompany + '\'', new List<String> {'Last_Activity__c'}, 
				this.userRole, this.user.Id);
	}
	
	public void calcAverageValue () {
		this.avgX = 0;
		this.avgY = 0;
		Integer i = 0;
		for (WrapperCollection.AccountBubbleWrapper obj : this.chartList) {
			this.avgX += obj.completeness;
			this.avgY += obj.insightIndex;
			i++;
		}
		
		if (i != 0) {
			this.avgX = (this.avgX / i).setScale(1);
			this.avgY = (this.avgY / i).setScale(1);
		}
	}
	
	public void splitData() {
		// init color map
		this.seriesColorMap.put('Stategy', '#000099');
		this.seriesColorMap.put('No Strategy', '#009900');
		this.seriesColorMap.put('No Contacts', '#990000');
		
		this.seriesMap.put('Stategy', new List<WrapperCollection.AccountBubbleWrapper>());
		this.seriesMap.put('No Strategy', new List<WrapperCollection.AccountBubbleWrapper>());
		this.seriesMap.put('No Contacts', new List<WrapperCollection.AccountBubbleWrapper>());
		
//		Decimal maxNoData = 0;
//		for (WrapperCollection.AccountBubbleWrapper obj : this.chartList) {
//			if (obj.noData != null && maxNoData < obj.noData) {
//				maxNoData = obj.noData;
//			}
//		}
		
		// fill no data
		for (WrapperCollection.AccountBubbleWrapper obj : this.chartList) {
			String seriaName = '';
			
			if (obj.noData != null && obj.noData == 0){
				this.allNoData++;
				continue; // do not show at all
			}
			
			if (obj.noStrategy == 0){ //(maxNoData != 0 && (obj.noData / maxNoData) > 0.25) {
				seriaName = 'Stategy';
				this.seriesMap.get(seriaName).add(obj);
				
			} else { //else if ((maxNoData != 0 && (obj.noData / maxNoData) <= 0.25) || maxNoData == 0) {
				seriaName = 'No Strategy';
				this.seriesMap.get(seriaName).add(obj);
			}
			
			if (obj.childCount == 0) {
				seriaName = 'No Contacts';
				this.seriesMap.get(seriaName).add(obj);
			}
		}
	}
	
	public override String doGenerateWhereClause() {
		String whereClause = '';
		if (this.userRole == 'Company Admin' || this.userRole == 'Company User') {
			if (this.sObjectLabel == 'User' || this.sObjectLabel == 'Account' || 
				this.sObjectLabel == 'Contact' || this.sObjectLabel == 'Insight' || 
				this.sObjectLabel == 'Strategy') {
				
				if (this.userRole == 'Company Admin') {
					whereClause = ' WHERE Company__c = \'' + this.userCompany + '\'';
				} else if (this.userRole == 'Company User') {
					List<AssignContact__c> junctions = [
						SELECT Contact__r.Account__c 
						FROM AssignContact__c 
						WHERE User__c = :this.user.Id 
						  AND Contact__c != null 
						LIMIT 10000];
					
					Set<String> accountIds = new Set<String>();
					for (AssignContact__c junction : junctions) {
						if (junction.Contact__r.Account__c != null) {
							accountIds.add('\'' + junction.Contact__r.Account__c + '\'');
						}
					}
					
					if ( ! accountIds.isEmpty()) {
						whereClause = ' WHERE Company__c = \'' + this.userCompany + '\' AND Id IN (' + 
							String.join(new List<String>(accountIds), ',') + ')';
					} else {
						whereClause = ' WHERE Id = null';
					}
				}
			}
		}
		return whereClause;
	}
	
	private void doAssignUser() {
		for (Integer i = 0; i < this.assignedUserOptions.size(); i++) {
			if (this.assignedUserOptions.get(i).getValue() == this.selectedUser) {
				this.selectedUsers.add(
					new AssignedUserWrapper(
						this.assignedUserOptions.get(i).getValue(), 
						this.assignedUserOptions.get(i).getLabel()
				));
				
				this.assignedUserOptions.remove(i--);
			}
		}
	}
	
	public void removeUser() {
		String userId = ApexPages.currentPage().getParameters().get('recordId');
		
		if ( ! String.isEmpty(userId)) {
			for (Integer i = 0; i < this.selectedUsers.size(); i++) {
				if (userId == this.selectedUsers.get(i).recordId) {
					this.assignedUserOptions.add(
						new SelectOption(this.selectedUsers.get(i).recordId, this.selectedUsers.get(i).name));
					
					this.selectedUsers.remove(i--);
				}
			}
		}
	}
	
	public void addAssignTeam() {
		String userIds = ApexPages.currentPage().getParameters().get('userIds');
		String accountIds = ApexPages.currentPage().getParameters().get('accountIds');
		
		if ( ! String.isEmpty(accountIds) && ! String.isEmpty(userIds)) {
			userIds = userIds.substring(1);
			
			List<AssignContact__c> junctions = [
				SELECT Contact__c, User__c 
				FROM AssignContact__c 
				WHERE User__c IN :userIds.split(',') 
				LIMIT 10000];
			
			Set<Id> existContactIds = new Set<Id>();
			for (AssignContact__c junction : junctions) {
				existContactIds.add(junction.Contact__c);
			}
			
			List<Contact__c> relatedContacts = [
				SELECT Name 
				FROM Contact__c 
				WHERE Account__c IN :accountIds.split(',') 
				LIMIT 10000];
			
			if ( ! relatedContacts.isEmpty()) {
				Map<Id, User__c> existUserMap = new Map<Id, User__c>([
					SELECT Name 
					FROM User__c 
					WHERE Id IN :userIds.split(',') 
					LIMIT 10000]);
				
				List<AssignContact__c> insertJunctions = new List<AssignContact__c>();
				for (String userId : userIds.split(',')) {
					for (Contact__c contact : relatedContacts) {
						if ( ! existContactIds.contains(contact.Id)) {
							insertJunctions.add(
								new AssignContact__c(
									Contact__c = contact.Id, 
									User__c = userId, 
									Contact_Name__c = contact.Name, 
									User_Name__c = existUserMap.get(userId).Name, 
									Owner_Name__c = this.username, 
									Owner__c = this.user.Id
								));
						}
					}
				}
				
				insert insertJunctions;
			} else {
				ApexPages.addMessage(
					new ApexPages.Message(
						ApexPages.Severity.ERROR, 
						'Please, check selected Account records. At least one of them haven\'t Contact records.'));
			}
		}
		
		for (ISA_SObjectHelper.SObjectWrapper wrapper : this.records) {
			wrapper.checked = false;
		}
	}
	
	private void doAssignTeam() {
		this.assignedUserOptions = new List<SelectOption> {new SelectOption('', '-- None --')};
		
		List<User__c> users = [
			SELECT Name 
			FROM User__c 
			WHERE Company__c = :this.userCompany 
			  AND Role__c = 'Company User'
			LIMIT 10000];
		
		if ( ! users.isEmpty()) {
			for (User__c user : users) {
				this.assignedUserOptions.add(new SelectOption(user.Id, user.Name));
			}
		}
	}
	
	public PageReference goLastActivity() {
		String viewId = ApexPages.currentPage().getParameters().get('viewId');
		
		if ( ! String.isEmpty(viewId)) {
			PageReference pr = new PageReference(AUTH_StaticVariables.prefixSite + '/ISA_LastActivity');
			addRetUrl(pr);
			pr.getParameters().put('sId', viewId);
			pr.setRedirect(true);
			
			return pr;
		}
		
		return null;
	}
	
	public class AssignedUserWrapper {
		public String recordId {get; set;}
		public String name {get; set;}
		
		public AssignedUserWrapper() {
			this.recordId = '';
			this.name = '';
		}
		
		public AssignedUserWrapper(String p_id , String p_name) {
			this();
			
			this.recordId = p_id;
			this.name = p_name;
		}
	}
}