public with sharing class HELP_MailUtility {
	
	public static Map<String, String> sendQuickMail(String p_email, String p_subject, String p_htmlBody) {
		Map<String, String> result = new Map<String, String>();
		if (String.isEmpty(p_email) || String.isEmpty(p_subject) || String.isEmpty(p_htmlBody)) {
			result.put('ERROR','PARAMETERS CAN\'T BE EMPTY');
		} else {
			try {
				Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
				message.setSubject(p_subject);
				message.setHtmlBody(p_htmlBody);
				message.setToAddresses(new List<String>{p_email});
				Messaging.sendEmail(new List<Messaging.SingleEmailMessage> { message });
				result.put('SUCCESS','MESSAGE SUCCESSFULLY SENT');
			} catch(Exception ex) {
				result.put('ERROR', ex.getMessage());
			}
		}
		return result;
	}
}