@isTest
private class ISA_ViewContactInsightsCtrlTest {
    static User__c tempUser(id id) {
        User__c testUser = new User__c(
            First_Name__c = 'First', 
            Last_Name__c = 'Last', 
            Phone__c = '123455678', 
            Username__c = 'testLogin', 
            Password__c = 'testPassword', 
            Role__c = 'Admin', 
            Company__c = id,
            Expiration_Date__c = Date.today(), 
            Create_Date__c = Date.today(),
            SessionId__c = '1',
            Active__c = true,
            Email__c = 'test@mail.com');
        return testUser;
    }
    
    static Company__c tempCompany() {
        Company__c company = new Company__c(
            Name = 'test name',
            Admin_First_Name__c = 'firstname',
            Admin_Last_Name__c = 'lastname',
            Admin_Email__c = 'test@mail.com',
            Admin_Login__c = 'testLogin',
            Admin_Password__c = 'testPassword',
            Address1__c = 'addr1',
            Address2__c = 'addr2',
            Sum_Max_Response__c = 10,
            Trial_Start__c = Date.today(),
            Maximum_Users__c = 10);
        return company;
    }
        
	static Account__c tempAccount(id companyId,id userId) {
    	Account__c account = new Account__c(
    		Name = 'test contact',
    		First_Name__c = 'first name',
    		Last_Name__c = 'last name', 
    		Phone__c = '123456', 
    		Email__C = 'test@mail.com', 
    		Address1__c = 'addr1', 
    		Address2__c = 'addr2', 
			City__c = 'City', 
			State__c = 'State',
			Zipcode__c = '1234', 
			Company__c = companyId,
			User__c = userId,
			Web_Site__c = 'web-site',
			Country__c = 'country');
		return account;
    } 
    
	static Contact__c tempContact(id companyId,id userId,id accountId) {
    	Contact__c contact = new Contact__c(
    		Name = 'test contact',
    		First_Name__c = 'first name',
    		Last_Name__c = 'last name', 
    		Phone_No__c = '123456', 
    		Email__C = 'test@mail.com', 
    		Address1__c = 'addr1', 
    		Address2__c = 'addr2', 
			City__c = 'City', 
			State__c = 'State',
			Pincode__c = '1234', 
			Company__c = companyId,
			User__c = userId,
			Account__c = accountId,
			Country__c = 'country');
		return contact;
    }
    
    static Survey_Design__c tempSurveyDesign(id companyId) {
    	Survey_Design__c surveyDesign = new Survey_Design__c(
    		Name = 'Name',
    		Company__c = companyId);
    	return surveyDesign;
    }
    
    static Question__c tempQuestion(id surveyDesignId) {
    	Question__c question = new Question__c(
    		Name = 'Name',
    		Title__c = 'Title',
    		Type__c = 'Text',
    		Response_Options__c = '10',
    		Point_Options__c = '10',
    		Index__c = 12,
    		Survey_Design__c = surveyDesignId);
    	return question;
    }
    
    static Answer__c tempAnswer(id contactId, id questionId) {
    	Answer__c answer = new Answer__c(
    		Name = 'test answer',
    		Value__c = '10',
    		Point__c = '10',
    		Contact__c = contactId,
    		Question__c = questionId);
    	return answer;
    }
    
    static Survey_Result__c tempSurveyResult(id contactId,id questionId) {
    	Survey_Result__c surveyResult = new Survey_Result__c(
    		Name = 'test survey',
    		Sum_Response__c = 10,
    		IsCompleteness__c = false,
    		Contact__c = contactId,
    		Question__c = questionId);
    	return surveyResult;
    }
    
    @isTest
    private static void testInit() {
    	
        Test.startTest();
        
        Company__c testCompany = tempCompany();           
        insert testCompany;      
        User__c testUser = tempUser(testCompany.Id);    
        insert testUser; 
                              
        ApexPages.currentPage().getParameters().put('cId', testUser.Id);
        
        ISA_ViewContactInsightsCtrl controller = new ISA_ViewContactInsightsCtrl();
        controller.initPage();
        
        System.assertNotEquals(null, controller.init());       
        
        Test.stopTest();
    } 
    
    @isTest
    private static void testInitPageWithContactId() {
        
        Test.startTest();
        
        Company__c testCompany = tempCompany();           
        insert testCompany;      
        User__c testUser = tempUser(testCompany.Id);    
        insert testUser; 
        Account__c testAccount = tempAccount(testCompany.Id,testUser.Id);    
        insert testAccount; 
        Contact__c testContact = tempContact(testCompany.Id,testUser.Id,testAccount.Id);    
        insert testContact; 
        Survey_Design__c testSurveyDesign = tempSurveyDesign(testCompany.Id);
        insert testSurveyDesign;
        Question__c testQuestion = tempQuestion(testSurveyDesign.Id);
        insert testQuestion;
        Survey_Result__c testSurveyResult = tempSurveyResult(testContact.Id, testQuestion.Id);
        insert testSurveyResult;
        Answer__c testAnswer = tempAnswer(testContact.Id, testQuestion.Id);
        insert testAnswer;
                              
        ApexPages.currentPage().getParameters().put('cId', testContact.Id);
        Cookie testCookie = new Cookie('SessionId','1',null,-1,false);
        ApexPages.currentPage().setCookies(new Cookie[] {testCookie});
        
        ISA_ViewContactInsightsCtrl controller = new ISA_ViewContactInsightsCtrl();
 
 		controller.initPage();
        System.assertEquals(testContact.Id, controller.contact.Id); 

        Test.stopTest();
    } 
    
    @isTest
    private static void testInitPageWithoutContactId() {
        
        Test.startTest();
        
        Company__c testCompany = tempCompany();           
        insert testCompany;      
        User__c testUser = tempUser(testCompany.Id);    
        insert testUser; 
                              
        ApexPages.currentPage().getParameters().put('cId', null);
        Cookie testCookie = new Cookie('SessionId','1',null,-1,false);
        ApexPages.currentPage().setCookies(new Cookie[] {testCookie});
        
        ISA_ViewContactInsightsCtrl controller = new ISA_ViewContactInsightsCtrl();
 
 		controller.initPage();
        System.assertEquals(null, controller.initPage()); 

        Test.stopTest();
    } 
    
    @isTest
    private static void testDoSaveMulti() {
        
        Test.startTest();
        
        Company__c testCompany = tempCompany();           
        insert testCompany;      
        User__c testUser = tempUser(testCompany.Id);    
        insert testUser; 
        Account__c testAccount = tempAccount(testCompany.Id,testUser.Id);    
        insert testAccount; 
        Contact__c testContact = tempContact(testCompany.Id,testUser.Id,testAccount.Id);    
        insert testContact; 
        Survey_Design__c testSurveyDesign = tempSurveyDesign(testCompany.Id);
        insert testSurveyDesign;
        Question__c testQuestion = tempQuestion(testSurveyDesign.Id);
        testQuestion.Type__c = 'Multi select drop-down';
        insert testQuestion;
        Answer__c testAnswer = tempAnswer(testContact.Id, testQuestion.Id);
        insert testAnswer;
        Survey_Result__c testSurveyResult = tempSurveyResult(testContact.Id, testQuestion.Id);
        insert testSurveyResult;
                              
        ApexPages.currentPage().getParameters().put('cId', testContact.Id);
        Cookie testCookie = new Cookie('SessionId','1',null,-1,false);
        ApexPages.currentPage().setCookies(new Cookie[] {testCookie});
        
        ISA_ViewContactInsightsCtrl controller = new ISA_ViewContactInsightsCtrl();
 
 		controller.initPage();
 		controller.doSave();

        Test.stopTest();        
    }
    
    @isTest
    private static void testDoSaveSingle() {
        
        Test.startTest();
        
        Company__c testCompany = tempCompany();           
        insert testCompany;      
        User__c testUser = tempUser(testCompany.Id);    
        insert testUser; 
        Account__c testAccount = tempAccount(testCompany.Id,testUser.Id);    
        insert testAccount; 
        Contact__c testContact = tempContact(testCompany.Id,testUser.Id,testAccount.Id);    
        insert testContact; 
        Survey_Design__c testSurveyDesign = tempSurveyDesign(testCompany.Id);
        insert testSurveyDesign;
        Question__c testQuestion = tempQuestion(testSurveyDesign.Id);
        testQuestion.Type__c = 'Single select drop-down';
        insert testQuestion;
        Answer__c testAnswer = tempAnswer(testContact.Id, testQuestion.Id);
        insert testAnswer;
        Survey_Result__c testSurveyResult = tempSurveyResult(testContact.Id, testQuestion.Id);
        insert testSurveyResult;
                              
        ApexPages.currentPage().getParameters().put('cId', testContact.Id);
        Cookie testCookie = new Cookie('SessionId','1',null,-1,false);
        ApexPages.currentPage().setCookies(new Cookie[] {testCookie});
        
        ISA_ViewContactInsightsCtrl controller = new ISA_ViewContactInsightsCtrl();
 
 		controller.initPage();
 		controller.doSave();

        Test.stopTest();        
    }
    
    @isTest
    private static void testDoSaveOpenEnded() {
        
        Test.startTest();
        
        Company__c testCompany = tempCompany();           
        insert testCompany;      
        User__c testUser = tempUser(testCompany.Id);    
        insert testUser; 
        Account__c testAccount = tempAccount(testCompany.Id,testUser.Id);    
        insert testAccount; 
        Contact__c testContact = tempContact(testCompany.Id,testUser.Id,testAccount.Id);    
        insert testContact; 
        Survey_Design__c testSurveyDesign = tempSurveyDesign(testCompany.Id);
        insert testSurveyDesign;
        Question__c testQuestion = tempQuestion(testSurveyDesign.Id);
        testQuestion.Type__c = 'Open-ended';
        insert testQuestion;
        Answer__c testAnswer = tempAnswer(testContact.Id, testQuestion.Id);
        insert testAnswer;
        Survey_Result__c testSurveyResult = tempSurveyResult(testContact.Id, testQuestion.Id);
        insert testSurveyResult;
                              
        ApexPages.currentPage().getParameters().put('cId', testContact.Id);
        Cookie testCookie = new Cookie('SessionId','1',null,-1,false);
        ApexPages.currentPage().setCookies(new Cookie[] {testCookie});
        
        ISA_ViewContactInsightsCtrl controller = new ISA_ViewContactInsightsCtrl();
 
 		controller.initPage();
 		controller.doSave();

        Test.stopTest();        
    }    
    
    @isTest
    private static void testDoSaveDate() {
        
        Test.startTest();
        
        Company__c testCompany = tempCompany();           
        insert testCompany;      
        User__c testUser = tempUser(testCompany.Id);    
        insert testUser; 
        Account__c testAccount = tempAccount(testCompany.Id,testUser.Id);    
        insert testAccount; 
        Contact__c testContact = tempContact(testCompany.Id,testUser.Id,testAccount.Id);    
        insert testContact; 
        Survey_Design__c testSurveyDesign = tempSurveyDesign(testCompany.Id);
        insert testSurveyDesign;
        Question__c testQuestion = tempQuestion(testSurveyDesign.Id);
        testQuestion.Type__c = 'Date';
        insert testQuestion;
        Answer__c testAnswer = tempAnswer(testContact.Id, testQuestion.Id);
        testAnswer.Value__c = '9/5/2014';
        insert testAnswer;
        Survey_Result__c testSurveyResult = tempSurveyResult(testContact.Id, testQuestion.Id);
        insert testSurveyResult;
                              
        ApexPages.currentPage().getParameters().put('cId', testContact.Id);
        Cookie testCookie = new Cookie('SessionId','1',null,-1,false);
        ApexPages.currentPage().setCookies(new Cookie[] {testCookie});
        
        ISA_ViewContactInsightsCtrl controller = new ISA_ViewContactInsightsCtrl();
 
 		controller.initPage();
 		controller.doSave();

        Test.stopTest();        
    } 
    
    @isTest
    private static void testDoSaveText() {
        
        Test.startTest();
        
        Company__c testCompany = tempCompany();           
        insert testCompany;      
        User__c testUser = tempUser(testCompany.Id);    
        insert testUser; 
        Account__c testAccount = tempAccount(testCompany.Id,testUser.Id);    
        insert testAccount; 
        Contact__c testContact = tempContact(testCompany.Id,testUser.Id,testAccount.Id);    
        insert testContact; 
        Survey_Design__c testSurveyDesign = tempSurveyDesign(testCompany.Id);
        insert testSurveyDesign;
        Question__c testQuestion = tempQuestion(testSurveyDesign.Id);
        testQuestion.Type__c = 'Text';
        insert testQuestion;
        Answer__c testAnswer = tempAnswer(testContact.Id, testQuestion.Id);
        insert testAnswer;
        Survey_Result__c testSurveyResult = tempSurveyResult(testContact.Id, testQuestion.Id);
        insert testSurveyResult;
                              
        ApexPages.currentPage().getParameters().put('cId', testContact.Id);
        Cookie testCookie = new Cookie('SessionId','1',null,-1,false);
        ApexPages.currentPage().setCookies(new Cookie[] {testCookie});
        
        ISA_ViewContactInsightsCtrl controller = new ISA_ViewContactInsightsCtrl();
 
 		controller.initPage();
 		controller.doSave();

        Test.stopTest();        
    } 
    
    @isTest
    private static void testDoSaveNumber() {
        
        Test.startTest();
        
        Company__c testCompany = tempCompany();           
        insert testCompany;      
        User__c testUser = tempUser(testCompany.Id);    
        insert testUser; 
        Account__c testAccount = tempAccount(testCompany.Id,testUser.Id);    
        insert testAccount; 
        Contact__c testContact = tempContact(testCompany.Id,testUser.Id,testAccount.Id);    
        insert testContact; 
        Survey_Design__c testSurveyDesign = tempSurveyDesign(testCompany.Id);
        insert testSurveyDesign;
        Question__c testQuestion = tempQuestion(testSurveyDesign.Id);
        testQuestion.Type__c = 'Number';
        insert testQuestion;
        Answer__c testAnswer = tempAnswer(testContact.Id, testQuestion.Id);
        insert testAnswer;
        Survey_Result__c testSurveyResult = tempSurveyResult(testContact.Id, testQuestion.Id);
        insert testSurveyResult;
                              
        ApexPages.currentPage().getParameters().put('cId', testContact.Id);
        Cookie testCookie = new Cookie('SessionId','1',null,-1,false);
        ApexPages.currentPage().setCookies(new Cookie[] {testCookie});
        
        ISA_ViewContactInsightsCtrl controller = new ISA_ViewContactInsightsCtrl();
 
 		controller.initPage();
 		controller.doSave();

        Test.stopTest();        
    } 
    
    @isTest
    private static void testDoSaveEmptyValue() {
        
        Test.startTest();
        
        Company__c testCompany = tempCompany();           
        insert testCompany;      
        User__c testUser = tempUser(testCompany.Id);    
        insert testUser; 
        Account__c testAccount = tempAccount(testCompany.Id,testUser.Id);    
        insert testAccount; 
        Contact__c testContact = tempContact(testCompany.Id,testUser.Id,testAccount.Id);    
        insert testContact; 
        Survey_Design__c testSurveyDesign = tempSurveyDesign(testCompany.Id);
        insert testSurveyDesign;
        Question__c testQuestion = tempQuestion(testSurveyDesign.Id);
        testQuestion.Type__c = 'Open-ended';
        insert testQuestion;
        Answer__c testAnswer = tempAnswer(testContact.Id, testQuestion.Id);
        testAnswer.Value__c = '';
        insert testAnswer;
        Survey_Result__c testSurveyResult = tempSurveyResult(testContact.Id, testQuestion.Id);
        insert testSurveyResult;
                              
        ApexPages.currentPage().getParameters().put('cId', testContact.Id);
        Cookie testCookie = new Cookie('SessionId','1',null,-1,false);
        ApexPages.currentPage().setCookies(new Cookie[] {testCookie});
        
        ISA_ViewContactInsightsCtrl controller = new ISA_ViewContactInsightsCtrl();
 
 		controller.initPage();
 		controller.doSave();

        Test.stopTest();        
    } 
}