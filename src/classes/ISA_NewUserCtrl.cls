/**
* @description 
*/
public with sharing class ISA_NewUserCtrl extends AbstractController {
	public User__c currentUser {get; set;}
	public List<SelectOption> companyOptions {get; set;}
	public String selectedCompany {get; set;}
	public Date startDate {get; set;}
	public Boolean isFromCompanyBySysAdmin{get;set;}
	
	private Boolean isUpdatePage;
	
	/**
	* @description 
	*/
	public ISA_NewUserCtrl() {
		super();
		
		this.selectedCompany = null;
		this.companyOptions = new List<SelectOption> { new SelectOption('', '--None--')};
		
		this.sObjectLabel = 'User';

	}
	
	public PageReference initPage() {
		PageReference afterInit = init();
		if (afterInit != null) {
			return afterInit;			
		}
		
		// init values
		
		String userId = ApexPages.currentPage().getParameters().get('sId');
		this.isFromCompanyBySysAdmin = false;
		
		if (! String.isEmpty(userId)) {
			List<User__c> users = [
					SELECT Name, First_Name__c, Last_Name__c, Phone__c, Username__c, Password__c, 
						Create_Date__c, Company__c, Email__c, Active__c, SessionId__c 
					FROM User__c 
					WHERE Id = :userId 
					LIMIT 1];
			
			if ( ! users.isEmpty()) {
				this.currentUser = users.get(0);
				
				this.selectedCompany = this.currentUser.Company__c;
				this.startDate = this.currentUser.Create_Date__c;
				this.isUpdatePage = true;
				this.currentUser.Password__c = this.currentUser.Password__c;
			}
		} else {
			this.currentUser = new User__c();
			this.startDate = Date.today();
			
			String companyId = ApexPages.currentPage().getParameters().get('companyId');
			// fill companyId (from EditCompany page)
			if (companyId != null && !String.isEmpty(companyId)) {
				this.currentUser.Company__c = companyId;
				this.isUpdatePage = false;
			} else {
				this.currentUser.Company__c = user.Company__c;
				if (user.Role__c == 'Admin') {
					this.isFromCompanyBySysAdmin = true;
				}
			}
		}
		
		List<Company__c> companies = [
				SELECT Name 
				FROM Company__c 
				ORDER BY Name 
				LIMIT 100];
		
		if ( ! companies.isEmpty()) {
			for (Company__c comp : companies) {
				companyOptions.add(new SelectOption(comp.Id, comp.Name));
			}
		}
		return null;
	}
	
	/**
	* @description 
	*/
	public PageReference doSave() {
		try {
			// check password
			if (Utils.isWeakPassword(this.currentUser.Password__c)) {
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 
						'Your password is weak.'));
				return null;
			}
			
			if (Utils.isTooSmallPassword(this.currentUser.Password__c)) {
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 
						'Your password should be at least 6 characters.'));
				return null;
			}
			
			if (this.userRole == 'Company Admin') {
				this.currentUser.Company__c = this.userCompany;
			} else if (selectedCompany != null && ! String.isEmpty(selectedCompany)) {
				this.currentUser.Company__c = selectedCompany;
			}
			
			// get company info
			
			List<Company__c> companies = [SELECT Id, Maximum_Users__c FROM Company__c WHERE Id = :this.currentUser.Company__c];
			
			if ( ! companies.isEmpty()) {
				// get users for this company
				Integer userCount = 0;
				List<User__c> users = [SELECT Id FROM User__c WHERE Role__c = 'Company User' AND Company__c = :companies.get(0).Id];
				
				if ( ! users.isEmpty()) {
					userCount = users.size();
				}
				
				// check limits 
				if (userCount >= companies.get(0).Maximum_Users__c) {
					ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 
							'You can not create new user - user limit for this company.'));
					return null;
				}	
				
				this.currentUser.Create_Date__c = (this.startDate != null) ? this.startDate : Date.today();
				this.currentUser.Role__c = 'Company User';
				this.currentUser.Password__c = AUTH_Utils.getMd5(this.currentUser.Password__c);
				
				// check Active checkbox
				if (!this.currentUser.Active__c) {
					this.currentUser.SessionId__c = null;
				}
				
				System.debug('this.currentUser:' + this.currentUser);
				
				this.currentUser.Owner__c = this.user.Id;
				
				upsert this.currentUser;
			}
			
			
			PageReference pr = new PageReference(AUTH_StaticVariables.prefixSite + '/ISA_ViewUser');
			pr.getParameters().put('sId', this.currentUser.Id);
			pr.getParameters().put('returnUrl', AUTH_StaticVariables.prefixSite + '/ISA_ListViewUser');
			pr.setRedirect(true);
			
			return pr;
		} catch(DMLException ex) {
			for (Integer i = 0; i < ex.getNumDml(); i++) {
				if (ex.getDmlStatusCode(i) == 'DUPLICATE_VALUE') {
					if (ex.getMessage().contains('Username__c')) {
						ApexPages.addMessage(
							new ApexPages.Message(
								ApexPages.Severity.ERROR, 'Username already exists.'));
					}
				} else {
					ApexPages.addMessage(
							new ApexPages.Message(
								ApexPages.Severity.ERROR, ex.getMessage()));
				}
			}
			
		}
		return null;
	}
	
	@TestVisible protected override void collectRetParams(PageReference pr) {
		String userId = ApexPages.currentPage().getParameters().get('sId');
		if (! String.isEmpty(userId)) {
			pr.getParameters().put('sId',ApexPages.currentPage().getParameters().get('sId'));
		} else {			
			pr.getParameters().put('sId',ApexPages.currentPage().getParameters().get('companyId'));
		}
		
	}
	
	
}