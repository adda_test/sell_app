public with sharing class ISA_NewContactCtrl extends AbstractController {
	public Contact__c contact {get; set;}
	public List<SelectOption> companyOptions {get; set;}
	public String selectedCompany {get; set;}
	public String selectedUser {get; set;}
	public List<SelectOption> userOptions {get; set;}
	public List<User__c> users {get; set;}
	public String selectedAccount {get; set;}
	public List<SelectOption> accountOptions {get; set;}
	
	public List<String> selectedUserIds {get; set;}
	public List<String> selectedUserNames {get; set;}
	
	/**
	* @description 
	*/
	public ISA_NewContactCtrl() {
		super();
		
		selectedCompany = null;
		companyOptions = new List<SelectOption> { new SelectOption('', '--None--') };
		
		selectedUser = null;
		userOptions = new List<SelectOption> { new SelectOption('', '--None--') };
		
		selectedAccount = null;
		accountOptions = new List<SelectOption> { new SelectOption('', '--None--') };
		
		this.sObjectLabel = 'Contact';
		this.selectedUserIds = new List<String>();
		this.selectedUserNames = new List<String>();
		this.users = new List<User__c>();
	}
	
	public PageReference initPage() {
		PageReference pr = super.init();
		if (pr != null) {
			return pr;
		}
		
		String contactId = ApexPages.currentPage().getParameters().get('sId');
		Set<Id> userIds = new Set<Id>();
		
		if (! String.isEmpty(contactId)) {
			List<Contact__c> contacts = [
					SELECT Name, First_Name__c, Last_Name__c, Phone_No__c, Email__C, Address1__c, Address2__c, 
						City__c, State__c, Pincode__c, Company__c, User__c, Account__c, Country__c, 
						Influence__c 
					FROM Contact__c 
					WHERE Id = :contactId 
					LIMIT 1];
			
			if ( ! contacts.isEmpty()) {
				this.contact = contacts.get(0);
				
				this.selectedAccount = this.contact.Account__c;
				this.selectedCompany = this.contact.Company__c;
				
				List<AssignContact__c> junctions = [
					SELECT User__r.Name 
					FROM AssignContact__c 
					WHERE Contact__c = :this.contact.Id 
					LIMIT 10000];
				
				if ( ! junctions.isEmpty()) {
					for (AssignContact__c junction : junctions) {
						this.users.add(
							new User__c(
								Id = junction.User__c, 
								Name = junction.User__r.Name
							));
						
						userIds.add(junction.User__c);
					}
				}
			}
		} else {
			this.contact = new Contact__c();
		}
		
		List<Company__c> companies = [
				SELECT Name, 
					(SELECT Name 
					FROM Users__r 
					WHERE Role__c = 'Company User'), 
					(SELECT Name 
					FROM Accounts__r)
				FROM Company__c 
				WHERE Id = :this.user.Company__c 
				LIMIT 1];
		
		if ( ! companies.isEmpty()) {
			this.selectedCompany = companies.get(0).Id;
			this.companyOptions.add(new SelectOption(companies.get(0).Id, companies.get(0).Name));
			
			for (Account__c account : companies.get(0).Accounts__r) {
				this.accountOptions.add(new SelectOption(account.Id, account.Name));
			}
			
			for (User__c user : companies.get(0).Users__r) {
				if ( ! userIds.contains(user.Id)) {
					this.userOptions.add(new SelectOption(user.Id, user.Name));
				}
			}
		}
		
		return null;
	}
	
	/**
	* @description 
	*/
	public PageReference doSave() {
		try {
			String paramUserIds = ApexPages.currentPage().getParameters().get('userIds');
			String paramUserNames = ApexPages.currentPage().getParameters().get('userNames');
			
			if ( ! String.isEmpty(paramUserIds) && ! String.isEmpty(paramUserNames)) {
				paramUserIds = paramUserIds.replace(',,', ',');
				paramUserNames = paramUserNames.replace(',,', ',');
				
				this.selectedUserIds = paramUserIds.split(',');
				this.selectedUserNames = paramUserNames.split(',');
			}
			
			contact.Account__c = ( ! String.isEmpty(selectedAccount)) ? selectedAccount : null;
			
			if (! String.isEmpty(selectedCompany)) {
				contact.Company__c = this.selectedCompany;
			} else {
				List<Account__c> accounts = [
					SELECT Company__c 
					FROM Account__c 
					WHERE Id = :this.contact.Account__c 
					LIMIT 1];
				
				if ( ! accounts.isEmpty()) {
					this.contact.Company__c = accounts.get(0).Company__c;
				} else {
					this.contact.Company__c = null;
				}
			}
			
			contact.Owner__c = this.user.Id;
			
			if (contact.Id == null) {
				this.selectedUserIds.add(this.user.Id);
			}
			
			upsert contact;
			
			List<AssignContact__c> deleteJunctions = new List<AssignContact__c>();
			List<AssignContact__c> insertJunctions = new List<AssignContact__c>();
			
			List<AssignContact__c> junctions = [
				SELECT Contact__c, User__c 
				FROM AssignContact__c 
				WHERE Contact__c = :this.contact.Id 
				LIMIT 10000];
			
			Set<String> selectedUserIdSet = new Set<String>(this.selectedUserIds);
			for (Integer i = 0; i < junctions.size(); i++) {
				if ( ! selectedUserIdSet.contains(junctions.get(i).User__c)) {
					deleteJunctions.add(junctions.get(i));
				} else {
					selectedUserIdSet.remove(junctions.get(i).User__c);
					junctions.remove(i--);
				}
			}
			
			Map<Id, User__c> usersMap = new Map<Id, User__c>([
				SELECT Name 
				FROM User__c 
				WHERE Id IN :this.selectedUserIds 
				LIMIT 10000]);
			
			for (String userId : selectedUserIdSet) {
				insertJunctions.add(
					new AssignContact__c(
						Contact__c = contact.Id, 
						User__c = userId, 
						User_Name__c = usersMap.get(userId).Name, 
						Contact_Name__c = contact.First_Name__c + ' ' + contact.Last_Name__c, 
						Owner_Name__c = this.username, 
						Owner__c = this.user.Id
					));
			}
			
			insert insertJunctions;
			
			delete deleteJunctions;
			
			PageReference pr = new PageReference(AUTH_StaticVariables.prefixSite + '/ISA_ViewContact');
			pr.getParameters().put('sId', this.contact.Id);
			pr.getParameters().put('returnUrl', AUTH_StaticVariables.prefixSite + '/ISA_ListViewContact');
			pr.setRedirect(true);
			
			return pr;
		} catch(Exception ex) {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
			
			return null;
		}
	}
	
	/**
	* @description 
	*/
	public void doSelectAccount() {
		if ( ! String.isEmpty(this.selectedUser)) {
			List<Account__c> accountList = [
						SELECT Name 
						FROM Account__c 
						WHERE User__c = :this.selectedUser 
						ORDER BY Name 
						LIMIT 100];
				
				if ( ! accountList.isEmpty()) {
					for (Account__c account : accountList) {
						accountOptions.add(new SelectOption(account.Id, account.Name));
					}
				}
		}
		
	}
	
	public void applySelectedUsers() {
		String userIds = ApexPages.currentPage().getParameters().get('userIds');
		String userNames = ApexPages.currentPage().getParameters().get('userNames');
		
		if ( ! String.isEmpty(userIds) && ! String.isEmpty(userNames)) {
			this.selectedUserIds = userIds.split(',');
			this.selectedUserNames = userNames.split(',');
		}
	}
	
	@TestVisible protected override void collectRetParams(PageReference pr) {
		pr.getParameters().put('sId',ApexPages.currentPage().getParameters().get('sId'));
	}
}