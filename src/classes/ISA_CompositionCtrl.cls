public with sharing class ISA_CompositionCtrl extends AbstractController {
	public List<SearchWrapper> searchResults {get; set;}
	public List<String> navigationObjects {get; set;}
	
	public ISA_CompositionCtrl() {
		super();
		this.searchResults = new List<SearchWrapper>();
	}
	
	public PageReference initPage() {
		PageReference afterInit = init();
		if (afterInit != null) {
			return afterInit;
		}
		this.navigationObjects = getObjectsByUserRole();
		return null;
	}
	
	public PageReference doSelect() {
		String label = ApexPages.currentPage().getParameters().get('labelObject');
		
		String sObjectName = ISA_SObjectHelper.convertToSingleNameMap.get(label);
		
		PageReference pr = new PageReference('/apex/ISA_ListView' + sObjectName);
		pr.setRedirect(true);
		
		return pr;
	}
	
	public PageReference doSearch() {
		PageReference pr = new PageReference('/apex/ISA_Search');
		pr.getParameters().put('searchString', ApexPages.currentPage().getParameters().get('searchValue'));
		pr.setRedirect(true);
		
		return pr;
	}
	
	public void doQuickSearch() {
		String findClause = ApexPages.currentPage().getParameters().get('searchString') + '*';
		
		String findQuery = 'FIND \'' + findClause + '\' IN Name FIELDS RETURNING ';
		String returningClause = String.join(getFINDSObjects(this.userRole), ', ');
		
		List<List<sObject>> selectedRecords = (! String.isEmpty(returningClause)) 
				? search.query(findQuery + returningClause) 
				: new List<List<sObject>>();
		
		this.searchResults.clear();
		for (List<sObject> records : selectedRecords) {
			for (sObject obj : records) {
				this.searchResults.add(
					new SearchWrapper(
						obj.getSObjectType().getDescribe().getLabel(), 
						obj.Id, 
						String.valueOf(obj.get('Name'))
					));
			}
		}
	}
	
	public PageReference logout() {
		// remove sessionId from database and clear cookie
		AUTH_Utils.removeSession();
		
		// remove cookie
		AUTH_Utils.clearCookie();
		PageReference loginPage = new PageReference(AUTH_StaticVariables.loggedOutURL);
		
		return loginPage;
	}
	
	/**
	* @description 
	*/
	@TestVisible private List<String> getObjectsByUserRole() {
		if (this.userRole == 'Admin') {
			return new List<String> {'Companies', 'Users', 'Insights', 'Strategies'};
		} else if (this.userRole == 'Company Admin') {
			return new List<String> {'Users', 'Accounts', 'Contacts', 'Insights', 'Strategies'};
		} else if (this.userRole == 'Company User') {
			return new List<String> {'Accounts', 'Contacts', 'Insights', 'Strategies'};
		}
		
		return new List<String>();
	}
	
	@TestVisible private List<String> getFINDSObjects(String p_userRole) {
		if (p_userRole == 'Admin') {
			return new List<String> {
				'Account__c(Id, Name)', 
				'Contact__c(Id, Name)', 
				'Company__c(Id, Name)', 
				'User__c(Id, Name)'
			};
		} else if (p_userRole == 'Company Admin') {
			String whereClause = 'WHERE Company__c = \'' + this.userCompany + '\'';
			
			return new List<String> {
				'Account__c(Id, Name ' + whereClause + ')', 
				'Contact__c(Id, Name ' + whereClause + ')', 
				'User__c(Id, Name ' + whereClause + ')'
			};
		} else if (p_userRole == 'Company User') {
			String whereClause = 'WHERE Company__c = \'' + this.userCompany + '\'';
			
			return new List<String> {
				'Account__c(Id, Name ' + whereClause + ')', 
				'Contact__c(Id, Name ' + whereClause + ')', 
				'Company__c(Id, Name WHERE Id = \'' + this.userCompany + '\')', 
				'User__c(Id, Name ' + whereClause + ')'
			};
		}
		
		return new List<String>();
	}
	
	public class SearchWrapper {
		public String objectType {get; set;}
		public String recordId {get; set;}
		public String name {get; set;}
		
		public SearchWrapper() {
			this.objectType = '';
			this.recordId = '';
			this.name = '';
		}
		
		public SearchWrapper(String p_type, String p_id, String p_name) {
			this();
			
			this.objectType = p_type;
			this.recordId = p_id;
			this.name = p_name;
		}
	}
}