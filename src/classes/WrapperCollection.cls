public with sharing class WrapperCollection {
	
	// used for paid info (admin home page)
	public class CompPaidWrapper{
		public Decimal last7Days {get;set;}
		public Decimal other {get;set;}
		
		public CompPaidWrapper() {
			last7Days = 0;
			other = 0;
		}
		
	}
	
	// used for usage info (admin home page)
	public class CompUsageWrapper {
		public Integer allCount {get;set;}
		public Decimal average {get;set;}
		public Decimal firstValue {get;set;}
		public String firstLabel {
			set;
			get {
				if (String.isEmpty(firstLabel)) {
					return firstLabel + '\\n' + objectPluralName + ':' +  firstValue;
				}
				return firstLabel;
			}
		}
		public Decimal secondValue {get;set;}
		public String secondLabel {
			set;
			get {
				if (String.isEmpty(secondLabel)) {
					return secondLabel + '\\n' + objectPluralName + ':' +  secondValue;
				}
				return secondLabel;
			}
		}
		public Decimal thirdValue {get;set;}
		public String thirdLabel {
			set;
			get {
				if (String.isEmpty(secondLabel)) {
					return thirdLabel + '\\n' + objectPluralName + ':' +  thirdValue;
				}
				return thirdLabel;
			}
		}
		public Decimal fourthValue {get;set;}
		public String fourthLabel {
			set;
			get {
				if (String.isEmpty(secondLabel)) {
					return fourthLabel + '\\n' + objectPluralName + ':' +  fourthValue;
				}
				return fourthLabel;
			}
		}
		public String objectPluralName {get;set;}
		
		public CompUsageWrapper() {
			this.average = 0;
			this.firstValue = 0;
			this.firstLabel = '';
			this.secondValue = 0;
			this.secondLabel = '';
			this.thirdValue = 0;
			this.thirdLabel = '';
			this.fourthValue = 0;
			this.fourthLabel = '';
		}
	}
	
	public class DistributionWrapper {
		public Integer allCount {get;set;}
		public Decimal average {get;set;}
		public String averageLabel {get;set;}
		public Integer firstValue {get;set;}
		public String firstLabel {
			set;
			get {
				if (String.isEmpty(firstLabel)) {
					return '<div style="padding:5px 5px 5px 5px;"><b>No data</b> <br/>Count: '+ firstValue + ' </div>';
				} else {
					return firstLabel;
				}
			}
		}
		public Integer secondValue {get;set;}
		public String secondLabel {
			set;
			get {
				if (String.isEmpty(secondLabel)) {
					if (secondValue != 0) {
						Decimal avgValue = secondSum / secondValue;
						avgValue = avgValue.setScale(2);
						return '<div style="padding:5px 5px 5px 5px;"><b>Low group</b><br/>Range: 1% - 15% </br>Average: ' + avgValue + '% <br/>Count: ' + secondValue + ' </div>';
					} else {
						return '<div style="padding:5px 5px 5px 5px;"><b>Low group</b><br/>Range: 1% - 15% <br/>Count: ' + secondValue + ' </div>';
					}
				} else {
					return secondLabel;
				}
			}
		}
		public Integer thirdValue {get;set;}
		public String thirdLabel {
			set;
			get {
				if (String.isEmpty(thirdLabel)) {
					if (thirdValue != 0) {
						Decimal avgValue = thirdSum / thirdValue;
						avgValue = avgValue.setScale(2);
						return '<div style="padding:5px 5px 5px 5px;"><b>Lower group</b><br/>Range: 15% - ' + (average-5) + '% <br/>Average: ' + avgValue + '%<br>Count: ' + thirdValue + ' </div>';
					} else {
						return '<div style="padding:5px 5px 5px 5px;"><b>Lower group</b><br/>Range: 15% - ' + (average-5) + '% <br/>Count: ' + thirdValue + ' </div>';
					}
				} else {
					return thirdLabel;
				}
			}
		}
		public Integer fourthValue {get;set;}
		public String fourthLabel {
			set;
			get {
				if (String.isEmpty(fourthLabel)) {
					if (fourthValue != 0) {
						Decimal avgValue = fourthSum / fourthValue;
						avgValue = avgValue.setScale(2);
						return '<div style="padding:5px 5px 5px 5px;"><b>Average group</b><br/>Range: ' + (average - 5) + '% - ' + (average + 5)+ '% <br/>Average: ' + avgValue + '%<br/>Count: ' + fourthValue + ' </div>';
					} else {
						return '<div style="padding:5px 5px 5px 5px;"><b>Average group</b><br/>Range: ' + (average - 5) + '% - ' + (average + 5)+ '% <br/>Count: ' + fourthValue + ' </div>';
					}
				} else {
					return fourthLabel;
				}
			}
		}
		public Integer fifthValue {get;set;}
		public String fifthLabel {
			set;
			get {
				if (String.isEmpty(fifthLabel)) {
					if (fifthValue != 0) {
						Decimal avgValue = fifthSum / fifthValue;
						avgValue = avgValue.setScale(2);
						return '<div style="padding:5px 5px 5px 5px;"><b>Upper group</b><br/>Range: ' + (average + 5) + '% - 90% <br/>Average: ' + avgValue + '% <br/>Count: ' + fifthValue + ' </div>';
					} else {
						return '<div style="padding:5px 5px 5px 5px;"><b>Upper group</b><br/>Range: ' + (average + 5) + '% - 90% <br/>Count: ' + fifthValue + ' </div>';
					}
				} else {
					return fifthLabel;
				}
			}
		}
		public Integer sixthValue {get;set;}
		public String sixthLabel {
			set;
			get {
				if (String.isEmpty(sixthLabel)) {
					if (sixthValue != 0) {
						Decimal avgValue = sixthSum / sixthValue;
						avgValue = avgValue.setScale(2);
						return '<div style="padding:5px 5px 5px 5px;"><b>Top 10%</b> <br/>Range: 90%-100% <br/>Average: ' + avgValue + '% <br/>Count: ' + sixthValue + ' </div>';
					} else {
						return '<div style="padding:5px 5px 5px 5px;"><b>Top 10%</b> <br/>Range: 90%-100% <br/>Count: ' + sixthValue + ' </div>';
					}
				} else {
					return sixthLabel;
				}
			}
		}
		
		// count
		public Decimal firstSum {get;set;}
		public Decimal secondSum {get;set;}
		public Decimal thirdSum {get;set;}
		public Decimal fourthSum {get;set;}
		public Decimal fifthSum {get;set;}
		public Decimal sixthSum {get;set;}	
		
		public DistributionWrapper() {
			this.allCount = 0;
			this.average = 0;
			
			this.firstValue = 0;
			this.secondValue = 0;
			this.thirdValue = 0;
			this.fourthValue = 0;
			this.fifthValue = 0;
			this.sixthValue = 0;
			// count
			this.firstSum = 0;
			this.secondSum = 0;
			this.thirdSum = 0;
			this.fourthSum = 0;
			this.fifthSum = 0;
			this.sixthSum = 0;
		}
		
	}
	
	public virtual class BubbleWrapper {
		public String bubbleName {get;set;}
		public String bubbleType {get;set;}
		public Decimal insightIndex {get;set;}
		public Decimal completeness {get;set;}
		public Decimal noData {get;set;}
		public Decimal childCount {get;set;}
		public Map<String, String> addInfo {get;set;}
		
		public String customPointFormat{
			set;
			get {
				return '<b>' + this.bubbleName + '</b><br />';
			}
			
		}
		
		public BubbleWrapper() {
			this.bubbleName = '';
			this.bubbleType = 'Default';
			this.insightIndex = 0;
			this.completeness = 0;
			this.noData = 0;
			this.childCount = 0;
			this.addInfo = new Map<String, String>();
		}
	}
	
	public class AccountBubbleWrapper extends BubbleWrapper {
		public DateTime lastActivity {get; set;}
		public Decimal noStrategy {get;set;}
		public Integer typeOfTooltip {get;set;}
		public String customPointFormat{
			set;
			get {
				if (this.bubbleType == 'Default') {
					return '<b>' + this.bubbleName + '</b><br />'+ 
							'Contacts: ' + this.childCount + '<br />' + 
							'Insight Index: ' + this.insightIndex + '<br />' +
							'Completeness: ' + this.completeness + '<br />' + 
							'No Strategy: ' + this.noStrategy + '<br />' + 
							'Last Activity: ' + this.lastActivity.format('MMM dd yyyy') + '<br />';
				}
				
				if (this.bubbleType == 'No Contacts') {
					return '<b>Accounts with no contacts</b><br />'+
							'Contacts: ' + this.childCount;
				}
				
				return '';
			}
			
		}
		
		public AccountBubbleWrapper() {
			super();
			
			this.insightIndex = 0;
			this.completeness = 0;
			this.noStrategy = 0;
		}
	}
	
	public class ContactBubbleWrapper extends BubbleWrapper {
		public String accountName {get; set;}
		public Decimal influence {get; set;}
		public String strategy {get;set;}
		public DateTime lastActivity {get; set;}
		public Integer year {get; set;}
		public Integer month {get; set;}
		public Integer day {get; set;}
		public Boolean isNoData {get; set;}
		public Integer typeOfTooltip {get; set;}
		
		public String customPointFormat {
			set;
			get {
				if (this.bubbleType == 'Default') {
					return '<b>' + this.bubbleName + '</b><br />' + 
							this.accountName + '<br />' + 
							'Influence: ' + this.influence + '<br />' + 
							'Insight Index: ' + this.insightIndex + '<br />' + 
							'Completeness: ' + this.completeness + '<br />' + 
							'Strategy: ' + this.strategy;
				}
				
				if (this.bubbleType == 'Contact Summary') {
					return '<b>' + this.bubbleName + '</b><br />' + 
							'Influence: ' + this.influence + '<br />' + 
							'Insight Index: ' + this.insightIndex + '<br />' + 
							'Completeness: ' + this.completeness;
				}
				return '';
			}
			
		}
		
		public ContactBubbleWrapper() {
			super();
			
			this.accountName = '';
			this.influence = 0;
			this.insightIndex = 0;
			this.completeness = 0;
			this.strategy = '';
			this.lastActivity = null;
			this.day = 0;
			this.month = 0;
			this.year = 0;
			this.isNoData = false;
		}
	}
	
	
	public class UserBubbleWrapper extends BubbleWrapper {
		public DateTime lastActivity {get; set;}
		public Decimal noStrategy {get;set;}
		public Integer typeOfTooltip {get;set;}
		public String customPointFormat{
			set;
			get {
				if (this.bubbleType == 'Default') {
					return '<b>' + this.bubbleName + '</b><br />'+
							'Contacts: ' + this.childCount + '<br />' +
							'Insight Index: ' + this.insightIndex + '<br />' +
							'Completeness: ' + this.completeness + '<br />' + 
							'No Strategy: ' + this.noStrategy + '<br />' + 
							'No Data: ' + this.noData + '<br />';
				}
				
				if (this.bubbleType == 'Accounts without contacts') {
					return '<b>Accounts with no contacts</b><br />'+
							'Contacts: ' + this.childCount;
				}
				
				return '';
			}
			
		}
		
		public UserBubbleWrapper() {
			super();
			
			this.insightIndex = 0;
			this.completeness = 0;
			this.noStrategy = 0;
		}
	}
	
	public class InsightBubbleWrapper extends BubbleWrapper {
		public Decimal maxScore {get; set;}
		public Decimal weight {get; set;}
		public Decimal avrScore {get;set;}
		public Decimal response {get; set;}
		public Decimal contacts {get; set;}
		
		public Integer typeOfTooltip {get; set;}
		public String customPointFormat {
			set;
			get {
				if (this.bubbleType == 'Default') {
					return '<b>' + this.bubbleName + '</b><br />' + 
							'Max score: ' + this.maxScore + '<br />' + 
							'Weight: ' + this.weight + '%<br />' + 
							'Avg % Max Score: ' + this.avrScore + '%<br />' + 
							'% Contact Response: ' + this.response + '%<br />' + 
							'Contacts:' + this.contacts;
				}
				
				return '';
			}
			
		}
		
		public InsightBubbleWrapper() {
			super();
			
			this.maxScore = 0;
			this.insightIndex = 0;
			this.avrScore = 0;
			this.response = 0;
			this.contacts = 0;
		}
	}
	
	public class ColumnChartWrapper {
		public String columnName {get; set;}
		public String columnType {get; set;}
		public Decimal assigned {get; set;}
		public Decimal completed {get; set;}
		
		public ColumnChartWrapper() {
			this.columnType = 'Default';
			this.columnName = '';
			this.assigned = 0;
			this.completed = 0;
		}
	}
	
	public class LineChartWrapper {
		public String columnType {get; set;}
		public Date dateManaged {get; set;}
		public Decimal insightIndex {get; set;}
		public Decimal completeness {get; set;}
		public Decimal users {get; set;}
		public Integer year {
			get {
				if (this.dateManaged != null) {
					return this.dateManaged.year();
				}
				return 0;
			}
			set;
		}
		public Integer month {
			get {
				if (this.dateManaged != null) {
					return this.dateManaged.month() - 1;
				}
				return 0;
			}
			set;
		}
		public Integer day {
			get {
				if (this.dateManaged != null) {
					return this.dateManaged.day();
				}
				return 0;
			}
			set;
		}
		
		public LineChartWrapper() {
			this.columnType = 'Default';
			this.dateManaged = Date.today();
			this.insightIndex = 0;
			this.completeness = 0;
			this.users = 0;
		}
	}
}