@isTest
private class CompanyHandlerTest {
	
	static Company__c testCompany;
	static User__c testUser;
	static Account__c testAccount;
	static Contact__c testContact;
	static Survey_Design__c testSurveyDesign;
    static Survey_Result__c testSurveyResult;
    static Question__c testQuestion;
   	static Answer__c testAnswer;
    
    /*
    * @description: Default object creation
    */
    static void init() {        	
    	     
        testCompany = new Company__c(
            Name = 'test name',
            Admin_First_Name__c = 'firstname',
            Admin_Last_Name__c = 'lastname',
            Admin_Email__c = 'test@mail.com',
            Admin_Login__c = 'testLogin',
            Admin_Password__c = 'testPassword',
            Address1__c = 'addr1',
            Address2__c = 'addr2',
            Active__c = true,
            Trial__c = 20,
            Trial_Start__c = date.newInstance(2014, 6, 25),
            Sum_Max_Response__c = 10,
            Maximum_Users__c = 10);
            
        insert testCompany;
        
        testUser = new User__c(
            First_Name__c = 'First', 
            Last_Name__c = 'Last', 
            Phone__c = '123455678', 
            Username__c = 'testLogin', 
            Password__c = 'testPassword', 
            Role__c = 'Admin', 
            Company__c = testCompany.Id,
            Expiration_Date__c = date.newInstance(2014, 8, 25), 
            Create_Date__c = date.newInstance(2014, 5, 25),
            SessionId__c = '1',
            Active__c = true,
            Email__c = 'test@mail.com');
            
        insert testUser;

    	testAccount = new Account__c(
    		Name = 'test contact',
    		First_Name__c = 'first name',
    		Last_Name__c = 'last name', 
    		Phone__c = '123456', 
    		Email__C = 'test@mail.com', 
    		Address1__c = 'addr1', 
    		Address2__c = 'addr2', 
			City__c = 'City', 
			State__c = 'State',
			Zipcode__c = '1234', 
			Company__c = testCompany.Id,
			User__c = testUser.Id,
			Web_Site__c = 'web-site',
			Country__c = 'country');
			
		insert testAccount;
    
    	testContact = new Contact__c(
    		Name = 'test contact',
    		First_Name__c = 'first name',
    		Last_Name__c = 'last name', 
    		Phone_No__c = '123456', 
    		Email__C = 'test@mail.com', 
    		Address1__c = 'addr1', 
    		Address2__c = 'addr2', 
			City__c = 'City', 
			State__c = 'State',
			Pincode__c = '1234', 
			Sum_Max_Response__c = 13,
			Company__c = testCompany.Id,
			User__c = testUser.Id,
			Account__c = testAccount.Id,
			Country__c = 'country');
			
		insert testContact;
                   
    }
    
    static testMethod void testUpdateCompany() {
    	
    	Test.startTest();
    	
    	init();
    	
    	testCompany.Trial__c = 2;
    	testCompany.Active__c = false;
    	update testCompany;
    	
    	Test.stopTest();
    }
}