public with sharing class ISA_NewQuestionCtrl extends AbstractController {
	public Question__c question {get; set;}
	public List<SelectOption> surveyOptions {get; set;}
	public String selectedSurvey {get; set;}
	
	/**
	* @description 
	*/
	public ISA_NewQuestionCtrl() {
		selectedSurvey = null;
		surveyOptions = new List<SelectOption> { new SelectOption('', '--None--') };
		
		String questionId = ApexPages.currentPage().getParameters().get('sId');
		
		if (! String.isEmpty(questionId)) {
			List<Question__c> questions = [
					SELECT Name, Title__c, Survey_Design__c 
					FROM Question__c 
					WHERE Id = :questionId 
					LIMIT 1];
			
			if ( ! questions.isEmpty()) {
				this.question = questions.get(0);
				this.selectedSurvey = this.question.Survey_Design__c;
			}
		} else {
			this.question = new Question__c();
		}
		
		List<Survey_Design__c> surveys = [
				SELECT Name 
				FROM Survey_Design__c 
				ORDER BY Name 
				LIMIT 100];
		
		if ( ! surveys.isEmpty()) {
			for (Survey_Design__c survey : surveys) {
				surveyOptions.add(new SelectOption(survey.Id, survey.Name));
			}
		}
	}
	
	/**
	* @description 
	*/
	public PageReference doSave() {
		try {
			upsert question;
			
			PageReference pr = new PageReference(AUTH_StaticVariables.prefixSite + '/ISA_ViewQuestion');
			pr.getParameters().put('sId', this.question.Id);
			pr.getParameters().put('returnUrl', AUTH_StaticVariables.prefixSite + '/ISA_ListViewQuestion');
			pr.setRedirect(true);
			
			return pr;
		} catch(Exception ex) {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Question can not be changed.'));
			
			return null;
		}
	}
	
	/**
	* @description 
	*/
	public override PageReference doCancel() {
		PageReference pr = new PageReference(AUTH_StaticVariables.successLoginURL);
		pr.getParameters().put('sObjectLabel', 'Questions');
		pr.setRedirect(true);
		
		return pr;
	}
	
	@TestVisible protected override void collectRetParams(PageReference pr) {
		pr.getParameters().put('sId',ApexPages.currentPage().getParameters().get('sId'));
	}
}