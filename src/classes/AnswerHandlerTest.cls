@isTest
private class AnswerHandlerTest {
	
	static Company__c testCompany;
	static User__c testUser;
	static Account__c testAccount;
	static Contact__c testContact;
	static Survey_Design__c testSurveyDesign;
    static Survey_Result__c testSurveyResult;
    static Question__c testQuestion;
   	static Answer__c testAnswer;
    
    /*
    * @description: Default object creation
    */
    static void init() {        	
    	     
        testCompany = new Company__c(
            Name = 'test name',
            Admin_First_Name__c = 'firstname',
            Admin_Last_Name__c = 'lastname',
            Admin_Email__c = 'test@mail.com',
            Admin_Login__c = 'testLogin',
            Admin_Password__c = 'testPassword',
            Address1__c = 'addr1',
            Address2__c = 'addr2',
            Sum_Max_Response__c = 10,
            Trial_Start__c = Date.today(),
            Maximum_Users__c = 10);
            
        insert testCompany;
        
        testUser = new User__c(
            First_Name__c = 'First', 
            Last_Name__c = 'Last', 
            Phone__c = '123455678', 
            Username__c = 'testLogin', 
            Password__c = 'testPassword', 
            Role__c = 'Admin', 
            Company__c = testCompany.Id,
            Expiration_Date__c = Date.today(), 
            Create_Date__c = Date.today(),
            SessionId__c = '1',
            Active__c = true,
            Email__c = 'test@mail.com');
            
        insert testUser;

    	testAccount = new Account__c(
    		Name = 'test contact',
    		First_Name__c = 'first name',
    		Last_Name__c = 'last name', 
    		Phone__c = '123456', 
    		Email__C = 'test@mail.com', 
    		Address1__c = 'addr1', 
    		Address2__c = 'addr2', 
			City__c = 'City', 
			State__c = 'State',
			Zipcode__c = '1234', 
			Company__c = testCompany.Id,
			User__c = testUser.Id,
			Web_Site__c = 'web-site',
			Country__c = 'country');
			
		insert testAccount;
    
    	testContact = new Contact__c(
    		Name = 'test contact',
    		First_Name__c = 'first name',
    		Last_Name__c = 'last name', 
    		Phone_No__c = '123456', 
    		Email__C = 'test@mail.com', 
    		Address1__c = 'addr1', 
    		Address2__c = 'addr2', 
			City__c = 'City', 
			State__c = 'State',
			Pincode__c = '1234', 
			Sum_Max_Response__c = 13,
			Company__c = testCompany.Id,
			User__c = testUser.Id,
			Account__c = testAccount.Id,
			Country__c = 'country');
			
		insert testContact;
	
    	testSurveyDesign = new Survey_Design__c(
    		Name = 'Name',
    		Company__c = testCompany.id);
    		
    	insert testSurveyDesign;
    
    	testQuestion = new Question__c(
    		Name = 'Name',
    		Title__c = 'Title',
    		Survey_Design__c = testSurveyDesign.id);
    		
    	insert testQuestion;
    	
    	testSurveyResult = new Survey_Result__c(
    		Name = 'Name',
    		Sum_Response__c = 10,
    		Sum_Max_Response__c = 15,
    		Survey_Design__c = testSurveyDesign.Id,
    		Question__c = testQuestion.Id,
    		Contact__c = testContact.Id);
    		
    	insert testSurveyResult;
    
    	testAnswer = new Answer__c(
    		Name = 'Name',
    		Point__c = '4',
    		Question__c = testQuestion.Id,
    		Contact__c = testContact.Id);
    		
    	insert testAnswer;                
    }
    
    static testMethod void testUpdateAnswer() {
    	
    	Test.startTest();
    	
    	init();
    	
    	testAnswer.Point__c = '12';
    	update testAnswer;
    	
    	Test.stopTest();
    }
    
    static testMethod void testDeleteAnswer() {
    	
    	Test.startTest();
    	
    	init();
    	
    	delete testAnswer;
    	
    	Test.stopTest();
    }
}