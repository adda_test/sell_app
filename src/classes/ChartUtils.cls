public with sharing class ChartUtils {
	
	public static List<WrapperCollection.CompPaidWrapper> fillPaidCompInfo() {
		List<WrapperCollection.CompPaidWrapper> resultList = new List<WrapperCollection.CompPaidWrapper>();
		WrapperCollection.CompPaidWrapper paidCompInfo = new WrapperCollection.CompPaidWrapper();
		WrapperCollection.CompPaidWrapper trialCompInfo = new WrapperCollection.CompPaidWrapper();
		
		// get company data
		List<Company__c> comps = [SELECT Id, Trial__c, Trial_End__c, Type__c FROM Company__c];
		for (Company__c comp: comps) {
			Date endTrial = comp.Trial_End__c;
			Date expectedDate = Date.today().addDays(7);
			if (comp.Type__c == 'Paid') {
				if (expectedDate <= endTrial) {
					paidCompInfo.last7Days++;
				} else {
					paidCompInfo.other++;
				}
			}
			if (comp.Type__c == 'Trial') {
				if (endTrial <= expectedDate) {
					trialCompInfo.last7Days++;
				} else {
					trialCompInfo.other++;
				}
			}
		}
		resultList.add(paidCompInfo);
		resultList.add(trialCompInfo);
		return resultList;
		
	}
	
	public static WrapperCollection.DistributionWrapper getCompanyInsightInfo(String calcFieldName, String objectName) {
		WrapperCollection.DistributionWrapper result = new WrapperCollection.DistributionWrapper();
		List<SObject> records = Database.query('SELECT Id, ' + calcFieldName + ' FROM ' + objectName + '');
		
		// calculate average
		Double avgValue = 0;
		
		for (SObject obj: records) {
			avgValue += Double.valueOf(obj.get(calcFieldName));
		}
		
		if (!records.isEmpty()) {
			avgValue = avgValue / records.size();
		}
		
		result.average = Decimal.valueOf(avgValue).setScale(2);
		
		// calculate other values
		for (SObject obj: records) {
			result.allCount++;
			Decimal val = Decimal.valueOf(Double.valueOf(obj.get(calcFieldName))).setScale(1);
			// 0, 0-15, 15-(avg-5), (avg-5)-(avg+5), (avg+5) - 90, 90-100 
			if (val == 0) {
				result.firstValue++;
				result.firstSum += val;
			}
			if (val > 0 && val <= 15) {
				result.secondValue++;
				result.secondSum += val;
			}
			if (val > 15 && val <= (result.average-5)) {
				result.thirdValue++;
				result.thirdSum += val;
			}
			if (result.average != 0 && val > (result.average-5) && val <= (result.average+5)) {
				result.fourthValue++;
				result.fourthSum += val;
			}
			if (val > (result.average+5) && val <= 90) {
				result.fifthValue++;
				result.fifthSum += val;
			}
			if (val > 90) {
				result.sixthValue++;
				result.sixthSum += val;
			}
		}
		
		System.debug('result_object:' + result);
		
		return result;
	}
	
	public static WrapperCollection.DistributionWrapper getCompanyStrategyInfo(String p_strategyType) {
		WrapperCollection.DistributionWrapper result = new WrapperCollection.DistributionWrapper();
		
		List<Strategy_Result__c> strategyResults = new List<Strategy_Result__c>();
		List<Contact__c> contacts = new List<Contact__c>();
		
		if (p_strategyType == 'Assigned') {
			strategyResults = (List<Strategy_Result__c>)Database.query(
				'SELECT Strategy_Design__r.Company__c ' + 
				'FROM Strategy_Result__c ' + 
				'LIMIT 25000');
		} else if (p_strategyType == 'Completed') {
			strategyResults = (List<Strategy_Result__c>)Database.query(
				'SELECT Strategy_Design__r.Company__c ' + 
				'FROM Strategy_Result__c ' + 
				'WHERE Is_Completeness__c = true ' + 
				'LIMIT 25000');
		}
		
		contacts = (List<Contact__c>)Database.query('SELECT Company__c FROM Contact__c LIMIT 25000');
		
		result.allCount = strategyResults.size();
		
		// calculate average assigned strategies
		Double avgValue = (strategyResults.size() != 0 && contacts.size() != 0) 
				? ((strategyResults.size() * 1.0) / contacts.size()) * 100 
				: 0.00;
		
		result.average = Decimal.valueOf(avgValue).setScale(2);
		
		//Group by Company
		Map<Id, Integer> assignedCompanyMap = new Map<Id, Integer>();
		for (Strategy_Result__c strategyResult : strategyResults) {
			Id key = strategyResult.Strategy_Design__r.Company__c;
			
			if ( ! assignedCompanyMap.containsKey(key)) {
				assignedCompanyMap.put(key, 1);
			} else {
				assignedCompanyMap.put(key, assignedCompanyMap.get(key) + 1);
			}
		}
		
		Map<Id, Integer> contactCompanyMap = new Map<Id, Integer>();
		for (Contact__c contact : contacts) {
			Id key = contact.Company__c;
			
			if ( ! contactCompanyMap.containsKey(key)) {
				contactCompanyMap.put(key, 1);
			} else {
				contactCompanyMap.put(key, contactCompanyMap.get(key) + 1);
			}
		}
		
		for (Id companyId : contactCompanyMap.keySet()) {
			Integer assignedCount = assignedCompanyMap.containskey(companyId) 
					? assignedCompanyMap.get(companyId) 
					: 0;
			
			Integer contactCount = contactCompanyMap.get(companyId);
			
			Decimal val = ((assignedCount * 1.0) / contactCount).setScale(1);
			
			//0-15, (avg - 10)-(avg + 10), 85-100
			if (val == 0) {
				result.firstValue++;
				result.firstSum += val;
			}
			if (val > 0 && val <= 15) {
				result.secondValue++;
				result.secondSum += val;
			}
			if (val > 15 && val <= (result.average-5)) {
				result.thirdValue++;
				result.thirdSum += val;
			}
			if (result.average != 0 && val > (result.average-5) && val <= (result.average+5)) {
				result.fourthValue++;
				result.fourthSum += val;
			}
			if (val > (result.average+5) && val <= 90) {
				result.fifthValue++;
				result.fifthSum += val;
			}
			if (val > 90) {
				result.sixthValue++;
				result.sixthSum += val;
			}
			/*
			if (val >= 0 && val <= 15) {
				++result.firstValue;
				result.firstSum += val;
			}
			
			if (val >= (result.average - 10) && val <= (result.average + 10)) {
				++result.thirdValue;
				result.thirdSum += val;
			}
			
			if (val >= 85) {
				++result.sixthValue;
				result.sixthSum += val;
			}
			*/
		}
		
		//result.firstLabel = 'Low group \\nCount: ' + result.firstValue;
		//result.thirdLabel = 'Middle group \\nRange: ' + ((result.average >= 10) ? (result.average - 10) : 0) + '% - ' + 
				//(result.average + 10) + '%\\nAverage: ' + result.average + '%\\nCount:' + result.thirdValue;
		//result.sixthLabel = 'High group \\nCount:' + result.sixthValue;
		
		return result;
	}
	
	public static List<WrapperCollection.CompUsageWrapper> fillCampUsageInfoByType(String objectName, String objPluralName) {
		List<WrapperCollection.CompUsageWrapper> resultList = new List<WrapperCollection.CompUsageWrapper>();
		WrapperCollection.CompUsageWrapper paidCompInfo = new WrapperCollection.CompUsageWrapper();
		WrapperCollection.CompUsageWrapper trialCompInfo = new WrapperCollection.CompUsageWrapper();
		
		// Id - Company Id
		Map<Id, SortObjectWrapper> paidCompCountMap = new Map<Id, SortObjectWrapper>();
		Map<Id, SortObjectWrapper> trialCompCountMap = new Map<Id, SortObjectWrapper>();
		Integer paidCount = 0;
		Integer trialCount = 0;
		// get users
		List<SObject> records = Database.query('SELECT Id, Company__c, Company__r.Type__c, Company__r.Name FROM ' + objectName + ' WHERE Company__c != null');
		for (SObject obj: records) {
			Id objId = (Id) obj.get('Company__c');
			
			if (objId == null) {
				continue; // skip user without company
			}
			
			String compType = String.valueOf(obj.getSObject('Company__r').get('Type__c'));			
			// fill paid map
			if (compType == 'Paid') {
				paidCount++;
				
				if ( ! paidCompCountMap.containsKey(objId)) {
					paidCompCountMap.put(objId, new SortObjectWrapper(0, objId, String.valueOf(obj.getSObject('Company__r').get('Name')), 'DESC'));
				}
				
				SortObjectWrapper sObj = paidCompCountMap.get(objId);
				sObj.listSize  = sObj.listSize + 1;
				paidCompCountMap.put(objId, sObj);
			}
			if (compType == 'Trial') {
				trialCount++;
				if (!trialCompCountMap.containsKey(objId)) {
					trialCompCountMap.put(objId, new SortObjectWrapper(0, objId, String.valueOf(obj.getSObject('Company__r').get('Name')), 'DESC'));
				}
				SortObjectWrapper sObj = trialCompCountMap.get(objId);
				sObj.listSize  = sObj.listSize + 1;
				trialCompCountMap.put(objId, sObj);
			}
			
		}
		// sort by DESC
		
		// paid
		List<SortObjectWrapper> paidSortList = new List<SortObjectWrapper>();
		paidSortList.addAll(paidCompCountMap.values());
		
		List<SortObjectWrapper> trialSortList = new List<SortObjectWrapper>();
		trialSortList.addAll(trialCompCountMap.values());
		
		paidCompInfo = fillUsageObject(paidSortList, objPluralName);
		paidCompInfo.allCount = paidCount;
		trialCompInfo = fillUsageObject(trialSortList, objPluralName);
		trialCompInfo.allCount = trialCount;
		resultList.add(paidCompInfo);
		resultList.add(trialCompInfo);
		return resultList;				
		
	}
	
	
	private static WrapperCollection.CompUsageWrapper fillUsageObject(List<SortObjectWrapper> sortObjList, String objPluralName) {
		WrapperCollection.CompUsageWrapper result = new WrapperCollection.CompUsageWrapper();
		result.objectPluralName = objPluralName;
		
		Map<Integer, List<SortObjectWrapper>> sortMap = new Map<Integer, List<SortObjectWrapper>>();
		for (SortObjectWrapper sortObj : sortObjList) {
			Integer key = sortObj.listSize;
			
			if ( ! sortMap.containsKey(key)) {
				sortMap.put(key, new List<SortObjectWrapper>());
			}
			sortMap.get(key).add(sortObj);
		}
		
		List<Integer> sortIntList = new List<Integer>(sortMap.keySet());
		sortIntList.sort();
		
		List<SortObjectWrapper> resultSortList = new List<SortObjectWrapper>();
		for (Integer intKey : sortIntList) {
			for (SortObjectWrapper sortObj : sortMap.get(intKey)) {
				resultSortList.add(sortObj);
			}
		}
		
		for (Integer i = 0; i < resultSortList.size(); i++) {
			if (i == 0) {
				result.fourthValue = resultSortList.get(0).listSize;
				result.fourthLabel = resultSortList.get(0).label + '\\n' + objPluralName + ':' + result.fourthValue;
			} else if (i == 1) {
				result.thirdValue = resultSortList.get(1).listSize;
				result.thirdLabel = resultSortList.get(1).label + '\\n' + objPluralName + ':' + result.thirdValue;
			} else if (i == 2) {
				result.secondValue = resultSortList.get(2).listSize;
				result.secondLabel = resultSortList.get(2).label + '\\n' + objPluralName + ':' + result.secondValue;
			} else if (i == 3) {
				result.firstValue = resultSortList.get(3).listSize;
				result.firstLabel = resultSortList.get(3).label + '\\n' + objPluralName + ':' + result.firstValue;
			}
		}
		
		Integer avg = 0;
		for (SortObjectWrapper sOjb: sortObjList) {
			avg += sOjb.listSize;
		}
		
		if (!sortObjList.isEmpty()) {
			result.average = avg / sortObjList.size();
		}
		
		return result;
	}
	
	public static List<WrapperCollection.BubbleWrapper> getBubbleChart(String p_objectName, 
			String p_countFieldName, String p_compFieldName, String p_insightIndexFieldName,
			String p_noDataFieldName, String p_noStrategyFieldName, String p_whereCondition, 
			List<String> p_additionalFields, String p_userRole, String p_userId) {
		
		if (p_objectName == 'User__c') {
			return getUserBubbleChart(p_countFieldName, p_compFieldName, p_insightIndexFieldName, 
					p_noDataFieldName, p_noStrategyFieldName, p_whereCondition, p_additionalFields);
		} else if (p_objectName == 'Account__c') {
			return getAccountBubbleChart(p_countFieldName, p_compFieldName, p_insightIndexFieldName, 
					p_noDataFieldName, p_noStrategyFieldName, p_whereCondition, p_additionalFields, p_userRole, p_userId);
		} else if (p_objectName == 'Contact__c') {
			return getContactBubbleChart(p_whereCondition, p_additionalFields, null);
		}
		
		return new List<WrapperCollection.BubbleWrapper>();
	}
	
	public static List<WrapperCollection.AccountBubbleWrapper> getAccountBubbleChart(
			String countFieldName, String compFieldName, String insightIndexFieldName,
			String noDataFieldName, String noStrategyFieldName, String whereCondition, 
			List<String> additionalFields, String p_userRole, String p_userId) {
		
		List<WrapperCollection.AccountBubbleWrapper> resultList = new List<WrapperCollection.AccountBubbleWrapper>();
		String addFieldString = '';
		
		if (additionalFields == null) {
			additionalFields = new List<String>();
		} else {
			addFieldString = ', ' + String.join(additionalFields, ', ');
		}
		
		if (p_userRole == 'Company User') {
			List<AssignContact__c> junctions = [
					SELECT Contact__r.Account__c 
					FROM AssignContact__c 
					WHERE User__c = :p_userId 
					  AND Contact__c != null 
					LIMIT 10000];
			
			Set<String> accountIds = new Set<String>();
			for (AssignContact__c junction : junctions) {
				if (junction.Contact__r.Account__c != null) {
					accountIds.add('\'' + junction.Contact__r.Account__c + '\'');
				}
			}
			
			if ( ! accountIds.isEmpty()) {
				whereCondition += ' AND Id IN (' + String.join(new List<String>(accountIds), ',') + ')';
			}
		}
		
		List<SObject> records = Database.query('SELECT Id, Name, ' + compFieldName + ',' + insightIndexFieldName + 
				', ' + countFieldName + ', ' + noDataFieldName + ', ' + noStrategyFieldName + 
				addFieldString + ' FROM Account__c ' + whereCondition);
		
		for (SObject obj: records) {
			String objName = (String) obj.get('Name');
			
			Decimal insighIndex = 0;
			if (obj.get(compFieldName) != null) {
				insighIndex = Decimal.valueOf(Double.valueOf(obj.get(insightIndexFieldName))).setScale(1);
			}
			Decimal completeness = 0;
			if (obj.get(insightIndexFieldName) != null) {
				completeness = Decimal.valueOf(Double.valueOf(obj.get(compFieldName))).setScale(1);
			}
			Decimal sizeValue = 0;
			if (obj.get(countFieldName) != null) {
				sizeValue = Decimal.valueOf(Double.valueOf(obj.get(countFieldName))).setScale(0);
			}
			Decimal noData = 0;
			if (obj.get(noDataFieldName) != null) {
				noData = Decimal.valueOf(Double.valueOf(obj.get(noDataFieldName))).setScale(0);
			}
			Decimal noStrategy = 0;
			if (obj.get(noStrategyFieldName) != null) {
				noStrategy = Decimal.valueOf(Double.valueOf(obj.get(noStrategyFieldName))).setScale(0);
			}
			
			DateTime lastActivity;
			if (obj.get('Last_Activity__c') != null) {
				lastActivity = DateTime.valueOf(obj.get('Last_Activity__c'));
			}
			
			WrapperCollection.AccountBubbleWrapper wrapObj = new WrapperCollection.AccountBubbleWrapper();
			wrapObj.insightIndex = insighIndex;
			wrapObj.completeness = completeness;
			wrapObj.childCount = sizeValue;
			wrapObj.bubbleName = objName;
			wrapObj.noData = noData;
			wrapObj.noStrategy = noStrategy;
			wrapObj.lastActivity = lastActivity;
			
			// fill add info
			Map<String, String> addInfo = new Map<String, String>();
			for (String field: additionalFields) {
				if (obj.get(field) != null) {
					addInfo.put(field, String.valueOf(obj.get(field)));
				}
				
			}
			wrapObj.addInfo = addInfo;
			resultList.add(wrapObj);
			
		}
		return resultList;
	}
	
	public static List<WrapperCollection.UserBubbleWrapper> getUserBubbleChart(
			String countFieldName, String compFieldName, String insightIndexFieldName,
			String noDataFieldName, String noStrategyFieldName, String whereCondition, 
			List<String> additionalFields) {
		
		List<WrapperCollection.UserBubbleWrapper> resultList = new List<WrapperCollection.UserBubbleWrapper>();
		String addFieldString = '';
		
		if (additionalFields == null) {
			additionalFields = new List<String>();
		} else {
			addFieldString = ', ' + String.join(additionalFields, ', ');
		}
		
		// get data
		List<SObject> records = Database.query('SELECT Id, Name, ' + compFieldName + ',' + insightIndexFieldName + ', ' + countFieldName + ', ' + 
			noDataFieldName + ', ' + noStrategyFieldName + addFieldString + ' FROM User__c ' + whereCondition);
		
		for (SObject obj: records) {
			String objName = (String) obj.get('Name');
			
			Decimal insighIndex = 0;
			if (obj.get(compFieldName) != null) {
				insighIndex = Decimal.valueOf(Double.valueOf(obj.get(insightIndexFieldName))).setScale(1);
			}
			Decimal completeness = 0;
			if (obj.get(insightIndexFieldName) != null) {
				completeness = Decimal.valueOf(Double.valueOf(obj.get(compFieldName))).setScale(1);
			}
			Decimal sizeValue = 0;
			if (obj.get(countFieldName) != null) {
				sizeValue = Decimal.valueOf(Double.valueOf(obj.get(countFieldName))).setScale(0);
			}
			Decimal noData = 0;
			if (obj.get(noDataFieldName) != null) {
				noData = Decimal.valueOf(Double.valueOf(obj.get(noDataFieldName))).setScale(0);
			}
			
			Decimal noStrategy = 0;
			if (obj.get(noStrategyFieldName) != null) {
				noStrategy = Decimal.valueOf(Double.valueOf(obj.get(noStrategyFieldName))).setScale(0);
			}
			
			DateTime lastActivity;
			if (obj.get('Last_Activity__c') != null) {
				lastActivity = DateTime.valueOf(obj.get('Last_Activity__c'));
			}
			
			WrapperCollection.UserBubbleWrapper wrapObj = new WrapperCollection.UserBubbleWrapper();
			wrapObj.insightIndex = insighIndex;
			wrapObj.completeness = completeness;
			wrapObj.childCount = sizeValue;
			wrapObj.bubbleName = objName;
			wrapObj.noData = noData;
			wrapObj.noStrategy = noStrategy;
			wrapObj.lastActivity = lastActivity;
			
			// fill add info
			Map<String, String> addInfo = new Map<String, String>();
			for (String field: additionalFields) {
				if (obj.get(field) != null) {
					addInfo.put(field, String.valueOf(obj.get(field)));
				}
				
			}
			wrapObj.addInfo = addInfo;
			resultList.add(wrapObj);
			
		}
		return resultList;
	}
	
	public static List<WrapperCollection.ContactBubbleWrapper> getContactBubbleChart(String p_whereCondition, 
			List<String> p_additionalFields, String p_userId) {
		
		List<WrapperCollection.ContactBubbleWrapper> resultList = new List<WrapperCollection.ContactBubbleWrapper>();
		String addFieldString = '';
		
		if (p_additionalFields == null || p_additionalFields.isEmpty()) {
			p_additionalFields = new List<String>();
		} else {
			addFieldString = ', ' + String.join(p_additionalFields, ', ');
		}
		
		// get data
		List<SObject> records = new List<sObject>();
		if ( ! String.isEmpty(p_userId)) {
			List<AssignContact__c> junctions = [
				SELECT Contact__c 
				FROM AssignContact__c 
				WHERE User__c = :p_userId 
				LIMIT 10000];
			
			Set<String> contactIds = new Set<String>();
			for (AssignContact__c junction : junctions) {
				contactIds.add('\'' + junction.Contact__c + '\'');
			}
			
			if ( ! contactIds.isEmpty()) {
				if ( ! String.isEmpty(p_whereCondition)) {
					p_whereCondition += ' AND Id IN (' + String.join(new List<String>(contactIds), ',') + ')';
				} else {
					p_whereCondition += ' WHERE Id IN (' + String.join(new List<String>(contactIds), ',') + ')';
				}
			}
			
			records = Database.query('SELECT Name, AccountName__c, Completeness_Score__c, ' + 
				'Influence__c, Is_No_Data__c, Insight_Index__c, Strategy_Name__c, Last_Activity__c' + addFieldString + 
				' FROM Contact__c ' + p_whereCondition);
		} else {
			records = Database.query('SELECT Name, AccountName__c, Completeness_Score__c, ' + 
				'Influence__c, Is_No_Data__c, Insight_Index__c, Strategy_Name__c, Last_Activity__c' + addFieldString + 
				' FROM Contact__c ' + p_whereCondition);
		}
		
		for (SObject obj: records) {
			String objName = (String)obj.get('Name');
			
			Boolean isNoData;
			if (obj.get('Is_No_Data__c') != null) {
				isNoData = (Boolean)obj.get('Is_No_Data__c');
			}
			
			String accountName = '';
			if (obj.get('AccountName__c') != null) {
				accountName = (String)obj.get('AccountName__c');
			}
			
			Decimal completeness = 0;
			if (obj.get('Completeness_Score__c') != null) {
				completeness = Decimal.valueOf(Double.valueOf(obj.get('Completeness_Score__c'))).setScale(1);
			}
			
			Decimal insightIndex = 0;
			if (obj.get('Insight_Index__c') != null) {
				insightIndex = Decimal.valueOf(Double.valueOf(obj.get('Insight_Index__c'))).setScale(1);
			}
			
			Decimal influence = 0;
			if (obj.get('Influence__c') != null) {
				influence = Decimal.valueOf(Double.valueOf(obj.get('Influence__c'))).setScale(1);
			}
			
			String strategy = '';
			if (obj.get('Strategy_Name__c') != null) {
				strategy = (String)obj.get('Strategy_Name__c');
			}
			
			DateTime lastActivity;
			Integer day = 0;
			Integer month = 0;
			Integer year = 0;
			if (obj.get('Last_Activity__c') != null) {
				lastActivity = (DateTime)obj.get('Last_Activity__c');
				year = lastActivity.year();
				month = lastActivity.month() - 1;
				day = lastActivity.day();
			}
			
			WrapperCollection.ContactBubbleWrapper wrapObj = new WrapperCollection.ContactBubbleWrapper();
			wrapObj.insightIndex = insightIndex;
			wrapObj.completeness = completeness;
			wrapObj.bubbleName = objName;
			wrapObj.strategy = strategy;
			wrapObj.accountName = accountName;
			wrapObj.influence = influence;
			wrapObj.lastActivity = lastActivity;
			wrapObj.day = day;
			wrapObj.month = month;
			wrapObj.year = year;
			wrapObj.isNoData = isNoData;
			
			// fill add info
			Map<String, String> addInfo = new Map<String, String>();
			for (String field : p_additionalFields) {
				if (obj.get(field) != null) {
					addInfo.put(field, String.valueOf(obj.get(field)));
				}
				
			}
			wrapObj.addInfo = addInfo;
			resultList.add(wrapObj);
			
		}
		
		return resultList;
	}
	
	public static List<WrapperCollection.InsightBubbleWrapper> getInsightBubbleChart(String p_whereCondition, 
			List<String> p_additionalFields) {
		
		List<WrapperCollection.InsightBubbleWrapper> resultList = new List<WrapperCollection.InsightBubbleWrapper>();
		String addFieldString = '';
		
		if (p_additionalFields == null || p_additionalFields.isEmpty()) {
			p_additionalFields = new List<String>();
		} else {
			addFieldString = ', ' + String.join(p_additionalFields, ', ');
		}
		
		// get data
		List<SObject> records = new List<sObject>();
		records = Database.query(
				'SELECT Name, Contacts__c, Weight__c, Completeness_Score__c, Point_Options__c, Count_Answers__c, ' + 
					'(SELECT Point__c ' + 
					' FROM Asnwers__r)' + 
					addFieldString + 
				' FROM Question__c ' + p_whereCondition);
		
		for (SObject obj: records) {
			String objName = (String)obj.get('Name');
			
			Decimal contacts = 0;
			if (obj.get('Contacts__c') != null) {
				contacts = Decimal.valueOf(String.valueOf(obj.get('Contacts__c'))).setScale(0);
			}
			
			Decimal completeness = 0;
			if (obj.get('Completeness_Score__c') != null) {
				completeness = Decimal.valueOf(Double.valueOf(obj.get('Completeness_Score__c'))).setScale(1);
			}
			
			Decimal weight = 0;
			if (obj.get('Weight__c') != null) {
				weight = Decimal.valueOf(Double.valueOf(obj.get('Weight__c'))).setScale(1);
			}
			
			Decimal maxScore = 0.0;
			if (obj.get('Point_Options__c') != null) {
				maxScore = ISA_ViewContactResultCtrl.getMaxValue(String.valueOf(obj.get('Point_Options__c')).split(','));
				maxScore.setScale(0);
			}
			
			Decimal response = 0.0;
			Decimal countAnswers = 0;
			if (obj.getSObjects('Asnwers__r') != null && ! obj.getSObjects('Asnwers__r').isEmpty()) {
				for (sObject answer : obj.getSObjects('Asnwers__r')) {
					if (answer.get('Point__c') != null) {
						response += ISA_ViewContactResultCtrl.getMaxValue(String.valueOf(answer.get('Point__c')).split(','));
					}
				}
				countAnswers = obj.get('Count_Answers__c') != null ? Decimal.valueOf(String.valueOf(obj.get('Count_Answers__c'))) : 0;
			}
			
			WrapperCollection.InsightBubbleWrapper wrapObj = new WrapperCollection.InsightBubbleWrapper();
			wrapObj.bubbleName = objName;
			wrapObj.maxScore = maxScore;
			wrapObj.weight = weight;
			wrapObj.avrScore = (countAnswers != 0) ? (response / countAnswers).setScale(1) : 0;
			wrapObj.response = completeness;
			wrapObj.contacts = contacts;
			
			// fill add info
			Map<String, String> addInfo = new Map<String, String>();
			for (String field : p_additionalFields) {
				if (obj.get(field) != null) {
					addInfo.put(field, String.valueOf(obj.get(field)));
				}
				
			}
			
			wrapObj.addInfo = addInfo;
			resultList.add(wrapObj);
		}
		
		return resultList;
	}
	
	public static List<WrapperCollection.ColumnChartWrapper> getStrategyColumnChart(String whereCondition, 
			String p_companyId) {
		
		List<WrapperCollection.ColumnChartWrapper> resultList = new List<WrapperCollection.ColumnChartWrapper>();
		
		// get data
		//SELECT All strategies results
		List<Strategy_Result__c> strategyResults = new List<Strategy_Result__c>();
		if ( ! String.isEmpty(p_companyId)) {
			strategyResults =  [
				SELECT Is_Completeness__c, Strategy_Design__r.Name 
				FROM Strategy_Result__c 
				WHERE Strategy_Design__r.Company__c = :p_companyId 
				LIMIT 50000];
		} else {
			strategyResults =  [
				SELECT Is_Completeness__c, Strategy_Design__r.Name 
				FROM Strategy_Result__c 
				LIMIT 50000];
		}
		
		
		if ( ! strategyResults.isEmpty()) {
			Map<String, Integer> assignedStrategyMap = new Map<String, Integer>();
			Map<String, Integer> completenessStrategyMap = new Map<String, Integer>();
			
			for (Strategy_Result__c result : strategyResults) {
				String key = result.Strategy_Design__r.Name;
				
				if ( ! assignedStrategyMap.containsKey(key)) {
					assignedStrategyMap.put(key, 1);
				} else {
					assignedStrategyMap.put(key, assignedStrategyMap.get(key) + 1);
				}
				
				if ( ! completenessStrategyMap.containsKey(key) && result.Is_Completeness__c) {
					completenessStrategyMap.put(key, 1);
				} else if (result.Is_Completeness__c) {
					completenessStrategyMap.put(key, completenessStrategyMap.get(key) + 1);
				}
			}
			
			for (String name : assignedStrategyMap.keySet()) {
				if (assignedStrategyMap.containsKey(name)) {
					WrapperCollection.ColumnChartWrapper wrapObj = new WrapperCollection.ColumnChartWrapper();
					wrapObj.assigned = assignedStrategyMap.get(name);
					wrapObj.columnName = name;
					wrapObj.completed = completenessStrategyMap.containsKey(name) ? completenessStrategyMap.get(name) : 0;
					
					resultList.add(wrapObj);
				}
			}
			
		}
		
		return resultList;
	}
	
	public static List<WrapperCollection.LineChartWrapper> getLineChartList(String p_companyId) {
		List<WrapperCollection.LineChartWrapper> results = new List<WrapperCollection.LineChartWrapper>();
		
		List<Company_History__c> histories = [
			SELECT Field__c, NewValue__c, CreatedDate 
			FROM Company_History__c 
			WHERE Company__c = :p_companyId 
			LIMIT 1000];
		
		Map<Date, List<Company_History__c>> historyMap = new Map<Date, List<Company_History__c>>();
		
		for (Company_History__c history : histories) {
			Date key = history.CreatedDate.date();
			
			if ( ! historyMap.containsKey(key)) {
				historyMap.put(key, new List<Company_History__c>());
			}
			historyMap.get(key).add(history);
		}
		
		List<Date> sortDateList = new List<Date>(historyMap.keySet());
		sortDateList.sort();
		
		for (Date dateSort : sortDateList) {
			Decimal insightIndex = 0;
			Decimal completeness = 0;
			Decimal users = 0;
			
			for (Company_History__c history : historyMap.get(dateSort)) {
				if (history.Field__c == 'Insight Index') {
					if (history.NewValue__c != null && insightIndex <= Decimal.valueOf(history.NewValue__c)) {
						insightIndex = Decimal.valueOf(history.NewValue__c);
					}
				} else if (history.Field__c == 'Completeness') {
					if (history.NewValue__c != null && completeness <= Decimal.valueOf(history.NewValue__c)) {
						completeness = Decimal.valueOf(history.NewValue__c);
					}
				} else if (history.Field__c == 'Users') {
					if (history.NewValue__c != null && users <= Decimal.valueOf(history.NewValue__c)) {
						users = Decimal.valueOf(history.NewValue__c);
					}
				}
			}
			
			WrapperCollection.LineChartWrapper wrapper = new WrapperCollection.LineChartWrapper();
			wrapper.dateManaged = dateSort;
			wrapper.completeness = completeness;
			wrapper.insightIndex = insightIndex;
			wrapper.users = users;
			
			System.debug('----wrapper=' + wrapper);
			
			results.add(wrapper);
		}
		
		return results;
	}
	
	public static List<WrapperCollection.ColumnChartWrapper> getInsightColumnChart(String whereCondition, 
			String p_companyId) {
		
		List<WrapperCollection.ColumnChartWrapper> resultList = new List<WrapperCollection.ColumnChartWrapper>();
		
		// get data
		List<Survey_Design__c> surveys = new List<Survey_Design__c>();
		
		if ( ! String.isEmpty(p_companyId)) {
			surveys = [
				SELECT Name 
				FROM Survey_Design__c 
				WHERE Company__c = :p_companyId 
				  AND Name != :AUTH_StaticVariables.DEFAULT_INSIGHT_PANEL_NAME 
				LIMIT 1000];
		} else {
			surveys = [
				SELECT Name 
				FROM Survey_Design__c 
				WHERE Name != :AUTH_StaticVariables.DEFAULT_INSIGHT_PANEL_NAME 
				LIMIT 1000];
		}
		
		List<Question__c> insights = [
			SELECT Title__c, 
				Index__c, Name, 
				(SELECT Id 
				FROM Asnwers__r 
				LIMIT 10000)
			FROM Question__c 
			WHERE Survey_Design__c IN :new Map<Id, Survey_Design__c>(surveys).keySet() 
			LIMIT 10000];
		
		//SELECT Contacts
		List<Contact__c> contacts = new List<Contact__c>();
		if ( ! String.isEmpty(p_companyId)) {
			contacts = [
				SELECT Id 
				FROM Contact__c 
				WHERE Company__c = :p_companyId 
				LIMIT 10000];
		} else {
			contacts = [
				SELECT Id 
				FROM Contact__c 
				LIMIT 10000];
		}
		
		for (Question__c insight : insights) {
			WrapperCollection.ColumnChartWrapper wrapObj = new WrapperCollection.ColumnChartWrapper();
			wrapObj.assigned = contacts.size();
			wrapObj.columnName = insight.Title__c.replace('\'', '\\\'');
			wrapObj.completed = ( ! insight.Asnwers__r.isEmpty()) ? insight.Asnwers__r.size() : 0;
			
			resultList.add(wrapObj);
		}
		return resultList;
	}
	
	public static WrapperCollection.DistributionWrapper getCompanyAdminInfo(String calcFieldName, 
			String objectName, String p_whereClause) {
		
		WrapperCollection.DistributionWrapper result = new WrapperCollection.DistributionWrapper();
		List<SObject> records = Database.query('SELECT Id, ' + calcFieldName + ' FROM ' + objectName + ' ' + 
				p_whereClause);
		
		// calculate average
		Double avgValue = 0;
		
		for (SObject obj: records) {
			if (obj.get(calcFieldName) != null) {
				avgValue += Double.valueOf(obj.get(calcFieldName));
			}
		}
		
		if ( ! records.isEmpty()) {
			avgValue = avgValue / records.size();
		}
		
		result.average = Decimal.valueOf(avgValue).setScale(2);
		result.allCount = records.size();
		
		// calculate other values
		for (SObject obj: records) {
			if (obj.get(calcFieldName) != null) {
				Decimal val = Decimal.valueOf(Double.valueOf(obj.get(calcFieldName))).setScale(1);
				
				// 0, 0-15, 15-(avg-5), (avg-5)-(avg+5), (avg+5) - 90, 90-100 
				if (val == 0) {
					result.firstValue++;
					result.firstSum += val;
				}
				
				if (val > 0 && val <= 15) {
					result.secondValue++;
					result.secondSum += val;
				}
				
				if (val > 15 && val <= (result.average-5)) {
					result.thirdValue++;
					result.thirdSum += val;
				}
				
				if (val > (result.average-5) && val <= (result.average+5)) {
					result.fourthValue++;
					result.fourthSum += val;
				}
				
				if (val > (result.average+5) && val <= 90) {
					result.fifthValue++;
					result.fifthSum += val;
				}
				
				if (val > 90) {
					result.sixthValue++;
					result.sixthSum += val;
				}
			}
		}
		
		System.debug('result_object:' + result);
		
		return result;
	}
	
	public static WrapperCollection.DistributionWrapper getCompanyAdminInsightInfo(String calcFieldName, 
			String objectName, String p_whereClause) {
		
		WrapperCollection.DistributionWrapper result = new WrapperCollection.DistributionWrapper();
		List<SObject> records = Database.query('SELECT Id, ' + calcFieldName + ' FROM ' + objectName + ' ' + 
				p_whereClause);
		
		// calculate average
		Double avgValue = 0;
		
		for (SObject obj: records) {
			avgValue += Double.valueOf(obj.get(calcFieldName));
		}
		
		if ( ! records.isEmpty()) {
			avgValue = avgValue / records.size();
		}
		
		result.average = Decimal.valueOf(avgValue).setScale(2);
		result.allCount = records.size();
		
		// calculate other values
		for (SObject obj: records) {
			Decimal val = Decimal.valueOf(Double.valueOf(obj.get(calcFieldName))).setScale(1);
			// 0, 0-15, 15-(avg-5), (avg-5)-(avg+5), (avg+5) - 90, 90-100 
			if (val == 0) {
				result.firstValue++;
				result.firstSum += val;
			}
			
			if (val > 0 && val <= 15) {
				result.secondValue++;
				result.secondSum += val;
			}
			
			if (val > 15 && val <= (result.average-5)) {
				result.thirdValue++;
				result.thirdSum += val;
			}
			
			if (val > (result.average-5) && val <= (result.average+5)) {
				result.fourthValue++;
				result.fourthSum += val;
			}
			
			if (val > (result.average+5) && val <= 90) {
				result.fifthValue++;
				result.fifthSum += val;
			}
			
			if (val > 90) {
				result.sixthValue++;
				result.sixthSum += val;
			}
		}
		
		System.debug('result_object:' + result);
		
		return result;
	}
	
	public static WrapperCollection.DistributionWrapper getCompanyAdminStrategyInfo(String calcFieldName, 
			String objectName, String p_whereClause) {
		
		WrapperCollection.DistributionWrapper result = new WrapperCollection.DistributionWrapper();
		List<SObject> records = Database.query('SELECT Id, ' + calcFieldName + ' FROM ' + objectName + ' ' + 
				p_whereClause);
		
		// calculate average
		Double avgValue = 0;
		
		for (SObject obj: records) {
			avgValue += Double.valueOf(obj.get(calcFieldName));
		}
		
		if ( ! records.isEmpty()) {
			avgValue = avgValue / records.size();
		}
		
		result.average = Decimal.valueOf(avgValue).setScale(2);
		result.allCount = records.size();
		
		// calculate other values
		for (SObject obj: records) {
			Decimal val = Decimal.valueOf(Double.valueOf(obj.get(calcFieldName))).setScale(1);
			// 0, 0-15, 15-(avg-5), (avg-5)-(avg+5), (avg+5) - 90, 90-100 
			if (val == 0) {
				result.firstValue++;
				result.firstSum += val;
			}
			
			if (val > 0 && val <= 15) {
				result.secondValue++;
				result.secondSum += val;
			}
			
			if (val > 15 && val <= (result.average-5)) {
				result.thirdValue++;
				result.thirdSum += val;
			}
			
			if (val > (result.average-5) && val <= (result.average+5)) {
				result.fourthValue++;
				result.fourthSum += val;
			}
			
			if (val > (result.average+5) && val <= 90) {
				result.fifthValue++;
				result.fifthSum += val;
			}
			
			if (val > 90) {
				result.sixthValue++;
				result.sixthSum += val;
			}
		}
		
		System.debug('result_object:' + result);
		
		return result;
	}
}