public with sharing class ISA_ViewContactCtrl extends AbstractController {
	public Contact__c contact {get; set;}
	public List<User__c> users {get; set;}
	
	public ISA_ViewContactCtrl() {
		
		this.sObjectLabel = 'Contact';
		this.users = new List<User__c>();
	}
	
	public PageReference initPage() {
		PageReference pr = super.init();
		if (pr != null) {
			return pr;
		}
		
		String contactId = ApexPages.currentPage().getParameters().get('sId');
		
		if (! String.isEmpty(contactId)) {
			List<Contact__c> contacts = [
					SELECT Name, First_Name__c, Last_Name__c, Phone_No__c, Email__C, Address1__c, Address2__c, 
						City__c, State__c, Pincode__c, Company__r.Name, User__c, Account__c, Country__c, 
						Influence__c 
					FROM Contact__c 
					WHERE Id = :contactId 
					LIMIT 1];
			
			if ( ! contacts.isEmpty()) {
				this.contact = contacts.get(0);
				
				List<AssignContact__c> junctions = [
					SELECT User__r.Name 
					FROM AssignContact__c 
					WHERE Contact__c = :this.contact.Id 
					LIMIT 10000];
				
				if ( ! junctions.isEmpty()) {
					Set<User__c> usersSet = new Set<User__c>();
					for (AssignContact__c junction : junctions) {
						usersSet.add(
							new User__c(
								Id = junction.User__c, 
								Name = junction.User__r.Name
							));
					}
					
					this.users = new List<User__c>(usersSet);
				}
			}
		} else {
			this.contact = new Contact__c();
			
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 
					'Selected Contact wasn\'t found.'));
		}
		
		return null;
	}
	
	public PageReference doEdit() {
		if (this.contact.Id != null) {
			PageReference pr = new PageReference(AUTH_StaticVariables.prefixSite + '/apex/ISA_NewContact');
			pr.getParameters().put('sId', this.contact.Id);
			addRetUrl(pr);
			pr.setRedirect(true);
			
			return pr;
		}
		
		return null;
	}
	
	public PageReference goChangeInfluence() {
		String accountId = ApexPages.currentPage().getParameters().get('accountId');
		
		if ( ! String.isEmpty(accountId)) {
			PageReference pr = new PageReference(AUTH_StaticVariables.prefixSite + '/ISA_InfluenceAccount');
			addRetUrl(pr);
			pr.getParameters().put('aId', accountId);
			pr.setRedirect(true);
			
			return pr;
		}
		
		return null;
	}
}