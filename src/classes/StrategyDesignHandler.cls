public with sharing class StrategyDesignHandler {
	public static Boolean enablesTrigger = true;
	
	public static void updateLastActivity(List<Strategy_Design__c> p_newRecords) {
		for (Strategy_Design__c strategy : p_newRecords) {
			strategy.Last_Activity__c = DateTime.now();
		}
	}
	
	public static void removeAssignStrategy(List<Strategy_Design__c> p_newRecords) {
		delete [
			SELECT Id 
			FROM AssignStrategy__c 
			WHERE Strategy__c IN :new Map<Id, Strategy_Design__c>(p_newRecords).keySet() 
			LIMIT 10000];
	}
	
	public static void recalculateContacts(Map<Id, Strategy_Design__c> p_oldRecordMap, 
			List<Strategy_Design__c> p_newRecords) {
		
		Set<Id> strategyIds = new Set<Id>();
		for (Strategy_Design__c strategy : p_newRecords) {
			Strategy_Design__c oldStrategy = p_oldRecordMap.get(strategy.Id);
			
			if (oldStrategy != null && oldStrategy.Order__c != strategy.Order__c) {
				strategyIds.add(strategy.Id);
			}
		}
		
		if ( ! strategyIds.isEmpty()) {
			List<AssignStrategy__c> junctions = [
				SELECT Account__c 
				FROM AssignStrategy__c 
				WHERE Strategy__c IN :strategyIds 
				LIMIT 10000];
			
			Set<Id> accountIds = new Set<Id>();
			for (AssignStrategy__c junction : junctions) {
				if (junction.Account__c != null) {
					accountIds.add(junction.Account__c);
				}
			}
			
			ISA_TriggerHelper.calculateNextStrategy(accountIds);
		}
	}
	
	public static void addStrategyHistory(List<Strategy_Design__c> p_newRecords) {
		addStrategyHistory(new Map<Id, Strategy_Design__c>(), p_newRecords);
	}
	
	public static void addStrategyHistory(Map<Id, Strategy_Design__c> p_oldRecordMap, List<Strategy_Design__c> p_newRecords) {
		List<Strategy_History__c> createHistories = new List<Strategy_History__c>();
		
		for (Strategy_Design__c strategy : p_newRecords) {
			Strategy_Design__c oldStrategy = p_oldRecordMap.get(strategy.Id);
			
			PageReference prUser = new PageReference(AUTH_StaticVariables.prefixSite + '/ISA_ViewUser');
			prUser.getParameters().put('sId', strategy.Owner__c);
			
			if (oldStrategy != null) {
				List<Schema.FieldSetMember> fieldSetMembers = Schema.getGlobalDescribe().get('Strategy_Design__c')
					.getDescribe().FieldSets.getMap().get('TrackFields').getFields();
				
				for (Schema.FieldSetMember field : fieldSetMembers) {
					if (oldStrategy.get(field.getFieldPath()) != strategy.get(field.getFieldPath())) {
						String oldValue = String.valueOf(oldStrategy.get(field.getFieldPath()));
						String newValue = String.valueOf(strategy.get(field.getFieldPath()));
						
						createHistories.add(
							new Strategy_History__c(
								Strategy__c = strategy.Id, 
								OldValue__c = oldValue, 
								NewValue__c = newValue, 
								Message__c = '<b><a href="' + prUser.getUrl() + '">' + strategy.Owner_Name__c + 
										'</a></b> updated <b>' + field.getLabel() + '</b>' + 
										' from ' + ( ! String.isEmpty(oldValue) ? '<b>' + oldValue + '</b>' : '') + 
										' to ' + ( ! String.isEmpty(newValue) ? '<b>' + newValue + '</b>' : '') + '.'
							));
					}
				}
			} else {
				createHistories.add(
					new Strategy_History__c(
						Strategy__c = strategy.Id, 
						Message__c = '<b><a href="' + prUser.getUrl() + '">' + strategy.Owner_Name__c + 
								'</a></b> created Strategy.'
					));
			}
		}
		
		insert createHistories;
	}
	
	public static void deleteStrategyResults(Map<Id, Strategy_Design__c> p_oldRecordMap) {
		delete [SELECT Id FROM Strategy_Result__c WHERE Strategy_Design__c IN :p_oldRecordMap.keySet() LIMIT 10000];
	}
	
	public static void deleteStrategyQuestions(Map<Id, Strategy_Design__c> p_oldRecordMap) {
		delete [SELECT Id FROM Strategy_Question__c WHERE Strategy_Design__c IN :p_oldRecordMap.keySet() LIMIT 10000];
	}
	
	public static void deleteStrategyHistory(Map<Id, Strategy_Design__c> p_oldRecordMap) {
		delete [SELECT Id FROM Strategy_History__c WHERE Strategy__c IN :p_oldRecordMap.keySet() LIMIT 10000];
	}
}