public with sharing class StrategyAnswerHandler {
	public static Boolean enablesTrigger = true;
	
	public static void recalculateStrategyResults(List<Strategy_Answer__c> p_newRecords, Boolean p_isInsert) {
		Set<Id> contactIds = new Set<Id>();
		Map<Id, Id> questionIds = new Map<Id, Id>();
		
		for (Strategy_Answer__c answer : p_newRecords) {
			if (answer.Contact__c != null) {
				contactIds.add(answer.Contact__c);
			}
			
			if (answer.Strategy_Question__c != null) {
				questionIds.put(answer.Strategy_Question__c, answer.Id);
			}
		}
		
		if ( ! contactIds.isEmpty() && ! questionIds.isEmpty()) {
			Map<Id, Strategy_Question__c> questions = new Map<Id, Strategy_Question__c>([
				SELECT Strategy_Design__c 
				FROM Strategy_Question__c 
				WHERE Id IN :questionIds.keySet() 
				LIMIT 10000]);
			
			Set<Id> strategyIds = new Set<Id>();
			for (Strategy_Question__c question : questions.values()) {
				if (question.Strategy_Design__c != null) {
					strategyIds.add(question.Strategy_Design__c);
				}
			}
			
			if ( ! strategyIds.isEmpty()) {
				List<Strategy_Result__c> results = [
					SELECT Contact__c, Strategy_Design__c, Count_Answers__c 
					FROM Strategy_Result__c 
					WHERE Strategy_Design__c IN :strategyIds 
					AND Contact__c IN :contactIds 
					LIMIT 10000];
				
				if ( ! results.isEmpty()) {
					Map<Id, Strategy_Result__c> updateResults = new Map<Id, Strategy_Result__c>();
					List<Strategy_Result__c> insertResults = new List<Strategy_Result__c>();
					for (Strategy_Answer__c answer : p_newRecords) {
						for (Strategy_Result__c result : results) {
							
							if (p_isInsert && 
								 ! contactIds.contains(result.Contact__c) && 
								 ! strategyIds.contains(result.Strategy_Design__c)) {//Create Strategy Result
								
								if (questions.containsKey(answer.Strategy_Question__c)) {
									insertResults.add(
										new Strategy_Result__c(
											Contact__c = answer.Contact__c, 
											Strategy_Design__c = questions.get(answer.Strategy_Question__c).Strategy_Design__c, 
											Owner__c = answer.Owner__c
										));
								}
							} else if (p_isInsert && 
								contactIds.contains(result.Contact__c) && 
								strategyIds.contains(result.Strategy_Design__c)) {//Update Strategy Result
								
								updateResults.put(result.Id, 
									new Strategy_Result__c(
										Id = result.Id, 
										Count_Answers__c = (result.Count_Answers__c != null) 
											? result.Count_Answers__c + 1 
											: 1, 
										Owner__c = answer.Owner__c
									));
								
								result.Count_Answers__c = (result.Count_Answers__c != null) 
										? result.Count_Answers__c + 1 
										: 1;
								
							} else if ( ! p_isInsert && 
								contactIds.contains(result.Contact__c) && 
								strategyIds.contains(result.Strategy_Design__c)) {//Minus 1 from Count Answers
								
								updateResults.put(result.Id, 
									new Strategy_Result__c(
										Id = result.Id, 
										Count_Answers__c = (result.Count_Answers__c != null) 
											? result.Count_Answers__c - 1 
											: 0, 
										Owner__c = answer.Owner__c
									));
								
								result.Count_Answers__c = (result.Count_Answers__c != null) 
										? result.Count_Answers__c - 1
										: 0;
							}
						}
					}
					
					if ( ! updateResults.keySet().isEmpty() || ! insertResults.isEmpty()) {
						insert insertResults;
						update updateResults.values();
					}
				}
			}
		}
	}
	
	public static void recalculateContacts(List<Strategy_Answer__c> p_newRecords) {
		Set<Id> contactIds = new Set<Id>();
		for (Strategy_Answer__c answer : p_newRecords) {
			if (answer.Contact__c != null) {
				contactIds.add(answer.Contact__c);
			}
		}
		
		if ( ! contactids.isEmpty()) {
			List<Contact__c> contacts = [
				SELECT Account__c, 
					(SELECT Id 
					FROM Strategy_Answers__r)
				FROM Contact__c 
				WHERE Id IN :contactIds 
				LIMIT 10000];
			
			Set<Id> accountIds = new Set<Id>();
			for (Contact__c contact : contacts) {
				contact.Count_Strategy_Answers__c = contact.Strategy_Answers__r.size();
				
				accountIds.add(contact.Account__c);
			}
			update contacts;
			
			ISA_TriggerHelper.calculateNextStrategy(accountIds);
		}
	}
}