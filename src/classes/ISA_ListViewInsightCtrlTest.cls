@isTest
private class ISA_ListViewInsightCtrlTest {
	
	static User__c tempUser(id id) {
        User__c testUser = new User__c(
            First_Name__c = 'First', 
            Last_Name__c = 'Last', 
            Phone__c = '123455678', 
            Username__c = 'testLogin', 
            Password__c = 'testPassword', 
            Role__c = 'Admin', 
            Company__c = id,
            Expiration_Date__c = Date.today(), 
            Create_Date__c = Date.today(),
            SessionId__c = '1',
            Active__c = true,
            Email__c = 'test@mail.com');
        return testUser;
    }
    
	static Company__c tempCompany() {
        Company__c company = new Company__c(
            Name = 'test name',
            Admin_First_Name__c = 'firstname',
            Admin_Last_Name__c = 'lastname',
            Admin_Email__c = 'test@mail.com',
            Admin_Login__c = 'testLogin',
            Admin_Password__c = 'testPassword',
            Address1__c = 'addr1',
            Address2__c = 'addr2',
            Sum_Max_Response__c = 10,
            Trial_Start__c = Date.today(),
            Maximum_Users__c = 10);
        return company;
    }
    
    static Account__c tempAccount(id companyId,id userId) {
    	Account__c account = new Account__c(
    		Name = 'test contact',
    		First_Name__c = 'first name',
    		Last_Name__c = 'last name', 
    		Phone__c = '123456', 
    		Email__C = 'test@mail.com', 
    		Address1__c = 'addr1', 
    		Address2__c = 'addr2', 
			City__c = 'City', 
			State__c = 'State',
			Zipcode__c = '1234', 
			Company__c = companyId,
			User__c = userId,
			Web_Site__c = 'web-site',
			Country__c = 'country');
		return account;
    } 
    
	static Contact__c tempContact(id companyId,id userId,id accountId) {
    	Contact__c contact = new Contact__c(
    		Name = 'test contact',
    		First_Name__c = 'first name',
    		Last_Name__c = 'last name', 
    		Phone_No__c = '123456', 
    		Email__C = 'test@mail.com', 
    		Address1__c = 'addr1', 
    		Address2__c = 'addr2', 
			City__c = 'City', 
			State__c = 'State',
			Pincode__c = '1234', 
			Company__c = companyId,
			User__c = userId,
			Account__c = accountId,
			Country__c = 'country');
		return contact;
    }
    
    static Survey_Design__c tempSurveyDesign(id id) {
    	Survey_Design__c surveyDesign = new Survey_Design__c(
    		Name = 'Name',
    		Company__c = id);
    	return surveyDesign;
    }
    
    static Question__c tempQuestion(id id) {
    	Question__c question = new Question__c(
    		Name = 'Name',
    		Title__c = 'Title',
    		Contacts__c = 10,
    		Accounts__c = 10,
    		Survey_Design__c = id);
    	return question;
    }
    
    @isTest
    private static void testInit() {
    	
        Test.startTest();
        
        ISA_ListViewInsightCtrl controller = new ISA_ListViewInsightCtrl();
        controller.initPage();
        
        System.assertNotEquals(null, controller.initPage());       
        
        Test.stopTest();
    } 
    
    @isTest
    private static void testInitWithUser() {
    	
        Test.startTest();
        
        Company__c testCompany = tempCompany();           
        insert testCompany;      
        User__c testUser = tempUser(testCompany.Id);    
        insert testUser;  
        Account__c testAccount = tempAccount(testCompany.Id,testUser.Id);    
        insert testAccount; 
        Contact__c testContact = tempContact(testCompany.Id,testUser.Id,testAccount.Id);    
        insert testContact; 
        Survey_Design__c testSurveyDesign = tempSurveyDesign(testCompany.Id);    
        insert testSurveyDesign;
        Question__c testQuestion = tempQuestion(testSurveyDesign.Id);
        insert testQuestion;

		ApexPages.currentPage().getParameters().put('insightIds',testQuestion.Id);
		ApexPages.currentPage().getParameters().put('insightId',testQuestion.Id);
		ApexPages.currentPage().getParameters().put('panelId',testSurveyDesign.Id);
 		Cookie testCookie = new Cookie('SessionId','1',null,-1,false);
        ApexPages.currentPage().setCookies(new Cookie[] {testCookie});
        
        ISA_ListViewInsightCtrl controller = new ISA_ListViewInsightCtrl();       
        controller.initPage();
        System.assertEquals(null, controller.initPage());  
        
        controller.doGenerateWhereClause();
        System.assertNotEquals(null, controller.doGenerateWhereClause());
        
        controller.doMapInsights();      
        controller.doManagePanels();
        
        controller.doDisplayContacts();
        System.assertNotEquals(null, controller.doDisplayContacts());  
       
        Test.stopTest();
    } 
}