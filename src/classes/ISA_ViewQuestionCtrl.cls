public with sharing class ISA_ViewQuestionCtrl extends AbstractController {
	public Question__c question {get; set;}
	
	public ISA_ViewQuestionCtrl() {
		String questionId = ApexPages.currentPage().getParameters().get('sId');
		
		if (! String.isEmpty(questionId)) {
			List<Question__c> questions = [
					SELECT Name, Title__c, Survey_Design__c 
					FROM Question__c 
					WHERE Id = :questionId 
					LIMIT 1];
			
			if ( ! questions.isEmpty()) {
				this.question = questions.get(0);
			}
		} else {
			this.question = new Question__c();
			
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 
					'Selected Question wasn\'t found.'));
		}
	}
	
	public override PageReference doCancel() {
		PageReference pr = new PageReference(AUTH_StaticVariables.successLoginURL);
		pr.getParameters().put('sObjectLabel', 'Questions');	
		pr.getParameters().put('sId',ApexPages.currentPage().getParameters().get('sId'));
		
		return pr;
	}
	
	public PageReference doEdit() {
		if (this.question.Id != null) {
			PageReference pr = new PageReference('/apex/ISA_NewQuestion');
			pr.getParameters().put('sId', this.question.Id);
			addRetUrl(pr);
			return pr;
		}
		
		return null;
	}
	
	
}