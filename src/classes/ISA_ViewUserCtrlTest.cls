@isTest
private class ISA_ViewUserCtrlTest {
	
    static User__c tempUser(id id) {
        User__c testUser = new User__c(
            First_Name__c = 'First', 
            Last_Name__c = 'Last', 
            Phone__c = '123455678', 
            Username__c = 'testLogin', 
            Password__c = 'password', 
            Role__c = 'Admin', 
            Company__c = id,
            Expiration_Date__c = Date.today(), 
            Create_Date__c = Date.today(),
            SessionId__c = '1',
            Active__c = true,
            Email__c = 'test@mail.com');
        return testUser;
    }
    
    static Company__c tempCompany() {
        Company__c company = new Company__c(
            Name = 'test name',
            Admin_First_Name__c = 'firstname',
            Admin_Last_Name__c = 'lastname',
            Admin_Email__c = 'test@mail.com',
            Admin_Login__c = 'testLogin',
            Admin_Password__c = 'testPassword',
            Address1__c = 'addr1',
            Address2__c = 'addr2',
            Trial_Start__c = Date.today(),
            Maximum_Users__c = 10);
        return company;
    }

     @isTest
     private static void testInitWithUserId() {
        
        Test.startTest();
        
        Company__c testCompany = tempCompany();           
        insert testCompany;      
        User__c testUser = tempUser(testCompany.Id);    
        insert testUser; 
                              
        ApexPages.currentPage().getParameters().put('sId', testUser.Id);
        
        ISA_ViewUserCtrl controller = new ISA_ViewUserCtrl();
        
        controller.doEdit();
        System.assertNotEquals(null, controller.doEdit());  
        
        Test.stopTest();
    } 
    
    @isTest
	private static void testInitWithoutUserId() {
        
        Test.startTest();
       
		ApexPages.currentPage().getParameters().put('sId', null);
        
        ISA_ViewUserCtrl controller = new ISA_ViewUserCtrl();
 
        controller.doEdit();
        System.assertEquals(null, controller.doEdit()); 
        
        Test.stopTest();
    } 
}