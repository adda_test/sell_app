public with sharing class AccountHandler {
	public static Boolean enablesTrigger = true;
	
	public static void recalculateCompany(Map<Id, Account__c> p_oldRecordsMap, List<Account__c> p_newRecords) {
		Set<Id> companyIds = new Set<Id>();
		
		for (Account__c account : p_newRecords) {
			Account__c oldAccount = p_oldRecordsMap.get(account.Id);
			
			if (oldAccount.Count_Answers__c != account.Count_Answers__c || 
				oldAccount.Count_Questions__c != account.Count_Questions__c || 
				oldAccount.Sum_Response__c != account.Sum_Response__c || 
				oldAccount.Sum_Max_Response__c != account.Sum_Max_Response__c) {
				
				if (account.Company__c != null) {
					companyIds.add(account.Company__c);
				}
			}
		}
		
		if ( ! companyIds.isEmpty()) {
			List<Company__c> companies = [
				SELECT Id, 
					(SELECT Count_Answers__c, Count_Questions__c, Sum_Response__c, Sum_Max_Response__c 
					FROM Accounts__r)
				FROM Company__c 
				WHERE Id IN :companyIds 
				LIMIT 10000];
			
			for (Company__c company : companies) {
				Integer countAnswers = 0;
				Integer countQuestions = 0;
				Integer sumResponse = 0;
				Integer sumMaxResponse = 0;
				
				for (Account__c account : company.Accounts__r) {
					countAnswers += (account.Count_Answers__c != null) ? (Integer)account.Count_Answers__c : 0;
					countQuestions += (account.Count_Questions__c != null) ? (Integer)account.Count_Questions__c : 0;
					sumResponse += (account.Sum_Response__c != null) ? (Integer)account.Sum_Response__c : 0;
					sumMaxResponse += (account.Sum_Max_Response__c != null) ? (Integer)account.Sum_Max_Response__c : 0;
				}
				
				company.Count_Answers__c = countAnswers;
				company.Count_Questions__c = countQuestions;
				company.Sum_Response__c = sumResponse;
				company.Sum_Max_Response__c = sumMaxResponse;
			}
			
			update companies;
		}
	}
	
	public static void recalculateSurveyDesign(List<Account__c> p_newRecords) {
		recalculateSurveyDesign(new Map<Id, Account__c>(), p_newRecords);
	}
	
	public static void recalculateSurveyDesign(Map<Id, Account__c> p_oldRecordMap, List<Account__c> p_newRecords) {
		Set<Id> newCompanyIds = new Set<Id>();
		Set<Id> oldCompanyIds = new Set<Id>();
		for (Account__c account : p_newRecords) {
			Account__c oldAccount = p_oldRecordMap.get(account.Id);
			
			if (oldAccount != null && (oldAccount.Company__c != account.Company__c)) {
				oldCompanyIds.add(oldAccount.Company__c);
				newCompanyIds.add(account.Company__c);
			} else if (oldAccount == null) {
				newCompanyIds.add(account.Company__c);
			}
		}
		
		if ( ! newCompanyIds.isEmpty() && ! oldCompanyIds.isEmpty()) {
			List<Survey_Design__c> surveys = [
				SELECT Accounts__c, Company__c 
				FROM Survey_Design__c 
				WHERE Company__c IN :newCompanyIds 
				   OR Company__c IN :oldCompanyIds 
				LIMIT 10000];
			
			for (Survey_Design__c survey : surveys) {
				if (newCompanyIds.contains(survey.Company__c)) {
					survey.Accounts__c = (survey.Accounts__c != null) ? survey.Accounts__c + 1 : 1;
				}
				
				if (oldCompanyIds.contains(survey.Company__c)) {
					survey.Accounts__c = (survey.Accounts__c != null) ? survey.Accounts__c - 1 : 0;
				}
			}
			
			update surveys;
		}
	}
	
	public static void removeAssignStrategies(List<Account__c> p_newRecords) {
		delete [
			SELECT Id 
			FROM AssignStrategy__c 
			WHERE Account__c IN :new Map<Id, Account__c>(p_newRecords).keySet() 
			LIMIT 10000];
	}
	
	public static void populateLastActivity(List<Account__c> p_newRecords) {
		for (Account__c account : p_newRecords) {
			account.Last_Activity__c = DateTime.now();
		}
	}
	
	public static void addAccountHistory(List<Account__c> p_newRecords) {
		addAccountHistory(new Map<Id, Account__c>(), p_newRecords);
	}
	
	public static void addAccountHistory(Map<Id, Account__c> p_oldRecordMap, List<Account__c> p_newRecords) {
		List<Account_History__c> createHistories = new List<Account_History__c>();
		
		for (Account__c account : p_newRecords) {
			Account__c oldAccount = p_oldRecordMap.get(account.Id);
			
			PageReference prUser = new PageReference(AUTH_StaticVariables.prefixSite + '/ISA_ViewUser');
			prUser.getParameters().put('sId', account.Owner__c);
			
			if (oldAccount != null) {
				List<Schema.FieldSetMember> fieldSetMembers = Schema.getGlobalDescribe().get('Account__c')
					.getDescribe().FieldSets.getMap().get('TrackFields').getFields();
				
				for (Schema.FieldSetMember field : fieldSetMembers) {
					if (oldAccount.get(field.getFieldPath()) != account.get(field.getFieldPath())) {
						String oldValue = String.valueOf(oldAccount.get(field.getFieldPath()));
						String newValue = String.valueOf(account.get(field.getFieldPath()));
						
						createHistories.add(
							new Account_History__c(
								Account__c = account.Id, 
								OldValue__c = oldValue, 
								NewValue__c = newValue, 
								Message__c = '<b><a href="' + prUser.getUrl() + '">' + account.Owner_Name__c + 
										'</a></b> updated <b>' + field.getLabel() + '</b>' + 
										' from ' + ( ! String.isEmpty(oldValue) ? '<b>' + oldValue + '</b>' : '') + 
										' to ' + ( ! String.isEmpty(newValue) ? '<b>' + newValue + '</b>' : '') + '.'
							));
					}
				}
			} else {
				createHistories.add(
					new Account_History__c(
						Account__c = account.Id, 
						Message__c = '<b><a href="' + prUser.getUrl() + '">' + account.Owner_Name__c + '</a></b> created Account.'
					));
			}
		}
		
		insert createHistories;
	}
	
	/*public static void checkContacts(List<Account__c> p_oldRecords) {
		List<Contact__c> relatedContacts = [
			SELECT Account__c 
			FROM Contact__c 
			WHERE Account__c IN :new Map<Id, Account__c>(p_oldRecords).keySet() 
			LIMIT 10000];
		
		if ( ! relatedContacts.isEmpty()) {
			Map<Id, Boolean> contactMap = new Map<Id, Boolean>();
			for (Contact__c con : relatedContacts) {
				Id key = con.Account__c;
				
				if ( ! contactMap.containsKey(key)) {
					contactMap.put(key, true);
				}
			}
			
			for (Account__c acc : p_oldRecords) {
				
				if (contactMap.get(acc.Id) != null) {
					acc.addError('Account(' + acc.Name + ') has related Contact records and couldn\'t be deleted.');
				}
			}
		}
	}*/
	
	public static void deleteContacts(Map<Id, Account__c> p_oldRecordMap) {
		delete [SELECT Id FROM Contact__c WHERE Account__c IN :p_oldRecordMap.keySet() LIMIT 10000];
	}
	
	public static void deleteAccountHistory(Map<Id, Account__c> p_oldRecordMap) {
		delete [SELECT Id FROM Account_History__c WHERE Account__c IN :p_oldRecordMap.keySet() LIMIT 10000];
	}
}