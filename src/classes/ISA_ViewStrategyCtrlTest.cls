@isTest
private class ISA_ViewStrategyCtrlTest {
	
	static User__c tempUser(id id) {
        User__c testUser = new User__c(
            First_Name__c = 'First', 
            Last_Name__c = 'Last', 
            Phone__c = '123455678', 
            Username__c = 'testLogin', 
            Password__c = 'testPassword', 
            Role__c = 'Admin', 
            Company__c = id,
            Expiration_Date__c = Date.today(), 
            Create_Date__c = Date.today(),
            SessionId__c = '1',
            Active__c = true,
            Email__c = 'test@mail.com');
        return testUser;
    }
    
    static Company__c tempCompany() {
        Company__c company = new Company__c(
            Name = 'test name',
            Admin_First_Name__c = 'firstname',
            Admin_Last_Name__c = 'lastname',
            Admin_Email__c = 'test@mail.com',
            Admin_Login__c = 'testLogin',
            Admin_Password__c = 'testPassword',
            Address1__c = 'addr1',
            Address2__c = 'addr2',
            Trial_Start__c = Date.today(),
            Maximum_Users__c = 10);
        return company;
    }
    
    static Strategy_Design__c tempStrategyDesign(id companyId) {
    	Strategy_Design__c strategyDesign = new Strategy_Design__c(
    		Name = 'Name',
    		Type__c = 'Single',
    		Company__c = companyId);
    	return strategyDesign;
    }
    
    static Strategy_Question__c tempQuestion(id strategyDesignId) {
    	Strategy_Question__c question = new Strategy_Question__c(
    		Name = 'Name',
    		Title__c = 'Title',
    		Description__c = 'Description',
    		Strategy_Design__c = strategyDesignId);
    	return question;
    }
    
    @isTest
    private static void testInit() {
    	
        Test.startTest();
        
	    Company__c testCompany = tempCompany();           
        insert testCompany;      
        User__c testUser = tempUser(testCompany.Id);    
        insert testUser; 
        Strategy_Design__c testStrategyDesign = tempStrategyDesign(testCompany.Id);    
        insert testStrategyDesign;
        Strategy_Question__c testQuestion = tempQuestion(testStrategyDesign.Id);
        insert testQuestion;
                              
        ApexPages.currentPage().getParameters().put('sId', testStrategyDesign.Id);
        
        ISA_ViewStrategyCtrl controller = new ISA_ViewStrategyCtrl();
        controller.initPage();
        
        System.assertNotEquals(null, controller.init());       
        
        Test.stopTest();
    }
    
    @isTest
    private static void testInitWithStrategyId() {
    	
        Test.startTest();
        
	    Company__c testCompany = tempCompany();           
        insert testCompany;      
        User__c testUser = tempUser(testCompany.Id);    
        insert testUser; 
        Strategy_Design__c testStrategyDesign = tempStrategyDesign(testCompany.Id);    
        insert testStrategyDesign;
        Strategy_Question__c testQuestion = tempQuestion(testStrategyDesign.Id);
        insert testQuestion;
                              
        ApexPages.currentPage().getParameters().put('sId', testStrategyDesign.Id);
        ApexPages.currentPage().getParameters().put('title','title test');
        ApexPages.currentPage().getParameters().put('description','description test');
        Cookie testCookie = new Cookie('SessionId','1',null,-1,false);
        ApexPages.currentPage().setCookies(new Cookie[] {testCookie});
        
        ISA_ViewStrategyCtrl controller = new ISA_ViewStrategyCtrl();
        controller.initPage();
        
        System.assertEquals(testStrategyDesign.Id, controller.strategy.Id); 
        
        controller.getRecords();
        
        controller.doEdit();
        System.assertNotEquals(null, controller.doEdit());
        
        controller.doSaveQuestion();      
        
        Test.stopTest();
    }
    
    @isTest
    private static void testInitWithoutStrategyId() {
    	
        Test.startTest();
        
	    Company__c testCompany = tempCompany();           
        insert testCompany;      
        User__c testUser = tempUser(testCompany.Id);    
        insert testUser;
                              
        ApexPages.currentPage().getParameters().put('sId', null);
        Cookie testCookie = new Cookie('SessionId','1',null,-1,false);
        ApexPages.currentPage().setCookies(new Cookie[] {testCookie});
        
        ISA_ViewStrategyCtrl controller = new ISA_ViewStrategyCtrl();
        controller.initPage();
        
        System.assertEquals(null, controller.initPage()); 
        
        controller.doEdit();
        System.assertEquals(null, controller.doEdit());

        Test.stopTest();
    }

	@isTest
    private static void testCollectRetParams() {
        Test.startTest();
        
        PageReference pageRef = ApexPages.currentPage();   
            
        Company__c testCompany = tempCompany();           
        insert testCompany;      
        User__c testUser = tempUser(testCompany.Id);    
        insert testUser; 
                              
        ApexPages.currentPage().getParameters().put('sId', testUser.Id);        
                 
        ISA_ViewStrategyCtrl controller = new ISA_ViewStrategyCtrl();
        
        controller.collectRetParams(pageRef);
        
        System.assertEquals(ApexPages.currentPage().getParameters().get('sId'), pageRef.getParameters().get('sId'));
        
        Test.stopTest();
    }
}