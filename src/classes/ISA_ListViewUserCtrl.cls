public with sharing class ISA_ListViewUserCtrl extends AbstractController {
	public LIst<WrapperCollection.UserBubbleWrapper> chartList {get; set;}
	public Decimal allNoData {get; set;}
	
	public ISA_ListViewUserCtrl() {
		super();
		
		this.sObjectLabel = 'User';
		this.chartList = new List<WrapperCollection.UserBubbleWrapper>();
		
		this.allNoData = 0;
	}
	
	public PageReference initPage() {
		PageReference pr = super.init();
		
		if (pr != null) {
			return pr;
		}
		
		this.sObjectType = sObjectNameMap.get(this.sObjectLabel);
		
		fillRecords();
		// bubble chart
		if (this.userCompany != null) {
			fillBubbleChartData();
			calcAverageValue();
			splitData();
		}
		
		return null;
	}
	
	public void fillBubbleChartData() {
		this.chartList = ChartUtils.getUserBubbleChart('Contacts__c', 'Completeness__c', 
				'Insight_Index__c', 'Contact_No_Data__c', 'No_Strategy__c', 
				' WHERE Company__c = ' + '\'' + this.userCompany + '\' AND Role__c = \'Company User\'', 
				new List<String> {'Last_Activity__c'});
	}
	
	public void calcAverageValue () {
		this.avgX = 0;
		this.avgY = 0;
		Integer i = 0;
		
		for (WrapperCollection.UserBubbleWrapper obj : this.chartList) {
			this.avgX += obj.completeness;
			this.avgY += obj.insightIndex;
			i++;
		}
		
		if (i != 0) {
			this.avgX = this.avgX / i;
			this.avgY = this.avgY / i;
		}
	}
	
	public void splitData() {
		// init color map
		this.seriesColorMap.put('High No Data', '#4DB247');
		this.seriesColorMap.put('Low No Data', '#386BB3');
		this.seriesColorMap.put('High % Strategy', '#4DB200');
		this.seriesColorMap.put('Low % Strategy', '#FFAA47');
		//this.seriesColorMap.put('Users with no assigned contacts', '#D97D5E');
		
		this.seriesMap.put('High No Data', new List<WrapperCollection.UserBubbleWrapper>());
		this.seriesMap.put('Low No Data', new List<WrapperCollection.UserBubbleWrapper>());
		this.seriesMap.put('High % Strategy', new List<WrapperCollection.UserBubbleWrapper>());
		this.seriesMap.put('Low % Strategy', new List<WrapperCollection.UserBubbleWrapper>());
		//this.seriesMap.put('Users with no assigned contacts', new List<WrapperCollection.UserBubbleWrapper>());
		
		Decimal maxNoData = 0;
		Decimal maxNoStrategy = 0;
		for (WrapperCollection.UserBubbleWrapper obj : this.chartList) {
			if (obj.noData != null && maxNoData < obj.noData) {
				maxNoData = obj.noData;
			}
			
			if (obj.noStrategy != null && maxNoStrategy < obj.noStrategy) {
				maxNoStrategy = obj.noStrategy;
			}
		}
		
		
		
		for (WrapperCollection.UserBubbleWrapper obj : this.chartList) {
			String seriaName = '';
			
			if ( obj.noData != null && obj.noData == 0 ){
				this.allNoData++;
			}
			
			if (obj.noData != null && maxNoData != 0 && (obj.noData / maxNoData) > 0.25) {
				seriaName = 'High No Data';
				this.seriesMap.get(seriaName).add(obj);
			} else if (obj.noData != null && maxNoData != 0 && obj.noData != 0 && (obj.noData / maxNoData) <= 0.25) {
				seriaName = 'Low No Data';
				this.seriesMap.get(seriaName).add(obj);
			}
			
						
			if (obj.noStrategy != null && maxNoStrategy != 0 && (obj.noStrategy / maxNoStrategy) > 0.25) {
				seriaName = 'High % Strategy';
			} else if (obj.noStrategy != null && maxNoStrategy != 0 && (obj.noStrategy / maxNoStrategy) <= 0.25) {
				seriaName = 'Low % Strategy';
			} else {
				seriaName = 'Users with no assigned contacts';
			}
			
			this.seriesMap.get(seriaName).add(obj);
		}
	}
	
	
	public override String doGenerateWhereClause() {
		if (this.userRole == 'Admin') {
			return '';
		}
		
		return ' WHERE Company__c = \'' + this.userCompany + '\' AND Role__c = \'Company User\'';
	}
	
	public PageReference goLastActivity() {
		String viewId = ApexPages.currentPage().getParameters().get('viewId');
		
		if ( ! String.isEmpty(viewId)) {
			PageReference pr = new PageReference(AUTH_StaticVariables.prefixSite + '/ISA_LastActivity');
			addRetUrl(pr);
			pr.getParameters().put('sId', viewId);
			pr.setRedirect(true);
			
			return pr;
		}
		
		return null;
	}
	
	public PageReference goViewContacts() {
		PageReference pr = getReference();
		
		if (pr != null) {
			pr.getParameters().put('typeContacts', AUTH_StaticVariables.USER_CONTACTS);
		}
		
		return pr;
	}
	
	public PageReference goViewAccounts() {
		PageReference pr = getReference();
		
		if (pr != null) {
			pr.getParameters().put('typeContacts', AUTH_StaticVariables.USER_ACCOUNTS);
		}
		
		return pr;
	}
	
	public PageReference goViewAccountNoData() {
		PageReference pr = getReference();
		
		if (pr != null) {
			pr.getParameters().put('typeContacts', AUTH_StaticVariables.USER_NO_DATA_ACCOUNTS);
		}
		
		return pr;
	}
	
	public PageReference goViewContactNoData() {
		PageReference pr = getReference();
		
		if (pr != null) {
			pr.getParameters().put('typeContacts', AUTH_StaticVariables.USER_NO_DATA_CONTACTS);
		}
		
		return pr;
	}
	
	public PageReference goViewContactNoStrategy() {
		PageReference pr = getReference();
		
		if (pr != null) {
			pr.getParameters().put('typeContacts', AUTH_StaticVariables.USER_NO_STRATEGY_CONTACTS);
		}
		
		return pr;
	}
	
	private PageReference getReference() {
		String viewId = ApexPages.currentPage().getParameters().get('viewId');
		
		if ( ! String.isEmpty(viewId)) {
			PageReference pr = new PageReference(AUTH_StaticVariables.prefixSite + '/ISA_ContactUsers');
			addRetUrl(pr);
			pr.getParameters().put('uId', viewId);
			pr.setRedirect(true);
			
			return pr;
		}
		
		return null;
	}
}