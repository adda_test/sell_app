public with sharing class ISA_ListViewContactCtrl extends AbstractController {
	public String selectedUser {get; set;}
	public List<SelectOption> assignedUserOptions {get; set;}
	public List<WrapperCollection.ContactBubbleWrapper> chartlist {get; set;}
	public Map<String, List<WrapperCollection.ContactBubbleWrapper>> seriesMap {get;set;}
	
	private String accountId;
	public Integer allNoData {get; set;}
	public Integer year {get; set;}
	public Integer month {get; set;}
	public Integer day {get; set;}
	
	public ISA_ListViewContactCtrl() {
		super();
		
		this.sObjectLabel = 'Contact';
		this.selecteduser = '';
		this.assignedUserOptions = new List<selectOption>();
		this.chartlist = new List<WrapperCollection.ContactBubbleWrapper>();
		this.seriesMap = new Map<String, List<WrapperCollection.ContactBubbleWrapper>>();
		
		this.allNoData = 0;
		this.year = 0;
		this.month = 0;
		this.day = 0;
	}
	
	public PageReference initPage() {
		PageReference pr = super.init();
		
		if (pr != null) {
			return pr;
		}
		
		this.accountId = ApexPages.currentPage().getParameters().get('aId');
		
		this.sObjectType = sObjectNameMap.get(this.sObjectLabel);
		
		fillRecords();
		
		this.assignedUserOptions = selectUsers();
		this.selectedUser = ( ! this.assignedUserOptions.isEmpty()) 
			? this.assignedUserOptions.get(0).getValue() 
			: '';
		
		// bubble chart
		fillBubbleChartData();
		calcAverageValue();
		splitData();
		
		return null;
	}
	
	public void fillBubbleChartData() {
		String userId = (this.userRole == 'Company User') ? this.user.Id : null;
		
		String whereClause = ' WHERE Company__c = ' + '\'' + this.userCompany + '\'';
		
		if (this.accountId != null){
			whereClause += ' AND Account__r.Id =\'' + accountId + '\'';
		}
					
		this.chartList = ChartUtils.getContactBubbleChart(whereClause, null, userId);
	}
	
	public void calcAverageValue () {
		//this.avgX = 0;
		this.avgY = 0;
		Integer i = 0;
		
		for (WrapperCollection.ContactBubbleWrapper obj : this.chartList) {
			//this.avgX += obj.completeness;
			this.avgY += obj.insightIndex;
			i++;
		}
		
		if (i != 0) {
			//this.avgX = (this.avgX / i).setScale(1);
			this.avgY = (this.avgY / i).setScale(1);
		}
	}
	
	public void splitData() {
		if ( ! this.chartList.isEmpty()) {
			// init color map
			this.seriesColorMap.put('Low Completeness', '#0000CC');
			this.seriesColorMap.put('High Completeness', '#4C9900');
			this.seriesColorMap.put('No Strategy', '#386BB3');
			this.seriesColorMap.put('Strategy', '#4DB247');
			
			this.seriesMap.put('Low Completeness', new List<WrapperCollection.ContactBubbleWrapper>());
			this.seriesMap.put('High Completeness', new List<WrapperCollection.ContactBubbleWrapper>());
			this.seriesMap.put('No Strategy', new List<WrapperCollection.ContactBubbleWrapper>());
			this.seriesMap.put('Strategy', new List<WrapperCollection.ContactBubbleWrapper>());
			
			
			Decimal averageCompleteness = 0.0;
			for (WrapperCollection.ContactBubbleWrapper obj: this.chartList) {
				if (obj.completeness != null) {
					averageCompleteness += (Decimal)obj.completeness;
				}
			}
			
			averageCompleteness = (averageCompleteness / this.chartList.size()).setScale(1);
			
			
			Date minDate = Date.today();
			
			for (WrapperCollection.ContactBubbleWrapper obj: this.chartList) {
				String seriaName = '';
				
				if (obj.isNoData == false){
					this.allNoData++;
				}
				
				Date nD = Date.newInstance(obj.year, obj.month, obj.day);
				System.debug('DDDD1 -> ' + nD);
				if(minDate > nD)
					minDate = nD;
				
				if ( ! String.isEmpty(obj.strategy)) {
					seriaName = 'Strategy';
				} else if (String.isEmpty(obj.strategy)) {
					seriaName = 'No Strategy';
				}
				
				this.seriesMap.get(seriaName).add(obj);
				
				if (obj.completeness > averageCompleteness) {
					seriaName = 'High Completeness';
				} else if (obj.completeness <= averageCompleteness) {
					seriaName = 'Low Completeness';
				}
				
				this.seriesMap.get(seriaName).add(obj);
			}
			
			System.debug('DDDD -> ' + minDate);
			
			this.year = minDate.year();
			this.month = minDate.month();
			this.day = minDate.day();
			
			System.debug('DDDD2 -> ' + this.year + '/' +  this.month + '/' + this.day);
		}
	}
	
	public override String doGenerateWhereClause() {
		String whereClause = '';
		if (this.userRole == 'Company Admin' || this.userRole == 'Company User') {
			if (this.sObjectLabel == 'User' || this.sObjectLabel == 'Account' || 
				this.sObjectLabel == 'Contact' || this.sObjectLabel == 'Insight' || 
				this.sObjectLabel == 'Strategy') {
				
				if (this.userRole == 'Company Admin') {
					whereClause = ' WHERE Company__c = \'' + this.userCompany + '\'';
					
					if (this.accountId != null){
						whereClause += ' AND Account__r.Id =\'' + accountId + '\'';
					}
					
				} else if (this.userRole == 'Company User') {
					List<AssignContact__c> junctions = [
						SELECT Contact__c 
						FROM AssignContact__c 
						WHERE User__c = :this.user.Id 
						  AND Contact__c != null 
						LIMIT 10000];
					
					Set<String> contactIds = new Set<String>();
					for (AssignContact__c junction : junctions) {
						contactIds.add('\'' + junction.Contact__c + '\'');
					}
					
					if ( ! contactIds.isEmpty()) {
						whereClause = ' WHERE Company__c = \'' + this.userCompany + '\' AND Id IN (' + 
							String.join(new List<String>(contactIds), ',') + ')';
					} else {
						whereClause = ' WHERE Id = null';
					}
				}
			}
		}
		return whereClause;
	}
	
	//Fourth method
	public override List<sObject> getSelectedRecords(List<String> p_fields, String p_whereClause, String p_orderClause) {
		List<sObject> selectedRecords = new List<sObject>();
		try {
			selectedRecords = Database.query(
				'SELECT Name, ' + String.join(p_fields, ', ') + 
					',Account__c,AccountName__c,Next_Strategy_Name__c' + 
				' FROM ' + this.sObjectType + 
				p_whereClause + p_orderClause + 
				' LIMIT 50000');
		} catch(Exception ex) {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
		}
		
		return selectedRecords;
	}
	
	/**
	* @description 
	*/
	public override void doSort() {
		sortField = ApexPages.currentPage().getParameters().get('sortField');
		sortOrder = ApexPages.currentPage().getParameters().get('sortOrder');
		
		if ((sortableMap.containsKey(sortField) && sortableMap.get(sortField)) || sortField == 'AccountName__c') {
			fillRecords();
		}
	}
	
	public PageReference doDisplayAccount() {
		PageReference pr;
		
		String accountId = Apexpages.currentPage().getParameters().get('accountId');
		
		if ( ! String.isEmpty(accountId)) {
			pr = new PageReference(AUTH_StaticVariables.prefixSite + '/ISA_ViewAccount');
			pr.getParameters().put('sId', accountId);
			addRetUrl(pr);
			pr.setRedirect(true);
		}
		
		return pr;
	}
	
	public List<SelectOption> selectUsers() {
		List<SelectOption> results = new List<SelectOption>();
		
		List<User__c> users = [
			SELECT Name 
			FROM User__c 
			WHERE Company__c = :this.userCompany 
			  AND Role__c = 'Company User' 
			LIMIT 1000];
		
		for (User__c user : users) {
			results.add(new SelectOption(user.Id, user.Name));
		}
		
		return results;
	}
	
	public void doAssign() {
		List<AssignContact__c> assignedContacts = new List<AssignContact__c>();
		
		String contactIds = ApexPages.currentPage().getParameters().get('contactIds');
		String userId = ApexPages.currentPage().getParameters().get('userId');
		
		if ( ! String.isEmpty(contactIds) && ! String.isEmpty(userId)) {
			List<AssignContact__c> existJunctions = [
				SELECT User__c, Contact__c 
				FROM AssignContact__c 
				WHERE User__c = :userId 
				  AND Contact__c IN :contactIds.split(',') 
				LIMIT 10000];
			
			Set<Id> existContactIds = new Set<Id>();
			for (AssignContact__c junction : existJunctions) {
				existContactIds.add(junction.Contact__c);
			}
			
			Map<Id, Contact__c> existContactMap = new Map<Id, Contact__c>([
				SELECT Name 
				FROM Contact__c 
				WHERE ID IN :contactIds.split(',') 
				  AND (NOT(ID IN :existContactIds))]);
			
			List<User__c> users = [
				SELECT Name 
				FROM User__c 
				WHERE Id = :userId 
				LIMIT 1];
			
			
			for (String contactId : contactIds.split(',')) {
				if ( ! existContactIds.contains(contactId)) {
					assignedContacts.add(
						new AssignContact__c(
							Contact__c = contactId, 
							User__c = userId, 
							Owner_Name__c = this.username, 
							Contact_Name__c = existContactMap.get(contactId).Name, 
							User_Name__c = users.get(0).Name, 
							Owner__c = this.user.Id
						));
					
					
				}
			}
			
			insert assignedContacts;
			
			
			
			for (ISA_SObjectHelper.SObjectWrapper wrapper : this.records) {
				wrapper.checked = false;
			}
		}
	}
	
	public PageReference goNextStrategy() {
		String viewId = ApexPages.currentPage().getParameters().get('viewId');
		
		if ( ! String.isEmpty(viewId)) {
			PageReference pr = new PageReference(AUTH_StaticVariables.prefixSite + '/ISA_ViewContact');
			pr.getParameters().put('sId', viewId);
			addRetUrl(pr);
			pr.setRedirect(true);
			
			return pr;
		}
		
		return null;
	}
	
	public pageReference goInfluencePage() {
		String viewId = ApexPages.currentPage().getParameters().get('viewId');
		
		if ( ! String.isEmpty(viewId)) {
			PageReference pr = new PageReference(AUTH_StaticVariables.prefixSite + '/ISA_InfluenceAccount');
			pr.getParameters().put('aId', viewId);
			addRetUrl(pr);
			pr.setRedirect(true);
			
			return pr;
		}
		
		return null;
	}
	
	public PageReference goLastActivity() {
		String viewId = ApexPages.currentPage().getParameters().get('viewId');
		
		if ( ! String.isEmpty(viewId)) {
			PageReference pr = new PageReference(AUTH_StaticVariables.prefixSite + '/ISA_LastActivity');
			addRetUrl(pr);
			pr.getParameters().put('sId', viewId);
			pr.setRedirect(true);
			
			return pr;
		}
		
		return null;
	}
}