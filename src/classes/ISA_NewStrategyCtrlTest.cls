@isTest
private class ISA_NewStrategyCtrlTest {
	static User__c tempUser(id id) {
        User__c testUser = new User__c(
            First_Name__c = 'First', 
            Last_Name__c = 'Last', 
            Phone__c = '123455678', 
            Username__c = 'testLogin', 
            Password__c = 'testPassword', 
            Role__c = 'Admin', 
            Company__c = id,
            Expiration_Date__c = Date.today(), 
            Create_Date__c = Date.today(),
            SessionId__c = '1',
            Active__c = true,
            Email__c = 'test@mail.com');
        return testUser;
    }
    
	static Company__c tempCompany() {
        Company__c company = new Company__c(
            Name = 'test name',
            Admin_First_Name__c = 'firstname',
            Admin_Last_Name__c = 'lastname',
            Admin_Email__c = 'test@mail.com',
            Admin_Login__c = 'testLogin',
            Admin_Password__c = 'testPassword',
            Address1__c = 'addr1',
            Address2__c = 'addr2',
            Trial_Start__c = Date.today(),
            Maximum_Users__c = 10);
        return company;
    }
    
    static Strategy_Design__c tempStrategyDesign(id id) {
    	Strategy_Design__c strategyDesign = new Strategy_Design__c(
    		Name = 'Name',
    		Type__c = 'Single',
    		Company__c = id);
    	return strategyDesign;
    }
    
    static Strategy_Question__c tempQuestion(id id) {
    	Strategy_Question__c question = new Strategy_Question__c(
    		Name = 'Name',
    		Title__c = 'Title',
    		Strategy_Design__c = id);
    	return question;
    }
    
    @isTest
    private static void testInit() {
        Test.startTest();
        
	    Company__c testCompany = tempCompany();           
        insert testCompany;      
        User__c testUser = tempUser(testCompany.Id);    
        insert testUser; 
        Strategy_Design__c testStrategyDesign = tempStrategyDesign(testCompany.Id);    
        insert testStrategyDesign;
        Strategy_Question__c testQuestion = tempQuestion(testStrategyDesign.Id);
        insert testQuestion;
                              
        ApexPages.currentPage().getParameters().put('sId', testStrategyDesign.Id);
        
        ISA_NewStrategyCtrl controller = new ISA_NewStrategyCtrl();
        controller.initPage();
        
        System.assertNotEquals(null, controller.init());       
        
        Test.stopTest();
    }
    
    @isTest
    private static void testInitPageWithStrategyDesignId() {
        Test.startTest();
        
	    Company__c testCompany = tempCompany();           
        insert testCompany;      
        User__c testUser = tempUser(testCompany.Id);    
        insert testUser; 
        Strategy_Design__c testStrategyDesign = tempStrategyDesign(testCompany.Id);    
        insert testStrategyDesign;
        Strategy_Question__c testQuestion = tempQuestion(testStrategyDesign.Id);
        insert testQuestion;
                              
        ApexPages.currentPage().getParameters().put('sId', testStrategyDesign.Id);
        Cookie testCookie = new Cookie('SessionId','1',null,-1,false);
        ApexPages.currentPage().setCookies(new Cookie[] {testCookie});
        
        ISA_NewStrategyCtrl controller = new ISA_NewStrategyCtrl();
        controller.initPage();
        
        System.assertEquals(testStrategyDesign.Id, controller.strategy.Id);  
       	System.assertNotEquals(null, controller.viewQuestions.size());  
       	
       	for (Integer i = 0; i < controller.viewQuestions.size(); i++) {
			//ISA_SObjectHelper.SObjectWrapper wrapper = controller.viewQuestions.get(i);
			//wrapper.checked = true;
			//controller.viewQuestions.get(i) = wrapper;
			controller.viewQuestions.get(i).checked = true;
       	}
       	       	        
        controller.removeQuestions();
        System.assertNotEquals(null, controller.selectQuestions.size()); 
        
        controller.doSave();
        System.assertNotEquals(null, controller.doSave());        
        
        Test.stopTest();
    }
    
    @isTest
    private static void testInitPageWithoutStrategyDesignId() {
        Test.startTest();
        
	    Company__c testCompany = tempCompany();           
        insert testCompany;      
        User__c testUser = tempUser(testCompany.Id);    
        insert testUser; 
        Strategy_Design__c testStrategyDesign = tempStrategyDesign(testCompany.Id);    
        insert testStrategyDesign;
        Strategy_Question__c testQuestion = tempQuestion(null);
        insert testQuestion;
                              
        ApexPages.currentPage().getParameters().put('sId', testStrategyDesign.Id);
        Cookie testCookie = new Cookie('SessionId','1',null,-1,false);
        ApexPages.currentPage().setCookies(new Cookie[] {testCookie});
        
        ISA_NewStrategyCtrl controller = new ISA_NewStrategyCtrl();
        controller.initPage();
          
       	System.assertNotEquals(null, controller.selectQuestions.size());  
       	
    	for (Integer i = 0; i < controller.selectQuestions.size(); i++) {
			controller.selectQuestions.get(i).checked = true;		
       	}
       	       	        
        controller.addQuestions();
      	System.assertNotEquals(null, controller.viewQuestions.size()); 
        
        controller.doSave();
        System.assertNotEquals(null, controller.doSave());        
        
        Test.stopTest();
    }
    
    @isTest
    private static void testCollectRetParams() {
        Test.startTest();
        
        PageReference pageRef = ApexPages.currentPage();   
            
        Company__c testCompany = tempCompany();           
        insert testCompany;      
        User__c testUser = tempUser(testCompany.Id);    
        insert testUser; 
        Strategy_Design__c testStrategyDesign = tempStrategyDesign(testCompany.Id);    
        insert testStrategyDesign;
        Strategy_Question__c testQuestion = tempQuestion(testStrategyDesign.Id);
        insert testQuestion;
                              
        ApexPages.currentPage().getParameters().put('sId', testStrategyDesign.Id);        
                 
        ISA_NewStrategyCtrl controller = new ISA_NewStrategyCtrl();
        
        controller.collectRetParams(pageRef);
        
        System.assertEquals(ApexPages.currentPage().getParameters().get('sId'), pageRef.getParameters().get('sId'));
        
        Test.stopTest();
    }

}