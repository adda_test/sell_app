public with sharing class ISA_NewStrategyCtrl extends AbstractController {
	public Strategy_Design__c strategy {get; set;}
	
	public List<ISA_SObjectHelper.sObjectWrapper> viewQuestions {get; set;}
	public List<ISA_SObjectHelper.sObjectWrapper> selectQuestions {get; set;}
	
	public ISA_NewStrategyCtrl() {
		super();
		
		this.viewQuestions = new List<ISA_SObjectHelper.sObjectWrapper>();
		this.selectQuestions = new List<ISA_SObjectHelper.sObjectWrapper>();
		
		this.sObjectLabel = 'Strategy';
	}
	
	public PageReference initPage() {
		PageReference pr = super.init();
		if (pr != null) {
			return pr;
		}
		
		this.strategy = new Strategy_Design__c(Company__c = this.userCompany, Order__c = 0);
		String strategyId = ApexPages.currentPage().getParameters().get('sId');
		
		if ( ! String.isEmpty(strategyId)) {
			List<Strategy_Design__c> strategies = [
					SELECT Name, Company__r.Name, Type__c, Order__c 
					FROM Strategy_Design__c 
					WHERE Id = :strategyId 
					LIMIT 1];
			
			if ( ! strategies.isEmpty()) {
				this.strategy = strategies.get(0);
			}
		}
		
		List<Strategy_Question__c> questions = [
				SELECT Title__c, Description__c, Strategy_Design__c 
				FROM Strategy_Question__c 
				WHERE Strategy_Design__c = null OR Strategy_Design__c = :this.strategy.Id 
				LIMIT 10000];
		
		if ( ! questions.isEmpty()) {
			for (Strategy_Question__c question : questions) {
				if (question.Strategy_Design__c != null) {
					this.viewQuestions.add(new ISA_SObjectHelper.sObjectWrapper(question));
				} else {
					this.selectQuestions.add(new ISA_SObjectHelper.sObjectWrapper(question));
				}
			}
		}
		
		return null;
	}
	
	public void addQuestions() {
		for (Integer i = 0; i < this.selectQuestions.size(); i++) {
			ISA_SObjectHelper.SObjectWrapper wrapper = this.selectQuestions.get(i);
			if (wrapper.checked) {
				this.viewQuestions.add(wrapper);
				
				this.selectQuestions.remove(i--);
			}
		}
	}
	
	public void removeQuestions() {
		for (Integer i = 0; i < this.viewQuestions.size(); i++) {
			ISA_SObjectHelper.SObjectWrapper wrapper = this.viewQuestions.get(i);
			
			if (wrapper.checked) {
				this.selectQuestions.add(wrapper);
				
				this.viewQuestions.remove(i--);
			}
		}
	}
	
	public PageReference doSave() {
		if (this.strategy.Type__c == 'Single' && this.strategy.Id != null && this.viewQuestions.size() > 1) {
			ApexPages.addMessage(
				new ApexPages.Message(
					ApexPages.Severity.ERROR, 'Single strategy shouldn\'t have more one item. Please check this strategy.'));
			return null;
		}
		
		try {
			this.strategy.Owner__c = this.user.Id;
			
			upsert this.strategy;
			
			//Assign a new Strategy to all company users by Strategy Design
			
			List<Strategy_Question__c> updatedQuestions = new List<Strategy_Question__c>();
			for (ISA_SObjectHelper.SObjectWrapper wrapper : this.viewQuestions) {
				wrapper.record.put('Strategy_Design__c', this.strategy.Id);
				updatedQuestions.add((Strategy_Question__c)wrapper.record);
			}
			
			for (ISA_SObjectHelper.SObjectWrapper wrapper : this.selectQuestions) {
				wrapper.record.put('Strategy_Design__c', null);
				updatedQuestions.add((Strategy_Question__c)wrapper.record);
			}
			
			update updatedQuestions;
			
			PageReference pr = new PageReference(AUTH_StaticVariables.prefixSite + '/ISA_ViewStrategy');
			pr.getParameters().put('sId', this.strategy.Id);
			pr.getParameters().put('returnUrl', AUTH_StaticVariables.prefixSite + '/ISA_ListViewStrategy');
			pr.setRedirect(true);
			
			return pr;
		} catch(Exception ex) {
			ApexPages.addMessage(
				new ApexPages.Message(
					ApexPages.Severity.ERROR, ex.getMessage()));
			
			return null;
		}
	}
	
	@TestVisible protected override void collectRetParams(PageReference pr) {
		pr.getParameters().put('sId',ApexPages.currentPage().getParameters().get('sId'));
	}
	
}