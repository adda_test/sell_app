public with sharing class ISA_AccountInsightsCtrl extends AbstractController {
	public List<Question__c> questions {get; set;}
	
	private String accountId;
	
	public ISA_AccountInsightsCtrl() {
		super();
		
		this.questions = new List<Question__c>();
	}
	
	public PageReference initPage() {
		PageReference afterInit = init();
		if (afterInit != null) {
			return afterInit;
		}
		
		String companyId = ApexPages.currentPage().getParameters().get('cId');
		this.accountId = ApexPages.currentPage().getParameters().get('aId');
		
		if ( ! String.isEmpty(companyId)) {
			this.questions = [
				SELECT Name 
				FROM Question__c 
				WHERE Survey_Design__r.Company__c = :companyId 
				  AND Survey_Design__r.Name != :AUTH_StaticVariables.DEFAULT_INSIGHT_PANEL_NAME 
				LIMIT 1000];
		}
		
		return null;
	}
	
	public PageReference doBack() {
		PageReference pr = new PageReference(AUTH_StaticVariables.prefixSite + '/apex/ISA_ViewAccount');
		pr.getParameters().put('sId', accountId);
		pr.getParameters().put('returnUrl', AUTH_StaticVariables.prefixSite + '/apex/ISA_ListViewAccount');
		pr.setRedirect(true);
		
		return pr;
	}
}