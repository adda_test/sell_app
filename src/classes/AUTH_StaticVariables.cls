public with sharing class AUTH_StaticVariables {
	public static final Blob sessionPKey = EncodingUtil.base64Decode('7iJhUVvG2EJBqiORX7DO');
	public static final Blob passRecoverPKey = EncodingUtil.base64Decode('I4F3zFUlfH70e8rswt50');
	public static final String rsaAlgorithm = 'hmacSHA256';
	
	public static final String passResetInfo = 'Enter your username to reset the password.';
	public static final String passResetError = 'There is no user with such username in the system.';
	public static final String passResetConfirm = 'Password reset instructions were sent to your email address.';
	
	public static final String passRecoverySubject = 'WaveOC Portal - Reset Forgotten Password';
	public static final String passRecoveryMessageHead = '<p>Dear User,</p>To reset the password please click the following link - ';
	public static final String passRecoveryMessageFooter = '<p>If you have any questions, please contact your administrator.</p><p>Thank you,</br>waveoc.com</p>';
	
	//Live time for Cookies
	public static final Integer cookieTime = 7200;
	
	//ToDo: replace /apex
	public static final String forgotPassURL = '/apex/forgotpassword';
	public static final String successLoginURL = '/apex/ISA_ListView';
	public static final String resetPasswordURL = 'https://ap1.salesforce.com/apex/resetPassword?t=';
	public static final String loggedOutURL = '/apex/signin';
	
	//Apex prefix for debuging on site side.
	public static final String prefixSite = '/apex';
	
	//Default Insight panel name
	public static final String DEFAULT_INSIGHT_PANEL_NAME = 'Unassigned Insights';
	
	//Chart Types
	public static final String BUBBLE = 'Bubble';
	
	//Type of users
	public static final String USER_CONTACTS = 'user_contacts';
	public static final String USER_ACCOUNTS = 'user_accounts';
	public static final String USER_NO_DATA_CONTACTS = 'user_no_data_contacts';
	public static final String USER_NO_DATA_ACCOUNTS = 'user_no_data_accounts';
	public static final String USER_NO_STRATEGY_CONTACTS = 'user_no_strategy_contacts';
}