public with sharing class AssignAccountHandler {
	public static Boolean enablesTrigger = true;
	
	public static void updateUsers(List<AssignAccount__c> p_newRecords) {
		Set<Id> userIds = new Set<Id>();
		for (AssignAccount__c junction : p_newRecords) {
			if (junction.User__c != null) {
				userIds.add(junction.User__c);
			}
		}
		
		if ( ! userIds.isEmpty()) {
			ISA_TriggerHelper.recalculateUsers(userIds);
		}
	}
}