public with sharing class ISA_ManageStrategiesCtrl extends AbstractController {
	public List<AccountWrapper> records {get; set;}
	
	private List<AssignStrategy__c> junctions;
	
	public ISA_ManageStrategiesCtrl() {
		super();
		
		this.records = new List<AccountWrapper>();
	}
	
	public PageReference initPage() {
		PageReference pr = super.init();
		if (pr != null) {
			return pr;
		}
		
		fillRecords();
		
		return null;
	}
	
	public override void fillRecords() {
		this.junctions = [
			SELECT Account__r.Name, Strategy__r.Name 
			FROM AssignStrategy__c 
			WHERE Account__r.Company__c = :this.userCompany 
			LIMIT 10000];
		
		Map<Account__c, List<Strategy_Design__c>> accountToStrategyMap = new Map<Account__c, List<Strategy_Design__c>>();
		for (AssignStrategy__c junction : junctions) {
			Account__c key = junction.Account__r;
			
			if ( ! accountToStrategyMap.containsKey(key)) {
				accountToStrategyMap.put(key, new List<Strategy_Design__c>());
			}
			accountToStrategyMap.get(key).add(junction.Strategy__r);
		}
		
		for (Account__c account : accountToStrategyMap.keySet()) {
			this.records.add(new AccountWrapper(account, accountToStrategyMap.get(account)));
		}
		
		if (this.junctions.isEmpty()) {
			ApexPages.addMessage(
				new ApexPages.Message(
					ApexPages.Severity.INFO, 'There is no any Strategy that is assigned to any Account.'));
		}
	}
	
	public override void doRemove() {
		String strategyId = ApexPages.currentPage().getParameters().get('strategyId');
		String accountId = ApexPages.currentPage().getParameters().get('accountId');
		
		if ( ! String.isEmpty(strategyId) && ! String.isEmpty(accountId)) {
			for (AccountWrapper accountW : this.records) {
				if (accountW.recordId == accountId) {
					accountW.strategyMap.remove(strategyId);
					
					for (Integer i = 0; i < this.junctions.size(); i++) {
						if (this.junctions.get(i).Account__c == accountId && 
							this.junctions.get(i).Strategy__c == strategyId) {
							
							delete this.junctions.get(i);
						}
					}
				}
			}
		}
	}
	
	public class AccountWrapper {
		public String recordId {get; set;}
		public String name {get; set;}
		public Map<String, String> strategyMap {get; set;}
		
		public AccountWrapper(Account__c p_account, List<Strategy_Design__c> p_strategies) {
			this.recordId = p_account.Id;
			this.name = p_account.Name;
			
			this.strategyMap = new Map<String, String>();
			for (Strategy_Design__c strategy : p_strategies) {
				this.strategyMap.put(strategy.Id, strategy.Name);
			}
		}
	}
}