public with sharing class ISA_ViewInsightCtrl extends AbstractController {
	public Question__c question {get; set;}
	public List<AnswerWrapper> answers {get; set;}
	
	public ISA_ViewInsightCtrl() {
		super();
		
		this.question = new Question__c();
		this.sObjectLabel = 'Insight';
		this.answers = new List<AnswerWrapper>();
	}
	
	public PageReference initPage() {
		PageReference afterInit = init();
		if (afterInit != null) {
			return afterInit;
		}
		
		String questionId = ApexPages.currentPage().getParameters().get('sId');
		
		if ( ! String.isEmpty(questionId)) {
			List<Question__c> questions = [
					SELECT Name, Survey_Design__r.Company__r.Name, Type__c, Response_Options__c, Point_Options__c 
					FROM Question__c 
					WHERE Id = :questionId 
					LIMIT 1];
			
			if ( ! questions.isEmpty()) {
				this.question = questions.get(0);
				
				if (this.question.Response_Options__c != null) {
					List<String> values = this.question.Response_Options__c.split(',');
					List<String> points = this.question.Point_Options__c.split(',');
					
					for (Integer i = 0; i < values.size(); i++) {
						this.answers.add(new AnswerWrapper(values.get(i), points.get(i)));
					}
				}
			}
		} else {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 
					'Selected Insight wasn\'t found.'));
		}
		
		return null;
	}
	
	public PageReference doEdit() {
		if (this.question.Id != null) {
			
			PageReference pr = new PageReference(AUTH_StaticVariables.prefixSite + '/ISA_NewInsight');
			pr.getParameters().put('sId', this.question.Id);
			addRetUrl(pr);
			pr.setRedirect(true);
			
			return pr;
		}
		
		return null;
	}
	
	public class AnswerWrapper {
		public String value {get; set;}
		public String point {get; set;}
		
		public AnswerWrapper(String p_value, String p_point) {
			this.value = p_value;
			this.point = p_point;
		}
	}
}