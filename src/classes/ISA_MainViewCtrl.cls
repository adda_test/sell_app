/**
* @description 
*/
public with sharing class ISA_MainViewCtrl extends AbstractController {
    //PROPERTY
    public Map<String, String> sObjectNameMap {get; set;}
    
    public List<ISA_SObjectHelper.sObjectWrapper> records {get; set;}
    public Map<String, String> fieldSetMap {get; set;}
    public Map<String, Boolean> sortableMap {get; set;}
    
    public String sObjectType {get; set;}
    public String sObjectLabel {get; set;}
    
    public Integer pageIndex {get; set;}
    public Integer countOfPage {get; set;}
    public Integer countRecords {get; set;}
    public List<SelectOption> countRecordOptions {get; set;}
    public String selectedCountRecords {get; set;}
    
    public String sortField {get; set;}
    public String sortOrder {get; set;}
    
    //PRIVATE
    private Map<String, SObjectType> globalDescribe;
    private List<Schema.FieldSetMember> fieldSetMembers;
    private Map<String, Schema.SObjectField> sObjectFieldMap;
    
    /**
    * @description 
    */
    public ISA_MainViewCtrl() {
        super();
        
        init();
        
        sObjectNameMap = getObjectsByUserRole();
        
        String sObjectLabel = ApexPages.currentPage().getParameters().get('sObjectLabel');
        if (String.isEmpty(sObjectLabel)) {
            this.sObjectLabel = 'Account';
        } else {
            this.sObjectLabel = sObjectLabel;
        }
        
        this.sObjectType = sObjectNameMap.get(this.sObjectLabel);
        
        sortField = 'Name';
        sortOrder = 'ASC';
        
        pageIndex = 1;
        countOfPage = 1;
        countRecords = 10;
        
        countRecordOptions = new List<SelectOption> {
            new SelectOption('10','10'), 
            new SelectOption('20','20'), 
            new SelectOption('50','50'), 
            new SelectOption('100','100'), 
            new SelectOption('200','200')
        };
        selectedCountRecords = '10';
        
        records = new List<ISA_SObjectHelper.sObjectWrapper>();
        fieldSetMap = new Map<String, String>();
        sortableMap = new Map<String, Boolean> {'Name' => true};
        
        globalDescribe = Schema.getGlobalDescribe();
        fieldSetMembers = globalDescribe.get(this.sObjectType).getDescribe().FieldSets.getMap()
                .get('MainView').getFields();
        
        sObjectFieldMap = globalDescribe.get(this.sObjectType).getDescribe().fields.getMap();
        
        //Collect fields that can be viewed.
        for (Schema.FieldSetMember v_field : fieldSetMembers) {
            fieldSetMap.put(v_field.getLabel(), v_field.getFieldPath());
            
            sortableMap.put(v_field.getFieldPath(), sObjectFieldMap.get(v_field.getFieldPath()).getDescribe().isSortable());
        }
        
        fillRecords();
    }
    
    /**
    * @description 
    */
    public override void fillRecords() {
        records.clear();
        fieldSetMap.clear();
        sortableMap.clear();
        
        if ( ! String.isEmpty(this.sObjectType) && globalDescribe.containsKey(this.sObjectType)) {
            fieldSetMembers = globalDescribe.get(this.sObjectType).getDescribe().FieldSets.getMap()
                    .get('MainView').getFields();
            
            sObjectFieldMap = globalDescribe.get(this.sObjectType).getDescribe().fields.getMap();
            
            //Collect fields that can be viewed.
            sortableMap = new Map<String, Boolean> {'Name' => true};
            for (Schema.FieldSetMember v_field : fieldSetMembers) {
                fieldSetMap.put(v_field.getLabel(), v_field.getFieldPath());
                
                sortableMap.put(v_field.getFieldPath(), sObjectFieldMap.get(v_field.getFieldPath()).getDescribe().isSortable());
            }
            
            String v_orderClause = ( ! String.isEmpty(sortField) ? 
                    ' ORDER BY ' + sortField + ' ' + sortOrder + ' NULLS LAST ' : 
                    ' ORDER BY Name ASC NULLS LAST ');
            
            String whereClause = '';
            if (this.userRole == 'Company Admin') {
                if (this.sObjectLabel == 'User' || this.sObjectLabel == 'Account' || 
                    this.sObjectLabel == 'Contact') {
                    
                    whereClause = ' WHERE Company__c = \'' + this.userCompany + '\' ';
                }
            }
            
            List<sObject> v_selectedRecords = new List<sObject>();
            try {
                v_selectedRecords = Database.query('SELECT Name, ' + String.join(fieldSetMap.values(), ', ') + 
                        ' FROM ' + this.sObjectType + whereClause + v_orderClause + ' LIMIT 50000');
            } catch(Exception ex) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
            }
            
            pageIndex = (pageIndex <= 0) ? 1 : pageIndex;
            
            if ( ! String.isEmpty(selectedCountRecords) && selectedCountRecords.isNumeric()) {
                countRecords = Integer.valueOf(selectedCountRecords);
            } else {
                countRecords = 10;
            }
            
            countOfPage = getCountOfPage(v_selectedRecords.size(), countRecords);
            
            for (Integer i = countRecords * (pageIndex - 1); i < v_selectedRecords.size() && i < countRecords * pageIndex; i++) {
                records.add(new ISA_SObjectHelper.sObjectWrapper(v_selectedRecords.get(i)));
            }
        }
    }
    
    public PageReference doSelect() {
        String name = ApexPages.currentPage().getParameters().get('nameObject');
        String label = ApexPages.currentPage().getParameters().get('labelObject');
        
        if ( ! String.isEmpty(name) && ! String.isEmpty(label)) {
            if (name == 'Survey_Design__c') {
                return new PageReference('/apex/ISA_InsightListView');
            }
            
            this.pageIndex = 1;
            this.countRecords = 10;
            this.countOfPage = 1;
            
            this.sObjectType = name;
            this.sObjectLabel = label;
            
            fillRecords();
        }
        
        return null;
    }
    
    public override PageReference doViewRecord() {
        String recordId = ApexPages.currentPage().getParameters().get('viewId');
        PageReference pr;
        
        if ( ! String.isEmpty(recordId)) {
            if (this.userRole == 'Company User' && sObjectLabel == 'Contact') {
                pr = new PageReference('/apex/ISA_ViewContactUser');
                pr.getParameters().put('cId', recordId);
            } else {
                pr = new PageReference('/apex/ISA_View' + sObjectLabel);
                pr.getParameters().put('sId', recordId);
            }
        }
        
        return pr;
    }
    
    /**
    * @description 
    */
    public override void doSort() {
        sortField = ApexPages.currentPage().getParameters().get('sortField');
        sortOrder = ApexPages.currentPage().getParameters().get('sortOrder');
        
        if (sortableMap.get(sortField)) {
            fillRecords();
        }
    }
    
    /**
    * @description 
    */
    public override void doFirstPage() {
        pageIndex = 1;
        
        fillRecords();
    }
    
    /**
    * @description 
    */
    public override void doPreviousPage() {
        pageIndex = (pageIndex > 1) ? --pageIndex : 1;
        
        fillRecords();
    }
    
    /**
    * @description 
    */
    public override void doNextPage() {
        pageIndex = (pageIndex < countOfPage) ? ++pageIndex : countOfPage;
        
        fillRecords();
    }
    
    /**
    * @description 
    */
    public override void doLastPage() {
        pageIndex = countOfPage;
        
        fillRecords();
    }
    
    /**
    * @description 
    */
    public override void doChange() {
        if ( ! String.isEmpty(selectedCountRecords) && selectedCountRecords.isNumeric()) {
            countRecords = Integer.valueOf(selectedCountRecords);
        }
        
        fillRecords();
    }
    
    /**
    * @description 
    */
    public override PageReference doEditRecord() {
        String v_recordId = ApexPages.currentPage().getParameters().get('editRecordId');
        
        if ( ! String.isEmpty(v_recordId)) {
            return new PageReference(AUTH_StaticVariables.prefixSite + '/ISA_New' + this.sObjectLabel + '?sId=' + v_recordId);
        }
        
        return null;
    }
    
    /**
    * @description 
    */
    public override void doRemoveRecord() {
        String v_recordId = ApexPages.currentPage().getParameters().get('removeRecordId');
        
        if ( ! String.isEmpty(v_recordId) && 
                v_recordId.left(3) == globalDescribe.get(this.sObjectType).getDescribe().getKeyPrefix()) {
            
            sObject v_deletedObject = globalDescribe.get(this.sObjectType).newSObject();
            v_deletedObject.Id = v_recordId;
            delete v_deletedObject;
            
            doChange();
        }
    }
    
    /**
    * @description 
    */
    public override Integer getCountOfPage(Integer p_sizeOfRecords, Integer p_countRecords) {
        if (p_sizeOfRecords >= p_countRecords) {
            if (Math.mod(p_sizeOfRecords, p_countRecords) > 0) {
                return Math.round(p_sizeOfRecords / p_countRecords) + 1;
            } else {
                return Math.round(p_sizeOfRecords / p_countRecords);
            }
        }
        return 1;
    }
    
    /**
    * @description 
    */
    public override PageReference doCreateObject() {
        if ( ! String.isEmpty(sObjectLabel)) {
            return new PageReference(AUTH_StaticVariables.prefixSite + '/ISA_New' + sObjectLabel);
        }
        
        return null;
    }
    
    /**
    * @description 
    */
    public override void doRemove() {
        List<sObject> deleteRecords = new List<sObject>();
        
        for (ISA_SObjectHelper.sObjectWrapper wrapperObj : records) {
            if (wrapperObj.checked) {
                deleteRecords.add(wrapperObj.record);
            }
        }
    }
    
    private Map<String, String> getObjectsByUserRole() {
        if (this.userRole == 'Admin') {
            return new Map<String, String> {
                'Account' => 'Account__c', 
                'Contact' => 'Contact__c', 
                'Company' => 'Company__c', 
                'User' => 'User__c', 
                'Question' => 'Question__c', 
                'Insight' => 'Survey_Design__c'
            };
        } else if (this.userRole == 'Company Admin') {
            return new Map<String, String> {
                'Account' => 'Account__c', 
                'Contact' => 'Contact__c', 
                'User' => 'User__c', 
                'Question' => 'Question__c', 
                'Insight' => 'Survey_Design__c', 
                'Strategy' => 'Strategy_Design__c'
            };
        } else if (this.userRole == 'Company User') {
            return new Map<String, String> {
                'Account' => 'Account__c', 
                'Contact' => 'Contact__c'
            };
        }
        
        return new Map<String, String>();
    }
}