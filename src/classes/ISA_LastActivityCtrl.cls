public with sharing class ISA_LastActivityCtrl extends AbstractController {
	public String recordId {get; set;}
	public List<ActivityWrapper> records {get; set;}
	public String accountName {get; set;}
	public String recordName {get; set;}
	public Date dateFilter {get; set;}
	public List<sObject> activities {get; set;}
	
	public ISA_LastActivityCtrl() {
		super();
		
		this.recordId = '';
		this.records = new List<ActivityWrapper>();
		this.accountName = '';
		this.recordName = '';
		this.activities = new List<sObject>();
		
		ApexPages.addMessage(
			new ApexPages.Message(
				ApexPages.Severity.INFO, 
				'Activity display last 20 actions. ' + 
				'You can use Date filter for displaying last activity on selected day.'));
	}
	
	public PageReference initPage() {
		PageReference pr = super.init();
		if (pr != null) {
			return pr;
		}
		
		fillRecords();
		
		return null;
	}
	
	public override void fillRecords() {
		this.recordId = ApexPages.currentPage().getParameters().get('sId');
		
		if ( ! String.isEmpty(this.recordId)) {
			if (Schema.Account__c.SObjectType.getDescribe().getKeyPrefix() == this.recordId.left(3)) {
				
				//Account History
				this.records = getActivityRecords('Account_History__c', 'Account__c');
			} else if (Schema.Contact__c.SObjectType.getDescribe().getKeyPrefix() == this.recordId.left(3)) {
				
				//Contact History
				this.records = getActivityRecords('Contact_History__c', 'Contact__c');
			} else if (Schema.Company__c.SObjectType.getDescribe().getKeyPrefix() == this.recordId.left(3)) {
				
				//Company History
				this.records = getActivityRecords('Company_History__c', 'Company__c');
			} else if (Schema.User__c.SObjectType.getDescribe().getKeyPrefix() == this.recordId.left(3)) {
				
				//User History
				this.records = getActivityRecords('User_History__c', 'User__c');
			} else if (Schema.Survey_Design__c.SObjectType.getDescribe().getKeyPrefix() == this.recordId.left(3)) {
				
				//Insight Panel History
				this.records = getActivityRecords('Insight_Panel_History__c', 'Survey_Design__c');
			} else if (Schema.Question__c.SObjectType.getDescribe().getKeyPrefix() == this.recordId.left(3)) {
				
				//Insight History
				this.records = getActivityRecords('Insight_History__c', 'Insight__c');
			} else if (Schema.Strategy_Design__c.SObjectType.getDescribe().getKeyPrefix() == this.recordId.left(3)) {
				
				//Strategy History
				this.records = getActivityRecords('Strategy_History__c', 'Strategy__c');
			}
		}
	}
	
	public PageReference goBack() {
		if ( ! String.isEmpty(this.recordId)) {
			PageReference pr;
			
			if (Schema.Account__c.SObjectType.getDescribe().getKeyPrefix() == this.recordId.left(3)) {
				pr = new PageReference(AUTH_StaticVariables.prefixSite + '/ISA_ViewAccount');
			} else if (Schema.Contact__c.SObjectType.getDescribe().getKeyPrefix() == this.recordId.left(3)) {
				pr = new PageReference(AUTH_StaticVariables.prefixSite + '/ISA_ViewContact');
			} else if (Schema.Company__c.SObjectType.getDescribe().getKeyPrefix() == this.recordId.left(3)) {
				pr = new PageReference(AUTH_StaticVariables.prefixSite + '/ISA_ViewCompany');
			} else if (Schema.User__c.SObjectType.getDescribe().getKeyPrefix() == this.recordId.left(3)) {
				pr = new PageReference(AUTH_StaticVariables.prefixSite + '/ISA_ViewUser');
			} else if (Schema.Survey_Design__c.SObjectType.getDescribe().getKeyPrefix() == this.recordId.left(3)) {
				pr = new PageReference(AUTH_StaticVariables.prefixSite + '/ISA_ManagePanels');
			} else if (Schema.Question__c.SObjectType.getDescribe().getKeyPrefix() == this.recordId.left(3)) {
				pr = new PageReference(AUTH_StaticVariables.prefixSite + '/ISA_ViewQuestion');
			} else if (Schema.Strategy_Design__c.SObjectType.getDescribe().getKeyPrefix() == this.recordId.left(3)) {
				pr = new PageReference(AUTH_StaticVariables.prefixSite + '/ISA_ViewStrategy');
			}
			
			addRetUrl(pr);
			pr.getParameters().put('sId', this.recordId);
			pr.setRedirect(true);
			
			return pr;
		}
		
		return null;
	}
	
	private List<ActivityWrapper> getActivityRecords(String p_sObjectType, String p_parentLookup) {
		String dateWhereClause = (dateFilter != null) 
			? ' AND DAY_ONLY(CreatedDate) = :dateFilter'
			: 'AND DAY_ONLY(CreatedDate) <= :dateFilter';
		
		List<sObject> histories = Database.query('SELECT CreatedDate, Message__c FROM ' + p_sObjectType + 
			' WHERE ' + p_parentLookup + ' = \'' + this.recordId + '\' ' + dateWhereClause + 
			' ORDER BY CreatedDate DESC LIMIT 20');
		
		List<ActivityWrapper> results = new List<ActivityWrapper>();
		if ( ! histories.isEmpty()) {
			for (sObject history : histories) {
				results.add(new ActivityWrapper(history));
			}
		}
		
		return results;
	}
	
	public void applyFilter() {
		fillRecords();
	}
	
	public class ActivityWrapper {
		public sObject record {get; set;}
		
		public ActivityWrapper(sObject p_sObject) {
			this.record = p_sObject;
		}
	}
}