public with sharing class AUTH_Utils {
	
	public static String checkSession() {
		if( ApexPages.currentPage().getCookies().get('SessionId') == null ||
			ApexPages.currentPage().getCookies().get('SessionId').getValue() == null ) {
			return '';
		} else {
			try {
				User__c v_currentUser = [
					SELECT Id
					FROM User__c
					WHERE SessionId__c = :ApexPages.currentPage().getCookies().get('SessionId').getValue() AND Active__c = true];
				if (v_currentUser == null) {
					return null;
				}
				return '' + v_currentUser.id;
			} catch(Exception e) {
				return '';
			}
			
		}
	}
	
	public static User__c generateSession(User__c p_user) {
		Blob v_input = Blob.valueOf(
			p_user.Username__c + ':' + p_user.Password__c 
			+ ':' + Datetime.now().getTime());
		p_user.SessionId__c = EncodingUtil.base64Encode(
			Crypto.generateMac(
				AUTH_StaticVariables.rsaAlgorithm, 
				v_input, 
				AUTH_StaticVariables.sessionPKey));
		return p_user;
	}
	
	public static void removeSession() {
		 if( ApexPages.currentPage().getCookies().get('SessionId') != null ||
			ApexPages.currentPage().getCookies().get('SessionId').getValue() != null ) {
			
			// remove sessionId from database	
			User__c v_currentUser = [
						SELECT Id
						FROM User__c
						WHERE SessionId__c = :ApexPages.currentPage().getCookies().get('SessionId').getValue()];
			if (v_currentUser != null) {
				v_currentUser.SessionId__c = null;
				update v_currentUser;
			}
		 }
	}
	
	public static String getMd5(String inputString) {
		return EncodingUtil.convertToHex(Crypto.generateDigest('MD5', Blob.valueOf(inputString)));
	}
	
	public static void clearCookie() {
		 ApexPages.currentPage().setCookies(new Cookie[]{ new Cookie('SessionId', null, null, -1, true)});
	}
	
	public static void updateSessionCookie(User__c p_user) {
		ApexPages.currentPage().setCookies(
			new Cookie[]{
				new Cookie('SessionId', p_user.SessionId__c, null, AUTH_StaticVariables.cookieTime, true)});
	}
	
	public static User__c getCheckedUser() {
		if (ApexPages.currentPage().getCookies().get('SessionId') == null ||
			ApexPages.currentPage().getCookies().get('SessionId').getValue() == null) {
			
			return null;
		} else {
			try {
				return [
					SELECT Role__c, Company__c, Last_Name__c, First_Name__c, SessionId__c 
					FROM User__c 
					WHERE SessionId__c = :ApexPages.currentPage().getCookies().get('SessionId').getValue() AND Active__c = true 
					LIMIT 1];
			} catch(Exception e) {
				return null;
			}
		}
	}
}