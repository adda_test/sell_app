@isTest
private class ISA_InsightListViewCtrlTest {
	
	static User__c tempUser(id id,string role) {
        User__c testUser = new User__c(
            First_Name__c = 'First', 
            Last_Name__c = 'Last', 
            Phone__c = '123455678', 
            Username__c = 'testLogin', 
            Password__c = 'testPassword', 
            Role__c = role, 
            Company__c = id,
            Expiration_Date__c = Date.today(), 
            Create_Date__c = Date.today(),
            SessionId__c = '1',
            Active__c = true,
            Email__c = 'test@mail.com');
        return testUser;
    }
    
    static Company__c tempCompany() {
        Company__c company = new Company__c(
            Name = 'test name',
            Admin_First_Name__c = 'firstname',
            Admin_Last_Name__c = 'lastname',
            Admin_Email__c = 'test@mail.com',
            Admin_Login__c = 'testLogin',
            Admin_Password__c = 'testPassword',
            Address1__c = 'addr1',
            Address2__c = 'addr2',
            Trial_Start__c = Date.today(),
            Maximum_Users__c = 10);
        return company;
    }
    
    static Survey_Design__c tempSurveyDesign(id id) {
    	Survey_Design__c surveyDesign = new Survey_Design__c(
    		Name = 'Name',
    		Company__c = id);
    	return surveyDesign;
    }
     
	@isTest
	private static void testInsightListViewCtrl() {
		
		Test.startTest();
		
		Company__c testCompany = tempCompany();           
        insert testCompany;      
        User__c testUser = tempUser(testCompany.Id,'Company Admin');    
        insert testUser;  
       
        Cookie testCookie = new Cookie('SessionId','1',null,-1,false);
        ApexPages.currentPage().setCookies(new Cookie[] {testCookie});                     
		ApexPages.currentPage().getParameters().put('sortField','Name');
		ApexPages.currentPage().getParameters().put('sortOrder','ASC');		
		ApexPages.currentPage().getParameters().put('editRecordId',testUser.Id);
		
  		ISA_InsightListViewCtrl controller = new ISA_InsightListViewCtrl();  
  		controller.init();	
  		controller.doSort(); 
  		
  		System.assertEquals(3,controller.getCountOfPage(12,5));
  		System.assertEquals(2,controller.getCountOfPage(10,5));
  		System.assertEquals(1,controller.getCountOfPage(10,15));
  		
  		controller.doChange();
  		 
  		controller.selectedCountRecords = null;
  		controller.doFirstPage();
  		controller.doPreviousPage();
  		controller.doNextPage();
  		controller.doLastPage();
  		
  		
  		controller.doEditRecord();
  		System.assertNotEquals(null,controller.doEditRecord());
  		
  		controller.doCreateObject();
  		System.assertNotEquals(null,controller.doCreateObject());
  		
  		controller.doRemoveObject();
  		
  		Test.stopTest();		
	}
	
	@isTest
	private static void testDoEditRecord() {
		
		Test.startTest();
		
		ISA_InsightListViewCtrl controller = new ISA_InsightListViewCtrl(); 
		controller.doEditRecord();
  		System.assertEquals(null,controller.doEditRecord());
  		controller.GetObjectsByUserRole();
  		
  		Test.stopTest(); 		
	}
	
	@isTest
	private static void testdoRemoveRecord() {
		
		Test.startTest();
		
		Company__c testCompany = tempCompany();           
        insert testCompany;      
        User__c testUser = tempUser(testCompany.Id,'Company Admin');    
        insert testUser;  
        Survey_Design__c testSurveyDesign = tempSurveyDesign(testCompany.Id);    
        insert testSurveyDesign;
		
		ApexPages.currentPage().getParameters().put('removeRecordId', testSurveyDesign.Id);
		
		ISA_InsightListViewCtrl controller = new ISA_InsightListViewCtrl(); 

		controller.doRemoveRecord();
  		
  		Test.stopTest();
	}
	
	@isTest
	private static void testGetObjectsByUserRoleAdmin() {
		
		Test.startTest();
		
		Company__c testCompany = tempCompany();           
        insert testCompany;      
        User__c testUser = tempUser(testCompany.Id,'Admin');    
        insert testUser;  
       
        Cookie testCookie = new Cookie('SessionId','1',null,-1,false);
        ApexPages.currentPage().setCookies(new Cookie[] {testCookie});                     
		
  		ISA_InsightListViewCtrl controller = new ISA_InsightListViewCtrl();  
  		controller.init();	
  		controller.GetObjectsByUserRole(); 		
  		System.assertNotEquals(null,controller.GetObjectsByUserRole());
  		
  		Test.stopTest();
	}
	
	@isTest
	private static void testGetObjectsByUserRoleCompanyAdmin() {
		
		Test.startTest();
		
		Company__c testCompany = tempCompany();           
        insert testCompany;      
        User__c testUser = tempUser(testCompany.Id,'Company Admin');    
        insert testUser;  
       
        Cookie testCookie = new Cookie('SessionId','1',null,-1,false);
        ApexPages.currentPage().setCookies(new Cookie[] {testCookie});                     
		
  		ISA_InsightListViewCtrl controller = new ISA_InsightListViewCtrl();  
  		controller.init();	
  		controller.GetObjectsByUserRole(); 		
  		System.assertNotEquals(null,controller.GetObjectsByUserRole());
  		
  		Test.stopTest();
	}
}