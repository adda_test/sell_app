public with sharing class AssignContactHandler {
	public static Boolean enablesTrigger = true;
	
	public static void recalculateJunctions(List<AssignContact__c> p_newRecords) {
		Set<Id> userIds = new Set<Id>();
		
		for (AssignContact__c junciton : p_newRecords) {
			if (junciton.User__c != null) {
				userIds.add(junciton.User__c);
			}
		}
		
		if ( ! userIds.isEmpty()) {
			ISA_TriggerHelper.recalculateUsers(userIds);
		}
	}
	
	public static void addHistory(List<AssignContact__c> p_newRecords, Boolean p_isInsert) {
		String assignValue = '';
		
		if (p_isInsert) {
			assignValue = 'assigned';
		} else {
			assignValue = 'unassigned';
		}
		
		List<Contact_History__c> contactHistories = new List<Contact_History__c>();
		List<User_History__c> userHistories = new List<user_History__c>();
		
		for (AssignContact__c junction : p_newRecords) {
			PageReference prOwner = new PageReference(AUTH_StaticVariables.prefixSite + '/ISA_ViewUser');
			prOwner.getParameters().put('sId', junction.Owner__c);
			
			PageReference prUser = new PageReference(AUTH_StaticVariables.prefixSite + '/ISA_ViewUser');
			prUser.getParameters().put('sId', junction.User__c);
			
			PageReference prContact = new PageReference(AUTH_StaticVariables.prefixSite + '/ISA_ViewContact');
			prContact.getParameters().put('sId', junction.Contact__c);
			
			if (junction.Contact__c != null) {
				contactHistories.add(
					new Contact_History__c(
						Contact__c = junction.Contact__c, 
						Message__c = '<b><a href="' + prOwner.getUrl() + '">' + junction.Owner_Name__c + 
							'</a></b> ' + assignValue + ' Contact(<b><a href="' + prContact.getUrl() + '">' + 
							junction.Contact_Name__c + '</a>)</b> to User(<b><a href="' + prUser.getUrl() + '">' + 
							junction.User_Name__c + '</a></b>)'
					));
			}
			
			if (junction.User__c != null) {
				userHistories.add(
					new User_History__c(
						User__c = junction.User__c, 
						Message__c = '<b><a href="' + prOwner.getUrl() + '">' + junction.Owner_Name__c + 
							'</a></b> ' + assignValue + ' Contact(<b><a href="' + prContact.getUrl() + '">' + 
							junction.Contact_Name__c + '</a>)</b>'
					));
			}
		}
		
		insert contactHistories;
		insert userHistories;
	}
}