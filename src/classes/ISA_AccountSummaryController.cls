public with sharing class ISA_AccountSummaryController extends AbstractController {
	
	private Account__c account;
	private AccountDetailWrapper accountDetail;
	private List<ActivityWrapper> activityRecords;
	private List<WrapperCollection.ContactBubbleWrapper> contactUserChartList;
	private List<WrapperCollection.InsightBubbleWrapper> insightChartList;
	
	private Integer countContacts;
	private Integer countNoInsight;
	private Integer countInsights;
	
	private Decimal accountAvgY;
	private Decimal topAccountAvgY;
	
	private Decimal insightsAvgY;
	private Decimal topInsightsAvgY;
	
	private Map<String, String> contactUserSeriesColorMap;
	
	private Decimal insightIndex;
	private Decimal insightIndexDifference;

	private Decimal completeness;
	private Decimal completenessDifference;
	
	private Integer retention;
	private Integer needsAssesment;
	private Integer valuesAssessment;
		
	private Integer countNoResponce;
	
	
	public ISA_AccountSummaryController(){
		super();
		
		this.activityRecords = new List<ActivityWrapper>();
		this.contactUserSeriesColorMap = new  Map<String, String>();
		this.contactUserChartList = new List<WrapperCollection.ContactBubbleWrapper>();
		this.insightChartList = new List<WrapperCollection.InsightBubbleWrapper>();
		
		this.countInsights = 0;
		this.countContacts = 0;
		this.countNoInsight = 0;
		this.accountAvgY = 0;
		this.topAccountAvgY = 0;

		this.countNoResponce = 0;

		this.insightsAvgY = 0;
		this.topInsightsAvgY = 0;
		
		this.insightIndex = 0;
		this.insightIndexDifference = 0;
		
		this.completeness = 0;
		this.completenessDifference = 0;
		
		this.retention = 0;
		this.needsAssesment = 0;
		this.valuesAssessment = 0;
	}
	
	public PageReference initPage() {
		PageReference pr = super.init();
		if (pr != null) {
			return pr;
		}
		
		String accountId = ApexPages.currentPage().getParameters().get('Id');
		
		this.account = getAccount(accountId);
		this.insightIndex = this.account.Insight_Index__c;
		this.completeness = this.account.Completeness_Score__c;
		
		this.accountDetail = new AccountDetailWrapper(this.account);
		
		this.activityRecords = getActivityRecords('Account_History__c', 'Account__c', accountId);
		String userId = (this.userRole == 'Company User') ? this.user.Id : null;
		
		this.insightChartList = ChartUtils.getInsightBubbleChart(' WHERE Survey_Design__r.Company__c = \'' + this.account.Company__c + '\'', null);
		this.countInsights = this.insightChartList.size();
		
		this.contactUserChartList = ChartUtils.getContactBubbleChart(' WHERE Account__c = \'' + accountId + '\'', null, userId);
		this.countContacts = contactUserChartList.size();
		
		for (WrapperCollection.InsightBubbleWrapper insightWrapper: insightChartList){
			if (insightWrapper.response == 0)
				this.countNoResponce++;
		}
		

		Integer level = 0;
		
		for (WrapperCollection.ContactBubbleWrapper contBblWrp: this.contactUserChartList){
			contBblWrp.bubbleType = 'Contact Summary';
			
			this.accountAvgY += contBblWrp.insightIndex;
			
			if (level == 0){
				this.contactUserseriesColorMap.put(contBblWrp.bubbleName, '#2e6dc8');
			} else if (level == 1){
				this.contactUserseriesColorMap.put(contBblWrp.bubbleName, '#568bd3');
			} else {
				this.contactUserseriesColorMap.put(contBblWrp.bubbleName, '#aac5e9');
			}
			level++;
			
			if (this.topAccountAvgY < contBblWrp.insightIndex){
				this.topAccountAvgY = contBblWrp.insightIndex;
			}
			
			if (contBblWrp.insightIndex == 0){
				this.countNoInsight++;
			}
			
			this.contactUserChartList = ChartUtils.getContactBubbleChart(' WHERE Account__c = \'' + accountId + '\'', null, userId);
			
		}
		
		if (this.contactUserChartList.size() != 0){
			this.accountAvgY = (this.accountAvgY / this.contactUserChartList.size()).setScale(1);
		}
		
		this.topAccountAvgY = this.topAccountAvgY * 0.9;
		
		this.insightsAvgY = this.accountAvgY;
		this.topInsightsAvgY = this.topAccountAvgY;
		
		return null;
	}

	public PageReference doShowInsights() {
		if (this.account.Id != null) {
			PageReference pr = new PageReference(AUTH_StaticVariables.prefixSite + '/apex/ISA_AccountInsights');
			pr.getParameters().put('cId', this.account.Company__c);
			pr.getParameters().put('aId', this.account.Id);
			addRetUrl(pr);
			pr.setRedirect(true);
			
			return pr;
		}
		
		return null;
	}
	
	public PageReference doShowSummary() {
		if (this.account.Id != null) {
			PageReference pr = new PageReference(AUTH_StaticVariables.prefixSite + '/apex/ISA_AccountSummary');
			pr.getParameters().put('Id', this.account.Id);
			addRetUrl(pr);
			pr.setRedirect(true);
			
			return pr;
		}
		
		return null;
	}
	
	public PageReference goContactView(){
		if (this.account.Id != null) {
			PageReference pr = new PageReference(AUTH_StaticVariables.prefixSite + '/apex/ISA_ListViewContact');
			pr.getParameters().put('aId', this.account.Id);
			addRetUrl(pr);
			pr.setRedirect(true);
			
			return pr;
		}
		
		return null;
	}
	
	public PageReference goAccountStrategiesView(){
		if (this.account.Id != null) {
			PageReference pr = new PageReference(AUTH_StaticVariables.prefixSite + '/apex/ISA_AccountStrategies');
			pr.getParameters().put('cId', this.account.Company__c);
			pr.getParameters().put('aId', this.account.Id);
			addRetUrl(pr);
			pr.setRedirect(true);
			
			return pr;
		}
		
		return null;
	}
	
	public PageReference goActivityView(){
		if (this.account.Id != null) {
			PageReference pr = new PageReference(AUTH_StaticVariables.prefixSite + '/apex/ISA_LastActivity');
			pr.getParameters().put('sId', this.account.Id);
			addRetUrl(pr);
			pr.setRedirect(true);
			
			return pr;
		}
		
		return null;
	}
//////////////  GETERS  /////////////////

	public Integer getCountNoResponce(){
		return this.countNoResponce;
	}

	public Decimal getInsightsAvgY(){
		return this.insightsAvgY;
	}
	public Decimal getTopInsightsAvgY(){
		return this.topInsightsAvgY;
	}

	public List<WrapperCollection.InsightBubbleWrapper> getInsightChartList(){
		return this.insightChartList;
	}
	public Integer getCountInsights(){
		return this.countInsights;
	}
	
	public Decimal getInsightIndex(){
		return this.insightIndex.setScale(2);
	}
	public Decimal getInsightIndexDifference(){
		return this.insightIndexDifference.setScale(2);
	}

	public Decimal getCompleteness(){
		return this.completeness.setScale(2);
	}
	
	public Decimal getCompletenessDifference(){
		return this.completenessDifference.setScale(2);
	}
	
	public Integer getRetention(){
		return this.retention;
	}
	public Integer getNeedsAssesment(){
		return this.needsAssesment;
	}
	public Integer getValuesAssessment(){
		return this.valuesAssessment;
	}

	public Map<String, String> getContactUserSeriesColorMap(){
		return this.contactUserSeriesColorMap;
	}
	
	public Decimal getTopAccountAvgY(){
		return this.topAccountAvgY;
	}
	
	public Decimal getAccountAvgY(){
		return this.accountAvgY;
	}
	
	public Integer getCountNoInsight(){
		return this.countNoInsight;
	}
	
	public Integer getCountContacts(){
		return this.countContacts;
	}
	
	public Account__c getAccount(){
		return this.account;
	}
	
	public AccountDetailWrapper getAccountDetail(){
		return this.accountDetail;
	}
	
	public List<ActivityWrapper> getActivityRecords(){
		return this.activityRecords;
	}
	
	public List<WrapperCollection.ContactBubbleWrapper> getContactUserChartList(){
		return this.contactUserChartList;
	}
	
///////////////////////////////
	private List<ActivityWrapper> getActivityRecords(String p_sObjectType, String p_parentLookup, String recordId) {
		
		List<sObject> histories = Database.query('SELECT CreatedDate, Message__c FROM ' + p_sObjectType + 
			' WHERE ' + p_parentLookup + ' = \'' + recordId + '\' ' + 
			' ORDER BY CreatedDate DESC LIMIT 12');
		
		List<ActivityWrapper> results = new List<ActivityWrapper>();
		if ( ! histories.isEmpty()) {
			for (sObject history : histories) {
				results.add(new ActivityWrapper(history));
			}
		}
		
		return results;
	}
	
	private Account__c getAccount(String Id){
		Account__c account = new Account__c();
		
		List<Account__c> accounts = [
				SELECT Name, First_Name__c, Last_Name__c, 
					Completeness_Score__c, Insight_Index__c,
					Phone__c, Email__c, Address1__c, Address2__c, 
					City__c, State__c, Zipcode__c, 
					Company__r.Name, User__c, Web_Site__c, Country__c 
				FROM Account__c 
				WHERE Id = :Id 
				LIMIT 1];
		
		if (! accounts.isEmpty())
			account = accounts.get(0);
		
		return account;
	}


///////////////  WRAPPERS ////////////////
	public class ActivityWrapper {
		public sObject record {get; set;}
		
		public ActivityWrapper(sObject p_sObject) {
			this.record = p_sObject;
		}
	}

	public class AccountDetailWrapper{
		private String name;
		private String address;
		private String phone;
		private String website;
		
		public AccountDetailWrapper(Account__c account){
			this.name = account.Name;
			this.address = generateAddress(account);
			this.phone = account.Phone__c;
			this.website = account.Web_Site__c;
		}
	
		public String getName(){
			return name;
		}
		
		public String getAddress(){
			return address;
		}
		
		public String getPhone(){
			return phone;
		}
		
		public String getWebSite(){
			return website;
		}
	
		private String generateAddress(Account__c account){
			String address = (account.Address1__c == null ? '' : account.Address1__c +  ', ') +
				+ (account.City__c == null ? '' : account.City__c + ', ') +  
				+ (account.Country__c == null ? '' : account.Country__c + ' ') + 
				+ (account.State__c  == null ? '' : account.State__c + ', ') + 
				+ (account.Zipcode__c == null ? '' : account.Zipcode__c);
			
			return address;
		}
	
	}

}