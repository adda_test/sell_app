public with sharing class AUTH_ForgotPasswordController {
	public String uName {get; set;}
	
	public AUTH_ForgotPasswordController() {
		uName = '';
		Apexpages.addMessage(
			new Apexpages.Message(
				Apexpages.SEVERITY.INFO,
				AUTH_StaticVariables.passResetInfo));
	}
	
	public void sendFrgtPassRequest() {
		try {
			User__c v_currentUser = [
					SELECT Username__c, Password__c, Password_Recover_Token__c, Email__c
					FROM User__c
					WHERE Username__c = :uName];
			
			update generatePRecoverToken(v_currentUser);
			generateRecoveyMessage(v_currentUser);
			
			
			Apexpages.addMessage(
				new Apexpages.Message(
					Apexpages.SEVERITY.CONFIRM,
					AUTH_StaticVariables.passResetConfirm));
		} catch(Exception e) {
			Apexpages.addMessage(
				new Apexpages.Message(
					Apexpages.SEVERITY.ERROR,
					AUTH_StaticVariables.passResetError));
		}
	}
	
	private User__c generatePRecoverToken(User__c p_user) {
			Blob v_input = Blob.valueOf(
					p_user.Username__c + ':' + p_user.Password__c + ':' 
					+ Datetime.now().getTime());
			p_user.Password_Recover_Token__c = EncodingUtil.convertToHex(
					Crypto.generateMac(
							AUTH_StaticVariables.rsaAlgorithm, 
							v_input, 
							AUTH_StaticVariables.passRecoverPKey));
			return p_user;
	}
	
	private void generateRecoveyMessage(User__c p_user) {
			String htmlBody = AUTH_StaticVariables.passRecoveryMessageHead + 
					AUTH_StaticVariables.resetPasswordURL + p_user.Password_Recover_Token__c
					+ AUTH_StaticVariables.passRecoveryMessageFooter;
			
			HELP_MailUtility.sendQuickMail(
							p_user.Email__c, 
							AUTH_StaticVariables.passRecoverySubject,
							htmlBody);
	}
	
	//TODO: add validation for the amount of reset emails sent
}