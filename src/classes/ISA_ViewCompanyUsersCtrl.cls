public with sharing class ISA_ViewCompanyUsersCtrl extends AbstractController {
	public String companyName {get; set;}
	public List<User__c> users {get; set;}
	
	private String companyId;
	
	public ISA_ViewCompanyUsersCtrl() {
		this.companyName = '';
		this.companyId = '';
		this.users = new List<User__c>();
	}
	
	public PageReference initPage() {
		PageReference afterInit = init();
		if (afterInit != null) {
			return afterInit;
		}
		
		this.companyId = ApexPages.currentPage().getParameters().get('cId');
		
		this.users = [
			SELECT Active__c, Name 
			FROM User__c 
			WHERE Company__c = :this.companyId 
			LIMIT 1000];
		
		return null;
	}
	
	public override PageReference doCancel() {
		PageReference pr = super.doCancel();
		
		pr.getParameters().put('sId', this.companyId);
		
		return pr;
	}
}