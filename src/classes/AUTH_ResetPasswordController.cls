public with sharing class AUTH_ResetPasswordController {
	public String newPass {get; set;}
	public String newPassRepeat {get; set;}
	public String resultMessage {get; set;}
	private User__c user;
	
	public AUTH_ResetPasswordController() {
		newPass = '';
		newPassRepeat = '';
	}
	
	public void init() {
		if(! validatePRecoverURL() ) {
			resultMessage = '';
		}
	}
	
	public PageReference updatePassword() {
		if(! String.isEmpty(newPass) && newPass == newPassRepeat && user != null) {
			user.Password__c = AUTH_Utils.getMd5(newPass);
			update user;
			// go to login page
			return new PageReference(AUTH_StaticVariables.loggedOutURL);
		}
		
		return null;
	}
	
	private Boolean validatePRecoverURL () {
		try {
			user = [
					SELECT Password__c, Password_Recover_Token__c
					FROM User__c
					WHERE Password_Recover_Token__c = :Apexpages.currentPage().getParameters().get('t')];
			user.Password_Recover_Token__c = '';
			
			return true;
		} catch(Exception e) {
			return false;
		}
	}
}