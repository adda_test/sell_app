public with sharing class UserHandler {
	public static Boolean enablesTrigger = true;
	
	public static void populateName(List<User__c> p_newRecords) {
		for (User__c user : p_newRecords) {
			user.Name = 
					( ! String.isEmpty(user.First_Name__c) ? user.First_Name__c : '') + ' ' + 
					( ! String.isEmpty(user.Last_Name__c) ? user.Last_Name__c : '');
		}
	}
	
	public static void setExpirationDate(List<User__c> p_newRecords) {
		Map<Id, List<User__c>> companyToUserMap = new Map<Id, List<User__c>>();
		for (User__c user : p_newRecords) {
			//
			if ((user.Role__c == 'Company Admin' || user.Role__c == 'Company User') && user.Company__c != null) {
				List<User__c> values = companyToUserMap.get(user.Company__c);
				
				if (values == null) {
					companyToUserMap.put(user.Company__c, new List<User__c>());
				}
				companyToUserMap.get(user.Company__c).add(user);
			}
		}
		
		List<Company__c> companies = [
			SELECT Trial_End__c 
			FROM Company__c 
			WHERE Id IN :companyToUserMap.keySet() 
			LIMIT 50000];
		
		if ( ! companies.isEmpty()) {
			for (Company__c company : companies) {
				for (User__c user : companyToUserMap.get(company.Id)) {
					user.Expiration_Date__c = company.Trial_End__c;
				}
			}
		}
	}
	
	public static void updateLastActivity(List<User__c> p_newRecords) {
		for (User__c user : p_newRecords) {
			user.Last_Activity__c = DateTime.now();
		}
	}
	
	public static void addUserHistory(List<User__c> p_newRecords) {
		addUserHistory(new Map<Id, User__c>(), p_newRecords);
	}
	
	public static void addUserHistory(Map<Id, User__c> p_oldRecordMap, List<User__c> p_newRecords) {
		List<User_History__c> createHistories = new List<User_History__c>();
		
		for (User__c user : p_newRecords) {
			User__c oldUser = p_oldRecordMap.get(user.Id);
			
			if (oldUser != null) {
				List<Schema.FieldSetMember> fieldSetMembers = Schema.getGlobalDescribe().get('User__c')
					.getDescribe().FieldSets.getMap().get('TrackFields').getFields();
				
				for (Schema.FieldSetMember field : fieldSetMembers) {
					if (oldUser.get(field.getFieldPath()) != user.get(field.getFieldPath())) {
						String oldValue = String.valueOf(oldUser.get(field.getFieldPath()));
						String newValue = String.valueOf(user.get(field.getFieldPath()));
						
						createHistories.add(
							new User_History__c(
								User__c = user.Id, 
								OldValue__c = oldValue, 
								NewValue__c = newValue, 
								Message__c = '<b>' + user.Owner_Name__c + '</b> updated <b>' + field.getLabel() + '</b>' + 
										' from ' + ( ! String.isEmpty(oldValue) ? '<b>' + oldValue + '</b>' : '') + 
										' to ' + ( ! String.isEmpty(newValue) ? '<b>' + newValue + '</b>' : '') + '.'
							));
					}
				}
			} else {
				createHistories.add(
					new User_History__c(
						User__c = user.Id, 
						Message__c = '<b>' + user.Owner_Name__c + '</b> created User.'
					));
			}
		}
		
		insert createHistories;
	}
	
	public static void updateCompany(List<User__c> p_newRecords) {
		Set<Id> companyIds = new Set<Id>();
		for (User__c user : p_newRecords) {
			if (user.Company__c != null) {
				companyIds.add(user.Company__c);
			}
		}
		
		if ( ! companyIds.isEmpty()) {
			List<Company__c> companies = [
				SELECT Id, 
					(SELECT Id 
					FROM Users__r)
				FROM Company__c 
				WHERE Id IN :companyIds 
				LIMIT 10000];
			
			for (Company__c company : companies) {
				company.Users__c = company.Users__r.size();
			}
			
			update companies;
		}
	}
	
	public static void deleteAssigns(Map<Id, User__c> p_oldRecordMap) {
		delete [SELECT Id FROM AssignContact__c WHERE User__c IN :p_oldRecordMap.keySet() LIMIT 10000];
	}
}