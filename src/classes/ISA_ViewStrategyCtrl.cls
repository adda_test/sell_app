public with sharing class ISA_ViewStrategyCtrl extends AbstractController {
	public Strategy_Design__c strategy {get; set;}
	public Strategy_Question__c question {get; set;}
	public List<ISA_SObjectHelper.sObjectWrapper> viewQuestions {get; set;}
	
	public ISA_ViewStrategyCtrl() {
		super();
		
		this.strategy = new Strategy_Design__c();
		this.question = new Strategy_Question__c();
		this.viewQuestions = new List<ISA_SObjectHelper.sObjectWrapper>();
		
		this.sObjectLabel = 'Strategy';
	}
	
	public PageReference initPage() {
		PageReference pr = super.init();
		if (pr != null) {
			return pr;
		}
		
		String strategyId = ApexPAges.currentPage().getParameters().get('sId');
		
		if ( ! String.isEmpty(strategyId)) {
			List<Strategy_Design__c> strategies = [
					SELECT Company__r.Name, Type__c, Order__c 
					FROM Strategy_Design__c 
					WHERE Id = :strategyId 
					LIMIT 1];
			
			if ( ! strategies.isEmpty()) {
				this.strategy = strategies.get(0);
				
				getRecords();
			}
		} else {
			ApexPages.addMessage(
				new ApexPages.Message(
					ApexPages.Severity.ERROR, 'Selected Strategy wasn\'t found.'));
		}
		return null;
	}
	
	public void getRecords() {
		this.viewQuestions = new List<ISA_SObjectHelper.sObjectWrapper>();
		
		List<Strategy_Question__c> questions = [
				SELECT Title__c, Description__c 
				FROM Strategy_Question__c 
				WHERE Strategy_Design__c = :this.strategy.Id 
				LIMIT 10000];
		
		if ( ! questions.isEmpty()) {
			for (Strategy_Question__c question : questions) {
				this.viewQuestions.add(new ISA_SObjectHelper.sObjectWrapper(question));
			}
		}
	}
	
	
	public PageReference doEdit() {
		if (this.strategy.Id != null) {
			PageReference pr = new PageReference(AUTH_StaticVariables.prefixSite + '/apex/ISA_NewStrategy');
			pr.getParameters().put('sId', this.strategy.Id);
			addRetUrl(pr);
			pr.setRedirect(true);
			
			return pr;
		}
		
		return null;
	}
	
	public void doSaveQuestion() {
		String titleQuestion = ApexPages.currentPage().getParameters().get('title');
		String descriptionQuestion = ApexPages.currentPage().getParameters().get('description');
		
		if ( ! String.isEmpty(titleQuestion) && ! String.isEmpty(descriptionQuestion)) {
			this.question.Strategy_Design__c = this.strategy.Id;
			
			try {
				this.question.Title__c = titleQuestion;
				this.question.Description__c = descriptionQuestion;
				
				insert this.question;
				
				getRecords();
				
				this.question = new Strategy_Question__c();
			} catch(Exception ex) {
				ApexPages.addMessage(
					new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
			}
		}
	}
	
	public void doEditItem() {
		String titleQuestion = ApexPages.currentPage().getParameters().get('title');
		String descriptionQuestion = ApexPages.currentPage().getParameters().get('description');
		String questionId = ApexPages.currentPage().getParameters().get('questionId');
		
		if ( ! String.isEmpty(titleQuestion) && ! String.isEmpty(descriptionQuestion) && ! String.isEmpty(questionId)) {
			
			try {
				List<Strategy_Question__c> updateQuestions = new List<Strategy_Question__c>();
				for (ISA_SObjectHelper.SObjectWrapper wrapper : this.viewQuestions) {
					if (wrapper.record.Id == questionId) {
						updateQuestions.add(
							new Strategy_Question__c(
								Id = questionId, 
								Title__c = titleQuestion, 
								Description__c = descriptionQuestion
							));
					}
				}
				
				update updateQuestions;
				
				getRecords();
				
				this.question = new Strategy_Question__c();
			} catch(Exception ex) {
				ApexPages.addMessage(
					new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
			}
		}
	}
	
	public void doRemoveItem() {
		String questionId = ApexPages.currentPage().getParameters().get('itemId');
		
		if ( ! String.isEmpty(questionId)) {
			
			try {
				delete new Strategy_Question__c(Id = questionId);
				
				getRecords();
			} catch(Exception ex) {
				ApexPages.addMessage(
					new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
			}
		}
	}
	
	@TestVisible protected override void collectRetParams(PageReference pr) {
		pr.getParameters().put('sId',ApexPages.currentPage().getParameters().get('sId'));
	}
}