public with sharing class ISA_ViewContactStrategyCtrl extends AbstractController {
	private static final List<Schema.PicklistEntry> STRATEGYTYPES = Strategy_Design__c.Cathegory__c
			.getDescribe().getPicklistValues();
	
	public List<StrategyWrapper> strategies {get; set;}
	
	private String contactId;
	
	public ISA_ViewContactStrategyCtrl() {
		super();
		
		this.strategies = new List<StrategyWrapper>();
		this.contactId = '';
	}
	
	public PageReference initPage() {
		PageReference afterInit = init();
		if (afterInit != null) {
			return afterInit;
		}
		
		this.contactId = ApexPages.currentPage().getParameters().get('cId');
		
		if ( ! String.isEmpty(this.contactId)) {
			List<Strategy_Result__c> strategyResults = [
					SELECT Strategy_Design__c, IsShow__c 
					FROM Strategy_Result__c 
					WHERE Contact__c = :this.contactId 
					  AND Strategy_Design__c != null 
					LIMIT 10000];
			
			if ( ! strategyResults.isEmpty()) {
				Map<Id, Strategy_Result__c> strategyMap = new Map<Id, Strategy_Result__c>();
				for (Strategy_Result__c result : strategyResults) {
					strategyMap.put(result.Strategy_Design__c, result);
				}
				
				List<Strategy_Design__c> strategyList = [
						SELECT Cathegory__c, Name, Order__c, Type__c 
						FROM Strategy_Design__c 
						WHERE Id IN :strategyMap.keySet() 
						ORDER BY Order__c 
						LIMIT 10000];
				
				for (Strategy_Design__c strategy : strategyList) {
					this.strategies.add(new StrategyWrapper(strategy, strategyMap.get(strategy.Id).IsShow__c));
				}
				
				List<Strategy_Question__c> questions = [
							SELECT Title__c, Description__c, Strategy_Design__c, 
								(SELECT Strategy_Question__c, Value__c, Point__c, Contact__c 
								FROM Strategy_Answers__r 
								WHERE Contact__c = :this.contactId 
								LIMIT 1) 
							FROM Strategy_Question__c 
							WHERE Strategy_Design__c IN :strategyMap.keySet()];
					
					Map<Id, List<Strategy_Question__c>> strategyToQuestionsMap = new Map<Id, List<Strategy_Question__c>>();
					for (Strategy_Question__c question : questions) {
						Id key = question.Strategy_Design__c;
						
						if ( ! strategyToQuestionsMap.containsKey(key)) {
							strategyToQuestionsMap.put(key, new List<Strategy_Question__c>());
						}
						strategyToQuestionsMap.get(key).add(question);
					}
					
					for (StrategyWrapper strategy : this.strategies) {
						strategy.AddQuestions(strategyToQuestionsMap.get(strategy.record.Id));
					}
			}
		}
		
		return null;
	}
	
	public void doSave() {
		List<Strategy_Answer__c> upsertAnswers = new List<Strategy_Answer__c>();
		List<Strategy_Answer__c> deleteAnswers = new List<Strategy_Answer__c>();
		
		for (StrategyWrapper strategy : this.strategies) {
			for (StrategyQuestionWrapper questionW : strategy.questions) {
				System.debug('-----questionW=' + questionW);
				if (questionW.selectedValue) {
					Strategy_Answer__c answer = new Strategy_Answer__c(
						Id = questionW.answer.Id, 
						Strategy_Question__c = questionW.record.Id, 
						Contact__c = this.contactId,
						Order__c = strategy.record.Order__c, 
						Owner__c = this.user.Id
					);
					
					upsertAnswers.add(answer);
				} else if (questionW.answer.Id != null) {
					deleteAnswers.add(new Strategy_Answer__c(Id = questionW.answer.Id));
				}
			}
		}
		
		upsert upsertAnswers;
		delete deleteAnswers;
		
		List<Strategy_Result__c> strategyResults = [
			SELECT Strategy_Design__c 
			FROM Strategy_Result__c 
			WHERE Contact__c = :this.contactId 
			  AND Strategy_Design__c != null 
			LIMIT 10000];
		
		for (StrategyWrapper design : this.strategies) {
			for (Strategy_Result__c result : strategyResults) {
				if (design.record.Id == result.Strategy_Design__c) {
					result.IsShow__c = design.isShow;
				}
			}
		}
		
		update strategyResults;
	}
	
	/**
	* @description 
	*/
	@TestVisible private String getSelectedPoints(String p_pointOptions, String p_responseOptions, List<String> p_selectedOptions) {
		List<Integer> selectedIndexes = new List<Integer>();
		List<String> responseOptions = p_responseOptions.split(',');
		List<String> pointOptions = p_pointOptions.split(',');
		for (Integer i = 0; i < responseOptions.size(); i++) {
			for (String selected : p_selectedOptions) {
				if (selected == responseOptions.get(i)) {
					selectedIndexes.add(i);
				}
			}
		}
		
		List<String> selectedPoints = new List<String>();
		for (Integer index : selectedIndexes) {
			selectedPoints.add(pointOptions.get(index));
		}
		return String.join(selectedPoints, ',');
	}
	
	public class StrategyWrapper {
		public Strategy_Design__c record {get; set;}
		public List<StrategyQuestionWrapper> questions {get; set;}
		public List<SelectOption> selectOptions {get; set;}
		public String selectedOption {get; set;}
		public Boolean isCompleteness {get; set;}
		public Boolean isShow {get; set;}
		
		public StrategyWrapper() {
			this.record = new Strategy_Design__c();
			this.questions = new List<StrategyQuestionWrapper>();
			this.selectOptions = new List<SelectOption>();
			this.selectedOption = '';
			this.isCompleteness = false;
			this.isShow = false;
		}
		
		public StrategyWrapper(Strategy_Design__c p_strategy, Boolean p_isShow) {
			this();
			
			this.record = p_strategy;
			this.selectedOption = this.record.Cathegory__c;
			this.isShow = p_isShow;
			
			for (Schema.PicklistEntry cathegory : ISA_ViewContactStrategyCtrl.STRATEGYTYPES) {
				this.selectOptions.add(new SelectOption(cathegory.getValue(), cathegory.getLabel()));
			}
		}
		
		public void AddQuestions(List<Strategy_Question__c> p_questions) {
			if (p_questions != null) {
				for (Strategy_Question__c question : p_questions) {
					this.questions.add(new StrategyQuestionWrapper(question));
				}
			}
		}
	}
	
	public class StrategyQuestionWrapper {
		public Strategy_Question__c record {get; set;}
		private Strategy_Answer__c answer;
		public Boolean selectedValue {get; set;}
		
		public StrategyQuestionWrapper() {
			this.record = new Strategy_Question__c();
			this.answer = new Strategy_Answer__c();
			this.selectedValue = false;
		}
		
		public StrategyQuestionWrapper(Strategy_Question__c p_question) {
			this();
			
			this.record = p_question;
			
			if ( ! p_question.Strategy_Answers__r.isEmpty()) {
				for (Strategy_Answer__c answer : p_question.Strategy_Answers__r) {
					if (answer.Contact__c == ApexPages.currentPage().getParameters().get('cId')) {
						this.answer = answer;
					}
				}
				
				this.selectedValue = true;
			}
		}
	}
}