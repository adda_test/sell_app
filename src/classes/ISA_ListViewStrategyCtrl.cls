public with sharing class ISA_ListViewStrategyCtrl extends AbstractController {
	public List<selectOption> accountOptions {get; set;}
	public String selectedAccount {get; set;}
	public List<WrapperCollection.ColumnChartWrapper> chartList {get; set;}
	
	public ISA_ListViewStrategyCtrl() {
		super();
		
		this.sObjectLabel = 'Strategy';
		this.accountOptions = new List<SelectOption>();
		this.selectedAccount = '';
		this.chartList = new List<WrapperCollection.ColumnChartWrapper>();
	}
	
	public PageReference initPage() {
		PageReference pr = super.init();
		
		if (pr != null) {
			return pr;
		}
		
		this.sObjectType = sObjectNameMap.get(this.sObjectLabel);
		
		for (Account__c account : getAccounts()) {
			this.accountOptions.add(new SelectOption(account.Id, account.Name));
		}
		
		if ( ! this.accountOptions.isEmpty()) {
			this.selectedAccount = this.accountOptions.get(0).getValue();
		}
		
		fillRecords();
		
		// column chart
		fillColumnChartData();
		
		return null;
	}
	
	public void fillColumnChartData() {
		
		if (this.userRole == 'Admin') {
			this.chartList = ChartUtils.getStrategyColumnChart('', '');
		} else {
			this.chartList = ChartUtils.getStrategyColumnChart(
				' WHERE Company__c = ' + '\'' + this.userCompany + '\'', this.userCompany);
			System.debug('--chartList=' + chartList);
		}
	}
	
	public List<Account__c> getAccounts() {
		return [
			SELECT Name 
			FROM Account__c 
			WHERE Company__c = :this.userCompany 
			LIMIT 1000];
	}
	
	public void doMapAccounts() {
		String strategyIds = ApexPages.currentPage().getParameters().get('strategyIds');
		String accountId = ApexPages.currentPage().getParameters().get('accountId');
		
		if ( ! String.isEmpty(strategyIds) && ! String.isEmpty(accountId)) {
			List<AssignStrategy__c> junctions = [
				SELECT Strategy__c 
				FROM AssignStrategy__c 
				WHERE Account__c = :accountId 
				  AND Strategy__c IN :strategyIds.split(',') 
				LIMIT 10000];
			
			Map<Id, Id> strategyToAssignMap = new Map<Id, Id>();
			for (AssignStrategy__c junction : junctions) {
				strategyToAssignMap.put(junction.Strategy__c, junction.Id);
			}
			
			List<AssignStrategy__c> assignedStrategies = new List<AssignStrategy__c>();
			for (String strategyId : strategyIds.split(',')) {
				if ( ! strategyToAssignMap.containsKey(strategyId)) {
					assignedStrategies.add(
						new AssignStrategy__c(
							Account__c = accountId, 
							Strategy__c = strategyId, 
							Owner__c = this.user.Id
						));
				}
			}
			
			if ( ! assignedStrategies.isEmpty()) {
				insert assignedStrategies;
			}
		}
		
		for (ISA_SObjectHelper.SObjectWrapper wrapper : this.records) {
			wrapper.checked = false;
		}
		
		fillRecords();
		
		fillColumnChartData();
	}
	
	public PageReference doManageStrategies() {
		PageReference pr = new PageReference(AUTH_StaticVariables.prefixSite + '/ISA_ManageStrategies');
		addRetUrl(pr);
		pr.setRedirect(true);
		
		return pr;
	}
	
	public PageReference goLastActivity() {
		String viewId = ApexPages.currentPage().getParameters().get('viewId');
		
		if ( ! String.isEmpty(viewId)) {
			PageReference pr = new PageReference(AUTH_StaticVariables.prefixSite + '/ISA_LastActivity');
			addRetUrl(pr);
			pr.getParameters().put('sId', viewId);
			pr.setRedirect(true);
			
			return pr;
		}
		
		return null;
	}
}