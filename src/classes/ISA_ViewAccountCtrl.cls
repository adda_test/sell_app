public with sharing class ISA_ViewAccountCtrl extends AbstractController {
	public Account__c account {get; set;}
	public List<User__c> users {get; set;}
	
	public ISA_ViewAccountCtrl() {
		super();
		
		this.sObjectLabel = 'Account';
		this.users = new List<User__c>();
	}
	
	public PageReference initPage() {
		PageReference pr = super.init();
		if (pr != null) {
			return pr;
		}
		
		String accountId = ApexPages.currentPage().getParameters().get('sId');
		
		if (! String.isEmpty(accountId)) {
			List<Account__c> accounts = [
					SELECT Name, First_Name__c, Last_Name__c, Phone__c, Email__c, Address1__c, Address2__c, 
						City__c, State__c, Zipcode__c, Company__r.Name, User__c, Web_Site__c, Country__c 
					FROM Account__c 
					WHERE Id = :accountId 
					LIMIT 1];
			
			if ( ! accounts.isEmpty()) {
				this.account = accounts.get(0);
				
				List<AssignContact__c> junctions = [
					SELECT User__r.Name 
					FROM AssignContact__c 
					WHERE Contact__r.Account__c = :accountId 
					LIMIT 10000];
				
				if ( ! junctions.isEmpty()) {
					Set<User__c> usersSet = new Set<User__c>();
					for (AssignContact__c junction : junctions) {
						usersSet.add(
							new User__c(
								Id = junction.User__c, 
								Name = junction.User__r.Name
							));
					}
					
					this.users = new List<User__c>(usersSet);
				}
			}
		} else {
			this.account = new Account__c();
			
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 
					'Selected Account wasn\'t found.'));
		}
		
		return null;
	}
	
	public PageReference doEdit() {
		if (this.account.Id != null) {
			PageReference pr = new PageReference(AUTH_StaticVariables.prefixSite + '/apex/ISA_NewAccount');
			pr.getParameters().put('sId', this.account.Id);
			addRetUrl(pr);
			pr.setRedirect(true);
			
			return pr;
		}
		
		return null;
	}
	
	public PageReference doShowContacts() {
		if (this.account.Id != null) {
			PageReference pr = new PageReference(AUTH_StaticVariables.prefixSite + '/apex/ISA_InfluenceAccount');
			pr.getParameters().put('aId', this.account.Id);
			addRetUrl(pr);
			pr.setRedirect(true);
			
			return pr;
		}
		
		return null;
	}
	
	public PageReference doShowSummary() {
		if (this.account.Id != null) {
			PageReference pr = new PageReference(AUTH_StaticVariables.prefixSite + '/apex/ISA_AccountSummary');
			pr.getParameters().put('Id', this.account.Id);
			addRetUrl(pr);
			pr.setRedirect(true);
			
			return pr;
		}
		
		return null;
	}
	
	public PageReference doShowInsights() {
		if (this.account.Id != null) {
			PageReference pr = new PageReference(AUTH_StaticVariables.prefixSite + '/apex/ISA_AccountInsights');
			pr.getParameters().put('cId', this.account.Company__c);
			pr.getParameters().put('aId', this.account.Id);
			addRetUrl(pr);
			pr.setRedirect(true);
			
			return pr;
		}
		
		return null;
	}
	
	public PageReference doShowStrategies() {
		if (this.account.Id != null) {
			PageReference pr = new PageReference(AUTH_StaticVariables.prefixSite + '/apex/ISA_AccountStrategies');
			pr.getParameters().put('cId', this.account.Company__c);
			pr.getParameters().put('aId', this.account.Id);
			addRetUrl(pr);
			pr.setRedirect(true);
			
			return pr;
		}
		
		return null;
	}
}