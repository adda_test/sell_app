public with sharing class StrategyResultHandler {
	public static Boolean enablesTrigger = true;
	
	public static void recalculateContacts(List<Strategy_Result__c> p_newRecords) {
		recalculateContacts(new Map<Id, Strategy_Result__c>(), p_newRecords);
	}
	
	public static void recalculateContacts(Map<Id, Strategy_Result__c> p_oldRecordMap, 
			List<Strategy_Result__c> p_newRecords) {
		
		Set<Id> contactIds = new Set<Id>();
		for (Strategy_Result__c result : p_newRecords) {
			Strategy_Result__c oldResult = p_oldRecordMap.get(result.Id);
			
			if ((oldResult != null && oldResult.Count_Questions__c != result.Count_Questions__c) || 
					oldResult == null) {
				
				if (result.Contact__c != null) {
					contactIds.add(result.Contact__c);
				}
			}
		}
		
		if ( ! contactIds.isEmpty()) {
			List<Contact__c> contacts = [
				SELECT Id, 
					(SELECT Count_Questions__c 
					FROM Strategy_Resuts__r 
					WHERE Count_Questions__c != null)
				FROM Contact__c 
				WHERE Id IN :contactIds 
				LIMIT 10000];
			
			for (Contact__c contact : contacts) {
				Integer countQuestions = 0;
				
				for (Strategy_Result__c result : contact.Strategy_Resuts__r) {
					countQuestions += (Integer)result.Count_Questions__c;
				}
				
				contact.Count_Strategy_Questions__c = countQuestions;
			}
			
			update contacts;
		}
	}
	
	public static void removeAnswers(List<Strategy_Result__c> p_newRecords) {
		Set<Id> strategyIds = new Set<Id>();
		for (Strategy_Result__c result : p_newRecords) {
			if (result.Strategy_Design__c != null) {
				strategyIds.add(result.Strategy_Design__c);
			}
		}
		
		if ( ! strategyIds.isEmpty()) {
			delete [
				SELECT Id 
				FROM Strategy_Answer__c 
				WHERE Strategy_Question__r.Strategy_Design__c IN :strategyIds 
				LIMIT 10000];
		}
	}
	
	public static void recalculateStrategies(Map<Id, Strategy_Result__c> p_oldRecordMap, List<Strategy_Result__c> p_newRecords) {
		Set<Id> strategyIds = new Set<Id>();
		for (Strategy_Result__c result : p_newRecords) {
			Strategy_Result__c oldResult = p_oldRecordMap.get(result.Id);
			
			if (oldResult != null && oldResult.Is_Completeness__c != result.Is_Completeness__c) {
				strategyIds.add(result.Strategy_Design__c);
			}
		}
		
		if ( ! strategyIds.isEmpty()) {
			List<Strategy_Design__c> strategies = [
				SELECT Id, 
					(SELECT Id 
					FROM Strategy_Resuts__r 
					WHERE Is_Completeness__c = true) 
				FROM Strategy_Design__c 
				WHERE Id IN :strategyIds 
				LIMIT 10000];
			
			for (Strategy_Design__c strategy : strategies) {
				strategy.Contacts_Complete__c = strategy.Strategy_Resuts__r.size();
			}
			
			update strategies;
		}
	}
	
	public static void addStrategyHistory(Map<Id, Strategy_Result__c> p_oldRecordMap, List<Strategy_Result__c> p_newRecords) {
		List<Strategy_History__c> strategyHistories = new List<Strategy_History__c>();
		
		for (Strategy_Result__c result : p_newRecords) {
			Strategy_Result__c oldResult = p_oldRecordMap.get(result.Id);
			
			PageReference prUser = new PageReference(AUTH_StaticVariables.prefixSite + '/ISA_ViewUser');
			prUser.getParameters().put('sId', result.Owner__c);
			
			PageReference prStrategy = new PageReference(AUTH_StaticVariables.prefixSite + '/ISA_ViewStrategy');
			prStrategy.getParameters().put('sId', result.Strategy_Design__c);
			
			PageReference prContact = new PageReference(AUTH_StaticVariables.prefixSite + '/ISA_ViewContact');
			prContact.getParameters().put('sId', result.Contact__c);
			
			if (oldResult != null && oldResult.Is_Completeness__c != result.Is_Completeness__c && 
				result.Is_Completeness__c) {
				
				strategyHistories.add(
					new Strategy_History__c(
						Strategy__c = result.Strategy_Design__c, 
						Message__c = '<b><a href="' + prUser.getUrl() + '">' + result.Owner_Name__c + 
							'</a></b> completed Strategy(<b><a href="' + prStrategy.getUrl() + '">' + 
							result.Strategy_Name__c + '</a></b>) for Contact(<b><a href="' + 
							prContact.getUrl() + '">' + result.Contact_Name__c + '</a></b>)'
					));
			}
		}
		
		insert strategyHistories;
	}
	
	public static void addContactHistory(Map<Id, Strategy_Result__c> p_oldRecordMap, List<Strategy_Result__c> p_newRecords) {
		List<Contact_History__c> contactHistories = new List<Contact_History__c>();
		
		for (Strategy_Result__c result : p_newRecords) {
			Strategy_Result__c oldResult = p_oldRecordMap.get(result.Id);
			
			if (oldResult != null && oldResult.Is_Completeness__c != result.Is_Completeness__c && 
				result.Is_Completeness__c) {
				
				PageReference prUser = new PageReference(AUTH_StaticVariables.prefixSite + '/ISA_ViewUser');
				prUser.getParameters().put('sId', result.Owner__c);
				
				PageReference prContact = new PageReference(AUTH_StaticVariables.prefixSite + '/ISA_ViewContact');
				prContact.getParameters().put('sId', result.Contact__c);
				
				PageReference prStrategy = new PageReference(AUTH_StaticVariables.prefixSite + '/ISA_ViewStrategy');
				prStrategy.getParameters().put('sId', result.Strategy_Design__c);
				
				contactHistories.add(
					new Contact_History__c(
						Contact__c = result.Contact__c, 
						Message__c = '<b><a href="' + prUser.getUrl() + '">' + result.Owner_Name__c + 
							'</a></b> completed Strategy(<b><a href="' + prStrategy.getUrl() + '">' + 
							result.Strategy_Name__c + '</a></b>) for Contact(<b><a href="' + prContact.getUrl() + 
							'">' + result.Contact_Name__c + '</a></b>)'
					));
			}
		}
		
		insert contactHistories;
	}
	
	public static void recalculateShowResults(Map<Id, Strategy_Result__c> p_oldRecordMap, List<Strategy_Result__c> p_newRecords) {
		Set<Id> strategyIds = new Set<Id>();
		Set<Id> contactIds = new Set<Id>();
		for (Strategy_Result__c result : p_newRecords) {
			Strategy_Result__c oldResult = p_oldRecordMap.get(result.Id);
			
			if (oldResult != null && oldResult.isShow__c != result.isShow__c && ! result.IsShow__c) {
				strategyIds.add(result.Strategy_Design__c);
				contactIds.add(result.Contact__c);
			}
		}
		
		if ( ! strategyIds.isEmpty()) {
			List<Strategy_Answer__c> answers = [
				SELECT Contact__c, Strategy_Question__r.Strategy_Design__c 
				FROM Strategy_Answer__c 
				WHERE Contact__c IN :contactIds 
				AND Strategy_Question__r.Strategy_Design__c IN :strategyIds 
				LIMIT 10000];
			
			Map<Id, List<Strategy_Answer__c>> asnwersMap = new Map<Id, List<Strategy_Answer__c>>();
			for (Strategy_Answer__c answer : answers) {
				Id key = answer.Strategy_Question__r.Strategy_Design__c;
				
				if ( ! asnwersMap.containsKey(key)) {
					asnwersMap.put(key, new List<Strategy_Answer__c>());
				}
				asnwersMap.get(key).add(answer);
			}
			
			List<Strategy_Answer__c> deleteAnswers = new List<Strategy_Answer__c>();
			for (Id strategyId : asnwersMap.keySet()) {
				for (Strategy_Answer__c answer : asnwersMap.get(strategyId)) {
					if (contactIds.contains(answer.Contact__c)) {
						deleteAnswers.add(answer);
					}
				}
			}
			
			delete deleteAnswers;
		}
	}
}