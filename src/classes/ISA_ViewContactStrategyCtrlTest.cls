@isTest
private class ISA_ViewContactStrategyCtrlTest {
	
	static User__c tempUser(id id) {
        User__c testUser = new User__c(
            First_Name__c = 'First', 
            Last_Name__c = 'Last', 
            Phone__c = '123455678', 
            Username__c = 'testLogin', 
            Password__c = 'testPassword', 
            Role__c = 'Admin', 
            Company__c = id,
            Expiration_Date__c = Date.today(), 
            Create_Date__c = Date.today(),
            SessionId__c = '1',
            Active__c = true,
            Email__c = 'test@mail.com');
        return testUser;
    }
    
    static Company__c tempCompany() {
        Company__c company = new Company__c(
            Name = 'test name',
            Admin_First_Name__c = 'firstname',
            Admin_Last_Name__c = 'lastname',
            Admin_Email__c = 'test@mail.com',
            Admin_Login__c = 'testLogin',
            Admin_Password__c = 'testPassword',
            Address1__c = 'addr1',
            Address2__c = 'addr2',
            Trial_Start__c = Date.today(),
            Maximum_Users__c = 10);
        return company;
    }
    
    static Account__c tempAccount(id companyId,id userId) {
    	Account__c account = new Account__c(
    		Name = 'test contact',
    		First_Name__c = 'first name',
    		Last_Name__c = 'last name', 
    		Phone__c = '123456', 
    		Email__C = 'test@mail.com', 
    		Address1__c = 'addr1', 
    		Address2__c = 'addr2', 
			City__c = 'City', 
			State__c = 'State',
			Zipcode__c = '1234', 
			Company__c = companyId,
			User__c = userId,
			Web_Site__c = 'web-site',
			Country__c = 'country');
		return account;
    } 
    
	static Contact__c tempContact(id companyId,id userId,id accountId) {
    	Contact__c contact = new Contact__c(
    		Name = 'test contact',
    		First_Name__c = 'first name',
    		Last_Name__c = 'last name', 
    		Phone_No__c = '123456', 
    		Email__C = 'test@mail.com', 
    		Address1__c = 'addr1', 
    		Address2__c = 'addr2', 
			City__c = 'City', 
			State__c = 'State',
			Pincode__c = '1234', 
			Company__c = companyId,
			User__c = userId,
			Account__c = accountId,
			Country__c = 'country');
		return contact;
    }
    
    static Strategy_Design__c tempStrategyDesign(id companyId) {
    	Strategy_Design__c strategyDesign = new Strategy_Design__c(
    		Name = 'Name',
    		Type__c = 'Single',
    		Company__c = companyId);
    	return strategyDesign;
    }
    
    static Strategy_Result__c tempStrategyResult(id strategyDesignId,id contactId) {
    	Strategy_Result__c strategyDesign = new Strategy_Result__c(
    		Name = 'Name',
    		Strategy_Design__c = strategyDesignId,
    		Contact__c = contactId);
    	return strategyDesign;
    }
    
    static Strategy_Question__c tempQuestion(id strategyDesignId) {
    	Strategy_Question__c question = new Strategy_Question__c(
    		Name = 'Name',
    		Title__c = 'Title',
    		Strategy_Design__c = strategyDesignId);
    	return question;
    }
    
    static Strategy_Answer__c tempStrategyAnswer(id contactId, id strategyQuestion) {
    	Strategy_Answer__c strategyAnswer = new Strategy_Answer__c(
    		Name = 'Answer Test',
    		Contact__c = contactId,
    		Strategy_Question__c = strategyQuestion);
    	return strategyAnswer;
	}
    
    @isTest
    private static void testInit() {
    	
        Test.startTest();
        
	    Company__c testCompany = tempCompany();           
        insert testCompany;      
        User__c testUser = tempUser(testCompany.Id);    
        insert testUser; 
        Account__c testAccount = tempAccount(testCompany.Id,testUser.Id);    
        insert testAccount;
        Strategy_Design__c testStrategyDesign = tempStrategyDesign(testCompany.Id);    
        insert testStrategyDesign;
        Contact__c testContact = tempContact(testCompany.Id,testUser.Id,testAccount.Id);    
        insert testContact; 
        Strategy_Result__c testStrategyResult = tempStrategyResult(testStrategyDesign.Id,testContact.Id);
        insert testStrategyResult;
                              
        ApexPages.currentPage().getParameters().put('cId', testContact.Id);
        
        ISA_ViewContactStrategyCtrl controller = new ISA_ViewContactStrategyCtrl();
        controller.initPage();
        
        System.assertNotEquals(null, controller.init());       
        
        Test.stopTest();
    }
    
    @isTest
    private static void testInitWithContactId() {
    	
        Test.startTest();
        
	    Company__c testCompany = tempCompany();           
        insert testCompany;      
        User__c testUser = tempUser(testCompany.Id);    
        insert testUser; 
        Account__c testAccount = tempAccount(testCompany.Id,testUser.Id);    
        insert testAccount;
        Strategy_Design__c testStrategyDesign = tempStrategyDesign(testCompany.Id);    
        insert testStrategyDesign;
        Contact__c testContact = tempContact(testCompany.Id,testUser.Id,testAccount.Id);    
        insert testContact; 
        Strategy_Result__c testStrategyResult = tempStrategyResult(testStrategyDesign.Id,testContact.Id);
        insert testStrategyResult;
        Strategy_Question__c testQuestion = tempQuestion(testStrategyDesign.Id);
        insert testQuestion;
        Strategy_Answer__c testStrategyAnswer = tempStrategyAnswer(testContact.Id, testQuestion.Id);
        insert testStrategyAnswer;
        String testPointOptions = 'one,two,three';
        String testResponseOptions = '10,12,43';
        List<String> testSelectOptions = new List<String>();
        testSelectOptions.add('10');
        testSelectOptions.add('13');
        testSelectOptions.add('16');
                              
        ApexPages.currentPage().getParameters().put('cId', testContact.Id);
        Cookie testCookie = new Cookie('SessionId','1',null,-1,false);
        ApexPages.currentPage().setCookies(new Cookie[] {testCookie});
        
        ISA_ViewContactStrategyCtrl controller = new ISA_ViewContactStrategyCtrl();
        controller.initPage();
        
        System.assertEquals(null, controller.init()); 
        
        controller.doSave(); 
        
        System.assertNotEquals(null,controller.getSelectedPoints(testPointOptions,testResponseOptions,testSelectOptions));    
        
        Test.stopTest();
    }
}