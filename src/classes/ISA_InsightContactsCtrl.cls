public with sharing class ISA_InsightContactsCtrl extends AbstractController {
	private Id insightId;
	
	public ISA_InsightContactsCtrl() {
		super();
		
		this.insightId = null;
	}
	
	public PageReference initPage() {
		PageReference pr = super.init();
		
		if (pr != null) {
			return pr;
		}
		
		String sId = ApexPages.currentPage().getParameters().get('sId');
		
		if ( ! String.isEmpty(sId)) {
			this.insightId = sId;
		}
		
		for (Contact__c contact : getContacts()) {
			this.records.add(new ISA_SObjectHelper.sObjectWrapper(contact));
		}
		
		return null;
	}
	
	public List<Contact__c> getContacts() {
		List<Question__c> questions = [
			SELECT Survey_Design__r.Company__c 
			FROM Question__c 
			WHERE Id = :this.insightId 
			LIMIT 1];
		
		return [
			SELECT Name 
			FROM Contact__c 
			WHERE Company__c = :questions.get(0).Survey_Design__r.Company__c 
			LIMIT 10000];
	}
}