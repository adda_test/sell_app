public with sharing class ISA_NewAccountCtrl extends AbstractController {
	public Account__c account {get; set;}
	public List<SelectOption> companyOptions {get; set;}
	public String selectedCompany {get; set;}
	public String selectedUser {get; set;}
	public List<SelectOption> userOptions {get; set;}
	public List<User__c> users {get; set;}
	
	public List<String> selectedUserIds {get; set;}
	public List<String> selectedUserNames {get; set;}
	
	public Boolean password {get; set;}
	
	/**
	* @description 
	*/
	public ISA_NewAccountCtrl() {
		super();
		
		selectedCompany = null;
		companyOptions = new List<SelectOption> { new SelectOption('', '--None--') };
		
		selectedUser = null;
		userOptions = new List<SelectOption> { new SelectOption('', '--None--') };
		
		this.sObjectLabel = 'Account';
		this.selectedUserIds = new List<String>();
		this.selectedUserNames = new List<String>();
		this.password = false;
		
		this.users = new List<User__c>();
	}
	
	public PageReference initPage() {
		PageReference pr = super.init();
		if (pr != null) {
			return pr;
		}
		
		String accountId = ApexPages.currentPage().getParameters().get('sId');
		Set<User__c> usersSet = new Set<User__c>();
		Set<Id> userIds = new Set<Id>();
		
		if (! String.isEmpty(accountId)) {
			List<Account__c> accounts = [
					SELECT Name, First_Name__c, Last_Name__c, Phone__c, Email__c, Address1__c, Address2__c, 
						City__c, State__c, Zipcode__c, Company__c, User__c, Web_Site__c, Country__c 
					FROM Account__c 
					WHERE Id = :accountId 
					LIMIT 1];
			
			if ( ! accounts.isEmpty()) {
				this.account = accounts.get(0);
				
				this.selectedCompany = this.account.Company__c;
				
				List<AssignContact__c> junctions = [
					SELECT User__r.Name 
					FROM AssignContact__c 
					WHERE Contact__r.Account__c = :accountId 
					LIMIT 10000];
				
				if ( ! junctions.isEmpty()) {
					for (AssignContact__c junction : junctions) {
						if ( ! userIds.contains(junction.User__c)) {
							users.add(
								new User__c(
									Id = junction.User__c, 
									Name = junction.User__r.Name
								));
							
							userIds.add(junction.User__c);
						}
					}
				}
			}
		} else {
			this.account = new Account__c();
		}
		
		List<Company__c> companies = [
				SELECT Name, 
					(SELECT Name 
					FROM Users__r)
				FROM Company__c 
				WHERE Id = :this.userCompany 
				LIMIT 1];
		
		if ( ! companies.isEmpty()) {
			this.companyOptions.add(new SelectOption(companies.get(0).Id, companies.get(0).Name));
			this.selectedCompany = companies.get(0).Id;
			
			for (User__c user : companies.get(0).Users__r) {
				if ( ! userIds.contains(user.Id)) {
					this.userOptions.add(new SelectOption(user.Id, user.Name));
				}
			}
		}
		
		return null;
	}
	
	/**
	* @description 
	*/
	public PageReference doSave() {
		try {
			String userIds = ApexPages.currentPage().getParameters().get('userIds');
			String userNames = ApexPages.currentPage().getParameters().get('userNames');
			
			if ( ! String.isEmpty(userIds) && ! String.isEmpty(userNames)) {
				userIds = userIds.replace(',,', ',');
				userNames = userNames.replace(',,', ',');
				
				this.selectedUserIds = userIds.split(',');
				this.selectedUserNames = userNames.split(',');
			}
			
			account.Company__c = ( ! String.isEmpty(selectedCompany)) ? selectedCompany : null;
			
			account.Owner__c = this.user.Id;
			
			if (chechValidationField(this.account.Phone__c)) {
				ApexPages.addMessage(
					new ApexPages.Message(
						ApexPages.Severity.ERROR, 
						'Phone Number shouldn\'t have non number characters.'));
				
				this.password = true;
				
				return null;
			}
			this.password = false;
			
			upsert account;
			
			List<AssignContact__c> deleteJunctions = new List<AssignContact__c>();
			List<AssignContact__c> insertJunctions = new List<AssignContact__c>();
			
			List<Contact__c> contacts = [
				SELECT First_Name__c, Last_Name__c 
				FROM Contact__c 
				WHERE Account__c = :this.account.Id 
				LIMIT 10000];
			
			if ( ! contacts.isEmpty()) {
				List<AssignContact__c> junctions = [
					SELECT Contact__c, User__c 
					FROM AssignContact__c 
					WHERE Contact__c IN :new Map<Id, Contact__c>(contacts).keySet() 
					LIMIT 10000];
				
				Set<String> selectedUserIdSet = new Set<String>(this.selectedUserIds);
				for (Integer i = 0; i < junctions.size(); i++) {
					if ( ! selectedUserIdSet.contains(junctions.get(i).User__c)) {
						deleteJunctions.add(junctions.get(i));
					} else {
						selectedUserIdSet.remove(junctions.get(i).User__c);
						junctions.remove(i--);
					}
				}
				
				Map<Id, User__c> usersMap = new Map<Id, User__c>([
					SELECT Name 
					FROM User__c 
					WHERE Id IN :this.selectedUserIds 
					LIMIT 10000]);
				
				for (String userId : selectedUserIdSet) {
					for (Contact__c contact : contacts) {
						insertJunctions.add(
							new AssignContact__c(
								Contact__c = contact.Id, 
								User__c = userId, 
								User_Name__c = usersMap.get(userId).Name, 
								Contact_Name__c = contact.First_Name__c + ' ' + contact.Last_Name__c, 
								Owner_Name__c = this.username, 
								Owner__c = this.user.Id
							));
					}
				}
				
				insert insertJunctions;
				
				delete deleteJunctions;
				
				PageReference pr = new PageReference(AUTH_StaticVariables.prefixSite + '/ISA_ViewAccount');
				pr.getParameters().put('sId', this.account.Id);
				pr.getParameters().put('returnUrl', AUTH_StaticVariables.prefixSite + '/ISA_ListViewAccount');
				pr.setRedirect(true);
				
				return pr;
			} else {
				this.selectedUserIds = new List<String>();
				this.selectedUserNames = new List<String>();
				
				ApexPages.addMessage(
					new ApexPages.Message(
						ApexPages.Severity.ERROR, 
						'Please, check Account record. This Account haven\'t elated Contact records.'));
				return null;
			}
			
			
		} catch(Exception ex) {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
			
			return null;
		}
	}
	
	public void applySelectedUsers() {
		String userIds = ApexPages.currentPage().getParameters().get('userIds');
		String userNames = ApexPages.currentPage().getParameters().get('userNames');
		
		if ( ! String.isEmpty(userIds) && ! String.isEmpty(userNames)) {
			this.selectedUserIds = userIds.split(',');
			this.selectedUserNames = userNames.split(',');
		}
	}
	
	@TestVisible protected override void collectRetParams(PageReference pr) {
		pr.getParameters().put('sId',ApexPages.currentPage().getParameters().get('sId'));
	}
	
	private Boolean chechValidationField(String p_field) {
		if ( ! String.isEmpty(p_field)) {
			Pattern p = Pattern.compile('\\w*[a-z]+\\w*');
			Matcher pm = p.matcher( p_field );
			
			if ( pm.matches() ){
				return true;
			} else {
				return false;
			}
		}
		return false;
	}
}