/**
* @description 
*/
@isTest
private class ISA_NewCompanyCtrlTest {
    
    static Company__c passCompany(string password) {
        Company__c company = new Company__c(
            Name = 'test name',
            Admin_First_Name__c = 'firstname',
            Admin_Last_Name__c = 'lastname',
            Admin_Email__c = 'test@mail.com',
            Admin_Login__c = 'testLogin',
            Admin_Password__c = password,
            Address1__c = 'addr1',
            Address2__c = 'addr2',
            Trial_Start__c = Date.today(),
            Maximum_Users__c = 10);
        return company;
    }
    
      static User__c idCompanyUser(id id) {
        User__c user = new User__c(
            First_Name__c = 'First', 
            Last_Name__c = 'Last', 
            Phone__c = '123455678', 
            Username__c = 'testLogin', 
            Password__c = 'qwerty', 
            Role__c = 'Company Admin', 
            Expiration_Date__c = Date.today(), 
            Company__c = id,
            SessionId__c = '1',
            Active__c = true,
            Create_Date__c = Date.today(),
            Email__c = 'test@mail.com');
        return user;
    }
        
    @isTest
    private static void testSaveRequireFields() {
        Test.startTest();
        
        ISA_NewCompanyCtrl controller = new ISA_NewCompanyCtrl();
        
        controller.company = passCompany('testPassword');
        controller.doSave();
        System.assertNotEquals(null, controller.company.Id); 
       
        controller.company = passCompany('pass');       
        controller.doSave();       
        System.assertEquals(null, controller.company.Id);          
        
        Test.stopTest();
    } 
    
     @isTest
     private static void testSaveUserNull() {
        
        Test.startTest();
        
        Company__c testCompany = passCompany('testPassword');            
        insert testCompany;                
        User__c testUser = idCompanyUser(testCompany.Id);   
        insert testUser; 
        
        ISA_NewCompanyCtrl controller = new ISA_NewCompanyCtrl();
        controller.company = testCompany;               
        controller.initPage();
        controller.doSave();
        System.assertEquals(null, controller.doSave());              
        
        Test.stopTest();
     } 
    
     @isTest
     private static void testSaveUserNotNull() {
        
        Test.startTest();
        
        Company__c testCompany = new Company__c(
        	Name = 'test name',
            Admin_First_Name__c = 'firstname',
            Admin_Last_Name__c = 'lastname',
            Admin_Email__c = 'test@mail.com',
            Admin_Login__c = 'newLogin',
            Admin_Password__c = 'testPassword',
            Address1__c = 'addr1',
            Address2__c = 'addr2',
            Trial_Start__c = Date.today(),
            Maximum_Users__c = 10);            
        insert testCompany;                
        User__c testUser = idCompanyUser(testCompany.Id);   
        insert testUser; 
        
        ApexPages.currentPage().getParameters().put('sId', testCompany.Id);
        Cookie testCookie = new Cookie('SessionId','1',null,-1,false);
        ApexPages.currentPage().setCookies(new Cookie[] {testCookie});
        
        ISA_NewCompanyCtrl controller = new ISA_NewCompanyCtrl();
               
        controller.initPage();
        controller.doSave();
        System.assertNotEquals(null, controller.doSave());              
        
        Test.stopTest();
    } 
    
     @isTest
     private static void testInitWithCompanyId() {
        
        Test.startTest();
        
        Company__c testCompany = passCompany('testPassword');           
        insert testCompany;      
        User__c testUser = idCompanyUser(testCompany.Id);    
        insert testUser; 
                              
        ApexPages.currentPage().getParameters().put('sId', testCompany.Id);
        Cookie testCookie = new Cookie('SessionId','1',null,-1,false);
        ApexPages.currentPage().setCookies(new Cookie[] {testCookie});
        
        ISA_NewCompanyCtrl controller = new ISA_NewCompanyCtrl();
        controller.initPage();
        System.assertEquals(testCompany.Id, controller.company.Id);
        System.assertEquals(testUser.Id, controller.companyUser.Id);      
        
        Test.stopTest();
    } 
    
     @isTest
     private static void testInitWithoutCompanyId() {
       
        Company__c testCompany = passCompany('testPassword');           
        insert testCompany;      
        User__c testUser = idCompanyUser(testCompany.Id);    
        insert testUser; 
                              
        Cookie testCookie = new Cookie('SessionId','1',null,-1,false);
        ApexPages.currentPage().setCookies(new Cookie[] {testCookie});
        
        Test.startTest();
        
        ISA_NewCompanyCtrl controller = new ISA_NewCompanyCtrl();
        controller.initPage();
        System.assertEquals(null, controller.company.Id);
        
        Test.stopTest();
    } 
     
    @isTest
    private static void testGetTypes() {
        Test.startTest();
        
        ISA_NewCompanyCtrl controller = new ISA_NewCompanyCtrl();
        controller.company = passCompany('qwerty1234567');
        controller.initPage();
        controller.doSave();
        controller.getTypes();
        
        System.assertNotEquals(null, controller.getTypes());
        
        Test.stopTest();
    }   
     
    @isTest
    private static void testCollectRetParams() {
        Test.startTest();
        
        PageReference pageRef = ApexPages.currentPage();       
        Company__c testCompany = passCompany('testPassword');           
        insert testCompany;      
                              
        ApexPages.currentPage().getParameters().put('sId', testCompany.Id);
         
        ISA_NewCompanyCtrl controller = new ISA_NewCompanyCtrl();
        controller.collectRetParams(pageRef);
        
        System.assertEquals(ApexPages.currentPage().getParameters().get('sId'), pageRef.getParameters().get('sId'));
        
        Test.stopTest();
    }
    
    @isTest
    private static void testCancel() {
        Test.startTest();
        
        ISA_NewCompanyCtrl controller = new ISA_NewCompanyCtrl();
        controller.doCancel();
        
        System.assertNotEquals(null, controller.doCancel());
        
        Test.stopTest();
    }

}