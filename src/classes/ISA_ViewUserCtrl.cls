public with sharing class ISA_ViewUserCtrl extends AbstractController {
	public User__c currentUser {get; set;}
	
	public ISA_ViewUserCtrl() {
		String userId = ApexPages.currentPage().getParameters().get('sId');
		this.sObjectLabel = 'User';
		
		if (! String.isEmpty(userId)) {
			List<User__c> users = [
					SELECT Name, First_Name__c, Last_Name__c, Phone__c, Username__c, Password__c, 
						Create_Date__c, Company__c, Email__c, Active__c 
					FROM User__c 
					WHERE Id = :userId 
					LIMIT 1];
			
			if ( ! users.isEmpty()) {
				this.currentUser = users.get(0);
			}
		} else {
			this.currentUser = new User__c();
			
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 
					'Selected User wasn\'t found.'));
		}
	}
	
	public PageReference doEdit() {
		if (this.currentUser.Id != null) {
			PageReference pr = new PageReference('/apex/ISA_NewUser');
			pr.getParameters().put('sId', this.currentUser.Id);
			addRetUrl(pr);
			return pr;
		}
		
		return null;
	}
	
	/*
	protected override void collectRetParams(PageReference pr) {
		pr.getParameters().put('sId',ApexPages.currentPage().getParameters().get('sId'));
	}
	*/
}