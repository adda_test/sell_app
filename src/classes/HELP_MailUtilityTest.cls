@isTest
private class HELP_MailUtilityTest {
	
    @isTest
    private static void testInit() {
    	
        Test.startTest();
        
        HELP_MailUtility controller = new HELP_MailUtility();
        String emailFilled = 'test@mail.com';
        String emailEmpty = '';
        String subject = 'subject';
        String htmlBody = 'html';
                
        System.assertNotEquals(null, HELP_MailUtility.sendQuickMail(emailFilled,subject,htmlBody));       
        System.assertNotEquals(null, HELP_MailUtility.sendQuickMail(emailEmpty,subject,htmlBody));
        
        Test.stopTest();
    }
}