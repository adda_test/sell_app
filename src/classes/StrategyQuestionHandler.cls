public with sharing class StrategyQuestionHandler {
	public static Boolean enablesTrigger = true;
	
	public static void updateContacts(List<Strategy_Question__c> p_newRecords) {
		Set<Id> strategyIds = new Set<Id>();
		for (Strategy_Question__c question : p_newRecords) {
			strategyIds.add(question.Strategy_Design__c);
		}
		
		if ( ! strategyIds.isEmpty()) {
			Map<Id, Strategy_Design__c> strategies = new Map<Id, Strategy_Design__c>([
				SELECT Id, 
					(SELECT Id 
					FROM Strategy_Questions__r)
				FROM Strategy_Design__c 
				WHERE Id IN :strategyIds 
				LIMIT 10000]);
			
			List<AssignStrategy__c> junctions = [
				SELECT Account__c, Strategy__c 
				FROM AssignStrategy__c 
				WHERE Strategy__c IN :strategyIds 
				LIMIT 10000];
			
			if ( ! junctions.isEmpty()) {
				Set<Id> accountIds = new Set<Id>();
				for (AssignStrategy__c junction : junctions) {
					if (junction.Account__c != null) {
						accountIds.add(junction.Account__c);
					}
				}
				
				Map<Id, Account__c> accounts = new Map<Id, Account__c>([
					SELECT Id, 
						(SELECT Id 
						FROM Contacts__r) 
					FROM Account__c 
					WHERE Id IN :accountIds 
					LIMIT 10000]);
				
				if ( ! accounts.isEmpty()) {
					Map<Id, Integer> contactToCountQuestionsMap = new Map<Id, Integer>();
					
					for (AssignStrategy__c junction : junctions) {
						if (accounts.containsKey(junction.Account__c)) {
							Account__c account = accounts.get(junction.Account__c);
							for (Contact__c contact : account.Contacts__r) {
								Integer countQuestions = 0;
								
								Strategy_Design__c strategy = strategies.get(junction.Strategy__c);
								
								if ( ! contactToCountQuestionsMap.containsKey(contact.Id)) {
									contactToCountQuestionsMap.put(contact.Id, 0);
								}
								
								contactToCountQuestionsMap.put(contact.Id, 
									contactToCountQuestionsMap.get(contact.Id) + strategy.Strategy_Questions__r.size());
							}
						}
					}
					
					List<Contact__c> updateContacts = new List<Contact__c>();
					for (Id contactId : contactToCountQuestionsMap.keySet()) {
						updateContacts.add(
							new Contact__c(
								Id = contactId, 
								Count_Strategy_Questions__c = contactToCountQuestionsMap.get(contactId)
							));
					}
					
					update updateContacts;
				}
			}
		}
	}
	
	public static void recalculateResults(List<Strategy_Question__c> p_newRecords) {
		Set<Id> strategyIds = new Set<Id>();
		for (Strategy_Question__c question : p_newRecords) {
			if (question.Strategy_Design__c != null) {
				strategyIds.add(question.Strategy_Design__c);
			}
		}
		
		if ( ! strategyIds.isEmpty()) {
			List<Strategy_Design__c> strategies = [
				SELECT Id, 
					(SELECT Id 
					FROM Strategy_Resuts__r), 
					(SELECT Id 
					FROM Strategy_Questions__r)
				FROM Strategy_Design__c 
				WHERE Id IN :strategyIds 
				LIMIT 10000];
			
			List<Strategy_Result__c> updateResults = new List<Strategy_Result__c>();
			for (Strategy_Design__c strategy : strategies) {
				for (Strategy_Result__c result : strategy.Strategy_Resuts__r) {
					updateResults.add(
						new Strategy_Result__c(
							Id = result.Id, 
							Count_Questions__c = strategy.Strategy_Questions__r.size()
						));
				}
			}
			
			update updateResults;
		}
	}
	
	public static void removeAnswers(List<Strategy_Question__c> p_newRecords) {
		delete [
			SELECT Id 
			FROM Strategy_Answer__c 
			WHERE Strategy_Question__c IN :new Map<Id, Strategy_Question__c>(p_newRecords).keySet() 
			LIMIT 10000];
	}
}