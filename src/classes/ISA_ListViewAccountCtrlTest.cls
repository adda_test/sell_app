@isTest
private class ISA_ListViewAccountCtrlTest {

	static User__c tempUser(id id) {
        User__c testUser = new User__c(
            First_Name__c = 'First', 
            Last_Name__c = 'Last', 
            Phone__c = '123455678', 
            Username__c = 'testLogin', 
            Password__c = 'testPassword', 
            Role__c = 'Company User', 
            Company__c = id,
            Expiration_Date__c = Date.today(), 
            Create_Date__c = Date.today(),
            SessionId__c = '1',
            Active__c = true,
            Email__c = 'test@mail.com');
        return testUser;
    }
    
    static Company__c tempCompany() {
        Company__c company = new Company__c(
            Name = 'test name',
            Admin_First_Name__c = 'firstname',
            Admin_Last_Name__c = 'lastname',
            Admin_Email__c = 'test@mail.com',
            Admin_Login__c = 'testLogin',
            Admin_Password__c = 'testPassword',
            Address1__c = 'addr1',
            Address2__c = 'addr2',
            Trial_Start__c = Date.today(),
            Maximum_Users__c = 10);
        return company;
    }
        
	static Account__c tempAccount(id companyId,id userId) {
    	Account__c account = new Account__c(
    		Name = 'test contact',
    		First_Name__c = 'first name',
    		Last_Name__c = 'last name', 
    		Phone__c = '123456', 
    		Email__C = 'test@mail.com', 
    		Address1__c = 'addr1', 
    		Address2__c = 'addr2', 
			City__c = 'City', 
			State__c = 'State',
			Zipcode__c = '1234',
			No_Data__c = 7,
			No_Strategy__c = 0,
			Contacts__c = 0,
			Count_Answers__c = 3,
			Count_Questions__c = 10,
			Sum_Response__c = 5,
			Sum_Max_Response__c = 14, 
			Company__c = companyId,
			User__c = userId,
			Web_Site__c = 'web-site',
			Country__c = 'country');
		return account;
    } 
    
   	static Contact__c tempContact(id companyId,id userId,id accountId) {
    	Contact__c contact = new Contact__c(
    		Name = 'test contact',
    		First_Name__c = 'first name',
    		Last_Name__c = 'last name', 
    		Phone_No__c = '123456', 
    		Email__C = 'test@mail.com', 
    		Address1__c = 'addr1', 
    		Address2__c = 'addr2', 
			City__c = 'City', 
			State__c = 'State',
			Pincode__c = '1234', 
			Company__c = companyId,
			User__c = userId,
			Account__c = accountId,
			Country__c = 'country');
		return contact;
    }
    
    @isTest
    private static void testInit() {
    	
        Test.startTest();
        
        Company__c testCompany = tempCompany();           
        insert testCompany;      
        User__c testUser = tempUser(testCompany.Id);    
        insert testUser; 
        Account__c testAccount = tempAccount(testCompany.Id,testUser.Id);   
        insert testAccount; 
        Contact__c testContact = tempContact(testCompany.Id,testUser.Id,testAccount.Id);    
        insert testContact; 

        ISA_ListViewAccountCtrl controller = new ISA_ListViewAccountCtrl();
        controller.initPage();
        
        System.assertNotEquals(null, controller.initPage());       
        
        Test.stopTest();
    } 
    
    @isTest
    private static void testInitWithUser() {
    	
        Test.startTest();
        
        Company__c testCompany = tempCompany();           
        insert testCompany;      
        User__c testUser = tempUser(testCompany.Id);    
        insert testUser; 
        Account__c testAccount = tempAccount(testCompany.Id,testUser.Id);   
        insert testAccount; 
        Contact__c testContact = tempContact(testCompany.Id,testUser.Id,testAccount.Id);    
        insert testContact; 

		ApexPages.currentPage().getParameters().put('recordId', testUser.Id);
 		Cookie testCookie = new Cookie('SessionId','1',null,-1,false);
        ApexPages.currentPage().setCookies(new Cookie[] {testCookie});
        
        ISA_ListViewAccountCtrl controller = new ISA_ListViewAccountCtrl();
        controller.selectedUser = testUser.Id;
        controller.initPage();
        //controller.doAssignTeam();
        //controller.doAssignUser();
        controller.removeUser();
        controller.addAssignTeam();       
        
        System.assertEquals(null, controller.initPage());  
       
        Test.stopTest();
    } 
}