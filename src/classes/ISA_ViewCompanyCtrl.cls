public with sharing class ISA_ViewCompanyCtrl extends AbstractController {
	public Company__c company {get; set;}
	
	public List<WrapperCollection.LineChartWrapper> chartList {get; set;}
	public Decimal insightIndex {get; set;}
	public Decimal completeness {get; set;}
	public Decimal users {get; set;}
	
	private Integer userCount;
	private String companyId;
	
	
	public ISA_ViewCompanyCtrl() {
		this.companyId = ApexPages.currentPage().getParameters().get('sId');
		this.sObjectLabel = 'Company';
		this.chartList = new List<WrapperCollection.LineChartWrapper>();
		this.insightIndex = 0;
		this.completeness = 0;
		this.users = 0;
	}
	
	public PageReference initPage() {
		PageReference afterInit = init();
		if (afterInit != null) {
			return afterInit;
		}
		
		if (! String.isEmpty(this.companyId)) {
			List<Company__c> companies = [
					SELECT Name, Address1__c, Address2__c, City__c, State__c, Pincode__c, 
						Admin_First_Name__c, Admin_Last_Name__c, Admin_Phone__c, 
						Admin_Email__c, Admin_Login__c, Admin_Password__c, Trial_Start__c, 
						Maximum_Users__c, Trial__c, Trial_End__c, Type__c, CreatedDate, Active__c, 
						Country__c						
					FROM Company__c 
					WHERE Id = :this.companyId 
					LIMIT 1];					
			
			if ( ! companies.isEmpty()) {
				this.company = companies.get(0);
				// get user count
				List<User__c> users = [SELECT Id FROM User__c WHERE Role__c = 'Company User' AND Company__c = :this.companyId];
				if (users != null && !users.isEmpty()) {
					this.userCount = users.size();
				}
			}
			
			fillLineChart();
		} else {
			this.company = new Company__c();
			
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 
					'Selected Company wasn\'t found.'));
		}
		return null;
	} 
	
	public void fillLineChart() {
		this.chartList = ChartUtils.getLineChartList(this.companyId);
		
		if ( ! this.chartList.isEmpty()) {
			this.insightIndex = this.chartList.get(this.chartList.size() - 1).insightIndex;
			this.completeness = this.chartList.get(this.chartList.size() - 1).completeness;
			this.users = this.chartList.get(this.chartList.size() - 1).users;
		}
		System.debug('----- this.chartList=' +  this.chartList);
	}
	
	public PageReference doEdit() {
		if (this.company.Id != null) {
			PageReference pr = new PageReference('/apex/ISA_NewCompany');
			pr.getParameters().put('sId', this.company.Id);
			addRetUrl(pr);
			return pr;
		}
		
		return null;
	}
		
	public PageReference doNewUser() {
		if (this.company.Id != null) {
			if (this.userCount >= this.company.Maximum_Users__c) {
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 
						'You can not create new user - user limit for this company.'));
				return null;
			}
			// check user count
			PageReference pr = new PageReference('/apex/ISA_NewUser');
			pr.getParameters().put('companyId', this.company.Id);
			addRetUrl(pr);
			return pr;
		}
		
		return null;
	}
	
	public PageReference doRedirectUsers() {
		PageReference pr = new PageReference(AUTH_StaticVariables.prefixSite + '/ISA_ViewCompanyUsers');
		pr.getParameters().put('cId', this.companyId);
		addRetUrl(pr);
		
		pr.setRedirect(true);
		
		return pr;
	}
	
	public PageReference doRemoveCompany() {
		delete new Company__c(Id = this.companyId);
		
		PageReference pr = new PageReference(AUTH_StaticVariables.prefixSite + '/ISA_ListViewCompany');
		pr.setRedirect(true);
		
		return pr;
	}
}