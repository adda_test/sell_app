public with sharing class ISA_AccountContactsCtrl extends AbstractController {
	public String accountName {get; set;}
	public List<ISA_SObjectHelper.SObjectWrapper> records {get; set;}
	
	private String accountId;
	
	public ISA_AccountContactsCtrl() {
		super();
		
		this.accountName = '';
		this.records = new List<ISA_SObjectHelper.SObjectWrapper>();
	}
	
	public PageReference initPage() {
		PageReference pr = super.init();
		
		if (pr != null) {
			return pr;
		}
		
		this.accountId = ApexPages.currentPage().getParameters().get('aId');
		
		if ( ! String.isEmpty(this.accountId)) {
			List<Account__c> accounts = [
				SELECT Name 
				FROM Account__c 
				WHERE Id = :this.accountId];
			
			if ( ! accounts.isEmpty()) {
				this.accountName = accounts.get(0).Name;
				
				List<Contact__c> contacts = [
					SELECT Name, Insight_Index__c, Completeness_Score__c, Influence__c, Last_Activity__c 
					FROM Contact__c 
					WHERE Account__c = :this.accountId 
					LIMIT 10000];
				
				for (Contact__c contact : contacts) {
					this.records.add(new ISA_SObjectHelper.SObjectWrapper(contact));
				}
			}
		}
		
		return null;
	}
	
	public override PageReference doCancel() {
		PageReference pr = new PageReference(AUTH_StaticVariables.prefixSite + '/ISA_ViewAccount');
		pr.getParameters().put('sId', this.accountId);
		pr.setRedirect(true);
		
		return pr;
	}
}