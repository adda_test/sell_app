public with sharing class ISA_ViewStrategyContactsCtrl extends AbstractController {
	public Boolean isCompletedContacts {get; set;}
	public List<ISA_SObjectHelper.sObjectWrapper> contacts {get; set;}
	public String strategyName {get; set;}
	
	private Id companyId;
	private String strategyId;
	
	public ISA_ViewStrategyContactsCtrl() {
		super();
		
		this.companyId = null;
		this.strategyId = '';
		this.isCompletedContacts = false;
		this.strategyName = '';
		this.contacts = new List<ISA_SObjectHelper.sObjectWrapper>();
	}
	
	public PageReference initPage() {
		PageReference pr = super.init();
		
		if (pr != null) {
			return pr;
		}
		
		this.strategyId = ApexPages.currentPage().getParameters().get('sId');
		String completeContacts = ApexPages.currentPage().getParameters().get('isComplete');
		
		if ( ! String.isEmpty(completeContacts)) {
			this.isCompletedContacts = true;
		}
		
		this.contacts.clear();
		if ( ! String.isEmpty(this.strategyId)) {
			List<Strategy_Design__c> strategies = [
				SELECT Company__c, Name 
				FROM Strategy_Design__c 
				WHERE Id = :this.strategyId 
				LIMIT 1];
			
			if ( ! strategies.isEmpty()) {
				this.companyId = strategies.get(0).Company__c;
				this.strategyName = strategies.get(0).Name;
				
				List<Schema.FieldSetMember> fieldSetMembers = globalDescribe.get('Contact__c').getDescribe()
						.FieldSets.getMap().get('MainView').getFields();
				
				Map<String, Schema.SObjectField> sObjectFieldMap = globalDescribe.get('Contact__c').getDescribe()
						.fields.getMap();
				
				for (Schema.FieldSetMember member : fieldSetMembers) {
					sortFields.add(member.getLabel());
					fieldSetMap.put(member.getLabel(), member.getFieldPath());
					
					sortableMap.put(member.getFieldPath(), sObjectFieldMap.get(member.getFieldPath()).getDescribe().isSortable());
				}
				
				this.contacts = getRecords();
			}
		}
		
		return null;
	}
	
	private List<ISA_SObjectHelper.sObjectWrapper> getRecords() {
		List<ISA_SObjectHelper.sObjectWrapper> returnList = new List<ISA_SObjectHelper.sObjectWrapper>();
		
		List<AssignStrategy__c> junctions = [
			SELECT Account__c 
			FROM AssignStrategy__c 
			WHERE Strategy__c = :this.strategyId 
			LIMIT 10000];
		
		if ( ! junctions.isEmpty()) {
			Set<Id> accountIds = new Set<Id>();
			for (AssignStrategy__c junction : junctions) {
				accountIds.add(junction.Account__c);
			}
			
			String orderClause = ( ! String.isEmpty(this.sortField) ? 
				' ORDER BY ' + this.sortField + ' ' + this.sortOrder + ' NULLS LAST ' : 
				' ORDER BY Name ASC NULLS LAST ');
			
			String resultQuery = (this.isCompletedContacts) 
				? ', (SELECT Is_Completeness__c FROM Strategy_resuts__r WHERE Strategy_Design__c = \'' + this.strategyId + '\')'
				: '';
			
			List<Contact__c> selectedRecords = Database.query(
				'SELECT Name, ' + String.join(fieldSetMap.values(), ', ') + 
					', Account__c, AccountName__c, Next_Strategy_Name__c' + resultQuery + 
				' FROM Contact__c ' + 
				' WHERE Account__c IN :accountIds ' + 
				orderClause + 
				' LIMIT 50000');
			
			System.debug('---' + 'SELECT Name, ' + String.join(fieldSetMap.values(), ', ') + 
					', Account__c, AccountName__c, Next_Strategy_Name__c' + resultQuery + 
				' FROM Contact__c ' + 
				' WHERE Account__c IN :accountIds ' + 
				orderClause + 
				' LIMIT 50000');
			
			for (Contact__c contact : selectedRecords) {
				System.debug('---' + contact.Strategy_resuts__r + '-----' + contact.Name);
				if (this.isCompletedContacts && ! contact.Strategy_resuts__r.isEmpty() && 
					contact.Strategy_resuts__r.get(0).Is_Completeness__c) {
					
					returnList.add(new ISA_SObjectHelper.sObjectWrapper(contact));
				} else if ( ! this.isCompletedContacts) {
					returnList.add(new ISA_SObjectHelper.sObjectWrapper(contact));
				}
			}
		}
		
		return returnList;
	}
	
	protected override void collectRetParams(PageReference pr) {
		pr.getParameters().put('sId', ApexPages.currentPage().getParameters().get('sId'));
	}
	
	public PageReference doDisplayAccount() {
		PageReference pr;
		
		String accountId = Apexpages.currentPage().getParameters().get('accountId');
		
		if ( ! String.isEmpty(accountId)) {
			pr = new PageReference(AUTH_StaticVariables.prefixSite + '/ISA_ViewAccount');
			pr.getParameters().put('sId', accountId);
			addRetUrl(pr);
			pr.setRedirect(true);
		}
		
		return pr;
	}
	
	public PageReference goNextStrategy() {
		String viewId = ApexPages.currentPage().getParameters().get('viewId');
		
		if ( ! String.isEmpty(viewId)) {
			PageReference pr = new PageReference(AUTH_StaticVariables.prefixSite + '/ISA_ViewContact');
			pr.getParameters().put('sId', viewId);
			addRetUrl(pr);
			pr.setRedirect(true);
			
			return pr;
		}
		
		return null;
	}
	
	public pageReference goInfluencePage() {
		String viewId = ApexPages.currentPage().getParameters().get('viewId');
		
		if ( ! String.isEmpty(viewId)) {
			PageReference pr = new PageReference(AUTH_StaticVariables.prefixSite + '/ISA_InfluenceAccount');
			pr.getParameters().put('aId', viewId);
			addRetUrl(pr);
			pr.setRedirect(true);
			
			return pr;
		}
		
		return null;
	}
	
	public PageReference goLastActivity() {
		String viewId = ApexPages.currentPage().getParameters().get('viewId');
		
		if ( ! String.isEmpty(viewId)) {
			PageReference pr = new PageReference(AUTH_StaticVariables.prefixSite + '/ISA_LastActivity');
			addRetUrl(pr);
			pr.getParameters().put('sId', viewId);
			pr.setRedirect(true);
			
			return pr;
		}
		
		return null;
	}
}