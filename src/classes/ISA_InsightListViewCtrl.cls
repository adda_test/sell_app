public with sharing class ISA_InsightListViewCtrl extends AbstractController {
	//PROPERTY
	public Map<String, String> sObjectNameMap {get; set;}
	public Map<String, String> fieldSetMap {get; set;}
	public Map<String, Boolean> sortableMap {get; set;}
	
	public String sortField {get; set;}
	public String sortOrder {get; set;}
	
	public Integer pageIndex {get; set;}
	public Integer countOfPage {get; set;}
	public Integer countRecords {get; set;}
	public List<SelectOption> countRecordOptions {get; set;}
	public String selectedCountRecords {get; set;}
	
	public List<ISA_SObjectHelper.sObjectWrapper> records {get; set;}
	
	//PRIVATE
	private Map<String, SObjectType> globalDescribe;
	private List<Schema.FieldSetMember> fieldSetMembers;
	private Map<String, Schema.SObjectField> sObjectFieldMap;
	
	//CONSTANTS
	private final String OBJECTTYPE = 'Survey_Design__c';
	
	public ISA_InsightListViewCtrl() {
		super();
		
		this.sObjectNameMap = getObjectsByUserRole();
		
		this.sortField = 'Name';
		this.sortOrder = 'ASC';
		
		this.pageIndex = 1;
		this.countOfPage = 1;
		this.countRecords = 10;
		
		this.countRecordOptions = new List<SelectOption> {
			new SelectOption('10','10'), 
			new SelectOption('20','20'), 
			new SelectOption('50','50'), 
			new SelectOption('100','100'), 
			new SelectOption('200','200')
		};
		this.selectedCountRecords = '10';
		
		this.records = new List<ISA_SObjectHelper.sObjectWrapper>();
		this.fieldSetMap = new Map<String, String>();
		this.sortableMap = new Map<String, Boolean> {'Name' => true};
		
		this.globalDescribe = Schema.getGlobalDescribe();
		this.fieldSetMembers = this.globalDescribe.get(OBJECTTYPE).getDescribe().FieldSets.getMap()
				.get('MainView').getFields();
		
		sObjectFieldMap = globalDescribe.get(OBJECTTYPE).getDescribe().fields.getMap();
		
		//Collect fields that can be viewed.
		for (Schema.FieldSetMember field : this.fieldSetMembers) {
			this.fieldSetMap.put(field.getLabel(), field.getFieldPath());
			
			this.sortableMap.put(field.getFieldPath(), this.sObjectFieldMap.get(field.getFieldPath()).getDescribe().isSortable());
		}
		
		fillRecords();
	}
	
	public override void fillRecords() {
		this.records.clear();
		this.fieldSetMap.clear();
		this.sortableMap.clear();
		
		if ( ! String.isEmpty(OBJECTTYPE) && globalDescribe.containsKey(OBJECTTYPE)) {
			fieldSetMembers = globalDescribe.get(OBJECTTYPE).getDescribe().FieldSets.getMap()
					.get('MainView').getFields();
			
			sObjectFieldMap = globalDescribe.get(OBJECTTYPE).getDescribe().fields.getMap();
			
			//Collect fields that can be viewed.
			sortableMap = new Map<String, Boolean> {'Name' => true};
			for (Schema.FieldSetMember v_field : fieldSetMembers) {
				fieldSetMap.put(v_field.getLabel(), v_field.getFieldPath());
				
				sortableMap.put(v_field.getFieldPath(), sObjectFieldMap.get(v_field.getFieldPath()).getDescribe().isSortable());
			}
			
			String v_orderClause = ( ! String.isEmpty(sortField) ? 
					' ORDER BY ' + sortField + ' ' + sortOrder + ' NULLS LAST ' : 
					' ORDER BY Name ASC NULLS LAST ');
			
			String whereClause = '';
			if (this.userRole == 'Company Admin') {
				whereClause = ' WHERE Company__c = \'' + this.userCompany + '\' ';
			}
			
			List<sObject> v_selectedRecords = new List<sObject>();
			try {
				v_selectedRecords = Database.query('SELECT Name, ' + String.join(fieldSetMap.values(), ', ') + 
						' FROM Survey_Design__c' + whereClause + v_orderClause + ' LIMIT 50000');
			} catch(Exception ex) {
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
			}
			
			pageIndex = (pageIndex <= 0) ? 1 : pageIndex;
			
			if ( ! String.isEmpty(selectedCountRecords) && selectedCountRecords.isNumeric()) {
				countRecords = Integer.valueOf(selectedCountRecords);
			} else {
				countRecords = 10;
			}
			
			countOfPage = getCountOfPage(v_selectedRecords.size(), countRecords);
			
			for (Integer i = countRecords * (pageIndex - 1); i < v_selectedRecords.size() && i < countRecords * pageIndex; i++) {
				records.add(new ISA_SObjectHelper.SObjectWrapper(v_selectedRecords.get(i)));
			}
		}
	}
	
	/**
	* @description 
	*/
	public override void doSort() {
		sortField = ApexPages.currentPage().getParameters().get('sortField');
		sortOrder = ApexPages.currentPage().getParameters().get('sortOrder');
		
		if (sortableMap.get(sortField)) {
			fillRecords();
		}
	}
	
	/**
	* @description 
	*/
	public override Integer getCountOfPage(Integer p_sizeOfRecords, Integer p_countRecords) {
		if (p_sizeOfRecords >= p_countRecords) {
			if (Math.mod(p_sizeOfRecords, p_countRecords) > 0) {
				return Math.round(p_sizeOfRecords / p_countRecords) + 1;
			} else {
				return Math.round(p_sizeOfRecords / p_countRecords);
			}
		}
		return 1;
	}
	
	/**
	* @description 
	*/
	public override void doFirstPage() {
		pageIndex = 1;
		
		fillRecords();
	}
	
	/**
	* @description 
	*/
	public override void doPreviousPage() {
		pageIndex = (pageIndex > 1) ? --pageIndex : 1;
		
		fillRecords();
	}
	
	/**
	* @description 
	*/
	public override void doNextPage() {
		pageIndex = (pageIndex < countOfPage) ? ++pageIndex : countOfPage;
		
		fillRecords();
	}
	
	/**
	* @description 
	*/
	public override void doLastPage() {
		pageIndex = countOfPage;
		
		fillRecords();
	}
	
	/**
	* @description 
	*/
	public override void doChange() {
		if ( ! String.isEmpty(selectedCountRecords) && selectedCountRecords.isNumeric()) {
			countRecords = Integer.valueOf(selectedCountRecords);
		}
		
		fillRecords();
	}
	
	/**
	* @description 
	*/
	public override PageReference doEditRecord() {
		String v_recordId = ApexPages.currentPage().getParameters().get('editRecordId');
		
		if ( ! String.isEmpty(v_recordId)) {
			PageReference pr = new PageReference(AUTH_StaticVariables.prefixSite + '/ISA_NewInsight');
			pr.getParameters().put('sId', v_recordId);
			
			return pr;
		}
		
		return null;
	}
	
	/**
	* @description 
	*/
	public override void doRemoveRecord() {
		String v_recordId = ApexPages.currentPage().getParameters().get('removeRecordId');
		
		if ( ! String.isEmpty(v_recordId) && 
				v_recordId.left(3) == Schema.getGlobalDescribe().get(OBJECTTYPE).getDescribe().getKeyPrefix()) {
			
			sObject v_deletedObject = Schema.getGlobalDescribe().get(OBJECTTYPE).newSObject();
			v_deletedObject.Id = v_recordId;
			delete v_deletedObject;
			
			doChange();
		}
	}
	
	/**
	* @description 
	*/
	public override PageReference doCreateObject() {
		return new PageReference(AUTH_StaticVariables.prefixSite + '/ISA_NewInsight');
	}
	
	/**
	* @description 
	*/
	public void doRemoveObject() {
		List<sObject> deleteRecords = new List<sObject>();
		
		for (ISA_SObjectHelper.SObjectWrapper wrapperObj : records) {
			if (wrapperObj.checked) {
				deleteRecords.add(wrapperObj.record);
			}
		}
	}
	
	@TestVisible private Map<String, String> getObjectsByUserRole() {
		if (this.userRole == 'Admin') {
			return new Map<String, String> {
				'Account' => 'Account__c', 
				'Contact' => 'Contact__c', 
				'Company' => 'Company__c', 
				'User' => 'User__c', 
				'Question' => 'Question__c', 
				'Insight' => 'Survey_Design__c'
			};
		} else if (this.userRole == 'Company Admin') {
			return new Map<String, String> {
				'Account' => 'Account__c', 
				'Contact' => 'Contact__c', 
				'User' => 'User__c', 
				'Question' => 'Question__c', 
				'Insight' => 'Survey_Design__c', 
				'Strategy' => 'Strategy_Design__c'
			};
		}
		
		return new Map<String, String>();
	}
}