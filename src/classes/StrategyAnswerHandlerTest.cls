@isTest
private class StrategyAnswerHandlerTest {

	static Company__c testCompany;
	static User__c testUser;
	static Account__c testAccount;
	static Contact__c testContact;
	static Survey_Design__c testSurveyDesign;
    static Survey_Result__c testSurveyResult;
    static Question__c testQuestion;
   	static Answer__c testAnswer;
   	static Strategy_Design__c testStrategyDesign;
   	static Strategy_Question__c testStrategyQuestion;
   	static Strategy_Answer__c testStrategyAnswer;   
   	static Strategy_Result__c testStrategyResultFirst;
   	static Strategy_Result__c testStrategyResultSecond;	
    
    /*
    * @description: Default object creation
    */
    static void init() {        	
    	     
   		testCompany = new Company__c(
            Name = 'test name',
            Admin_First_Name__c = 'firstname',
            Admin_Last_Name__c = 'lastname',
            Admin_Email__c = 'test@mail.com',
            Admin_Login__c = 'testLogin',
            Admin_Password__c = 'testPassword',
            Address1__c = 'addr1',
            Address2__c = 'addr2',
            Count_Answers__c = 10,
			Count_Questions__c = 14,
			Sum_Response__c = 8,
			Sum_Max_Response__c = 20,
            Trial_Start__c = Date.today(),
            Maximum_Users__c = 10);
            
        insert testCompany;
        
        testUser = new User__c(
            First_Name__c = 'First', 
            Last_Name__c = 'Last', 
            Phone__c = '123455678', 
            Username__c = 'testLogin', 
            Password__c = 'testPassword', 
            Role__c = 'Admin', 
            Company__c = testCompany.Id,
            Expiration_Date__c = date.newInstance(2014, 8, 25), 
            Create_Date__c = date.newInstance(2014, 5, 25),
            SessionId__c = '1',
            Active__c = true,
            Email__c = 'test@mail.com');
            
        insert testUser;

    	testAccount = new Account__c(
    		Name = 'test contact',
    		First_Name__c = 'first name',
    		Last_Name__c = 'last name', 
    		Phone__c = '123456', 
    		Email__C = 'test@mail.com', 
    		Address1__c = 'addr1', 
    		Address2__c = 'addr2', 
			City__c = 'City', 
			State__c = 'State',
			Zipcode__c = '1234', 
			Company__c = testCompany.Id,
			User__c = testUser.Id,
			Web_Site__c = 'web-site',
			Country__c = 'country');
			
		insert testAccount;
    
    	testContact = new Contact__c(
    		Name = 'test contact',
    		First_Name__c = 'first name',
    		Last_Name__c = 'last name', 
    		Phone_No__c = '123456', 
    		Email__C = 'test@mail.com', 
    		Address1__c = 'addr1', 
    		Address2__c = 'addr2', 
			City__c = 'City', 
			State__c = 'State',
			Pincode__c = '1234', 
			Sum_Max_Response__c = 13,
			Company__c = testCompany.Id,
			User__c = testUser.Id,
			Account__c = testAccount.Id,
			Country__c = 'country');
			
		insert testContact;
	
    	testSurveyDesign = new Survey_Design__c(
    		Name = 'Name First',
    		Company__c = testCompany.id);
    		
    	insert testSurveyDesign;
    
    	testQuestion = new Question__c(
    		Name = 'Name',
    		Type__c = 'Multi select drop-down',
    		Accounts__c = 12,
    		Contacts__c = 5,
    		Point_Options__c = '12',
    		Survey_Design__c = testSurveyDesign.id);
    		
    	insert testQuestion;
    	
    	testSurveyResult = new Survey_Result__c(
    		Name = 'Name',
    		Sum_Response__c = 10,
    		Sum_Max_Response__c = 15,
    		Survey_Design__c = testSurveyDesign.Id,
    		Question__c = testQuestion.Id,
    		Contact__c = testContact.Id);
    		
    	insert testSurveyResult;
    
    	testAnswer = new Answer__c(
    		Name = 'Name',
    		Point__c = '4',
    		Question__c = testQuestion.Id,
    		Contact__c = testContact.Id);
    		
    	insert testAnswer; 
    	
    	testStrategyDesign = new Strategy_Design__c(
    		Name = 'Name',
    		Accounts__c = 10,
    		Contacts__c = 12,
    		Order__c = 13,
    		Account__c = testAccount.Id,
    		Company__c = testCompany.Id);
    		
    	insert testStrategyDesign; 
    	
    	testStrategyResultFirst = new Strategy_Result__c(
    		Name = 'Name',
    		Contact__c = testContact.Id,
    		Strategy_Design__c = testStrategyDesign.Id);
    		
    	insert testStrategyResultFirst;  
    	
    	testStrategyResultSecond = new Strategy_Result__c(
    		Name = 'Name',
    		Contact__c = testContact.Id,
    		Strategy_Design__c = testStrategyDesign.Id);
    		
    	insert testStrategyResultSecond;  
    	
    	testStrategyQuestion = new Strategy_Question__c(
    		Name = 'Name',
    		Strategy_Design__c = testStrategyDesign.Id);
    		
    	insert testStrategyQuestion;
    	
    	testStrategyAnswer = new Strategy_Answer__c(
    		Name = 'Name',
    		Value__c = 'Value',
    		Contact__c = testContact.Id,
    		Strategy_Question__c = testStrategyQuestion.Id);
    		
    	insert testStrategyAnswer;    	 
    }
    
    static testMethod void testUpdateStrategyAnswer() {
    	
    	Test.startTest();
    	
    	init();
    	
    	testStrategyAnswer.Value__c = 'New_Value';
    	update testStrategyAnswer;
    	
    	Test.stopTest();
    }
    
    static testMethod void testDeleteStrategyAnswer() {
    	
    	Test.startTest();
    	
    	init();
    	
    	delete testStrategyAnswer;
    	
    	Test.stopTest();
    	
    }
}