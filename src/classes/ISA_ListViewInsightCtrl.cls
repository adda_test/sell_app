public with sharing class ISA_ListViewInsightCtrl extends AbstractController {
	public List<selectOption> panelOptions {get; set;}
	public String selectedPanel {get; set;}
	public List<WrapperCollection.InsightBubbleWrapper> chartList {get; set;}
	public Map<String, List<WrapperCollection.InsightBubbleWrapper>> seriesMap {get;set;}
	
	public ISA_ListViewInsightCtrl() {
		super();
		
		this.sObjectLabel = 'Insight';
		this.panelOptions = new List<SelectOption>();
		this.selectedPanel = '';
		
		this.chartlist = new List<WrapperCollection.InsightBubbleWrapper>();
		this.seriesMap = new Map<String, List<WrapperCollection.InsightBubbleWrapper>>();
	}
	
	public PageReference initPage() {
		PageReference pr = super.init();
		
		if (pr != null) {
			return pr;
		}
		
		this.sObjectType = sObjectNameMap.get(this.sObjectLabel);
		
		//Select Insight panels
		for (Survey_Design__c panel : getInsightPanels()) {
			this.panelOptions.add(new SelectOption(panel.Id, panel.Name));
		}
		
		if ( ! this.panelOptions.isEmpty()) {
			this.selectedPanel = this.panelOptions.get(0).getValue();
		}
		
		fillRecords();
		
		// bubble chart
		fillBubbleChartData();
		//calcAverageValue();
		splitData();
		
		return null;
	}
	
	public void fillBubbleChartData() {
		String whereClause = (this.userCompany != null) 
				? ' WHERE Survey_Design__r.Company__c = ' + '\'' + this.userCompany + '\'' 
				: '';
		
		this.chartList = ChartUtils.getInsightBubbleChart(whereClause, new List<String>());
	}
	
	public void calcAverageValue () {
		/*this.avgX = 0;
		this.avgY = 0;
		Integer i = 0;
		
		for (WrapperCollection.InsightBubbleWrapper obj : this.chartList) {
			//this.avgX += obj.completeness;
			this.avgY += obj.insightIndex;
			i++;
		}
		
		if (i != 0) {
			//this.avgX = (this.avgX / i).setScale(1);
			this.avgY = (this.avgY / i).setScale(1);
		}*/
	}
	
	public void splitData() {
		if ( ! this.chartList.isEmpty()) {
			// init color map
			this.seriesColorMap.put('Insights', '#00BFFF');
			
			this.seriesMap.put('Insights', new List<WrapperCollection.InsightBubbleWrapper>());
			
			
			Decimal maxScore = 0.0;
			for (WrapperCollection.InsightBubbleWrapper obj: this.chartList) {
				if (maxScore < obj.maxScore) {
					maxScore = obj.maxScore;
				}
			}
			
			maxScore.setScale(1);
			
			for (WrapperCollection.InsightBubbleWrapper obj: this.chartList) {
				this.seriesMap.get('Insights').add(obj);
			}
		}
	}
	
	public override String doGenerateWhereClause() {
		if (this.userRole == 'Admin') {
			return '';
		}
		
		return ' WHERE Survey_Design__r.Company__c = \'' + this.userCompany + '\'';
	}
	
	public List<Survey_Design__c> getInsightPanels() {
		return [
			SELECT Name 
			FROM Survey_Design__c 
			WHERE Company__c = :this.userCompany 
			LIMIT 1000];
	}
	
	public void doMapInsights() {
		String insightIds = ApexPages.currentPage().getParameters().get('insightIds');
		String panelId = ApexPages.currentPage().getParameters().get('panelId');
		
		if ( ! String.isEmpty(insightIds) && ! String.isEmpty(panelId)) {
			List<Question__c> assignedInsights = new List<Question__c>();
			for (String insightId : insightIds.split(',')) {
				assignedInsights.add(
					new Question__c(
						Id = insightId, 
						Survey_Design__c = panelId
				));
			}
			
			update assignedInsights;
		}
		
		fillRecords();
	}
	
	public PageReference doManagePanels() {
		return new PageReference(AUTH_StaticVariables.prefixSite + '/ISA_ManagePanels');
	}
	
	public PageReference doDisplayContacts() {
		String insightId = ApexPages.currentPage().getParameters().get('insightId');
		
		PageReference pr;
		if ( ! String.isEmpty(insightId)) {
			pr = new PAgeReference(AUTH_StaticVariables.prefixSite + '/ISA_InsightContacts');
			pr.getParameters().put('sId', insightId);
			addRetUrl(pr);
			pr.setRedirect(true);
		}
		
		return pr;
	}
	
	public PageReference goLastActivity() {
		String viewId = ApexPages.currentPage().getParameters().get('viewId');
		
		if ( ! String.isEmpty(viewId)) {
			PageReference pr = new PageReference(AUTH_StaticVariables.prefixSite + '/ISA_LastActivity');
			addRetUrl(pr);
			pr.getParameters().put('sId', viewId);
			pr.setRedirect(true);
			
			return pr;
		}
		
		return null;
	}
}