public with sharing class ISA_ContactUsersCtrl extends AbstractController {
	public List<sObject> sObjects {get; set;}
	public String typeOfContacts {get; set;}
	public String userId {get; set;}
	public String title {get; set;}
	
	public ISA_ContactUsersCtrl() {
		this.sObjects = new List<sObject>();
		this.typeOfContacts = '';
		this.userId = '';
	}
	
	public PageReference initPage() {
		PageReference pr = super.init();
		
		if (pr != null) {
			return pr;
		}
		
		this.typeOfContacts = ApexPages.currentPage().getParameters().get('typeContacts');
		this.userId = ApexPages.currentPage().getParameters().get('uId');
		
		if (this.typeOfContacts == AUTH_StaticVariables.USER_ACCOUNTS) {
			this.title = 'Accounts of user';
		} else if (this.typeOfContacts == AUTH_StaticVariables.USER_CONTACTS) {
			this.title = 'Contacts of user';
		} else if (this.typeOfContacts == AUTH_StaticVariables.USER_NO_DATA_ACCOUNTS) {
			this.title = 'Accounts no data of user';
		} else if (this.typeOfContacts == AUTH_StaticVariables.USER_NO_DATA_CONTACTS) {
			this.title = 'Contacts no data of user';
		} else if (this.typeOfContacts == AUTH_StaticVariables.USER_NO_STRATEGY_CONTACTS) {
			this.title = 'Contacts no strategy of user';
		}
		
		if ( ! String.isEmpty(this.typeOfContacts) && ! String.isEmpty(this.userId)) {
			fillRecords();
		}
		
		return null;
	}
	
	public override void fillRecords() {
		List<AssignContact__c> junctions = [
			SELECT Contact__c, User__c 
			FROM AssignContact__c 
			WHERE User__c = :this.userId 
			LIMIT 10000];
		
		Set<Id> contactIds = new Set<Id>();
		for (AssignContact__c jun : junctions) {
			contactIds.add(jun.Contact__c);
		}
		
		if (this.typeOfContacts == AUTH_StaticVariables.USER_CONTACTS) {
			if ( ! contactIds.isEmpty()) {
				this.sObjects = [
					SELECT Name 
					FROM Contact__c 
					WHERE Id IN :contactIds 
					LIMIT 10000];
			}
		} else if (this.typeOfContacts == AUTH_StaticVariables.USER_ACCOUNTS) {
			if ( ! contactIds.isEmpty()) {
				List<Contact__c> contacts = [
					SELECT Account__c 
					FROM Contact__c 
					WHERE Id IN :contactIds 
					LIMIT 10000];
				
				Set<Id> accountIds = new Set<Id>();
				for (Contact__c con : contacts) {
					if (con.Account__c != null) {
						accountIds.add(con.Account__c);
					}
				}
				
				this.sObjects = [
					SELECT Name 
					FROM Account__c 
					WHERE Id IN :accountIds 
					LIMIT 10000];
			}
		} else if (this.typeOfContacts == AUTH_StaticVariables.USER_NO_DATA_ACCOUNTS) {
			if ( ! contactIds.isEmpty()) {
				List<Contact__c> contacts = [
					SELECT Account__c, Is_No_Data__c 
					FROM Contact__c 
					WHERE Id IN :contactIds 
					LIMIT 10000];
				
				Set<Id> accountIds = new Set<Id>();
				for (Contact__c con : contacts) {
					if (con.Account__c != null && con.Is_No_Data__c) {
						accountIds.add(con.Account__c);
					}
				}
				
				this.sObjects = [
					SELECT Name 
					FROM Account__c 
					WHERE Id IN :accountIds 
					LIMIT 10000];
			}
		} else if (this.typeOfContacts == AUTH_StaticVariables.USER_NO_DATA_CONTACTS) {
			if ( ! contactIds.isEmpty()) {
				this.sObjects = [
					SELECT Name 
					FROM Contact__c 
					WHERE Id IN :contactIds 
					  AND Is_No_Data__c = true
					LIMIT 10000];
			}
		} else if (this.typeOfContacts == AUTH_StaticVariables.USER_NO_STRATEGY_CONTACTS) {
			if ( ! contactIds.isEmpty()) {
				this.sObjects = [
					SELECT Name 
					FROM Contact__c 
					WHERE Id IN :contactIds 
					  AND Is_No_Strategy__c = true
					LIMIT 10000];
			}
		}
	}
}