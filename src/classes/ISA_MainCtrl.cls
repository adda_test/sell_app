public with sharing class ISA_MainCtrl extends AbstractController {
	public String userName {get;set;}
	
	public ISA_MainCtrl() {
	}
	
	public override PageReference init() {
		this.userName = UserInfo.getUserName();
		
		if (String.isEmpty(AUTH_Utils.checkSession())) {
			return new PageReference(AUTH_StaticVariables.loggedOutURL);
		}
		
		return null;
	}
	
	public PageReference logout() {
		// remove sessionId from database and clear cookie
		AUTH_Utils.removeSession();
		
		// remove cookie
		AUTH_Utils.clearCookie();
		PageReference loginPage = new PageReference(AUTH_StaticVariables.loggedOutURL);
		
		return loginPage;
	}
}