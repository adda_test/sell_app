public with sharing class ISA_ViewContactInfoCtrl extends AbstractController {
	public Contact__c contact {get; set;}
	
	public ISA_ViewContactInfoCtrl() {
		super();
		
		this.contact = new Contact__c();
	}
	
	public PageReference initPage() {
		PageReference afterInit = init();
		if (afterInit != null) {
			return afterInit;
		}
		
		String contactId = ApexPages.currentPage().getParameters().get('cId');
		
		if ( ! String.isEmpty(contactId)) {
			List<Contact__c> contacts = [
					SELECT Name, Phone_No__c, Email__c 
					FROM Contact__c 
					WHERE Id = :contactId 
					LIMIT 1];
			
			if ( ! contacts.isEmpty()) {
				this.contact = contacts.get(0);
			}
		} else {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 
					'Selected Contact wasn\'t found.'));
		}
		
		return null;
	}
}