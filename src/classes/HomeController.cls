public with sharing class HomeController extends AbstractController { 
	
	public String userName {get;set;}
	public WrapperCollection.CompPaidWrapper paidCompInfo {get;set;}
	public WrapperCollection.CompPaidWrapper trialCompInfo {get;set;}
	
	public WrapperCollection.CompUsageWrapper userCompPaidUsageInfo {get;set;}
	public WrapperCollection.CompUsageWrapper userCompTrialUsageInfo {get;set;}
	public WrapperCollection.CompUsageWrapper accountPaidCompUsageInfo {get;set;}
	public WrapperCollection.CompUsageWrapper accountTrialCompUsageInfo {get;set;}
	public WrapperCollection.CompUsageWrapper contactPaidCompUsageInfo {get;set;}
	public WrapperCollection.CompUsageWrapper contactTrialCompUsageInfo {get;set;}
	
	public Integer companySize {get; set;}
	
	// company insight
	public WrapperCollection.DistributionWrapper companyInsightIndexInfo{get;set;}
	public WrapperCollection.DistributionWrapper companyCompletenessInfo{get;set;}
	
	//company Stratagy
	public WrapperCollection.DistributionWrapper companyAssignStrategyInfo {get;set;}
	public WrapperCollection.DistributionWrapper companyCompletedStrategyInfo {get;set;}
	
	// global insight
	public WrapperCollection.DistributionWrapper globInsightIndexInfo{get;set;}
	public WrapperCollection.DistributionWrapper globCompletenessInfo{get;set;}
	
	
	//Company Admin : Users
	public List<WrapperCollection.UserBubbleWrapper> userChartList {get; set;}
	public Integer countUsers {get; set;}
	public Map<String, List<WrapperCollection.UserBubbleWrapper>> userSeriesMap {get;set;}
	public Map<String, String> userSeriesColorMap {get;set;}
	public Decimal userAvgX {get;set;}
	public Decimal userAvgY {get;set;}
	
	//Company Admin : Accounts
	public List<WrapperCollection.AccountBubbleWrapper> accountChartList {get; set;}
	public Integer countAccounts {get; set;}
	public Map<String, List<WrapperCollection.AccountBubbleWrapper>> accountSeriesMap {get;set;}
	public Map<String, String> accountSeriesColorMap {get;set;}
	public Decimal accountAvgX {get;set;}
	public Decimal accountAvgY {get;set;}
	
	//Company Admin : Contacts
	public WrapperCollection.DistributionWrapper companyAdminContactInsightIndexInfo {get;set;}
	public WrapperCollection.DistributionWrapper companyAdminContactCompletenessInfo {get;set;}
	public Integer countContacts {get; set;}
	
	//Company admin : Insights
	public WrapperCollection.DistributionWrapper companyAdminInsightInsightIndexInfo {get;set;}
	public WrapperCollection.DistributionWrapper companyAdminInsightCompletenessInfo {get;set;}
	public Integer countInsights {get; set;}
	
	//Company admin : Strategies
	public WrapperCollection.DistributionWrapper companyAdminStrategyInsightIndexInfo {get;set;}
	public WrapperCollection.DistributionWrapper companyAdminStrategyCompletenessInfo {get;set;}
	public Integer countStrategies {get; set;}
	
	//Company User : Contacts
	public List<WrapperCollection.ContactBubbleWrapper> contactUserChartList {get; set;}
	public Integer countUserContacts {get; set;}
	public Map<String, List<WrapperCollection.ContactBubbleWrapper>> contactUserSeriesMap {get;set;}
	public Map<String, String> contactUserSeriesColorMap {get;set;}
	public Decimal contactUserAvgX {get;set;}
	public Decimal contactUserAvgY {get;set;}
	
	public HomeController() {
		super();
		
		//System Admin graphics
		this.paidCompInfo = new WrapperCollection.CompPaidWrapper();
		this.trialCompInfo = new WrapperCollection.CompPaidWrapper();
		this.userCompPaidUsageInfo = new WrapperCollection.CompUsageWrapper();
		this.userCompTrialUsageInfo = new WrapperCollection.CompUsageWrapper();
		this.accountPaidCompUsageInfo = new WrapperCollection.CompUsageWrapper();
		this.accountTrialCompUsageInfo = new WrapperCollection.CompUsageWrapper();
		this.contactPaidCompUsageInfo = new WrapperCollection.CompUsageWrapper();
		this.contactTrialCompUsageInfo = new WrapperCollection.CompUsageWrapper();
		this.companyInsightIndexInfo = new WrapperCollection.DistributionWrapper();
		this.companyCompletenessInfo = new WrapperCollection.DistributionWrapper();
		this.companyAssignStrategyInfo = new WrapperCollection.DistributionWrapper();
		this.companyCompletedStrategyInfo = new WrapperCollection.DistributionWrapper();
		this.globInsightIndexInfo = new WrapperCollection.DistributionWrapper();
		this.globCompletenessInfo = new WrapperCollection.DistributionWrapper();
		
		this.companySize = 0;
		
		//Company Admin User graphics
		this.userChartList = new List<WrapperCollection.UserBubbleWrapper>();
		this.userSeriesColorMap = new Map<String, String>();
		this.userSeriesMap = new Map<String, List<WrapperCollection.UserBubbleWrapper>>();
		this.userAvgX = 0;
		this.userAvgY = 0;
		this.countUsers = 0;
		
		//Company Admin Account graphics
		this.accountChartList = new List<WrapperCollection.AccountBubbleWrapper>();
		this.accountSeriesColorMap = new Map<String, String>();
		this.accountSeriesMap = new Map<String, List<WrapperCollection.AccountBubbleWrapper>>();
		this.accountAvgX = 0;
		this.accountAvgY = 0;
		this.countAccounts = 0;
		
		//Company Admin Contact graphics
		this.companyAdminContactInsightIndexInfo = new WrapperCollection.DistributionWrapper();
		this.companyAdminContactCompletenessInfo = new WrapperCollection.DistributionWrapper();
		this.countContacts = 0;
		
		//Company Admin Insight graphics
		this.companyAdminInsightInsightIndexInfo = new WrapperCollection.DistributionWrapper();
		this.companyAdminInsightCompletenessInfo = new WrapperCollection.DistributionWrapper();
		this.countInsights = 0;
		
		//Company Admin Strategy graphics
		this.companyAdminStrategyInsightIndexInfo = new WrapperCollection.DistributionWrapper();
		this.companyAdminStrategyCompletenessInfo = new WrapperCollection.DistributionWrapper();
		this.countStrategies = 0;
		
		//Company User Contact graphics
		this.contactUserChartList = new List<WrapperCollection.ContactBubbleWrapper>();
		this.contactUserSeriesColorMap = new Map<String, String>();
		this.contactUserSeriesMap = new Map<String, List<WrapperCollection.ContactBubbleWrapper>>();
		this.contactUserAvgX = 0;
		this.contactUserAvgY = 0;
		this.countUserContacts = 0;
	}
	
	public PageReference initPage() {
		
		PageReference afterInit = init();
		if (afterInit != null) {
			return afterInit;
		}
		this.userName = UserInfo.getUserName();
		
		if (String.isEmpty(AUTH_Utils.checkSession())) {
			return new PageReference(AUTH_StaticVariables.loggedOutURL);
		}
		
		fillPaidCompInfo();
		fillCampUsageInfo();
		fillCompanyInsightInfo();
		fillCompanyStrategyInfo();
		fillGlobalInsightInfo();
		
		// init color map
		//this.userSeriesColorMap.put('High No Data', '#4DB247');
		//this.userSeriesColorMap.put('Low No Data', '#386BB3');
		//this.userSeriesColorMap.put('Users with no assigned contacts', '#D97D5E');
		this.userSeriesColorMap.put('Users', '#386BB3');
		this.userSeriesColorMap.put('Users with no assigned contacts', '#FF0000');
		
		//this.accountSeriesColorMap.put('High No Data', '#4DB247');
		//this.accountSeriesColorMap.put('Low No Data', '#386BB3');
		//this.accountSeriesColorMap.put('Users with no assigned contacts', '#D97D5E');
		this.accountSeriesColorMap.put('Accounts', '#4DB247');
		this.accountSeriesColorMap.put('Accounts with no assigned contacts', '#FF0000');
		
		// bubble chart
		if (this.userCompany != null) {
			//Users
			this.userChartList = fillUserBubbleChartData();
			this.countUsers = this.userChartList.size();
			
			calcUserAverageValue();
			this.userSeriesMap = splitUserData();
			
			//Accounts
			this.accountChartList = fillAccountBubbleChartData();
			this.countAccounts = this.accountChartList.size();
			
			calcAccountAverageValue();
			this.accountSeriesMap = splitAccountData();
			
			fillCompanyAdminContactInfo();
			fillCompanyAdminInsightInfo();
			fillCompanyAdminStrategyInfo();
			
			//Company User : Contacts
			fillBubbleContactUserChartData();
			calcContactUserAverageValue();
			splitContactUserData();
			this.countUserContacts = this.contactUserChartList.size();
		}
		
		return null;
	}
	
	public List<WrapperCollection.UserBubbleWrapper> fillUserBubbleChartData() {
		return ChartUtils.getUserBubbleChart('Contacts__c', 'Completeness__c', 
				'Insight_Index__c', 'Contact_No_Data__c', 'No_Strategy__c', 
				' WHERE Company__c = ' + '\'' + this.userCompany + '\' AND Role__c = \'Company User\'', 
				new List<String> {'Last_Activity__c'});
	}
	
	public List<WrapperCollection.AccountBubbleWrapper> fillAccountBubbleChartData() {
		return ChartUtils.getAccountBubbleChart('Contacts__c', 'Completeness_Score__c', 
				'Insight_Index__c', 'No_Data__c', 'No_Strategy__c', 
				' WHERE Company__c = ' + '\'' + userCompany + '\'', new List<String> {'Last_Activity__c'}, 
				this.userRole, this.user.Id);
	}
	
	public void calcAccountAverageValue() {
		this.accountAvgX = 0;
		this.accountAvgY = 0;
		Integer i = 0;
		
		for (WrapperCollection.BubbleWrapper obj : this.accountChartList) {
			this.accountAvgX += obj.completeness;
			this.accountAvgY += obj.insightIndex;
			i++;
		}
		
		if (i != 0) {
			this.accountAvgX = (this.accountAvgX / i).setScale(1);
			this.accountAvgY = (this.accountAvgY / i).setScale(1);
		}
	}
	
	public void calcUserAverageValue() {
		this.userAvgX = 0;
		this.userAvgY = 0;
		Integer i = 0;
		
		for (WrapperCollection.BubbleWrapper obj : this.userChartList) {
			this.userAvgX += obj.completeness;
			this.userAvgY += obj.insightIndex;
			i++;
		}
		
		if (i != 0) {
			this.userAvgX = (this.userAvgX / i).setScale(1);
			this.userAvgY = (this.userAvgY / i).setScale(1);
		}
	}
	
	public Map<String, List<WrapperCollection.AccountBubbleWrapper>> splitAccountData() {
		Decimal maxNoData = 0;
		for (WrapperCollection.AccountBubbleWrapper obj : this.accountChartList) {
			if (obj.noData != null && maxNoData < obj.noData) {
				maxNoData = obj.noData;
			}
		}
		
		Map<String, List<WrapperCollection.AccountBubbleWrapper>> resultMap = 
				new Map<String, List<WrapperCollection.AccountBubbleWrapper>> {
			//'High No Data' => new List<WrapperCollection.AccountBubbleWrapper>(), 
			//'Low No Data' => new List<WrapperCollection.AccountBubbleWrapper>(), 
			//'Users with no assigned contacts' => new List<WrapperCollection.AccountBubbleWrapper>()
			'Accounts' => new List<WrapperCollection.AccountBubbleWrapper>(), 
			'Accounts with no assigned contacts' => new List<WrapperCollection.AccountBubbleWrapper>()
		};
		
		for (WrapperCollection.AccountBubbleWrapper obj : this.accountChartList) {
			/*if (obj.noData != null && maxNoData != 0 && (obj.noData / maxNoData) > 0.25) {
				seriaName = 'High No Data';
			} else if (obj.noData != null && maxNoData != 0 &&(obj.noData / maxNoData) <= 0.25) {
				seriaName = 'Low No Data';
			} else {
				seriaName = 'Users with no assigned contacts';
			}
			
			resultMap.get(seriaName).add(obj);*/
			
			if (obj.childCount == 0) {
				resultMap.get('Accounts with no assigned contacts').add(obj);
			} else {
				resultMap.get('Accounts').add(obj);
			}
		}
				
		return resultMap;
	}
	
	public Map<String, List<WrapperCollection.UserBubbleWrapper>> splitUserData() {
		Decimal maxNoData = 0;
		for (WrapperCollection.UserBubbleWrapper obj : this.userChartList) {
			if (obj.noData != null && maxNoData < obj.noData) {
				maxNoData = obj.noData;
			}
		}
		
		Map<String, List<WrapperCollection.UserBubbleWrapper>> resultMap = 
				new Map<String, List<WrapperCollection.UserBubbleWrapper>> {
			//'High No Data' => new List<WrapperCollection.UserBubbleWrapper>(), 
			//'Low No Data' => new List<WrapperCollection.UserBubbleWrapper>(), 
			//'Users with no assigned contacts' => new List<WrapperCollection.UserBubbleWrapper>()
			'Users' => new List<WrapperCollection.UserBubbleWrapper>(), 
			'Users with no assigned contacts' => new List<WrapperCollection.UserBubbleWrapper>()
		};
		
		for (WrapperCollection.UserBubbleWrapper obj : this.userChartList) {
			/*if (obj.noData != null && maxNoData != 0 && (obj.noData / maxNoData) > 0.25) {
				seriaName = 'High No Data';
			} else if (obj.noData != null && maxNoData != 0 &&(obj.noData / maxNoData) <= 0.25) {
				seriaName = 'Low No Data';
			} else {
				seriaName = 'Users with no assigned contacts';
			}*/
			
			if (obj.childCount == 0) {
				resultMap.get('Users with no assigned contacts').add(obj);
			} else {
				resultMap.get('Users').add(obj);
			}
		}
		
		return resultMap;
	}
	
	public void fillCompanyInsightInfo() {
		this.companyInsightIndexInfo = ChartUtils.getCompanyInsightInfo('Insight_Index__c','Company__c');
		this.companyCompletenessInfo = ChartUtils.getCompanyInsightInfo('Completeness_Score__c','Company__c');		
	}
	
	public void fillCompanyStrategyInfo() {
		this.companyAssignStrategyInfo = ChartUtils.getCompanyStrategyInfo('Assigned');
		this.companyCompletedStrategyInfo = ChartUtils.getCompanyStrategyInfo('Completed');
	}
	
	public void fillGlobalInsightInfo() {
		this.globInsightIndexInfo = ChartUtils.getCompanyInsightInfo('Insight_Index__c','Survey_Result__c');
		this.globCompletenessInfo = ChartUtils.getCompanyInsightInfo('Completeness_Score__c','Survey_Result__c');
	}
	
	public void fillCompanyAdminContactInfo() {
		this.companyAdminContactInsightIndexInfo = ChartUtils.getCompanyAdminInfo('Insight_Index__c', 
				'Contact__c', 'WHERE Company__c = \'' + this.userCompany + '\'');
		this.countContacts = companyAdminContactInsightIndexInfo.allCount;
		
		this.companyAdminContactCompletenessInfo = ChartUtils.getCompanyAdminInfo('Completeness_Score__c', 
				'Contact__c', 'WHERE Company__c = \'' + this.userCompany + '\'');
	}
	
	public void fillCompanyAdminInsightInfo() {
		this.companyAdminInsightInsightIndexInfo = ChartUtils.getCompanyAdminInsightInfo('Insight_Index__c', 
				'Contact__c', 'WHERE Company__c = \'' + this.userCompany + '\'');
		this.companyAdminInsightCompletenessInfo = ChartUtils.getCompanyAdminInsightInfo('Completeness_Score__c', 
				'Question__c', 'WHERE Survey_Design__r.Company__c = \'' + this.userCompany + '\'');
		this.countInsights = companyAdminInsightCompletenessInfo.allCount;
	}
	
	public void fillCompanyAdminStrategyInfo() {
		//this.companyAdminStrategyInsightIndexInfo = ChartUtils.getCompanyAdminInfo('Insight_Index__c', 
		//		'Strategy_Design__c', 'WHERE Company__c = \'' + this.userCompany + '\'');
		this.companyAdminStrategyCompletenessInfo = ChartUtils.getCompanyAdminStrategyInfo('Completeness_Score__c', 
				'Strategy_Design__c', 'WHERE Company__c = \'' + this.userCompany + '\'');
		this.countStrategies = companyAdminStrategyCompletenessInfo.allCount;
	}
	
	public void fillPaidCompInfo() {
		List<WrapperCollection.CompPaidWrapper> compPaidList =  ChartUtils.fillPaidCompInfo();
		this.paidCompInfo = compPaidList.get(0);
		this.trialCompInfo = compPaidList.get(1);
		this.companySize = Integer.valueOf(this.paidCompInfo.last7Days + this.paidCompInfo.other + 
				this.trialCompInfo.last7Days + this.trialCompInfo.other);
	}
	
	public void fillCampUsageInfo() {
		// user usage
		List<WrapperCollection.CompUsageWrapper> compUsageList =  ChartUtils.fillCampUsageInfoByType('User__c','Users');
		this.userCompPaidUsageInfo = compUsageList.get(0);
		this.userCompTrialUsageInfo = compUsageList.get(1);
		
		Decimal paidSize = this.userCompPaidUsageInfo.fourthValue + this.userCompPaidUsageInfo.thirdValue + 
				this.userCompPaidUsageInfo.secondValue + this.userCompPaidUsageInfo.firstValue + 
				this.userCompPaidUsageInfo.average;
		
		Decimal trialSize = this.userCompTrialUsageInfo.fourthValue + this.userCompTrialUsageInfo.thirdValue + 
				this.userCompTrialUsageInfo.secondValue + this.userCompTrialUsageInfo.firstValue + 
				this.userCompTrialUsageInfo.average;
		
		Decimal multi = 0;
		if (paidSize > trialSize) {
			if (trialSize == 0) {
				multi = 0;
			} else {
				multi = ((paidSize * 1.0) / trialSize).setScale(2);
			}
			
			this.userCompTrialUsageInfo.average *= multi;
			this.userCompTrialUsageInfo.fourthValue *= multi;
			this.userCompTrialUsageInfo.thirdValue *= multi;
			this.userCompTrialUsageInfo.secondValue *= multi;
			this.userCompTrialUsageInfo.firstValue *= multi;
			
			this.userCompTrialUsageInfo.fourthValue.setScale(2);
			this.userCompTrialUsageInfo.thirdValue.setScale(2);
			this.userCompTrialUsageInfo.secondValue.setScale(2);
			this.userCompTrialUsageInfo.firstValue.setScale(2);
		} else {
			if (paidSize == 0) {
				multi = 0;
			} else {
				multi = ((trialSize * 1.0) / paidSize).setScale(2);
			}
			
			this.userCompPaidUsageInfo.average *= multi;
			this.userCompPaidUsageInfo.fourthValue *= multi;
			this.userCompPaidUsageInfo.thirdValue *= multi;
			this.userCompPaidUsageInfo.secondValue *= multi;
			this.userCompPaidUsageInfo.firstValue *= multi;
			
			this.userCompPaidUsageInfo.fourthValue.setScale(2);
			this.userCompPaidUsageInfo.thirdValue.setScale(2);
			this.userCompPaidUsageInfo.secondValue.setScale(2);
			this.userCompPaidUsageInfo.firstValue.setScale(2);
		}
		
		// account usage
		compUsageList =  ChartUtils.fillCampUsageInfoByType('Account__c','Accounts');
		this.accountPaidCompUsageInfo = compUsageList.get(0);
		this.accountTrialCompUsageInfo = compUsageList.get(1);
		
		paidSize = this.accountPaidCompUsageInfo.fourthValue + this.accountPaidCompUsageInfo.thirdValue + 
				this.accountPaidCompUsageInfo.secondValue + this.accountPaidCompUsageInfo.firstValue + 
				this.accountPaidCompUsageInfo.average;
		
		trialSize = this.accountTrialCompUsageInfo.fourthValue + this.accountTrialCompUsageInfo.thirdValue + 
				this.accountTrialCompUsageInfo.secondValue + this.accountTrialCompUsageInfo.firstValue + 
				this.accountTrialCompUsageInfo.average;
		
		multi = 0;
		if (paidSize > trialSize) {
			if (trialSize == 0) {
				multi = 0;
			} else {
				multi = ((paidSize * 1.0) / trialSize).setScale(2);
			}
			
			this.accountTrialCompUsageInfo.average *= multi;
			this.accountTrialCompUsageInfo.fourthValue *= multi;
			this.accountTrialCompUsageInfo.thirdValue *= multi;
			this.accountTrialCompUsageInfo.secondValue *= multi;
			this.accountTrialCompUsageInfo.firstValue *= multi;
			
			this.accountTrialCompUsageInfo.fourthValue.setScale(2);
			this.accountTrialCompUsageInfo.thirdValue.setScale(2);
			this.accountTrialCompUsageInfo.secondValue.setScale(2);
			this.accountTrialCompUsageInfo.firstValue.setScale(2);
		} else {
			if (paidSize == 0) {
				multi = 0;
			} else {
				multi = ((trialSize * 1.0) / paidSize).setScale(2);
			}
			
			this.accountPaidCompUsageInfo.average *= multi;
			this.accountPaidCompUsageInfo.fourthValue *= multi;
			this.accountPaidCompUsageInfo.thirdValue *= multi;
			this.accountPaidCompUsageInfo.secondValue *= multi;
			this.accountPaidCompUsageInfo.firstValue *= multi;
			
			this.accountPaidCompUsageInfo.fourthValue.setScale(2);
			this.accountPaidCompUsageInfo.thirdValue.setScale(2);
			this.accountPaidCompUsageInfo.secondValue.setScale(2);
			this.accountPaidCompUsageInfo.firstValue.setScale(2);
		}
		
		// contact usage
		compUsageList =  ChartUtils.fillCampUsageInfoByType('Contact__c', 'Contacts');
		this.contactPaidCompUsageInfo = compUsageList.get(0);
		this.contactTrialCompUsageInfo = compUsageList.get(1);
		
		paidSize = this.contactPaidCompUsageInfo.fourthValue + this.contactPaidCompUsageInfo.thirdValue + 
				this.contactPaidCompUsageInfo.secondValue + this.contactPaidCompUsageInfo.firstValue + 
				this.contactPaidCompUsageInfo.average;
		
		trialSize = this.contactTrialCompUsageInfo.fourthValue + this.contactTrialCompUsageInfo.thirdValue + 
				this.contactTrialCompUsageInfo.secondValue + this.contactTrialCompUsageInfo.firstValue + 
				this.contactTrialCompUsageInfo.average;
		
		multi = 0;
		if (paidSize > trialSize) {
			if (trialSize == 0) {
				multi = 0;
			} else {
				multi = ((paidSize * 1.0) / trialSize).setScale(2);
			}
			
			this.contactTrialCompUsageInfo.average *= multi;
			this.contactTrialCompUsageInfo.fourthValue *= multi;
			this.contactTrialCompUsageInfo.thirdValue *= multi;
			this.contactTrialCompUsageInfo.secondValue *= multi;
			this.contactTrialCompUsageInfo.firstValue *= multi;
			
			this.contactTrialCompUsageInfo.fourthValue.setScale(2);
			this.contactTrialCompUsageInfo.thirdValue.setScale(2);
			this.contactTrialCompUsageInfo.secondValue.setScale(2);
			this.contactTrialCompUsageInfo.firstValue.setScale(2);
		} else {
			if (paidSize == 0) {
				multi = 0;
			} else {
				multi = ((trialSize * 1.0) / paidSize).setScale(2);
			}
			
			this.contactPaidCompUsageInfo.average *= multi;
			this.contactPaidCompUsageInfo.fourthValue *= multi;
			this.contactPaidCompUsageInfo.thirdValue *= multi;
			this.contactPaidCompUsageInfo.secondValue *= multi;
			this.contactPaidCompUsageInfo.firstValue *= multi;
			
			this.contactPaidCompUsageInfo.fourthValue.setScale(2);
			this.contactPaidCompUsageInfo.thirdValue.setScale(2);
			this.contactPaidCompUsageInfo.secondValue.setScale(2);
			this.contactPaidCompUsageInfo.firstValue.setScale(2);
		}
	}
	
	public void fillBubbleContactUserChartData() {
		String userId = (this.userRole == 'Company User') ? this.user.Id : null;
		
		this.contactUserChartList = ChartUtils.getContactBubbleChart(' WHERE Company__c = ' + '\'' + this.userCompany + '\'', null, userId);
	}
	
	public void calcContactUserAverageValue () {
		this.contactUserAvgY = 0;
		
		for (WrapperCollection.ContactBubbleWrapper obj : this.contactUserChartList) {
			this.contactUserAvgY += obj.insightIndex;
		}
		
		if ( ! this.contactUserChartList.isEmpty()) {
			this.contactUserAvgY = (this.contactUserAvgY / this.contactUserChartList.size()).setScale(1);
		}
	}
	
	public void splitContactUserData() {
		if ( ! this.contactUserChartList.isEmpty()) {
			// init color map
			this.contactUserseriesColorMap.put('No Strategy', '#FF0000');
			this.contactUserseriesColorMap.put('Strategy', '#0000FF');
			
			this.contactUserseriesMap.put('No Strategy', new List<WrapperCollection.ContactBubbleWrapper>());
			this.contactUserseriesMap.put('Strategy', new List<WrapperCollection.ContactBubbleWrapper>());
			
			Decimal averageCompleteness = 0.0;
			for (WrapperCollection.ContactBubbleWrapper obj: this.contactUserChartList) {
				if (obj.completeness != null) {
					averageCompleteness += (Decimal)obj.completeness;
				}
			}
			
			averageCompleteness = (averageCompleteness / this.contactUserChartList.size()).setScale(1);
			
			for (WrapperCollection.ContactBubbleWrapper obj: this.contactUserChartList) {
				String seriaName = '';
				
				if ( ! String.isEmpty(obj.strategy)) {
					seriaName = 'Strategy';
				} else if (String.isEmpty(obj.strategy)) {
					seriaName = 'No Strategy';
				}
				
				this.contactUserSeriesMap.get(seriaName).add(obj);
			}
		}
	}

	public PageReference logout() {
		// remove sessionId from database and clear cookie
		AUTH_Utils.removeSession();
		
		// remove cookie
		AUTH_Utils.clearCookie();
		PageReference loginPage = new PageReference(AUTH_StaticVariables.loggedOutURL);
		
		return loginPage;
	}
	
	public void doAccount() {
		ApexPages.currentPage().getParameters().put('nameObj', 'Account');
	}
	
	public void doContact() {
		ApexPages.currentPage().getParameters().put('nameObj', 'Contact');
	}
	
	public PageReference doCreateCompany() {
		PageReference pr = new PageReference(AUTH_StaticVariables.prefixSite + '/ISA_NewCompany');
		addRetUrl(pr);
		pr.setRedirect(true);
		
		return pr;
	}
	
	public PageReference doCreateAccount() {
		PageReference pr = new PageReference(AUTH_StaticVariables.prefixSite + '/ISA_NewAccount');
		addRetUrl(pr);
		pr.setRedirect(true);
		
		return pr;
	}
	
	public PageReference doCreateContact() {
		PageReference pr = new PageReference(AUTH_StaticVariables.prefixSite + '/ISA_NewContact');
		addRetUrl(pr);
		pr.setRedirect(true);
		
		return pr;
	}
	
	public PageReference doCreateUser() {
		PageReference pr = new PageReference(AUTH_StaticVariables.prefixSite + '/ISA_NewUser');
		addRetUrl(pr);
		pr.setRedirect(true);
		
		return pr;
	}
}