public with sharing class ISA_ViewContactInsightsCtrl extends AbstractController {
	//PROPERTY
	public Contact__c contact {get; set;}
	public SurveyWrapper survey {get; set;}
	public List<SelectOption> panelOptions {get; set;}
	public String selectedPanel {get; set;}
	
	//PRIVATE
	private Map<Id, Id> surveyToResultMap;
	private String contactId;
	
	public ISA_ViewContactInsightsCtrl() {
		super();
		
		this.contact = new Contact__c();
		this.survey = new SurveyWrapper();
		this.surveyToResultMap = new Map<Id, Id>();
		
		this.panelOptions = new List<SelectOption>();
		this.selectedPanel = '';
	}
	
	public PageReference initPage() {
		PageReference afterInit = init();
		if (afterInit != null) {
			return afterInit;
		}
		
		this.contactId = ApexPages.currentPage().getParameters().get('cId');
		
		if ( ! String.isEmpty(this.contactId)) {
			List<Contact__c> contacts = [
					SELECT Name 
					FROM Contact__c 
					WHERE Id = :this.contactId 
					LIMIT 1];
			
			if ( ! contacts.isEmpty()) {
				this.contact = contacts.get(0);
				
				//Select all insight panels instead of "Unassigned Insightsl"
				List<Survey_Design__c> surveys = [
					SELECT Name 
					FROM Survey_Design__c 
					WHERE Company__c = :this.userCompany 
					  AND Name != :AUTH_StaticVariables.DEFAULT_INSIGHT_PANEL_NAME 
					LIMIT 1000];
				
				for (Survey_Design__c record : surveys) {
					this.panelOptions.add(new SelectOption(record.Id, record.Name));
				}
				
				if ( ! this.panelOptions.isEmpty()) {
					this.selectedPanel = this.panelOptions.get(0).getValue();
					this.survey = new SurveyWrapper(this.selectedPanel);
					
					//Select all insights for selected insight panel
					List<Question__c> questions = getQuestions(this.selectedPanel);
					this.survey.addQuestions(questions);
				}
			}
		}
		
		return null;
	}
	
	public void doSave() {
		List<Answer__c> upsertAnswers = new List<Answer__c>();
		List<Answer__c> deleteAnswers = new List<Answer__c>();
		
		for (QuestionWrapper questionW : this.survey.questions) {
			String selectedValue = '';
			String selectedPoint = '';
			
			if (questionW.record.Type__c == 'Multi select drop-down') {
				selectedValue = String.join(questionW.selectedValues, ',');
				
				//Save point question for response answer
				selectedPoint = getSelectedPoints(questionW.record.Point_Options__c, 
						questionW.record.Response_Options__c, questionW.selectedValues);
			} else if (questionW.record.Type__c == 'Single select drop-down') {
				selectedValue = questionW.selectedValue;
				
				//Save point question for response answer
				selectedPoint = getSelectedPoints(questionW.record.Point_Options__c, 
						questionW.record.Response_Options__c, new List<String> {questionW.selectedValue});
			} else if (questionW.record.Type__c == 'Open-ended') {
				if ( ! String.isEmpty(questionW.selectedValue)) {
					selectedValue = questionW.selectedValue;
					
					//Save point question for response answer
					selectedPoint = getSelectedPoints(questionW.record.Point_Options__c, 
							questionW.record.Response_Options__c, new List<String> {questionW.selectedValue});
				} else {
					selectedValue = questionW.answerStringValue;
					
					List<String> points = questionW.record.Point_Options__c.split(',');
					selectedPoint = points.get(points.size() - 1);
				}
			} else if (questionW.record.Type__c == 'Date') {
				if ( ! String.isEmpty(questionW.answerStringValue)) {
					List<String> arr = questionW.answerStringValue.split('/');
					
					if (Integer.valueOf(arr.get(0)) < 10) {
						arr.set(0, '0' + arr.get(0));
					}
					
					if (Integer.valueOf(arr.get(1)) < 10) {
						arr.set(1, '0' + arr.get(1));
					}
					selectedValue = arr.get(2) + '-' + arr.get(0) + '-' + arr.get(1);
				}
				
				selectedPoint = questionW.record.Point_Options__c;
			} else if (questionW.record.Type__c == 'Text') {
				selectedValue = questionW.answerStringValue;
				
				selectedPoint = questionW.record.Point_Options__c;
			} else if (questionW.record.Type__c == 'Number' || questionW.record.Type__c == 'Currency') {
				if (questionW.answerIntegerValue != null && questionW.answerIntegerValue != 0) {
					selectedValue = String.valueOf(questionW.answerIntegerValue);
				}
				
				selectedPoint = questionW.record.Point_Options__c;
			}
			
			if ( ! String.isEmpty(selectedValue)) {
				Answer__c answer = new Answer__c(
					Question__c = questionW.record.Id, 
					Contact__c = this.contactId, 
					Value__c = selectedValue, 
					Point__c = selectedPoint, 
					Owner__c = this.user.Id, 
					Contact_Name__c = this.contact.Name, 
					Insight_Name__c = QuestionW.record.Name, 
					Insight__c = questionW.record.Survey_Design__c
				);
				
				if (questionW.answer.Id != null) {
					answer.Id = questionW.answer.Id;
				}
				
				upsertAnswers.add(answer);
			} else if (questionW.answer.Id != null) {
				deleteAnswers.add(
					new Answer__c(Id = questionW.answer.Id));
			}
		}
		
		List<Id> questionIds = new List<Id>();
		for (Answer__c answer : upsertAnswers) {
			questionIds.add(answer.Question__c);
		}
		
		if ( ! questionIds.isEmpty()) {
			List<Question__c> questions = [
				SELECT Id, Point_Options__c, 
					(SELECT Question__c 
					FROM Survey_Results__r 
					WHERE Contact__c = :this.contactId)
				FROM Question__c 
				WHERE Id IN :questionIds 
				LIMIT 10000];
			
			List<Survey_Result__c> insertResults = new List<Survey_Result__c>();
			for (Question__c question : questions) {
				if (question.Survey_Results__r.isEmpty()) {
					Decimal maxPoint = 0.0;
					if (question.Point_Options__c != null) {
						maxPoint = ISA_ViewContactResultCtrl.getMaxValue(question.Point_Options__c.split(','));
					}
					
					insertResults.add(
						new Survey_Result__c(
							Contact__c = contact.Id, 
							Question__c = question.Id, 
							Count_Questions__c = 1, 
							Sum_Max_Response__c = maxPoint
					));
				}
			}
			
			insert insertResults;
		}
		
		upsert upsertAnswers;
		
		delete deleteAnswers;
	}
	
	/**
	* @description 
	*/
	private String getSelectedPoints(String p_pointOptions, String p_responseOptions, List<String> p_selectedOptions) {
		List<Integer> selectedIndexes = new List<Integer>();
		List<String> responseOptions = p_responseOptions.split(',');
		List<String> pointOptions = p_pointOptions.split(',');
		for (Integer i = 0; i < responseOptions.size(); i++) {
			for (String selected : p_selectedOptions) {
				if (selected == responseOptions.get(i)) {
					selectedIndexes.add(i);
				}
			}
		}
		
		List<String> selectedPoints = new List<String>();
		for (Integer index : selectedIndexes) {
			selectedPoints.add(pointOptions.get(index));
		}
		return String.join(selectedPoints, ',');
	}
	
	private List<Question__c> getQuestions(String p_insightPanelId) {
		return [
			SELECT Survey_Design__c, Title__c, Type__c, Response_Options__c, Point_Options__c, 
				Index__c, Name, 
				(SELECT Question__c, Value__c, Point__c 
				FROM Asnwers__r 
				WHERE Contact__c = :this.contactId 
				LIMIT 1) 
			FROM Question__c 
			WHERE Survey_Design__c = :p_insightPanelId 
			LIMIT 10000];
	}
	
	private List<Question__c> getUnasnweredQuestions(String p_insightPanelId) {
		List<Question__c> insights = [
			SELECT Survey_Design__c, Title__c, Type__c, Response_Options__c, Point_Options__c, 
				Index__c, Name, 
				(SELECT Question__c, Value__c, Point__c 
				FROM Asnwers__r 
				WHERE Contact__c = :this.contactId 
				LIMIT 1) 
			FROM Question__c 
			WHERE Survey_Design__c = :p_insightPanelId 
			LIMIT 10000];
		
		List<Question__c> resultQuestions = new List<Question__c>();
		for (Question__c insight : insights) {
			if (insight.Asnwers__r.isEmpty()) {
				resultQuestions.add(insight);
			}
		}
		
		return resultQuestions;
	}
	
	public void doSelectPanel() {
		List<Survey_Design__c> surveys = [
			SELECT Name 
			FROM Survey_Design__c 
			WHERE Company__c = :this.userCompany 
			  AND Id = :this.selectedPanel 
			LIMIT 1];
		
		this.survey = new SurveyWrapper(this.selectedPanel);
		
		//Select all insights for selected insight panel
		List<Question__c> questions = getQuestions(this.selectedPanel);
		this.survey.addQuestions(questions);
	}
	
	public void doDisplayUnanswered() {
		String unanswered = ApexPages.currentPage().getParameters().get('unanswered');
		this.survey.questions.clear();
		
		if ( ! String.isEmpty(unanswered) && unanswered == 'true') {
			//Select unanswered insights for selected insight panel
			List<Question__c> questions = getUnasnweredQuestions(this.selectedPanel);
			this.survey.addQuestions(questions);
		} else {
			//Select all insights for selected insight panel
			List<Question__c> questions = getQuestions(this.selectedPanel);
			this.survey.addQuestions(questions);
		}
	}
	
	public class SurveyWrapper {
		public Id surveyId {get; set;}
		public List<QuestionWrapper> questions {get; set;}
		
		public SurveyWrapper() {
			this.surveyId = null;
			this.questions = new List<QuestionWrapper>();
		}
		
		public SurveyWrapper(Id p_surveyId) {
			this();
			
			this.surveyId = p_surveyId;
		}
		
		public void addQuestions(List<Question__c> p_questions) {
			for (Question__c question : p_questions) {
				this.questions.add(new QuestionWrapper(question));
			}
		}
	}
	
	public class QuestionWrapper {
		public Question__c record {get; set;}
		private Answer__c answer;
		public String selectedValue {get; set;}
		public List<String> selectedValues {get; set;}
		public List<SelectOption> picklistOptions {get; set;}
		public String answerStringValue {get; set;}
		public Integer answerIntegerValue {get; set;}
		
		public QuestionWrapper() {
			this.answer = new Answer__c();
			this.selectedValue = '';
			this.selectedValues = new List<String>();
			this.picklistOptions = new List<SelectOption>();
			this.answerStringValue = '';
			this.answerIntegerValue = 0;
		}
		
		public QuestionWrapper(Question__c p_question) {
			this();
			
			this.record = p_question;
			
			if (this.record.Type__c == 'Single select drop-down' || 
					this.record.Type__c == 'Multi select drop-down' || 
					this.record.Type__c == 'Open-ended') {
				
				if (this.record.Type__c == 'Open-ended') {
					this.picklistOptions.add(new SelectOption('', '-- None--'));
				}
				
				for (String value : this.record.Response_Options__c.split(',')) {
					this.picklistOptions.add(new SelectOption(value, value));
				}
				
				if ( ! p_question.Asnwers__r.isEmpty()) {
					this.answer = p_question.Asnwers__r.get(0);
					
					String answerValue = this.answer.Value__c;
					
					if (this.record.Type__c == 'Multi select drop-down') {
						this.selectedValues = answerValue.split(',');
					} else if (this.record.Type__c == 'Single select drop-down') {
						this.selectedValue = answerValue;
					} else if (this.record.Type__c == 'Open-ended') {
						for (SelectOption option : this.picklistOptions) {
							if (option.getValue() == answerValue) {
								this.selectedValue = answerValue;
								break;
							}
						}
						
						if (String.isEmpty(this.selectedValue)) {
							this.answerStringValue = answerValue;
						}
					}
				}
			} else {
				if ( ! p_question.Asnwers__r.isEmpty()) {
					this.answer = p_question.Asnwers__r.get(0);
					
					if (this.record.Type__c == 'Date' || this.record.Type__c == 'Text') {
						
						this.answerStringValue = this.answer.Value__c;
					} else if (this.answer.Value__c.isNumeric() && 
							(this.record.Type__c == 'Number' || this.record.Type__c == 'Currency')) {
						
						this.answerIntegerValue = Integer.valueOf(this.answer.Value__c);
					}
				}
			}
		}
	}
}