public with sharing class CompanyHandler {
	public static Boolean enablesTrigger = true;
	
	public static void createDefaultPanel(List<Company__c> p_newRecords) {
		List<Survey_Design__c> insightPanels = new List<Survey_Design__c>();
		for (Company__c company : p_newRecords) {
			insightPanels.add(
				new Survey_Design__c(
					Company__c = company.Id, 
					Name = AUTH_StaticVariables.DEFAULT_INSIGHT_PANEL_NAME
			));
		}
		
		insert insightPanels;
	}
	
	public static void updateExpirationDateForUsers(Map<Id, Company__c> p_oldRecordMap, List<Company__c> p_newRecords) {
		Map<Id, Company__c> companyMap = new Map<Id, Company__c>();
		for (Company__c company : p_newRecords) {
			Company__c oldCompany = p_oldRecordMap.get(company.Id);
			
			if (oldCompany.Trial__c != company.Trial__c || oldCompany.Trial_Start__c != company.Trial_Start__c) {
				companyMap.put(company.Id, company);
			}
		}
		
		if ( ! companyMap.keySet().isEmpty()) {
			List<User__c> users = [
				SELECT Expiration_Date__c, Company__c
				FROM User__c 
				WHERE Company__c IN :companyMap.keySet() 
				LIMIT 10000];
			
			if ( ! users.isEmpty()) {
				for (User__c user : users) {
					Company__c company = companyMap.get(user.Company__c);
					
					user.Expiration_Date__c = (company.Trial__c != null && company.Trial_Start__c != null) 
							? company.Trial_Start__c.addDays(Math.round(company.Trial__c)) 
							: null;
					
					if (user.Expiration_Date__c != null && user.Expiration_Date__c < Date.today()) {
						user.Active__c = false;
					}
				}
				
				update users;
			}
		}
	}
	
	public static void updateActiveUsers(Map<Id, Company__c> p_oldRecordMap, List<Company__c> p_newRecords) {
		Map<Id, Boolean> activeCompanyMap = new Map<Id, Boolean>();
		for (Company__c company : p_newRecords) {
			Company__c oldCompany = p_oldRecordMap.get(company.Id);
			if (oldCompany.Active__c != company.Active__c) {
				activeCompanyMap.put(company.Id, company.Active__c);
			}
		}
		
		if ( ! activeCompanyMap.keySet().isEmpty()) {
			List<User__c> selectedUsers = [
			SELECT Active__c, Company__c 
			FROM User__c 
			WHERE Company__c IN :activeCompanyMap.keySet()
			LIMIT 10000];
			
			if ( ! selectedUsers.isEmpty()) {
				Map<Id, List<User__c>> companyToUsersMap = new Map<Id, List<User__c>>();
				for (User__c user : selectedUsers) {
					if ( ! companyToUsersMap.containsKey(user.Company__c)) {
						companyToUsersMap.put(user.Company__c, new List<User__c>());
					}
					companyToUsersMap.get(user.Company__c).add(user);
				}
				
				List<User__c> updateUsers = new List<User__c>();
				for (Id companyId : companyToUsersMap.keySet()) {
					Boolean isActive = activeCompanyMap.get(companyId);
					
					for (User__c user : companyToUsersMap.get(companyId)) {
						user.Active__c = isActive;
					}
					
					updateUsers.addAll(companyToUsersMap.get(companyId));
				}
				
				if ( ! updateUsers.isEmpty()) {
					update updateUsers;
				}
			}
		}
	}
	
	public static void createCompanyHistory(List<Company__c> p_newRecords) {
		createCompanyHistory(new Map<Id,Company__c>(), p_newRecords);
	}
	
	public static void createCompanyHistory(Map<Id, Company__c> p_oldRecordMap, List<Company__c> p_newRecords) {
		List<Company_History__c> histories = new List<Company_History__c>();
		
		for (Company__c company : p_newRecords) {
			Company__c oldCompany = p_oldRecordMap.get(company.Id);
			
			if ((oldCompany != null && oldCompany.Insight_Index__c != company.Insight_Index__c) || oldCompany == null) {
				histories.add(
					new Company_History__c(
						Company__c = company.Id, 
						NewValue__c = String.valueOf(company.Insight_Index__c), 
						Field__c = 'Insight Index'
					));
			}
			
			if ((oldCompany != null && oldCompany.Completeness_Score__c != company.Completeness_Score__c) || oldCompany == null) {
				histories.add(
					new Company_History__c(
						Company__c = company.Id, 
						NewValue__c = String.valueOf(company.Completeness_Score__c), 
						Field__c = 'Completeness'
					));
			}
			
			if ((oldCompany != null && oldCompany.Users__c != company.Users__c) || oldCompany == null) {
				histories.add(
					new Company_History__c(
						Company__c = company.Id, 
						NewValue__c = String.valueOf(company.Users__c), 
						Field__c = 'Users'
					));
			}
		}
		
		insert histories;
	}
	
	public static void deleteUsers(Map<Id, Company__c> p_oldRecordMap) {
		delete [SELECT Id FROM User__c WHERE Company__c IN :p_oldRecordMap.keySet() LIMIT 10000];
	}
	
	public static void deleteAccounts(Map<Id, Company__c> p_oldRecordMap) {
		delete [SELECT Id FROM Account__c WHERE Company__c IN :p_oldRecordMap.keySet() LIMIT 10000];
	}
	
	public static void deleteSurveyDesigns(Map<Id, Company__c> p_oldRecordMap) {
		delete [SELECT Id FROM Survey_Design__c WHERE Company__c IN :p_oldRecordMap.keySet() LIMIT 10000];
	}
	
	public static void deleteStrategyDesigns(Map<Id, Company__c> p_oldRecordMap) {
		delete [SELECT Id FROM Strategy_Design__c WHERE Company__c IN :p_oldRecordMap.keySet() LIMIT 10000];
	}
	
	public static void deleteCompanyHistory(Map<Id, Company__c> p_oldRecordMap) {
		delete [SELECT Id FROM Company_History__c WHERE Company__c IN :p_oldRecordMap.keySet() LIMIT 10000];
	}
}