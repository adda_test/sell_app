public with sharing class AssignStrategyHandler {
	public static Boolean enablesTrigger = true;
	
	public static void recalculateStrategyContacts(List<AssignStrategy__c> p_newRecords, Boolean p_isInsert) {
		Set<Id> accountIds = new Set<Id>();
		Set<Id> strategyIds = new Set<Id>();
		for (AssignStrategy__c junction : p_newRecords) {
			if (junction.Account__c != null) {
				accountIds.add(junction.Account__c);
			}
			
			if (junction.Strategy__c != null) {
				strategyIds.add(junction.Strategy__c);
			}
		}
		
		if ( ! accountIds.isEmpty() && ! strategyIds.isEmpty()) {
			List<Contact__c> contacts = [
				SELECT Account__c 
				FROM Contact__c 
				WHERE Account__c IN :accountIds
				LIMIT 10000];
			
			Map<Id, Strategy_Design__c> strategyMap = new Map<Id, Strategy_Design__c>([
				SELECT Contacts__c, 
					(SELECT Id 
					FROM AssignStrategies__r)
				FROM Strategy_Design__c 
				WHERE ID IN :strategyIds 
				LIMIT 10000]);
			
			Map<Id, List<Id>> accountToContactsMap = new Map<Id, List<Id>>();
			for (Contact__c contact : contacts) {
				Id key = contact.Account__c;
				
				if ( ! accountToContactsMap.containsKey(key)) {
					accountToContactsMap.put(key, new List<Id>());
				}
				accountToContactsMap.get(key).add(contact.Id);
			}
			
			Set<Strategy_Design__c> updateStrategies = new Set<Strategy_Design__c>();
			for (AssignStrategy__c junction : p_newRecords) {
				Strategy_Design__c strategy = strategyMap.get(junction.Strategy__c);
				
				strategy.Accounts__c = strategyMap.get(junction.Strategy__c).AssignStrategies__r.size();
				Integer countContacts = (strategyMap.get(junction.Strategy__c).Contacts__c != null) 
					? (Integer)strategyMap.get(junction.Strategy__c).Contacts__c 
					: 0;
				Integer additionalContacts = (accountToContactsMap.containsKey(junction.Account__c) 
						? accountToContactsMap.get(junction.Account__c).size() 
						: 0);
				
				if (p_isInsert) {
					strategy.Contacts__c = countContacts + additionalContacts;
				} else {
					strategy.Contacts__c = countContacts - additionalContacts;
				}
				
				updateStrategies.add(strategy);
			}
			
			update new List<Strategy_Design__c>(updateStrategies);
		}
	}
	
	public static void recalculateStrategyResults(List<AssignStrategy__c> p_newRecords, Boolean p_isInsert) {
		Set<Id> accountIds = new Set<Id>();
		Set<Id> strategyIds = new Set<Id>();
		for (AssignStrategy__c junction : p_newRecords) {
			if (junction.Account__c != null) {
				accountIds.add(junction.Account__c);
			}
			
			if (junction.Strategy__c != null) {
				strategyIds.add(junction.Strategy__c);
			}
		}
		
		ISA_TriggerHelper.recalculateStrategyResults(p_newRecords, accountIds, strategyIds, p_isInsert);
	}
	
	public static void recalculateContacts(List<AssignStrategy__c> p_newRecords) {
		Set<Id> accountIds = new Set<Id>();
		for (AssignStrategy__c junction : p_newRecords) {
			if (junction.Account__c != null) {
				accountIds.add(junction.Account__c);
			}
		}
		
		ISA_TriggerHelper.calculateNextStrategy(accountIds);
	}
	
	public static void createAccountHistory(List<AssignStrategy__c> p_newRecords, Boolean p_isInsert) {
		List<Account_History__c> accountHistories = new List<Account_History__c>();
		
		String assignValue = (p_isInsert) ? 'assigned' : 'unassigned';
		
		for (AssignStrategy__c junction : p_newRecords) {
			PageReference prUser = new PageReference(AUTH_StaticVariables.prefixSite + '/ISA_ViewUser');
			prUser.getParameters().put('sId', junction.Owner__c);
			
			PageReference prStrategy = new PageReference(AUTH_StaticVariables.prefixSite + '/ISA_ViewStrategy');
			prStrategy.getParameters().put('sId', junction.Strategy__c);
			
			PageReference prAccount = new PageReference(AUTH_StaticVariables.prefixSite + '/ISA_ViewAccount');
			prAccount.getParameters().put('sId', junction.Account__c);
			
			if (junction.Account__c != null) {
				accountHistories.add(
					new Account_History__c(
						Account__c = junction.Account__c, 
						Message__c = '<b><a href="' + prUser.getUrl() + '">' + junction.Owner_Name__c + 
							'</a></b> ' + assignValue + ' Strategy(<b><a href="' + prStrategy.getUrl() + '">' + 
							junction.Strategy_Name__c + '</a></b>) to Account(<b><a href="' + prAccount.getUrl() + 
							'">' + junction.Account_Name__c + '</a></b>)'
					));
			}
		}
		
		if ( ! accountHistories.isEmpty()) {
			insert accountHistories;
		}
	}
	
	public static void createStrategyHistory(List<AssignStrategy__c> p_newRecords, Boolean p_isInsert) {
		List<Strategy_History__c> strategyHistories = new List<Strategy_History__c>();
		
		String assignValue = (p_isInsert) ? 'assigned' : 'unassigned';
		
		for (AssignStrategy__c junction : p_newRecords) {
			PageReference prUser = new PageReference(AUTH_StaticVariables.prefixSite + '/ISA_ViewUser');
			prUser.getParameters().put('sId', junction.Owner__c);
			
			PageReference prAccount = new PageReference(AUTH_StaticVariables.prefixSite + '/ISA_ViewAccount');
			prAccount.getParameters().put('sId', junction.Account__c);
			
			PageReference prStrategy = new PageReference(AUTH_StaticVariables.prefixSite + '/ISA_ViewStrategy');
			prStrategy.getParameters().put('sId', junction.Strategy__c);
			
			if (junction.Strategy__c != null) {
				strategyHistories.add(
					new Strategy_History__c(
						Strategy__c = junction.Strategy__c, 
						Message__c = '<b><a href="' + prUser.getUrl() + '">' + junction.Owner_Name__c + 
							'</a></b> ' + assignValue + 
							'Account(<b><a href="' + prAccount.getUrl() + '">' + junction.Account_Name__c + 
							'</a></b>)' + ' to Strategy(<b><a href="' + prStrategy.getUrl() + '">' + 
							junction.Strategy_Name__c + '</a></b>)'
					));
			}
		}
		
		if ( ! strategyHistories.isEmpty()) {
			insert strategyHistories;
		}
	}
}