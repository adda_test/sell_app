public with sharing class ISA_SObjectHelper {
	public static Map<String, String> getFieldSetValues(String p_sObjectName, String p_FieldSetName) {
		List<Schema.FieldSetMember> fieldSetMembers = new List<Schema.FieldSetMember>();
		Map<String, Schema.SObjectField> sObjectFieldMap = new Map<String, Schema.SObjectField>();
		Map<String, String> fieldSetMap = new Map<String, String>();
		
		try {
			fieldSetMembers = Schema.getGlobalDescribe().get(p_sObjectName).
					getDescribe().FieldSets.getMap().get(p_FieldSetName).getFields();
			
			sObjectFieldMap = Schema.getGlobalDescribe().get(p_sObjectName).getDescribe().fields.getMap();
			
			//Collect fields that can be viewed.
			for (Schema.FieldSetMember v_field : fieldSetMembers) {
				fieldSetMap.put(v_field.getLabel(), v_field.getFieldPath());
			}
		} catch(Exception ex) {
			return new Map<String, String>();
		}
		
		return fieldSetMap;
	}
	
	public static Map<String, String> convertToSingleNameMap = new Map<String, String> {
		'Accounts' => 'Account', 
		'Contacts' => 'Contact', 
		'Companies' => 'Company', 
		'Users' => 'User', 
		'Insights' => 'Insight', 
		'Strategies' => 'Strategy', 
		'Questions' => 'Question'
	};
	
	public static Map<String, String> convertToMultiNameMap = new Map<String, String> {
		'Account' => 'Accounts', 
		'Contact' => 'Contacts', 
		'Company' => 'Companies', 
		'User' => 'Users', 
		'Insight' => 'Insights', 
		'Strategy' => 'Strategies', 
		'Qustion' => 'Questions'
	};
	
	public static Map<String, String> objectNameMap = new Map<String, String> {
		'Account' => 'Account__c', 
		'Contact' => 'Contact__c', 
		'User' => 'User__c', 
		'Insight' => 'Survey_Design__c', 
		'Strategy' => 'Strategy_Design__c', 
		'Company' => 'Company__c', 
		'Question' => 'Question__c'
	};
	
	public class SObjectWrapper {
		public Boolean checked {get; set;}
		public sObject record {get; set;}
		
		public SObjectWrapper(sObject p_sObject) {
			this.checked = false;
			this.record = p_sObject;
		}
	}
}