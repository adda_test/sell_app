public with sharing class ContactHandler {
	public static Boolean enablesTrigger = true;
	
	public static void populateName(List<Contact__c> p_newRecords) {
		for (Contact__c contact : p_newRecords) {
			contact.Name = 
					( ! String.isEmpty(contact.First_Name__c) ? contact.First_Name__c : '') + ' ' + 
					( ! String.isEmpty(contact.Last_Name__c) ? contact.Last_Name__c : '');
		}
	}
	
	public static void setDefaultInfluence(List<Contact__c> p_newRecords) {
		setDefaultInfluence(new Map<Id, Contact__c>(), p_newRecords);
	}
	
	public static void setDefaultInfluence(Map<Id, Contact__c> p_oldRecordMap, List<Contact__c> p_newRecords) {
		Map<Id, List<Contact__c>> accountToContactsMap = new Map<Id, List<Contact__c>>();
		Map<Id, List<Contact__c>> accountToOldContactsMap = new Map<Id, List<Contact__c>>();
		Set<Id> accountIds = new Set<Id>();
		for (Contact__c contact : p_newRecords) {
			Contact__c oldContact = p_oldRecordMap.get(contact.Id);
			
			if ((oldContact != null && oldContact.Account__c != contact.Account__c) || 
					oldContact == null) {
				
				if (oldContact != null) {
					Id key = oldContact.Account__c;
					
					if (key != null) {
						if ( ! accountToOldContactsMap.containsKey(key)) {
							accountToOldContactsMap.put(key, new List<Contact__c>{oldContact});
						} else {
							accountToOldContactsMap.get(key).add(oldContact);
						}
					}
				}
				
				Id key = contact.Account__c;
				
				if (key != null) {
					if ( ! accountToContactsMap.containsKey(key)) {
						accountToContactsMap.put(key, new List<Contact__c>{contact});
					} else {
						accountToContactsMap.get(key).add(contact);
					}
				}
			}
		}
		
		if ( ! accountToContactsMap.keyset().isEmpty() || ! accountToOldContactsMap.keyset().isEmpty()) {
			List<Account__c> accounts = [
				SELECT Id, 
					(SELECT Influence__c 
					FROM Contacts__r 
					WHERE NOT(Id IN : p_oldRecordMap.keySet()))
				FROM Account__c 
				WHERE Id IN :accountToOldContactsMap.keySet() 
				   OR ID IN :accountToContactsMap.keySet() 
				LIMIT 10000];
			
			List<Contact__c> updateContacts = new List<Contact__c>();
			for (Account__c account : accounts) {
				if (Trigger.isInsert) {
					
					if (accountToContactsMap.containsKey(account.Id)) {
						updateContacts.addAll(getUpdateNewContacts(account, accountToContactsMap.get(account.Id)));
					}
					
				} else if (Trigger.isUpdate) {
					
					if (accountToContactsMap.containsKey(account.Id)) {
						updateContacts.addAll(getUpdateNewContacts(account, accountToContactsMap.get(account.Id)));
					} else if (accountToOldContactsMap.containsKey(account.Id)) {
						updateContacts.addAll(getUpdateOldContacts(account));
					}
					
				} else if (Trigger.isDelete) {
					
					if (accountToContactsMap.containsKey(account.Id)) {
						updateContacts.addAll(getUpdateOldContacts(account));
					}
					
				}
			}
			
			update updateContacts;
		}
	}
	
	private static List<Contact__c> getUpdateNewContacts(Account__c p_account, List<Contact__c> p_contacts) {
		List<Contact__c> updateContacts = new List<Contact__c>();
		Decimal remainInfluenceValue = 100;
		
		if (p_account.Contacts__r.size() != 0) {
			for (Contact__c contact : p_account.Contacts__r) {
				remainInfluenceValue -= (contact.Influence__c != null) ? contact.Influence__c : 0;
			}
		}
		
		for (Contact__c contact : p_contacts) {
			updateContacts.add(
				new Contact__c(
					Id = contact.Id, 
					Influence__c = 0
			));
		}
		updateContacts.get(0).Influence__c = remainInfluenceValue;
		
		return updateContacts;
	}
	
	private static List<Contact__c> getUpdateOldContacts(Account__c p_account) {
		List<Contact__c> updateContacts = new List<Contact__c>();
		Decimal remainInfluenceValue = 100;
		
		if (p_account.Contacts__r.size() != 0) {
			for (Contact__c contact : p_account.Contacts__r) {
				remainInfluenceValue -= (contact.Influence__c != null) ? contact.Influence__c : 0;
			}
			
			updateContacts.add(
				new Contact__c(
					Id = p_account.Contacts__r.get(0).Id, 
					Influence__c = p_account.Contacts__r.get(0).Influence__c + remainInfluenceValue
			));
		}
		
		return updateContacts;
	}
	
	public static void recalculateAccount(Map<Id, Contact__c> p_oldRecordsMap, List<Contact__c> p_newRecords) {
		Map<Id, List<Contact__c>> accountToContactsMap = new Map<Id, List<Contact__c>>();
		Map<Id, List<Contact__c>> changeAccountMap = new Map<Id, List<Contact__c>>();
		Set<Id> contactIds = new Set<Id>();
		
		Set<id> accountIds = new Set<Id>();
		Set<Id> changeAccountIds = new Set<Id>();
		for (Contact__c contact : p_newRecords) {
			Contact__c oldContact = p_oldRecordsMap.get(contact.Id);
			
			if (oldContact.Count_Answers__c != contact.Count_Answers__c || 
				oldContact.Count_Questions__c != contact.Count_Questions__c || 
				oldContact.Count_Strategy_Questions__c != contact.Count_Strategy_Questions__c || 
				oldContact.Count_Strategy_Answers__c != contact.Count_Strategy_Answers__c || 
				oldContact.Sum_Response__c != contact.Sum_Response__c || 
				oldContact.Sum_Max_Response__c != contact.Sum_Max_Response__c || 
				oldContact.Influence__c != contact.Influence__c || 
				oldContact.Insight_Index__c != contact.Insight_Index__c || 
				oldContact.Account__c != contact.Account__c || 
				oldContact.Is_No_Strategy__c != contact.Is_No_Strategy__c) {
				
				if (contact.Account__c != null) {
					accountIds.add(contact.Account__c);
				}
				
				if (oldContact != null && oldContact.Account__c != null) {
					accountIds.add(oldContact.Account__c);
				}
				
				if (oldContact.Account__c != contact.Account__c) {
					changeAccountIds.add(contact.Account__c);
				}
			}
		}
		
		ISA_TriggerHelper.recalculateAccounts(accountIds);
		
		ISA_TriggerHelper.calculateNextStrategy(changeAccountIds);
	}
	
	public static void addContactsToAccount(List<Contact__c> p_newRecords) {
		Set<Id> accountIds = new Set<Id>();
		for (Contact__c contact : p_newRecords) {
			if (contact.Account__c != null) {
				accountIds.add(contact.Account__c);
			}
		}
		
		ISA_TriggerHelper.recalculateAccounts(accountIds);
		
		ISA_TriggerHelper.calculateNextStrategy(accountIds);
	}
	
	public static void removeContactsFromAccount(List<Contact__c> p_oldRecords) {
		//Recalculate Account fields after remove Contacts
		ContactHandler.addContactsToAccount(p_oldRecords);
	}
	
	public static void recalculateInsights(List<Contact__c> p_newRecords) {
		recalculateInsights(new Map<Id, Contact__c>(), p_newRecords);
	}
	
	public static void recalculateInsights(Map<Id, Contact__c> p_oldRecordMap, List<Contact__c> p_newRecords) {
		
		Set<Id> companyIds = new Set<Id>();
		for (Contact__c contact : p_newRecords) {
			Contact__c oldContact = p_oldRecordMap.get(contact.Id);
			
			if (oldContact != null && (oldContact.Company__c != contact.Company__c)) {
				companyIds.add(oldContact.Company__c);
				companyIds.add(contact.Company__c);
			} else if (oldContact == null) {
				companyIds.add(contact.Company__c);
			}
		}
		
		if ( ! companyIds.isEmpty()) {
			Map<Id, Company__c> companyMap = new Map<Id, Company__c>([
				SELECT Id, 
					(SELECT Account__c 
					FROM Contacts__r)
				FROM Company__c 
				WHERE Id IN :companyIds 
				LIMIT 10000]);
			
			List<Question__c> insights = [
				SELECT Accounts__c, Contacts__c, Survey_Design__r.Company__c 
				FROM Question__c 
				WHERE Survey_Design__r.Company__c IN :companyIds 
				  AND Survey_Design__r.Name != :AUTH_StaticVariables.DEFAULT_INSIGHT_PANEL_NAME 
				LIMIT 10000];
			
			for (Question__c insight : insights) {
				insight.Contacts__c = companyMap.get(insight.Survey_Design__r.Company__c).Contacts__r.size();
				
				Set<Id> accountIds = new Set<Id>();
				for (Contact__c contact : companyMap.get(insight.Survey_Design__r.Company__c).Contacts__r) {
					accountIds.add(contact.Account__c);
				}
				
				insight.Accounts__c = accountIds.size();
			}
			
			update insights;
		}
	}
	
	public static void updateLastActivity(List<Contact__c> p_newRecords) {
		for (Contact__c contact : p_newRecords) {
			contact.Last_Activity__c = DateTime.now();
		}
	}
	
	public static void recalculateStrategy(List<Contact__c> p_newRecords) {
		recalculateStrategy(new Map<Id, Contact__c>(), p_newRecords);
	}
	
	public static void recalculateStrategy(Map<Id, Contact__c> p_oldRecordMap, List<Contact__c> p_newRecords) {
		Set<Id> accountIds = new Set<Id>();
		for (Contact__c contact : p_newRecords) {
			Contact__c oldContact = p_oldRecordMap.get(contact.Id);
		
			if ((oldContact != null && oldContact.Account__c != contact.Account__c) || 
					oldContact == null) {
				
				if (contact.Account__c != null) {
					accountIds.add(contact.Account__c);
				}
				
				if (oldContact != null) {
					if (oldContact.Account__c != null) {
						accountIds.add(oldContact.Account__c);
					}
				}
			}
		}
		
		ISA_TriggerHelper.calculateNextStrategy(accountIds);
		
		if ( ! accountIds.isEmpty()) {
			List<Contact__c> contacts = [
				SELECT Id, Account__c 
				FROM Contact__c 
				WHERE Account__c IN :accountIds 
				LIMIT 20000];
			
			Map<Id, Integer> accountToContactsMap = new Map<Id, Integer>();
			for (Contact__c contact : contacts) {
				Id key = contact.Account__c;
				
				if ( ! accountToContactsMap.containsKey(key)) {
					accountToContactsMap.put(key, 0);
				}
				accountToContactsMap.put(key, accountToContactsMap.get(key) + 1);
			}
			
			List<AssignStrategy__c> junctions = [
				SELECT Strategy__r.Contacts__c, Strategy__r.Accounts__c, Account__c
				FROM AssignStrategy__c 
				WHERE Account__c IN :accountIds 
				ORDER BY Strategy__r.Order__c 
				LIMIT 10000];
			
			Map<Id, Integer> strategyToAccountsMap = new Map<Id, Integer>();
			Map<Id, List<Strategy_Design__c>> accountToStrategiesMap = new Map<Id, List<Strategy_Design__c>>();
			for (AssignStrategy__c junction : junctions) {
				Id strategyKey = junction.Strategy__c;
				
				if ( ! strategyToAccountsMap.containsKey(strategyKey)) {
					strategyToAccountsMap.put(strategyKey, 0);
				}
				
				strategyToAccountsMap.put(strategyKey, strategyToAccountsMap.get(strategyKey) + 1);
				
				Id accountKey = junction.Account__c;
				
				if ( ! accountToStrategiesMap.containsKey(accountKey)) {
					accountToStrategiesMap.put(accountKey, new List<Strategy_Design__c>());
				}
				
				accountToStrategiesMap.get(accountKey).add(junction.Strategy__r);
			}
			
			Set<Strategy_Design__c> updateStrategies = new Set<Strategy_Design__c>();
			for (Id accountId : accountToStrategiesMap.keySet()) {
				for (Strategy_Design__c strategy : accountToStrategiesMap.get(accountId)) {
					if (accountToContactsMap.containsKey(accountId)) {
						strategy.Contacts__c = accountToContactsMap.get(accountId);
					}
					
					if (strategyToAccountsMap.containsKey(strategy.Id)) {
						strategy.Accounts__c = strategyToAccountsMap.get(strategy.Id);
					}
					
					updateStrategies.add(strategy);
				}
			}
			
			update new List<Strategy_Design__c>(updateStrategies);
		}
	}
	
	public static void recalculateCounts(List<Contact__c> p_newRecords) {
		Set<Id> companyIds = new Set<Id>();
		for (Contact__c contact : p_newRecords) {
			if (contact.Company__c != null) {
				companyIds.add(contact.Company__c);
			}
		}
		
		if ( ! companyIds.isEmpty()) {
			List<Company__c> companies = [
				SELECT Id, 
					(SELECT Id 
					FROM Survey_Designs__r 
					WHERE Name != :AUTH_StaticVariables.DEFAULT_INSIGHT_PANEL_NAME)
				FROM Company__c 
				WHERE Id IN :companyIds 
				LIMIT 10000];
			
			List<Survey_Design__c> surveys = new List<Survey_Design__c>();
			for (Company__c company : companies) {
				if ( ! company.Survey_Designs__r.isEmpty()) {
					surveys.addAll(company.Survey_Designs__r);
				}
			}
			
			if ( ! surveys.isEmpty()) {
				List<Question__c> questions = [
					SELECT Survey_Design__r.Company__c, Point_Options__c 
					FROM Question__c 
					WHERE Survey_Design__c IN :new Map<Id, Survey_Design__c>(surveys).keySet() 
					LIMIT 50000];
				
				Map<Id, Integer> companyToQuestionsMap = new Map<Id, Integer>();
				Map<Id, Decimal> companyToMaxResponseMap = new Map<Id, Decimal>();
				for (Question__c question : questions) {
					Decimal maxPoint = 0.0;
					if (question.Point_Options__c != null) {
						maxPoint = ISA_ViewContactResultCtrl.getMaxValue(question.Point_Options__c.split(','));
					}
					
					Id key = question.Survey_Design__r.Company__c;
					
					if ( ! companyToQuestionsMap.containsKey(key)) {
						companyToQuestionsMap.put(key, 1);
						
						companyToMaxResponseMap.put(key, maxPoint);
					} else {
						companyToQuestionsMap.put(key, companyToQuestionsMap.get(key) + 1);
						companyToMaxResponseMap.put(key,companyToMaxResponseMap.get(key) + maxPoint);
					}
				}
				
				for (Contact__c contact : p_newRecords) {
					if (contact.Company__c != null && companyToQuestionsMap.containsKey(contact.Company__c)) {
						contact.Count_Questions__c = companyToQuestionsMap.get(contact.Company__c);
						contact.Sum_Max_Response__c = companyToMaxResponseMap.get(contact.Company__c);
					}
				}
			}
		}
	}
	
	public static void removeAnswers(List<Contact__c> p_oldRecords) {
		delete [
			SELECT Id 
			FROM Answer__c 
			WHERE Contact__c IN :new Map<Id, Contact__c>(p_oldRecords).keySet() 
			LIMIT 10000];
	}
	
	public static void recalculateStrategyResults(List<Contact__c> p_newRecords, Boolean p_isInsert) {
		Set<Id> accountIds = new Set<Id>();
		for (Contact__c contact : p_newRecords) {
			if (contact.Account__c != null) {
				accountIds.add(contact.Account__c);
			}
		}
		
		if ( ! accountIds.isEmpty()) {
			List<AssignStrategy__c> junctions = [
				SELECT Strategy__c, Account__c 
				FROM AssignStrategy__c 
				WHERE Account__c IN :accountIds 
				LIMIT 10000];
			
			Set<Id> strategyIds = new Set<Id>();
			for (AssignStrategy__c junction : junctions) {
				strategyIds.add(junction.Strategy__c);
			}
			
			ISA_TriggerHelper.recalculateStrategyResults(junctions, accountIds, strategyIds, p_isInsert);
		}
	}
	
	public static void updateUsers(List<Contact__c> p_newRecords) {
		updateUsers(new Map<Id, Contact__c>(), p_newRecords);
	}
	
	public static void updateUsers(Map<Id, Contact__c> p_oldRecordMap, List<Contact__c> p_newRecords) {
		Set<Id> contactIds = new Set<Id>();
		for (Contact__c contact : p_newRecords) {
			Contact__c oldContact = p_oldRecordMap.get(contact.Id);
			
			if (oldContact != null && 
				(oldContact.Account__c != contact.Account__c || 
				oldContact.Insight_Index__c != contact.Insight_Index__c || 
				oldContact.Completeness_Score__c != contact.Completeness_Score__c || 
				oldContact.Is_No_Data__c != contact.Is_No_Data__c || 
				oldContact.Is_No_Strategy__c != contact.Is_No_Strategy__c)) {
				
				if (oldContact.Is_No_Data__c != contact.Is_No_Data__c || 
					oldContact.Is_No_Strategy__c != contact.Is_No_Strategy__c) {
					
					contactIds.add(contact.Id);
				}
			}
		}
		
		if ( ! contactIds.isEmpty()) {
			List<AssignContact__c> junctions = [
				SELECT User__c
				FROM AssignContact__c 
				WHERE Contact__c IN :contactIds 
				LIMIT 10000];
			
			Set<Id> userIds = new Set<Id>();
			for (AssignContact__c junction : junctions) {
				userIds.add(junction.User__c);
			}
			
			ISA_TriggerHelper.recalculateUsers(userIds);
		}
	}
	
	public static void addContactHistory(List<Contact__c> p_newRecords) {
		addContactHistory(new Map<Id, Contact__c>(), p_newRecords);
	}
	
	public static void addContactHistory(Map<Id, Contact__c> p_oldRecordMap, List<Contact__c> p_newRecords) {
		List<Contact_History__c> createHistories = new List<Contact_History__c>();
		
		for (Contact__c contact : p_newRecords) {
			Contact__c oldContact = p_oldRecordMap.get(contact.Id);
			
			PageReference prUser = new PageReference(AUTH_StaticVariables.prefixSite + '/ISA_ViewUser');
			prUser.getParameters().put('sId', contact.Owner__c);
			
			if (oldContact != null) {
				List<Schema.FieldSetMember> fieldSetMembers = Schema.getGlobalDescribe().get('Contact__c')
					.getDescribe().FieldSets.getMap().get('TrackFields').getFields();
				
				for (Schema.FieldSetMember field : fieldSetMembers) {
					if (oldContact.get(field.getFieldPath()) != contact.get(field.getFieldPath())) {
						String oldValue = '';
						String newValue = '';
						
						if (field.getFieldPath() == 'Account__c') {
							oldValue = String.valueOf(oldContact.get('AccountName__c'));
							newValue = String.valueOf(contact.get('AccountName__c'));
						} else {
							oldValue = String.valueOf(oldContact.get(field.getFieldPath()));
							newValue = String.valueOf(contact.get(field.getFieldPath()));
						}
						
						createHistories.add(
							new Contact_History__c(
								Contact__c = contact.Id, 
								OldValue__c = oldValue, 
								NewValue__c = newValue, 
								Message__c = '<b><a href="' + prUser.getUrl() + '">' + contact.Owner_Name__c + 
										'</a></b> updated <b>' + field.getLabel() + '</b>' + 
										' from ' + ( ! String.isEmpty(oldValue) ? '<b>' + oldValue + '</b>' : '') + 
										' to ' + ( ! String.isEmpty(newValue) ? '<b>' + newValue + '</b>' : '') + '.'
							));
					}
				}
			} else {
				createHistories.add(
					new Contact_History__c(
						Contact__c = contact.Id, 
						Message__c = '<b><a href="' + prUser.getUrl() + '">' + contact.Owner_Name__c + 
							'</a></b> created Contact.'
					));
			}
		}
		
		insert createHistories;
	}
	
	public static void addAccountHistory(List<Contact__c> p_newRecords, Boolean p_isDelete) {
		addAccountHistory(new Map<Id, Contact__c>(), p_newRecords, p_isDelete);
	}
	
	public static void addAccountHistory(Map<Id, Contact__c> p_oldRecordMap, List<Contact__c> p_newRecords, 
			Boolean p_isDelete) {
		
		List<Account_History__c> accountHistories = new List<Account_History__c>();
		
		for (Contact__c contact : p_newRecords) {
			Contact__c oldContact = p_oldRecordMap.get(contact.Id);
			
			PageReference prUser = new PageReference(AUTH_StaticVariables.prefixSite + '/ISA_ViewUser');
			prUser.getParameters().put('sId', contact.Owner__c);
			
			PageReference prContact = new PageReference(AUTH_StaticVariables.prefixSite + '/ISA_ViewContact');
			prContact.getParameters().put('sId', contact.Id);
			
			PageReference prAccount = new PageReference(AUTH_StaticVariables.prefixSite + '/ISA_ViewAccount');
			prAccount.getParameters().put('sId', contact.Account__c);
			
			if (oldContact != null && oldContact.Account__c != contact.Account__c) {
				String oldValue = String.valueOf(oldContact.get('AccountName__c'));
				String newValue = String.valueOf(contact.get('AccountName__c'));
				
				PageReference prOldAccount = new PageReference(AUTH_StaticVariables.prefixSite + '/ISA_ViewAccount');
				prOldAccount.getParameters().put('sId', oldContact.Account__c);
				
				accountHistories.add(
					new Account_History__c(
						Account__c = oldContact.Account__c, 
						Message__c = '<b><a href="' + prUser.getUrl() + '">' + contact.Owner_Name__c + 
							'</a></b> unassgined Contact(<b><a href="' + prContact.getUrl() + '">' + contact.Name + 
							'</a></b>) to Account(<b><a href="' + prAccount.getUrl() + '">' + 
							contact.AccountName__c + '</a></b>)'
					));
				accountHistories.add(
					new Account_History__c(
						Account__c = contact.Account__c, 
						Message__c = '<b><a href="' + prUser.getUrl() + '">' + contact.Owner_Name__c + 
							'</a></b> assgined Contact(<b><a href="' + prContact.getUrl() + '">' + contact.Name + 
							'</a></b>) from Account(<b><a href="' + prOldAccount.getUrl() + '">' + 
							oldContact.AccountName__c + '</a></b>)'
					));
			} else if (oldContact == null) {
				if (p_isDelete) {
					accountHistories.add(
						new Account_History__c(
							Account__c = contact.Account__c, 
							Message__c = '<b><a href="' + prUser.getUrl() + '">' + contact.Owner_Name__c + 
								'</a></b> remove Contact(<b><a href="' + prContact.getUrl() + '">' + contact.Name + 
								'</a></b>)' + ' from Account(<b><a href="' + prAccount.getUrl() + '">' + 
								contact.AccountName__c + '</a></b>)'
						));
				} else {
					accountHistories.add(
						new Account_History__c(
							Account__c = contact.Account__c, 
							Message__c = '<b><a href="' + prUser.getUrl() + '">' + contact.Owner_Name__c + 
								'</a></b> assigned Contact(<b><a href="' + prContact.getUrl() + '">' + contact.Name + 
								'</a></b>)' + ' to Account(<b><a href="' + prAccount.getUrl() + '">' + 
								contact.AccountName__c + '</a></b>).'
						));
				}
			}
		}
		
		insert accountHistories;
	}
	
	public static void removeAssignContact(List<Contact__c> p_oldRecords) {
		delete [SELECT Id FROM AssignContact__c WHERE Contact__c IN :new Map<Id, Contact__c>(p_oldRecords).keySet() LIMIT 10000];
	}
	
	public static void deleteSurveyResults(Map<Id, Contact__c> p_oldRecordMap) {
		delete [SELECT Id FROM Survey_Result__c WHERE Contact__c IN :p_oldRecordMap.keySet() LIMIT 10000];
	}
	
	public static void deleteSurveyAnswers(Map<Id, Contact__c> p_oldRecordMap) {
		delete [SELECT Id FROM Answer__c WHERE Contact__c IN :p_oldRecordMap.keySet() LIMIT 10000];
	}
	
	public static void deleteStrategyResults(Map<Id, Contact__c> p_oldRecordMap) {
		delete [SELECT Id FROM Strategy_Result__c WHERE Contact__c IN :p_oldRecordMap.keySet() LIMIT 10000];
	}
	
	public static void deleteStrategyAnswers(Map<Id, Contact__c> p_oldRecordMap) {
		delete [SELECT Id FROM Strategy_Answer__c WHERE Contact__c IN :p_oldRecordMap.keySet() LIMIT 10000];
	}
	
	public static void deleteContactHistory(Map<Id, Contact__c> p_oldRecordMap) {
		delete [SELECT Id FROM Contact_History__c WHERE Contact__c IN :p_oldRecordMap.keySet() LIMIT 10000];
	}
}