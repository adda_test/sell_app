public with sharing class ISA_SearchCtrl extends AbstractController {
	public String searchString {get; set;}
	public Map<String, List<sObject>> searchResultMap {get; set;}
	public Map<String, List<String>> fieldsMap {get; set;}
	private Map<String, List<String>> fieldsNameMap {get; set;}
	public Map<String, String> displayFieldsMap {get; set;}
	
	public ISA_SearchCtrl() {
		super();
		
		this.searchResultMap = new Map<String, List<sObject>>();
		this.fieldsMap = new Map<String, List<String>>();
		this.fieldsNameMap = new Map<String, List<String>>();
		this.displayFieldsMap = new Map<String, String>();
		
		Map<String, String> fieldSetMap = ISA_SObjectHelper.getFieldSetValues('Account__c', 'SearchFields');
		displayFieldsMap.putAll(fieldSetMap);
		List<String> fieldList = new List<String>();
		fieldList.addAll(fieldSetMap.values());
		this.fieldsMap.put('Account__c', new List<String>(fieldSetMap.keySet()));
		this.fieldsNameMap.put('Account__c', fieldList);
		
		fieldSetMap = ISA_SObjectHelper.getFieldSetValues('Contact__c', 'SearchFields');
		displayFieldsMap.putAll(fieldSetMap);
		fieldList = new List<String>();
		fieldList.addAll(fieldSetMap.values());
		this.fieldsMap.put('Contact__c', new List<String>(fieldSetMap.keySet()));
		this.fieldsNameMap.put('Contact__c', fieldList);
		
		fieldSetMap = ISA_SObjectHelper.getFieldSetValues('Company__c', 'SearchFields');
		displayFieldsMap.putAll(fieldSetMap);
		fieldList = new List<String>();
		fieldList.addAll(fieldSetMap.values());
		this.fieldsMap.put('Company__c', new List<String>(fieldSetMap.keySet()));
		this.fieldsNameMap.put('Company__c', fieldList);
		
		fieldSetMap = ISA_SObjectHelper.getFieldSetValues('User__c', 'SearchFields');
		displayFieldsMap.putAll(fieldSetMap);
		fieldList = new List<String>();
		fieldList.addAll(fieldSetMap.values());
		this.fieldsMap.put('User__c', new List<String>(fieldSetMap.keySet()));
		this.fieldsNameMap.put('User__c', fieldList);
		
		fieldSetMap = ISA_SObjectHelper.getFieldSetValues('Question__c', 'SearchFields');
		displayFieldsMap.putAll(fieldSetMap);
		fieldList = new List<String>();
		fieldList.addAll(fieldSetMap.values());
		this.fieldsMap.put('Question__c', new List<String>(fieldSetMap.keySet()));
		this.fieldsNameMap.put('Question__c', fieldList);
	}
	
	public PageReference initPage() {
		PageReference afterInit = init();
		if (afterInit != null) {
			return afterInit;
		}
		
		doSearch();
		
		return null;
	}
	
	public void doSearch() {
		String searchValue = ApexPages.currentPage().getParameters().get('searchString');
		
		if (String.isEmpty(this.searchString)) {
			this.searchString = searchValue;
		}
		
		this.searchResultMap.keySet().clear();
		
		if ( ! String.isEmpty(this.searchString)) {
			String findClause = this.searchString + '*';
			
			String whereClause = ( ! String.isEmpty(this.userCompany)) 
				? ' WHERE Company__c = \'' + this.userCompany + '\'' 
				: '';
			
			String companyWhereClause = ( ! String.isEmpty(this.userCompany)) 
				? ' WHERE Id =\'' + this.userCompany + '\'' 
				: '';
			
			String surveyWhereClause = ( ! String.isEmpty(this.userCompany)) 
				? ' WHERE Survey_Design__r.Company__c = \'' + this.userCompany + '\'' 
				: '';
			
			List<String> additionalAccountFields = new List<String> {'Company__r.Name'};
			List<String> additionalContactFields = new List<String> {'Account__r.Name', 'Company__r.Name', 'User__r.Name'};
			List<String> additionalCompanyFields = new List<String> {};
			List<String> additionalUserFields = new List<String> {'Company__r.Name'};
			List<String> additionalQuestionFields = new List<String> {'Survey_Design__r.Name'};
			
			List<List<sObject>> selectedRecords = search.query('FIND \'' + findClause + '\' IN Name FIELDS ' + 
					'RETURNING Account__c (' + String.join(this.fieldsNameMap.get('Account__c'), ', ') + 
							( ! additionalAccountFields.isEmpty() ? ', ' : '') + 
							String.join(additionalAccountFields, ',') + whereClause + '), ' + 
					'Contact__c(' + String.join(this.fieldsNameMap.get('Contact__c'), ', ') + 
							( ! additionalContactFields.isEmpty() ? ', ' : '') + 
							String.join(additionalContactFields, ',') + whereClause + '), ' + 
					'Company__c(' + String.join(this.fieldsNameMap.get('Company__c'), ', ') + 
							( ! additionalCompanyFields.isEmpty() ? ', ' : '') + 
							String.join(additionalCompanyFields, ',') + companyWhereClause + '), ' + 
					'User__c(' + String.join(this.fieldsNameMap.get('User__c'), ', ') + 
							( ! additionalUserFields.isEmpty() ? ', ' : '') + 
							String.join(additionalUserFields, ',') + whereClause + '), ' + 
					'Question__c(' + String.join(this.fieldsNameMap.get('Question__c'), ', ') + 
							( ! additionalQuestionFields.isEmpty() ? ', ' : '') + 
							String.join(additionalQuestionFields, ',') + surveyWhereClause + ')');
			
			for (List<sObject> records : selectedRecords) {
				for (sObject obj : records) {
					String key = obj.getSObjectType().getDescribe().getLabel();
					List<sObject> mapRecords = this.searchResultMap.get(key);
					
					if (mapRecords == null) {
						this.searchResultMap.put(key, new List<sObject>());
					}
					
					this.searchResultMap.get(key).add(obj);
				}
			}
			
			if (this.searchResultMap.keySet().isEmpty()) {
				ApexPages.addMessage(
					new ApexPAges.Message(
						ApexPages.Severity.INFO, 
						'Your search did not match any results.'));
			}
		}
	}
	
	public PageReference doView() {
		String filedName = ApexPages.currentPage().getParameters().get('fieldName');
		String recordId = ApexPages.currentPage().getParameters().get('recordId');
		
		if ( ! String.isEmpty(recordId) && ! String.isEmpty(filedName)) {
			if (recordId.left(3) == Account__c.getSObjectType().getDescribe().getKeyPrefix()) {
				PageReference pr = new PageReference(AUTH_StaticVariables.prefixSite + '/ISA_ViewAccount');
				pr.getParameters().put('sId', recordId);
				pr.getParameters().put('returnUrl', AUTH_StaticVariables.prefixSite + '/ISA_Search?searchString=' + searchString);
				pr.setRedirect(true);
				
				return pr;
			} else if (recordId.left(3) == Contact__c.getSObjectType().getDescribe().getKeyPrefix()) {
				PageReference pr;
				
				if (this.userRole == 'Company User') {
					pr = new PageReference(AUTH_StaticVariables.prefixSite + '/ISA_ViewContactUser');
					pr.getParameters().put('cId', recordId);
				} else {
					pr = new PageReference(AUTH_StaticVariables.prefixSite + '/ISA_ViewContact');
					pr.getParameters().put('sId', recordId);
				}
				
				pr.getParameters().put('returnUrl', AUTH_StaticVariables.prefixSite + '/ISA_Search?searchString=' + searchString);
				pr.setRedirect(true);
				
				return pr;
			} else if (recordId.left(3) == Company__c.getSObjectType().getDescribe().getKeyPrefix()) {
				PageReference pr = new PageReference(AUTH_StaticVariables.prefixSite + '/ISA_ViewCompany');
				pr.getParameters().put('sId', recordId);
				pr.getParameters().put('returnUrl', AUTH_StaticVariables.prefixSite + '/ISA_Search?searchString=' + searchString);
				pr.setRedirect(true);
				
				return pr;
			} else if (recordId.left(3) == User__c.getSObjectType().getDescribe().getKeyPrefix()) {
				PageReference pr = new PageReference(AUTH_StaticVariables.prefixSite + '/ISA_ViewUser');
				pr.getParameters().put('sId', recordId);
				pr.getParameters().put('returnUrl', AUTH_StaticVariables.prefixSite + '/ISA_Search?searchString=' + searchString);
				pr.setRedirect(true);
				
				return pr;
			} else {
				ApexPages.addMessage(
					new ApexPages.Message(
						ApexPages.Severity.INFO, 
						'Object is not defined.'));
			}
		}
		
		return null;
	}
}