public with sharing class SurveyDesignHandler {
	public static Boolean enablesTrigger = true;
	
	/**
	* @description Create Survey Result for each Survey Design.
	*/
	public static void createSurveyResult(List<Survey_Design__c> p_newRecords) {
		Set<Id> companyIds = new Set<Id>();
		
		for (Survey_Design__c survey : p_newRecords) {
			if (survey.Company__c != null) {
				companyIds.add(survey.Company__c);
			}
		}
		
		List<Contact__c> selectedContacts = [
			SELECT Account__r.Company__c 
			FROM Contact__c 
			WHERE Account__r.Company__c IN :companyIds 
			LIMIT 10000];
		
		if ( ! selectedContacts.isEmpty()) {
			Map<Id, List<Contact__c>> companyToContactsMap = new Map<Id, List<Contact__c>>();
			
			for (Contact__c contact : selectedContacts) {
				Id key = contact.Account__r.Company__c;
				
				if ( ! companyToContactsMap.containsKey(key)) {
					companyToContactsMap.put(key, new List<Contact__c>());
				}
				companyToContactsMap.get(key).add(contact);
			}
			
			List<Survey_Result__c> surveyResults = new List<Survey_Result__c>();
			for (Survey_Design__c survey : p_newRecords) {
				if (survey.Company__c != null) {
					for (Contact__c contact : companyToContactsMap.get(survey.Company__c)) {
						surveyResults.add(
							new Survey_Result__c(
								Survey_Design__c = survey.Id, 
								Contact__c = contact.Id, 
								Count_Questions__c = 0, 
								Count_Answers__c = 0, 
								Sum_Response__c = 0, 
								Sum_Max_Response__c = 0
							));
					}
				}
			}
			
			if ( ! surveyResults.isEmpty()) {
				insert surveyResults;
			}
		}
	}
	
	public static void populateLastActivity(List<Survey_Design__c> p_newRecords) {
		for (Survey_Design__c survey : p_newRecords) {
			survey.Last_Activity__c = DateTime.now();
		}
	}
	
	public static void deleteInsights(Map<Id, Survey_Design__c> p_oldRecordMap) {
		delete [SELECT Id FROM Question__c WHERE Survey_Design__c IN :p_oldRecordMap.keySet() LIMIT 10000];
	}
}