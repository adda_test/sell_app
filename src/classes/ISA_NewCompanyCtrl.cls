public with sharing class ISA_NewCompanyCtrl extends AbstractController {
	public Company__c company {get; set;}
	public User__c companyUser {get;set;}
	public Date startDate {get; set;}
	public Boolean passwordError {get; set;}
	public Integer maxUsers {get; set;}
	
	private String companyId;
	
	public ISA_NewCompanyCtrl() {
		this.companyId = ApexPages.currentPage().getParameters().get('sId');
		this.sObjectLabel = 'Company';
		this.passwordError = false;
		this.maxUsers = 0;
	}
	
	public PageReference initPage() {
		PageReference afterInit = init();
		if (afterInit != null) {
			return afterInit;
		}
		
		if (! String.isEmpty(companyId)) {
			List<Company__c> companies = [
					SELECT Name, Address1__c, Address2__c, City__c, State__c, Pincode__c, 
						Admin_First_Name__c, Admin_Last_Name__c, Admin_Phone__c, 
						Admin_Email__c, Admin_Login__c, Admin_Password__c, Trial_Start__c, 
						Maximum_Users__c, Trial__c, Trial_End__c, Type__c, CreatedDate, Active__c, 
						Country__c 
					FROM Company__c 
					WHERE Id = :companyId 
					LIMIT 1];
			
			if ( ! companies.isEmpty()) {
				this.company = companies.get(0);
				this.startDate = this.company.Trial_Start__c;
			}
			//get company user
			List<User__c> users= [SELECT Id, Username__c, Password__c FROM User__c WHERE Role__c = 'Company Admin' AND Company__c = :this.company.Id];
			System.debug('users:' + users);
			if ( ! users.isEmpty()) {
				this.companyUser = users.get(0);
			}
		} else {
			this.company = new Company__c();
			this.startDate = Date.today();
		}
		
		return null;
	}
	
	public PageReference doSave() {
		try {
			if (String.isEmpty(this.company.Admin_Password__c)) {
				passwordError = true;
				
				ApexPages.addMessage(
					new ApexPages.Message(
						ApexPages.Severity.ERROR, 
						'Admin Password: Value is required.'));
				return null;
			} else {
				passwordError = false;
			}
		
			// check password
			if (Utils.isWeakPassword(this.company.Admin_Password__c)) {
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 
						'Your password is weak.'));
				return null;
			}
			
			if (Utils.isTooSmallPassword(this.company.Admin_Password__c)) {
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 
						'Your password should be at least 6 characters.'));
				return null;
			}
			
			this.company.Maximum_Users__c = maxUsers;
			this.company.Trial_Start__c = (this.startDate != null) ? this.startDate : Date.today();
			
			upsert this.company;
			
			if (String.isEmpty(this.companyId)) {
				// check user
				List<User__c> existUsers = [SELECT Id FROM User__c WHERE Username__c = :this.company.Admin_Login__c];
				if (existUsers != null && !existUsers.isEmpty()) {
					ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 
						'User with this login already exists.'));
					return null;
				}
				//Create a Company Admin user by Admin fields in Company
				User__c companyAdmin = new User__c(
					First_Name__c = this.company.Admin_First_Name__c, 
					Last_Name__c = this.company.Admin_Last_Name__c, 
					Phone__c = this.company.Admin_Phone__c, 
					Username__c = this.company.Admin_Login__c, 
					Password__c = AUTH_Utils.getMd5(this.company.Admin_Password__c), 
					Role__c = 'Company Admin', 
					Expiration_Date__c = this.company.Trial_End__c, 
					Company__c = this.company.Id,
					Create_Date__c = Date.today(),
					Email__c = this.company.Admin_Email__c
				);
				
				try {
					insert companyAdmin;
				} catch(Exception ex) {
					ApexPages.addMessage(
						new ApexPages.Message(
							ApexPages.Severity.ERROR, ex.getMessage()));
				}
			} else {
				// update password
				// TODO: more fields to update
				this.companyUser.Password__c = AUTH_Utils.getMd5(this.company.Admin_Password__c);
				this.companyUser.Username__c = this.company.Admin_Login__c;
				update this.companyUser;
			}				
			
			PageReference pr = new PageReference(AUTH_StaticVariables.prefixSite + '/ISA_ViewCompany');
			pr.getParameters().put('sId', this.company.Id);
			pr.getParameters().put('returnUrl', AUTH_StaticVariables.prefixSite + '/ISA_ListViewCompany');
			pr.setRedirect(true);
			
			return pr;
		} catch(DmlException ex) {
			System.debug('Exception:' + ex);
			//TODO:
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Company can not be changed.'));
			return null;
		}
	}
	
	public List<String> getTypes() {
		Schema.sObjectType sobject_type = Company.getSObjectType();
		Schema.DescribeSObjectResult sobject_describe = sobject_type.getDescribe();
		Map<String, Schema.SObjectField> field_map = sobject_describe.fields.getMap();
		List<Schema.PicklistEntry> pick_list_values = field_map.get('Type__c').getDescribe().getPickListValues();
		List<String> options = new List<String>();
		
		for (Schema.PicklistEntry a : pick_list_values) {
			options.add(a.getValue());
		}
		
		return options;
	}
	
	@TestVisible protected override void collectRetParams(PageReference pr) {
		pr.getParameters().put('sId',ApexPages.currentPage().getParameters().get('sId'));
	}
}