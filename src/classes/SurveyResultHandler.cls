public with sharing class SurveyResultHandler {
	public static Boolean enablesTrigger = true;
	
	public static void recalculateContact(List<Survey_Result__c> p_newRecords) {
		recalculateContact(new Map<Id, Survey_Result__c>(), p_newRecords);
	}
	
	public static void recalculateContact(Map<Id, Survey_Result__c> p_oldRecordsMap, List<Survey_Result__c> p_newRecords) {
		Map<Id, List<Survey_Result__c>> contactToResultMap = new Map<Id, List<Survey_Result__c>>();
		
		for (Survey_Result__c result : p_newRecords) {
			Survey_Result__c oldResult = p_oldRecordsMap.get(result.Id);
			
			if ((oldResult != null && (oldResult.IsCompleteness__c != result.IsCompleteness__c || 
				oldResult.Sum_Response__c != result.Sum_Response__c || 
				oldResult.Sum_Max_Response__c != result.Sum_Max_Response__c)) || 
					oldResult == null) {
				
				if (result.Contact__c != null) {
					Id key = result.Contact__c;
					
					if ( ! contactToResultMap.containsKey(key)) {
						contactToResultMap.put(key, new List<Survey_Result__c>());
					}
					
					contactToResultMap.get(key).add(result);
				}
			}
		}
		
		if ( ! contactToResultMap.keySet().isEmpty()) {
			List<Contact__c> contacts = [
				SELECT Count_Answers__c, Count_Questions__c, Sum_Response__c, Sum_Max_Response__c, 
					(SELECT Sum_Response__c 
					FROM Survey_Results__r),
					(SELECT Id 
					FROM Asnwers__r)
				FROM Contact__c 
				WHERE Id IN :contactToResultMap.keySet() 
				LIMIT 10000];
			
			if ( ! contacts.isEmpty()) {
				for (Contact__c contact : contacts) {
					contact.Count_Answers__c = ( ! contact.Asnwers__r.isEmpty()) 
						? contact.Asnwers__r.size() 
						: 0;
					
					if ( ! contact.Survey_Results__r.isEmpty()) {
						Decimal sumResponse = 0;
						for (Survey_Result__c result : contact.Survey_Results__r) {
							
							sumResponse += (result.Sum_Response__c != null) ? result.Sum_Response__c : 0;
						}
						contact.Sum_Response__c = sumResponse;
					}
				}
				
				update contacts;
			}
		}
	}
	
	public static void recalculateSurvey(Map<Id, Survey_Result__c> p_oldRecordMap, List<Survey_Result__c> p_newRecords) {
		Set<Id> resultIds = new Set<Id>();
		for (Survey_Result__c result : p_newRecords) {
			Survey_Result__c oldResult = p_oldRecordMap.get(result.Id);
			
			if (oldResult != null && oldResult.Sum_Max_Response__c != result.Sum_Max_Response__c) {
				if (result.Survey_Design__c != null) {
					resultIds.add(result.Id);
				}
			}
		}
		
		if ( ! resultIds.isEmpty()) {
			List<Survey_Result__c> results = [
				SELECT Survey_Design__r.Company__c 
				FROM Survey_Result__c 
				WHERE Id IN :resultIds 
				  AND Survey_Design__r.Company__c != null 
				LIMIT 10000];
			
			Set<Id> companyIds = new Set<Id>();
			for (Survey_Result__c result : results) {
				companyIds.add(result.Survey_Design__r.Company__c);
			}
			
			List<Survey_Design__c> surveys = [
				SELECT Id, 
					(SELECT Sum_Max_Response__c 
					FROM Survey_Results__r) 
				FROM Survey_Design__c 
				WHERE Company__c IN :companyIds 
				LIMIT 10000];
			
			if ( ! surveys.isEmpty()) {
				Decimal sumMaxResponse = 0;
				Map<Id, Decimal> surveyMaxResponseMap = new Map<Id, Decimal>();
				for (Survey_Design__c survey : surveys) {
					Decimal maxResponse = 0;
					
					for (Survey_Result__c result : survey.Survey_Results__r) {
						if (result.Sum_Max_Response__c != null) {
							sumMaxResponse += result.Sum_Max_Response__c;
							
							if (maxResponse < result.Sum_Max_Response__c) {
								maxResponse = result.Sum_Max_Response__c;
							}
						}
					}
					
					surveyMaxResponseMap.put(survey.Id, maxResponse);
				}
				
				for (Survey_Design__c survey : surveys) {
					survey.Weight__c = ((surveyMaxResponseMap.get(survey.Id) * 1.0) / sumMaxResponse) * 100;
				}
				
				update surveys;
			}
		}
	}
	
	private static Decimal calculate(Decimal p_source, Decimal p_oldValue, Decimal p_newValue) {
		return ((p_source != null) ? p_source : 0) - 
				((p_oldValue != null) ? p_oldValue : 0) + 
				((p_newValue != null) ? p_newValue : 0);
	}
}