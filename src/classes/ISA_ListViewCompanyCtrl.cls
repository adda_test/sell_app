public with sharing class ISA_ListViewCompanyCtrl extends AbstractController {
	public ISA_ListViewCompanyCtrl() {
		super();
		
		this.sObjectLabel = 'Company';
	}
	
	public PageReference initPage() {
		PageReference pr = super.init();
		
		if (pr != null) {
			return pr;
		}
		
		this.sObjectType = sObjectNameMap.get(this.sObjectLabel);
		
		fillRecords();
		return null;
	}
}