@isTest
private class ISA_TransportDataCtrlTest {
	
	static User__c tempUser(id id) {
        User__c testUser = new User__c(
            First_Name__c = 'First', 
            Last_Name__c = 'Last', 
            Phone__c = '123455678', 
            Username__c = 'testLogin', 
            Password__c = 'testPassword', 
            Role__c = 'Company Admin', 
            Company__c = id,
            Expiration_Date__c = Date.today(), 
            Create_Date__c = Date.today(),
            SessionId__c = '1',
            Active__c = true,
            Email__c = 'test@mail.com');
        return testUser;
    }
    
	static Company__c tempCompany() {
        Company__c company = new Company__c(
            Name = 'test name',
            Admin_First_Name__c = 'firstname',
            Admin_Last_Name__c = 'lastname',
            Admin_Email__c = 'test@mail.com',
            Admin_Login__c = 'testLogin',
            Admin_Password__c = 'testPassword',
            Address1__c = 'addr1',
            Address2__c = 'addr2',
            Trial_Start__c = Date.today(),
            Maximum_Users__c = 10);
        return company;
    }
	
	@isTest
    private static void testInitPageExport() {
    	
        Test.startTest();
        
        Company__c testCompany = tempCompany();           
        insert testCompany;      
        User__c testUser = tempUser(testCompany.Id);    
        insert testUser; 
        
        ApexPages.currentPage().getParameters().put('sobjectlabel', 'User');
		ApexPages.currentPage().getParameters().put('tranfertype','Export');
		Cookie testCookie = new Cookie('SessionId','1',null,-1,false);
        ApexPages.currentPage().setCookies(new Cookie[] {testCookie});
        
        ISA_TransportDataCtrl controller = new ISA_TransportDataCtrl();
        
        controller.init();
        controller.initPage();
        controller.doRun();
        controller.doBack();
        
        Test.stopTest();
    }  
    
    @isTest
    private static void testInitPageImport() {
    	
        Test.startTest();
        
        Company__c testCompany = tempCompany();           
        insert testCompany;      
        User__c testUser = tempUser(testCompany.Id);    
        insert testUser; 
        
        ApexPages.currentPage().getParameters().put('sobjectlabel', 'Contact');
		ApexPages.currentPage().getParameters().put('tranfertype','Import');
		Cookie testCookie = new Cookie('SessionId','1',null,-1,false);
        ApexPages.currentPage().setCookies(new Cookie[] {testCookie});
        
        ISA_TransportDataCtrl controller = new ISA_TransportDataCtrl();
        
        controller.init();
        controller.initPage();
        
        controller.attachment.Body = Blob.valueOf('Test Body');
        controller.doRun();
        
        Test.stopTest();
    }    
    
    @isTest
    private static void testInitPageAnotherTranferType() {
    	
        Test.startTest();
        
        Company__c testCompany = tempCompany();           
        insert testCompany;      
        User__c testUser = tempUser(testCompany.Id);    
        insert testUser; 
        
        ApexPages.currentPage().getParameters().put('sobjectlabel', 'Contact');
		ApexPages.currentPage().getParameters().put('tranfertype','Test');
		Cookie testCookie = new Cookie('SessionId','1',null,-1,false);
        ApexPages.currentPage().setCookies(new Cookie[] {testCookie});
        
        ISA_TransportDataCtrl controller = new ISA_TransportDataCtrl();
        
        controller.init();
        controller.initPage();
        
        Test.stopTest();
    }   
        
}