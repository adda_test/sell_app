public with sharing class ISA_NewInsightCtrl extends AbstractController {
	public Question__c question {get; set;}
	public List<AnswerWrapper> answers {get; set;}
	public String pointValue {get; set;}
	public String selectedPanel {get; set;}
	public List<SelectOption> panelOptions {get; set;}
	
	private Id defaultSurveyId;
	
	public ISA_NewInsightCtrl() {
		super();
		
		this.sObjectLabel = 'Insight';
		this.defaultSurveyId = null;
		this.answers = new List<AnswerWrapper>();
		this.pointValue = '0';
		this.selectedPanel = '';
		this.panelOptions = new List<SelectOption>();
	}
	
	public PageReference initPage() {
		PageReference afterInit = init();
		if (afterInit != null) {
			return afterInit;
		}
		
		this.question = new Question__c();
		String questionId = ApexPages.currentPage().getParameters().get('sId');
		String panelId = ApexPages.currentPage().getParameters().get('panelId');
		
		if ( ! String.isEmpty(questionId)) {
			List<Question__c> questions = [
					SELECT Name, Survey_Design__r.Company__r.Name, Type__c, Response_Options__c, Point_Options__c, 
						Survey_Design__r.Name 
					FROM Question__c 
					WHERE Id = :questionId 
					LIMIT 1];
			
			if ( ! questions.isEmpty()) {
				this.question = questions.get(0);
				
				List<Survey_Design__c> surveys = [
					SELECT Name 
					FROM Survey_Design__c 
					WHERE Company__c = :this.question.Survey_Design__r.Company__c 
					LIMIT 1000];
				
				if ( ! surveys.isEmpty()) {
					for (Survey_Design__c survey : surveys) {
						this.panelOptions.add(new SelectOption(survey.Id, survey.Name));
					}
					this.selectedPanel = this.panelOptions.get(0).getValue();
				}
				
				if ( ! String.isEmpty(panelId)) {
					this.selectedPanel = panelId;
				}
				
				if (this.question.Response_Options__c != null) {
					List<String> values = this.question.Response_Options__c.split(',');
					List<String> points = this.question.Point_Options__c.split(',');
					
					for (Integer i = 0; i < values.size(); i++) {
						this.answers.add(new AnswerWrapper(values.get(i), points.get(i)));
					}
					
					if (this.question.Type__c == 'Single select drop-down' || 
						this.question.Type__c == 'Multi select drop-down' || 
						this.question.Type__c == 'Open-ended') {
						
						this.pointValue = points.get(points.size() - 1);
					} else {
						this.pointValue = points.get(0);
					}
				}
			}
		} else {
			if (this.userRole == 'Admin') {
				List<Survey_Design__c> surveys = [
					SELECT Name, Company__r.Name 
					FROM Survey_Design__c 
					ORDER BY Company__r.Name ASC
					LIMIT 1000];
				
				if ( ! surveys.isEmpty()) {
					this.panelOptions.clear();
					
					for (Survey_Design__c survey : surveys) {
						this.panelOptions.add(new SelectOption(survey.Id, survey.Company__r.Name + ': ' + survey.Name));
					}
					this.selectedPanel = this.panelOptions.get(0).getValue();
				}
			} else {
				List<Survey_Design__c> surveys = [
					SELECT Name 
					FROM Survey_Design__c 
					WHERE Company__c = :this.userCompany 
					LIMIT 1000];
				
				if ( ! surveys.isEmpty()) {
					this.panelOptions.clear();
					
					for (Survey_Design__c survey : surveys) {
						this.panelOptions.add(new SelectOption(survey.Id, survey.Name));
					}
					this.selectedPanel = this.panelOptions.get(0).getValue();
				}
			}
			
			
		}
		
		return null;
	}
	
	public PageReference doSave() {
		try {
			if (String.isEmpty(this.question.Name)) {
				ApexPages.addMessage(
					new ApexPages.Message(
						ApexPages.Severity.ERROR, 'You should enter Insight Name.'));
				
				return null;
			}
			
			this.question.Title__c = this.question.Name.replace('\'', '\"');
			this.question.Survey_Design__c = this.selectedPanel;
			
			if (this.question.Type__c == 'Single select drop-down' || 
				this.question.Type__c == 'Multi select drop-down' || 
				this.question.Type__c == 'Open-ended') {
				
				List<String> values = new List<String>();
				List<String> points = new List<String>();
				for (AnswerWrapper wrapper : this.answers) {
					if ( ! String.isEmpty(wrapper.value) && ! String.isEmpty(wrapper.point)) {
						values.add(wrapper.value);
						points.add(wrapper.point);
					}
				}
				
				if ( ! values.isEmpty()) {
					this.question.Response_Options__c = String.join(values, ',');
					this.question.Point_Options__c = String.join(points, ',');
					
					if (this.question.Type__c == 'Open-ended') {
						this.question.Point_Options__c += ',' + this.pointValue;
					}
				}
			} else if (this.pointValue.isNumeric() && ! this.pointValue.contains(',')) {
				this.question.Point_Options__c = this.pointValue;
			} else if ( ! this.pointValue.isNumeric() || this.pointValue.contains(',')) {
				ApexPages.addMessage(
					new ApexPages.Message(
						ApexPages.Severity.ERROR, 'Answer point should\'t contains "," character.'));
			}
			
			this.question.Owner__c = this.user.Id;
			
			upsert this.question;
			
			String returnUrl = ApexPages.currentPage().getParameters().get('returnUrl');
			PageReference pr;
			
			pr = new PageReference(AUTH_StaticVariables.prefixSite + '/ISA_ViewInsight');
			pr.getParameters().put('returnUrl', AUTH_StaticVariables.prefixSite + '/ISA_ListViewInsight');
			
			if (this.question.Id != null) {
				pr.getParameters().put('sId', this.question.Id);
			}
			
			if (ApexPages.currentPage().getParameters().containsKey('panelId')) {
				pr.getParameters().put('panelId', ApexPages.currentPage().getParameters().get('panelId'));
			}
			
			pr.setRedirect(true);
			
			return pr;
		} catch(Exception ex) {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
			
			return null;
		}
	}
	
	public void addAnswer() {
		this.answers.add(new AnswerWrapper('', ''));
	}
	
	public void removeAnswer() {
		String answerIndex = Apexpages.currentPage().getParameters().get('answerIndex');
		
		if ( ! String.isEmpty(answerIndex) && answerIndex.isNumeric()) {
			this.answers.remove(Integer.valueOf(answerIndex));
		}
	}
	
	public void doCreateInsightPanel() {
		String insightName = Apexpages.currentPage().getParameters().get('insightName');
		
		if ( ! String.isEmpty(insightName)) {
			Survey_Design__c survey = new Survey_Design__c(Name = insightName, Company__c = this.userCompany);
			insert survey;
			
			this.panelOptions.add(new SelectOption(survey.Id, survey.Name));
			this.selectedPanel = survey.Id;
		}
	}
	
	@TestVisible protected override void collectRetParams(PageReference pr) {
		pr.getParameters().put('sId',ApexPages.currentPage().getParameters().get('sId'));
	}
	
	public class AnswerWrapper {
		public String value {get; set;}
		public String point {get; set;}
		
		public AnswerWrapper(String p_value, String p_point) {
			this.value = p_value;
			this.point = p_point;
		}
	}
}