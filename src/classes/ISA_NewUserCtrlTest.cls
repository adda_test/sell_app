/**
* @description 
*/
@isTest
private class ISA_NewUserCtrlTest {    

    static User__c passUser(id id,string password) {
        User__c testUser = new User__c(
            First_Name__c = 'First', 
            Last_Name__c = 'Last', 
            Phone__c = '123455678', 
            Username__c = 'testLogin', 
            Password__c = password, 
            Role__c = 'Admin', 
            Company__c = id,
            Expiration_Date__c = Date.today(), 
            Create_Date__c = Date.today(),
            SessionId__c = '1',
            Active__c = true,
            Email__c = 'test@mail.com');
        return testUser;
    }
    
    static User__c companyUser(id id,string password) {
        User__c testUser = new User__c(
            First_Name__c = 'First', 
            Last_Name__c = 'Last', 
            Phone__c = '123455678', 
            Username__c = 'testLogin', 
            Password__c = password, 
            Role__c = 'Company User', 
            Company__c = id,
            Expiration_Date__c = Date.today(), 
            Create_Date__c = Date.today(),
            SessionId__c = '1',
            Active__c = false,
            Email__c = 'test@mail.com');
        return testUser;
    }
    
    static Company__c passCompany(Integer max) {
        Company__c company = new Company__c(
            Name = 'test name',
            Admin_First_Name__c = 'firstname',
            Admin_Last_Name__c = 'lastname',
            Admin_Email__c = 'test@mail.com',
            Admin_Login__c = 'testLogin',
            Admin_Password__c = 'testPassword',
            Address1__c = 'addr1',
            Address2__c = 'addr2',
            Trial_Start__c = Date.today(),
            Maximum_Users__c = max);
        return company;
    }
    
    @isTest
    private static void testInit() {
        Test.startTest();
        
        Company__c testCompany = passCompany(10);           
        insert testCompany;      
        User__c testUser = passUser(testCompany.Id,'testPassword');    
        insert testUser; 
                              
        ApexPages.currentPage().getParameters().put('sId', testUser.Id);
        
        ISA_NewUserCtrl controller = new ISA_NewUserCtrl();
        controller.initPage();
        
        System.assertNotEquals(null, controller.init());       
        
        Test.stopTest();
    }  
    
     @isTest
     private static void testInitWithUserId() {
        
        Test.startTest();
        
        Company__c testCompany = passCompany(10);           
        insert testCompany;      
        User__c testUser = passUser(testCompany.Id,'testPassword');    
        insert testUser; 
                              
        ApexPages.currentPage().getParameters().put('sId', testUser.Id);
        
        Cookie testCookie = new Cookie('SessionId','1',null,-1,false);
        ApexPages.currentPage().setCookies(new Cookie[] {testCookie});
        
        ISA_NewUserCtrl controller = new ISA_NewUserCtrl();
        controller.initPage();
  
        System.assertEquals(testUser.Id, controller.currentUser.Id);    
        
        Test.stopTest();
    } 
    
    @isTest
	private static void testInitWithoutUserId() {
        
        Test.startTest();
       
        Company__c testCompany = passCompany(10);           
        insert testCompany;      
        User__c testUser = passUser(testCompany.Id,'testPassword');    
        insert testUser;  
                              
        Cookie testCookie = new Cookie('SessionId','1',null,-1,false);
        ApexPages.currentPage().setCookies(new Cookie[] {testCookie});
        
        ISA_NewUserCtrl controller = new ISA_NewUserCtrl();
        controller.initPage();
 
        System.assertEquals(null, controller.currentUser.Id);
        
        Test.stopTest();
    } 
    
    @isTest
	private static void testInitWithUserCompany() {
        
    	Test.startTest();
       
        Company__c testCompany = passCompany(10);           
        insert testCompany;      
        User__c testUser = passUser(testCompany.Id,'testPassword');    
        insert testUser;  
                               
        Cookie testCookie = new Cookie('SessionId','1',null,-1,false);
        ApexPages.currentPage().setCookies(new Cookie[] {testCookie});
        
        ISA_NewUserCtrl controller = new ISA_NewUserCtrl();
        controller.initPage();

        System.assertEquals(true, controller.isFromCompanyBySysAdmin);
        
        Test.stopTest();
	} 
	
    @isTest
    private static void testInitWithCurrentUserCompany() {
        
    	Test.startTest();
       
        Company__c testCompany = passCompany(10);           
        insert testCompany;      
        User__c testUser = passUser(testCompany.Id,'testPassword');    
        insert testUser;  
         
        ApexPages.currentPage().getParameters().put('companyId', testCompany.Id);                      
        Cookie testCookie = new Cookie('SessionId','1',null,-1,false);
        ApexPages.currentPage().setCookies(new Cookie[] {testCookie});
        
        ISA_NewUserCtrl controller = new ISA_NewUserCtrl();
        controller.initPage();
 
        System.assertEquals(testUser.Company__c, controller.currentUser.Company__c);
        
        Test.stopTest();
    } 
    	
    @isTest
    private static void testSaveRequireFields() {
        Test.startTest();
        
        Company__c testCompany = passCompany(10);
            
        insert testCompany;         
    
        ISA_NewUserCtrl controller = new ISA_NewUserCtrl();
        
        controller.currentUser = passUser(testCompany.Id,'testPassword');
        controller.doSave();
        System.assertNotEquals(null, controller.currentUser.Id); 
       
        controller.currentUser = passUser(testCompany.Id,'pass');       
        controller.doSave();       
        System.assertEquals(null, controller.currentUser.Id);          
        
        Test.stopTest();
    }
	        
	@isTest
    private static void testDoSaveCurrentUserActive() {
        
    	Test.startTest();
       
        Company__c testCompany = passCompany(10);           
        insert testCompany;      
        User__c testUser = companyUser(testCompany.Id,'testPassword');    
        insert testUser;  
       
        
        ISA_NewUserCtrl controller = new ISA_NewUserCtrl();
        controller.selectedCompany = testCompany.Id;
		controller.currentUser = testUser;
        controller.doSave();
        
        System.assertEquals(null, controller.currentUser.SessionId__c);
        System.assertEquals(controller.selectedCompany, controller.currentUser.Company__c);
        
        Test.stopTest();
    } 
    
    @isTest
    private static void testDoSaveWithUserCount() {
        
    	Test.startTest();
       
        Company__c testCompany = passCompany(0);           
        insert testCompany;      
        User__c testUser = companyUser(testCompany.Id,'testPassword');    
        insert testUser;  
        
        ISA_NewUserCtrl controller = new ISA_NewUserCtrl();
        controller.currentUser = testUser;
        controller.doSave();
 
        System.assertEquals(null, controller.doSave());
        
        Test.stopTest();
    } 
    
    @isTest
    private static void collectRetParamsWithUser() {
        Test.startTest();
        
        PageReference pageRef = ApexPages.currentPage();
        Company__c testCompany = passCompany(10);           
        insert testCompany;      
        User__c testUser = passUser(testCompany.Id,'testPassword');    
        insert testUser; 
                              
        ApexPages.currentPage().getParameters().put('sId', testUser.Id);
        
        ISA_NewUserCtrl controller = new ISA_NewUserCtrl();
        controller.collectRetParams(pageRef);
        
        System.assertEquals(ApexPages.currentPage().getParameters().get('sId'), pageRef.getParameters().get('sId'));
        
        Test.stopTest();
    }
    
    @isTest
    private static void collectRetParamsWithCompany() {
        Test.startTest();
        
        PageReference pageRef = ApexPages.currentPage();
        Company__c testCompany = passCompany(10);           
        insert testCompany;       
                              
        ApexPages.currentPage().getParameters().put('oId', testCompany.Id);
        
        ISA_NewUserCtrl controller = new ISA_NewUserCtrl();
        controller.collectRetParams(pageRef);
        
        System.assertEquals(testCompany.Id, pageRef.getParameters().get('oId'));
        
        Test.stopTest();
    }
    
    @isTest
    private static void testCancel() {
        Test.startTest();
        
        ISA_NewUserCtrl controller = new ISA_NewUserCtrl();
        controller.doCancel();
        
        Test.stopTest();
    }
}