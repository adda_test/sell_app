public class ISA_ManagePanelsCtrl extends AbstractController {
	public List<InsightPanelWrapper> panels {get; set;}
	private String defaultPanelId;
	
	public ISA_ManagePanelsCtrl() {
		super();
		
		this.panels = new List<InsightPanelWrapper>();
		this.defaultPanelId = '';
	}
	
	public PageReference initPage() {
		PageReference pr = super.init();
		
		if (pr != null) {
			return pr;
		}
		
		for (Survey_Design__c panel : getPanels()) {
			this.panels.add(new InsightPanelWrapper(panel));
			
			if (panel.Name == AUTH_StaticVariables.DEFAULT_INSIGHT_PANEL_NAME) {
				this.defaultPanelId = panel.Id;
			}
		}
		
		return null;
	}
	
	private List<Survey_Design__c> getPanels() {
		return [
			SELECT Name, 
				(SELECT Name 
				FROM Questions__r) 
			FROM Survey_Design__c 
			WHERE Company__c = :this.userCompany 
			LIMIT 1000];
	}
	
	public void doCreatePanel() {
		String panelName = ApexPages.currentPage().getParameters().get('panelName');
		String panelId = ApexPages.currentPage().getParameters().get('panelId');
		
		if ( ! String.isEmpty(panelName)) {
			if ( ! String.isEmpty(panelId) && panelId.length() > 10) {
				update new Survey_Design__c(Id = panelId, Name = panelName);
			} else {
				insert new Survey_Design__c(Name = panelName, Company__c = this.userCompany);
			}
			
			this.panels = new List<InsightPanelWrapper>();
			for (Survey_Design__c panel : getPanels()) {
				this.panels.add(new InsightPanelWrapper(panel));
			}
		}
	}
	
	public PageReference doCreateInsight() {
		String panelId = ApexPages.currentPage().getParameters().get('panelId');
		
		if ( ! String.isEmpty(panelId)) {
			PageReference pr = new PageReference(AUTH_StaticVariables.prefixSite + '/ISA_NewInsight');
			pr.getParameters().put('panelId', panelId);
			addRetUrl(pr);
			pr.setRedirect(true);
			
			return pr;
		}
		
		return null;
	}
	
	public void doRemoveInsight() {
		String insightId = ApexPages.currentPage().getParameters().get('insightId');
		
		if ( ! String.isEmpty(insightId)) {
			update new Question__c(
				Id = insightId, 
				Survey_Design__c = this.defaultPanelId
			);
			
			this.panels = new List<InsightPanelWrapper>();
			for (Survey_Design__c panel : getPanels()) {
				this.panels.add(new InsightPanelWrapper(panel));
			}
		}
	}
	
	public void doRemovePanel() {
		String panelId = ApexPages.currentPage().getParameters().get('panelId');
		
		if ( ! String.isEmpty(panelId)) {
			for (InsightPanelWrapper panel : panels) {
				if (panel.record.Id == panelId) {
					for (Question__c insight : panel.insights) {
						insight.Survey_Design__c = this.defaultPanelId;
					}
					
					update panel.insights;
					
					delete new Survey_Design__c(Id = panel.record.Id);
					break;
				}
			}
			
			this.panels = new List<InsightPanelWrapper>();
			for (Survey_Design__c panel : getPanels()) {
				this.panels.add(new InsightPanelWrapper(panel));
			}
		}
	}
	
	public void doSaveInsight() {
		List<Question__c> updateInsights = new List<Question__c>();
		String panelParam = ApexPages.currentPage().getParameters().get('panels');
		
		if ( ! String.isEmpty(panelParam)) {
			List<String> panels = panelParam.split('!');
			
			for (String panel : panels) {
				List<String> insightValues = panel.split(':');
				
				if (insightValues.size() == 2) {
					String panelValue = insightValues.get(0);
					
					for (String insight : insightValues.get(1).split(',')) {
						updateInsights.add(
							new Question__c(
								Id = insight, 
								Survey_Design__c = panelValue
							));
					}
				}
			}
		}
		
		update updateInsights;
		
		this.panels = new List<InsightPanelWrapper>();
		
		for (Survey_Design__c panel : getPanels()) {
			this.panels.add(new InsightPanelWrapper(panel));
			
			if (panel.Name == AUTH_StaticVariables.DEFAULT_INSIGHT_PANEL_NAME) {
				this.defaultPanelId = panel.Id;
			}
		}
	}
	
	public class InsightPanelWrapper {
		public Survey_Design__c record {get; set;}
		public List<Question__c> insights {get; set;}
		
		public InsightPanelWrapper() {
			this.record = new Survey_Design__c();
			this.insights = new List<Question__c>();
		}
		
		public InsightPanelWrapper(Survey_Design__c p_insightPanel) {
			this();
			
			this.record = p_insightPanel;
			this.insights.addAll(p_insightPanel.Questions__r);
		}
	}
}