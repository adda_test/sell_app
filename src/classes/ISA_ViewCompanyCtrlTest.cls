@isTest
private class ISA_ViewCompanyCtrlTest {

	static User__c tempUser(id id) {
        User__c testUser = new User__c(
            First_Name__c = 'First', 
            Last_Name__c = 'Last', 
            Phone__c = '123455678', 
            Username__c = 'testLogin', 
            Password__c = 'testPassword', 
            Role__c = 'Company User', 
            Company__c = id,
            Expiration_Date__c = Date.today(), 
            Create_Date__c = Date.today(),
            SessionId__c = '1',
            Active__c = true,
            Email__c = 'test@mail.com');
        return testUser;
    }
    
    static Company__c tempCompany() {
        Company__c company = new Company__c(
            Name = 'test name',
            Admin_First_Name__c = 'firstname',
            Admin_Last_Name__c = 'lastname',
            Admin_Email__c = 'test@mail.com',
            Admin_Login__c = 'testLogin',
            Admin_Password__c = 'testPassword',
            Address1__c = 'addr1',
            Address2__c = 'addr2',
            Trial_Start__c = Date.today(),
            Maximum_Users__c = 10);
        return company;
    }
    
    @isTest
    private static void testInit() {
    	
        Test.startTest();
        
        Company__c testCompany = tempCompany();           
        insert testCompany;      
        User__c testUser = tempUser(testCompany.Id);    
        insert testUser; 
                              
        ApexPages.currentPage().getParameters().put('sId', testCompany.Id);
        
        ISA_ViewCompanyCtrl controller = new ISA_ViewCompanyCtrl();
        controller.initPage();
        
        System.assertNotEquals(null, controller.init());       
        
        Test.stopTest();
    } 
    
    @isTest
    private static void testInitPageWithCompanyId() {
        
        Test.startTest();
        
        Company__c testCompany = tempCompany();           
        insert testCompany;      
        User__c testUser = tempUser(testCompany.Id);    
        insert testUser; 
                              
        ApexPages.currentPage().getParameters().put('sId', testCompany.Id);
        Cookie testCookie = new Cookie('SessionId','1',null,-1,false);
        ApexPages.currentPage().setCookies(new Cookie[] {testCookie});
        
        ISA_ViewCompanyCtrl controller = new ISA_ViewCompanyCtrl();
        controller.initPage();
        System.assertEquals(testCompany.Id, controller.company.Id);  
        
        controller.doEdit();
        System.assertNotEquals(null, controller.doEdit());   
        
        controller.doNewUser(); 
        System.assertNotEquals(null, controller.doNewUser()); 
        
        Test.stopTest();
    } 
    
    @isTest
    private static void testInitPageWithoutCompanyId() {
        
        Test.startTest();
        
        Company__c testCompany = tempCompany();         
        insert testCompany;      
        User__c testUser = tempUser(testCompany.Id);    
        insert testUser; 
                              
        ApexPages.currentPage().getParameters().put('sId', null);
        Cookie testCookie = new Cookie('SessionId','1',null,-1,false);
        ApexPages.currentPage().setCookies(new Cookie[] {testCookie});
        
        ISA_ViewCompanyCtrl controller = new ISA_ViewCompanyCtrl();
        controller.initPage();
        System.assertEquals(null, controller.company.Id);  
        
        controller.doEdit();
        System.assertEquals(null, controller.doEdit());   
        
        controller.doNewUser(); 
        System.assertEquals(null, controller.doNewUser()); 
        
        Test.stopTest();
    }
    
    @isTest
    private static void testDoNewUsersWithMaxUsers() {
        
        Test.startTest();
        
        Company__c testCompany = tempCompany();  
        testCompany.Maximum_Users__c = 0;           
        insert testCompany;      
        User__c testUser = tempUser(testCompany.Id);    
        insert testUser; 
                              
        ApexPages.currentPage().getParameters().put('sId', testCompany.Id);
        Cookie testCookie = new Cookie('SessionId','1',null,-1,false);
        ApexPages.currentPage().setCookies(new Cookie[] {testCookie});
        
        ISA_ViewCompanyCtrl controller = new ISA_ViewCompanyCtrl();
        controller.initPage();
       
        controller.doNewUser(); 
        System.assertEquals(null, controller.doNewUser()); 
        
        Test.stopTest();
    } 
}