public with sharing class ISA_AccountStrategiesCtrl extends AbstractController {
	public List<Strategy_Design__c> strategies {get; set;}
	
	private String accountId;
	
	public ISA_AccountStrategiesCtrl() {
		super();
		
		this.strategies = new List<Strategy_Design__c>();
	}
	
	public PageReference initPage() {
		PageReference afterInit = init();
		if (afterInit != null) {
			return afterInit;
		}
		
		String companyId = ApexPages.currentPage().getParameters().get('cId');
		this.accountId = ApexPages.currentPage().getParameters().get('aId');
		
		if ( ! String.isEmpty(companyId)) {
			this.strategies = [
				SELECT Name 
				FROM Strategy_Design__c 
				WHERE Company__c = :companyId 
				LIMIT 1000];
		}
		
		return null;
	}
	
	public PageReference doBack() {
		PageReference pr = new PageReference(AUTH_StaticVariables.prefixSite + '/apex/ISA_ViewAccount');
		pr.getParameters().put('sId', accountId);
		pr.getParameters().put('returnUrl', AUTH_StaticVariables.prefixSite + '/apex/ISA_ListViewAccount');
		pr.setRedirect(true);
		
		return pr;
	}
}