@isTest
private class ISA_CompositionCtrlTest {

    static User__c passUser(id id, string role) {
        User__c testUser = new User__c(
            First_Name__c = 'test', 
            Last_Name__c = 'test', 
            Phone__c = '123455678', 
            Username__c = 'test', 
            Password__c = 'testPassword', 
            Role__c = role, 
            Company__c = id,
            Expiration_Date__c = Date.today(), 
            Create_Date__c = Date.today(),
            SessionId__c = '1',
            Active__c = true,
            Email__c = 'test@mail.com');
        return testUser;
    }
    
    static Company__c passCompany() {
        Company__c company = new Company__c(
            Name = 'test',
            Admin_First_Name__c = 'test',
            Admin_Last_Name__c = 'test',
            Admin_Email__c = 'test@mail.com',
            Admin_Login__c = 'testLogin',
            Admin_Password__c = 'testPassword',
            Address1__c = 'addr1',
            Address2__c = 'addr2',
            Trial_Start__c = Date.today(),
            Maximum_Users__c = 10);
        return company;
    }
    
    static Account__c tempAccount(id companyId,id userId) {
    	Account__c account = new Account__c(
    		Name = 'test',
    		First_Name__c = 'test',
    		Last_Name__c = 'test', 
    		Phone__c = '123456', 
    		Email__C = 'test@mail.com', 
    		Address1__c = 'addr1', 
    		Address2__c = 'addr2', 
			City__c = 'City', 
			State__c = 'State',
			Zipcode__c = '1234', 
			Company__c = companyId,
			User__c = userId,
			Web_Site__c = 'web-site',
			Country__c = 'country');
		return account;
    } 
    
    static Contact__c tempContact(id companyId,id userId,id accountId) {
    	Contact__c contact = new Contact__c(
    		Name = 'test',
    		First_Name__c = 'test',
    		Last_Name__c = 'test', 
    		Phone_No__c = '123456', 
    		Email__C = 'test@mail.com', 
    		Address1__c = 'addr1', 
    		Address2__c = 'addr2', 
			City__c = 'City', 
			State__c = 'State',
			Pincode__c = '1234', 
			Company__c = companyId,
			User__c = userId,
			Account__c = accountId,
			Country__c = 'country');
		return contact;
    }
    
    @isTest
    private static void testInitNull() {
        Test.startTest();
        
        ISA_CompositionCtrl controller = new ISA_CompositionCtrl();
        controller.initPage();
        
        System.assertNotEquals(null, controller.init());       
        
        Test.stopTest();
    }
 
    @isTest
    private static void testInitNotNull() {
        Test.startTest();
        
        Company__c testCompany = passCompany();           
        insert testCompany;      
        User__c testUser = passUser(testCompany.Id,'Admin');    
        insert testUser; 
     
        PageReference pr = new PageReference('/apex/ISA_ListViewUser');
        
        ApexPages.currentPage().getParameters().put('sId', testUser.Id);
        ApexPages.currentPage().getParameters().put('labelObject','Users');
        ApexPages.currentPage().getParameters().put('searchValue','test');
        Cookie testCookie = new Cookie('SessionId','1',null,-1,false);
        ApexPages.currentPage().setCookies(new Cookie[] {testCookie});
        
        ISA_CompositionCtrl controller = new ISA_CompositionCtrl();
        controller.initPage();
        System.assertEquals(null, controller.init());  
        
        controller.doSelect();          
        System.assertEquals(pr.getUrl() , controller.doSelect().getUrl()); 
        
        controller.doSearch(); 
        System.assertEquals(ApexPages.currentPage().getParameters().get('searchValue'), controller.doSearch().getParameters().get('searchString'));         
        
        Test.stopTest();
    } 
    
    @isTest
    private static void testQuickSearch() {
    	
    	Test.startTest();
        
        Company__c testCompany = passCompany();           
        insert testCompany;      
        User__c testUser = passUser(testCompany.Id,'Company Admin');    
        insert testUser;      
        Account__c testAccount = tempAccount(testCompany.Id,testUser.Id);    
        insert testAccount; 
        Contact__c testContact = tempContact(testCompany.Id,testUser.Id,testAccount.Id);    
        insert testContact;
        
        ApexPages.currentPage().getParameters().put('searchString','test');
        Cookie testCookie = new Cookie('SessionId','1',null,-1,false);
        ApexPages.currentPage().setCookies(new Cookie[] {testCookie});
        
        ISA_CompositionCtrl controller = new ISA_CompositionCtrl();
        
        controller.initPage();       
        controller.doQuickSearch(); 
        System.assertNotEquals(null,controller.searchResults.size());
        
        controller.logout();
        System.assertNotEquals(null,controller.logout());
        
        Test.stopTest();
    }  
    
    @isTest
    private static void testGetFINDSObjectsAdmin() {
    	
    	Test.startTest();
    	
    	Company__c testCompany = passCompany();           
        insert testCompany;      
        User__c testUser = passUser(testCompany.Id,'Admin');    
        insert testUser;
        
        Cookie testCookie = new Cookie('SessionId','1',null,-1,false);
        ApexPages.currentPage().setCookies(new Cookie[] {testCookie}); 
        
        ISA_CompositionCtrl controller = new ISA_CompositionCtrl();
        controller.initPage();
        
        System.assertNotEquals(null,controller.getObjectsByUserRole());
        
        Test.stopTest();
    }   
    
    @isTest
    private static void testGetFINDSObjectsCompanyUser() {
    	
    	Test.startTest();
    	
    	Company__c testCompany = passCompany();           
        insert testCompany;      
        User__c testUser = passUser(testCompany.Id,'Company User');    
        insert testUser;
        
        Cookie testCookie = new Cookie('SessionId','1',null,-1,false);
        ApexPages.currentPage().setCookies(new Cookie[] {testCookie}); 
        
        ISA_CompositionCtrl controller = new ISA_CompositionCtrl();
        controller.initPage();
        
        System.assertNotEquals(null,controller.getObjectsByUserRole());
        
        Test.stopTest();
    }
    
        @isTest
    private static void testGetFINDSObjectsWithoutUserRole() {
    	
    	Test.startTest();
    	
    	Company__c testCompany = passCompany();           
        insert testCompany;      
        User__c testUser = passUser(testCompany.Id,null);    
        insert testUser;
        
        Cookie testCookie = new Cookie('SessionId','1',null,-1,false);
        ApexPages.currentPage().setCookies(new Cookie[] {testCookie}); 
        
        ISA_CompositionCtrl controller = new ISA_CompositionCtrl();
        controller.initPage();
        
        System.assertNotEquals(null,controller.getObjectsByUserRole());
        
        Test.stopTest();
    }    
    
    @isTest
    private static void testGetFINDSObjects() {
    	
    	Test.startTest(); 
        
        ISA_CompositionCtrl controller = new ISA_CompositionCtrl();     
        System.assertNotEquals(null,controller.getFINDSObjects('Admin'));   
        System.assertNotEquals(null,controller.getFINDSObjects('Company Admin'));     
        System.assertNotEquals(null,controller.getFINDSObjects('Company User'));
        
        Test.stopTest();
    } 
      
}