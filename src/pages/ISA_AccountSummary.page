<apex:page controller="ISA_AccountSummaryController" action="{! initPage }" showHeader="false" sidebar="false">

	<apex:includeScript value="https://www.google.com/jsapi"/>
	<apex:includeScript value="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"/>
	<!--Load the AJAX API-->
	 
	<!-- highcharts -->
	<script src="https://code.highcharts.com/highcharts.js"></script>
	<script src="https://code.highcharts.com/highcharts-more.js"></script>
	
	<title>Account Summary</title>
	
	<style>
		.buttonClass {
			padding: 2px 2px;
			margin: 10px 10px;
			font-size: 12px;
		}
		
		.navigationButton{
			float: left;
			background-color: white;
			border: 1px solid #222;
			-webkit-border-radius: 2px;
			-moz-border-radius: 2px;
			border-radius: 3px;
			background-color: rgb(66, 66, 66);
			color: white;
		}
		
	</style>
	
	<script>
		j$ = jQuery.noConflict();

		// highcharts
		j$(function () {
		
			// insightsChartId
			j$('#insightsChartId').highcharts({
				chart: {
					type: 'bubble'
				},
				legend: 'none',
				plotOptions : {
					bubble : {
						tooltip : {
							headerFormat: '<div>',
							pointFormat : '{point.customPointFormat}',
							footerFormat: '</div>'
							
						}
					}
				},
				title: {
					text: ''
				},
				xAxis: {
					title: {
						text: '% Response'
					}				
				},
				yAxis: {
					plotLines: [ {
							color: '#F8BB0E',
							width: 2,
							value: {! accountAvgY },
							label: { 
								text: '',
								rotation: 360
							}
					},
					{
							color: '#F8BB0E',
							width: 2,
							value: {! topInsightsAvgY },
							label: { 
								text: 'top 10%: {! topInsightsAvgY }%',
								rotation: 360
							}
					}],
					minPadding: 0.1,
					max: 100,
					startOnTick: false,
            		endOnTick: false,
            		
					title: {
						text: '% Max Score'
					}
				},
			
				series: [
			    		{
			    			name: 'No Data',
			    			data: [{ x:0, y:0, z:2, count: '{! countNoResponce }', customPointFormat: 'No Data'}],
			    			color: 'red',
							dataLabels:
							{
				                enabled: true,
				                formatter: function () {
				                    return this.point.count;
				                },
				                style: {
				                    color: '#FFFFFF',
				                    fontWeight: 'bold',
				                    textShadow: '0px 0px 3px black'
				                }
				            }
			    		},
						{
							data: [
								<apex:repeat value="{!insightChartList }" var="a">
						   			{x:{! a.maxScore }, y:{! a.response }, z:{! a.weight }, customPointFormat: '{!a.customPointFormat}'},
					   			</apex:repeat>
							],
							color: '#aac5e9'
						}						
					]
			   		
			});
			// -- end insightsChartId
		
			j$('#contactChartId').highcharts({
				chart: {
					type: 'column'
				},


				title: {
					text: ''
				},
				
				plotOptions : {
					column : {
						pointPadding: 0,
                		groupPadding: 0,
						dataLabels: {
		                    inside: true,
		                    enabled: true,
			                style: {
			                    color: '#FFFFFF',
			                    fontWeight: 'bold',
			                    textShadow: '0px 0px 3px black'
			                },
							formatter: function () {								
			                    if(typeof this.point.countNoInsight != 'undefined')
			                        return this.point.countNoInsight;
							
								return this.y;
							}
		                },
						tooltip : {
							headerFormat: '<div>',
							pointFormat : '{point.customPointFormat}',
							footerFormat: '</div>'
						}
					}
				},
				
				xAxis: { 
					
			        labels: { enabled: false},
			        tickLength: 0,
					title: {
						text: 'Influence'
					}
				},
				
				yAxis: {
					floor: 0,
					
					plotLines: [ {
							color: '#F8BB0E',
							width: 2,
							value: {! accountAvgY },
							label: { 
								text: 'Avg: {! accountAvgY }%',
								rotation: 360
							},
							zIndex: 50,
							id: 'avg-1'
					},
					{
							color: '#F8BB0E',
							width: 2,
							value: {! topAccountAvgY },
							label: { 
								text: 'top 10%: {! topAccountAvgY }%',
								rotation: 360,
							},
							zIndex: 50,
							id: 'avg-2'
					}],
					
					startOnTick: true,
            		endOnTick: false,
					title: {
						text: 'Insight Index'
					}
				},
				
				legend: {
					enabled: false
				},
			
				series: [
								<apex:repeat value="{! contactUserChartList }" var="a">
								{
									name: '{! a.bubbleName }',
									data: [{ y:{! a.insightIndex }, pointName:'{! a.bubbleName }', 
											strategy: '{! a.strategy }', accountName: '{! a.accountName }', 
											influence: {! a.influence }, customPointFormat: '{! a.customPointFormat }'},
									],
									color: '{! contactUserseriesColorMap[a.bubbleName] }',
								},								
								</apex:repeat>
								{
									name: 'No Insight data',
									data: [{ y: 20, countNoInsight:'{! countNoInsight}', customPointFormat: 'No Insight data' }],
									color: '#999999'
									
								}
					]
			});
			
			j$("text:contains('Highcharts.com')").hide();
		});
	</script>
	
	<apex:composition template="ISA_Composition" >
		<apex:define name="body">
			<apex:form id="formId">
			
				<div class="buttonsBlock">
					<div style="float: left; padding-right: 17px; font-size: 17px">{! account.Name }</div>
					<apex:commandButton value="Summary" action="{! doShowSummary }" styleClass="navigationButton" style="" />
					<apex:commandButton value="Contacts" action="{!goContactView}" styleClass="navigationButton" style="margin-left: 5px;" />
					<apex:commandButton value="Insights" action="{!doShowInsights}" styleClass="navigationButton" style="margin-left: 5px;" />
					<apex:commandButton value="Strategies" action="{! goAccountStrategiesView }" styleClass="navigationButton" style="margin-left: 5px;" />
				</div>
			
				<apex:outputPanel>
					<div class="row">
						<div class="col-md-6" style="padding-top: 21px">
<!-- 							<h4>1</h4> -->
							<div id="accountDetail" style="border: 1px solid #ccc; width: 100%; height: 230px;">
								<div style="/* border: 1px solid #ccc; */height: 100%;width: 55%;float: left; padding: 17px;font-size: larger;">
									<div style="color: blue;">{! accountDetail.name }</div>
									<div style="width: 90%; padding-top: 5px;">{! accountDetail.address }</div>
									<div style="bottom: 53px; position: absolute;">PH {! accountDetail.phone }</div>
									<div style="position: absolute;bottom: 31px;">WS {! accountDetail.website }</div>
								</div>
								<div style="width: 45%; height: 100%; position: relative;float: left; color: white; padding-left: 20px; padding-right: 20px; font-size: larger;">
									<div style="background-color: rgb(255, 153, 0); text-align: center; margin-top: 20px; border-radius: 5px">Insight Index:<br/> {!insightIndex}% ( &#9660; {! insightIndexDifference}%)</div>
									<div style="background-color: rgb(32, 167, 59); text-align: center; margin-top: 10px; border-radius: 5px">Completeness<br/> {! completeness }% ( &#9650; {! completenessDifference }%)</div>
									<div style="background-color: rgb(105, 105, 240); text-align: center; margin-top: 10px; border-radius: 5px">Retention ({! retention })<br/>Needs Assessment ({! needsAssesment })<br/>Values assessment ({! valuesAssessment })</div>
								</div>
							</div>
						</div>
						<div class="col-md-6"> 
							<apex:commandLink value="" action="{!goContactView}"><h4>Contacts: {! countContacts }</h4></apex:commandLink>
							<div id="contactChartId" style="border: 1px solid #ccc; width: 100%; height: 230px;"></div>
						</div>
					</div>
					
					<div class="row" style="padding-top: 10px;">
						<div class="col-md-6">
							<apex:commandLink value="" action="{! goActivityView }" ><h4>Activity</h4></apex:commandLink>
							<div id="activityBoxId" style="border: 1px solid #ccc; padding:7px; width: 100%; height: 230px; overflow: scroll;">

									<table style="width:100%; font-size: smaller;">
										<apex:repeat value="{! activityRecords }" var="wrapper">
											<tr>
												<td align="right" style="width: 180px; vertical-align: top;">
													<apex:outputText value="{0, date, EEEEEE MMM d, YYYY, h:m a}" >
														<apex:param value="{! wrapper.record['CreatedDate'] }" />
													</apex:outputText>
												</td>
												
												<td align="left" style="padding:0px 10px;">
													<apex:outputText escape="false" value="{! wrapper.record['Message__c'] }" />
												</td>
											</tr>
										</apex:repeat>
									</table>
								
							</div>
						</div>
						<div class="col-md-6">
							<apex:commandLink value="" action="{!doShowInsights}"><h4>Insights: {! countInsights }</h4></apex:commandLink>
							<div id="insightsChartId" style="border: 1px solid #ccc; width: 100%; height: 230px;">
							</div>
						</div>
					</div>
				</apex:outputPanel>
			</apex:form>
		</apex:define>
	</apex:composition>

</apex:page>