<apex:page controller="ISA_ListViewAccountCtrl" action="{! initPage }" showHeader="false" sidebar="false" standardstylesheets="false">
	<apex:stylesheet value="{!URLFOR($Resource.Spinner, 'spinner.css')}"/>
	
	<apex:includeScript value="{!URLFOR($Resource.Spinner, 'jquery.min.js')}"/>
	<apex:includeScript value="{!URLFOR($Resource.Spinner, 'spinner.js')}"/>
	
	<link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/themes/smoothness/jquery-ui.css" />
	<apex:includeScript value="https://www.google.com/jsapi"/>
	<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
	
	<!-- highcharts -->	
	<script src="https://code.highcharts.com/highcharts.js"></script>
	<script src="https://code.highcharts.com/highcharts-more.js"></script>
	
	<title>Accounts</title>
	
	<style>
		
		li#nvAccounts a{
			color:white ;
		}
		.topButtons {
			text-align: right;
		}
		
		.topActions {
			width: 100%;
		}
		
		.pagination {
			float: right;
		}
		
	</style>
	
	<script>
		j$ = jQuery.noConflict();
		
		function assign() {
			var userIds = '';
			var accountIds = j$('#accountIds').val();
			
			j$('#selectedUsersId li').each(function(index, value) {
				userIds += ',' + j$(value).find('div').attr('userId');
			});
			
			addAssignedUsers(userIds, accountIds);
			
			j$('#dialogAssignTeamId').dialog('destroy');
		}
	</script>
	
	<script>
		// highcharts
		j$(function () {
		    j$('#accountChartId').highcharts({
		
			    chart: {
			        type: 'bubble'
			    },
			    legend: {
			    	align: 'right',
			    	layout: 'vertical',
			    	verticalAlign: 'top'
			    },			    
		        plotOptions : {
		            bubble : {		            	
		                tooltip : {
		                    headerFormat: '<div>',
		                    pointFormat : '{point.customPointFormat}',		                    
		                    footerFormat: '</div>'
		                    
		                }
		            }
		        },
			    title: {
			    	text: ''
			    },
			    xAxis: {
			    	title: {
                    	text: 'Completeness'
                	},  
                	plotLines: [ {
                			color: '#F8BB0E',
			                width: 2,
			                value: {! avgX },
			                label: {
			                	text: 'Average: {! avgX }%'
			                },
			                id: 'avg-1'
            		}]                	
			    },
			    yAxis: {
					startOnTick: false,
            		endOnTick: false,
			    	title: {
                    	text: 'Insight Index'
                	},  
                	plotLines: [ {
                			color: '#F8BB0E',
			                width: 2,
			                value: {! avgY },
			                label: {
			                	text: 'Average: {! avgY }%'
			                },
			                id: 'avg-2'
            		}] 
			    },
			
			    series: [
			    		{
			    			name: 'No Data',
			    			data: [{ x:0, y:0, z:2, count: {! allNoData }, customPointFormat: 'No Data'}],
			    			color: 'red',
							dataLabels:
							{
				                enabled: true,
				                formatter: function () {
				                    return this.point.count;
				                },
				                style: {
				                    color: '#FFFFFF',
				                    fontWeight: 'bold',
				                    textShadow: '0px 0px 3px black'
				                }
				            },
				            zIndex: 100
			    		},
				    	<apex:repeat value="{! seriesMap }" var="serKey">
				    	{
				    		name: '{! serKey }',
				    		data: [
					    		<apex:repeat value="{! seriesMap[serKey] }" var="a">
			               			{x:{! a.completeness },y:{! a.insightIndex },z:{! a.childCount }, pointName:'{! a.bubbleName }', 
			               					noData: '{! a.noData }', noStrategy: '{! a.noStrategy }', customPointFormat: '{! a.customPointFormat }'},
			           			</apex:repeat>			    			
				    		],
				    		color: '{! seriesColorMap[serKey] }'
				    	},				    	
				    	</apex:repeat>				    	
			    	]
			   		
			});
			
			j$("text:contains('Highcharts.com')").hide();
			
		});
		
	</script>
	
	<apex:composition template="ISA_Composition" >
		<apex:define name="body">
			<apex:form id="formId">
				<div class="splashStatus" id="splashDiv">
					<div class="circle"><img src="{!URLFOR($Resource.Spinner, 'spinner.gif')}"/></div>
					<div class="txt">Please Wait. Loading...</div>
				</div>
				
				<apex:actionStatus id="splashStatus" onstart="startSplash();" onstop="endSplash(); "/>
				
				<apex:actionFunction name="addAssignedUsers" action="{! addAssignTeam }" reRender="pageBlockId" status="splashStatus" >
					<apex:param name="userIds" value="" />
					<apex:param name="accountIds" value="" />
				</apex:actionFunction>
				
				<apex:actionFunction name="deleteR" action="{! doRemove }" reRender="pageBlockId, paginationId" status="splashStatus">
				</apex:actionFunction>
				
				<div class="topButtons">
					<apex:commandLink style="margin: 5px;" styleClass="btn btn-blue-spin btn-sm" value="Create Account" action="{! doCreateObject }" />
					<apex:commandLink styleClass="btn btn-blue-spin btn-sm" value="Upload Accounts" action="{! doUploadSObjects }" 
							rendered="{! IF(userRole != 'Company User', TRUE, FALSE) }" />
				</div>
				
				<div id="accountChartId" style="width: 100%; height: 300px; font-size: 18px;">		
				</div>
				
				<apex:outputPanel id="paginationId">
					<table class="topActions">
						<tr>
							<td width="70px">
								Showing:
							</td>
							<td width="80px;">
								<apex:selectList styleClass="form-control" value="{! selectedCountRecords }" size="1">
									<apex:selectOptions value="{! countRecordOptions }" />
									
									<apex:actionSupport event="onchange" action="{! doChange }" reRender="pageBlockId" />
								</apex:selectList>
							</td>
							<td width="100px;" align="left" style="padding-left: 5px;">
								 / {! records.size }
							</td>
							<td>
								Bulk actions:
									<apex:commandButton style="margin: 5px;" styleClass="btn btn-blue-spin btn-sm" 
											value="Assign Team" onclick="showDialog(); return false;" 
											rendered="{! AND(userRole == 'Company Admin') }" />
									
									<apex:commandButton style="margin: 5px;" styleClass="btn btn-blue-spin btn-sm" 
											value="Delete" onclick="deleteRecords(); return false;" 
											rendered="{! AND(userRole != 'Company User') }" status="splashStatus"/>
							</td>
							<td>
								<table class="pagination">
									<tr>
										<td>
											<apex:commandButton value="<<" styleClass="btn btn-default btn-xs" action="{! doFirstPage }" reRender="pageBlockId" />
										</td>
										<td>
											<apex:commandButton styleClass="btn btn-default btn-xs" value="<" action="{! doPreviousPage }" reRender="pageBlockId" />
										</td>
										<td>
											<apex:outputText value="{! pageIndex }" />
										</td>
										<td>
											<apex:commandButton styleClass="btn btn-default btn-xs" value=">" action="{! doNextPage }" reRender="pageBlockId" />
										</td>
										<td>
											<apex:commandButton styleClass="btn btn-default btn-xs" value=">>" action="{! doLastPage }" reRender="pageBlockId" />
										</td>
										<td>
											<apex:outputText value="{! countOfPage }" />
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</apex:outputPanel>
				
				<apex:pageBlock id="pageBlockId">
					<apex:pageMessages />
					
					<apex:pageblockTable id="pageBlockTableId" value="{! records }" var="rec" 
							styleClass="table table-responsive table-bordered table-hover" >
						
						<apex:column headerValue="Action" >
							<apex:facet name="header" >
								<input type="checkbox" id="allCheckboxId" onchange="checkAll(this);" />
								&nbsp;Action
							</apex:facet>
							
							<apex:inputCheckbox styleClass="sObject recordId{! rec.record['Id'] }" value="{! rec.checked }" />
						</apex:column>
						
						<apex:column >
								<apex:facet name="header">
									<apex:commandLink action="{! doSort }" reRender="pageBlockTableId">
										<apex:outputText escape="false" value="Account {! 
												IF(sortableMap['Name'], 
													IF(sortField == 'Name', 
														IF (sortOrder == 'ASC', 
															'&#9650;', 
															IF (sortOrder == 'DESC', 
																'&#9660;', 
																''
															)
														), 
														''
													),
													''
												)}" />
										
										<apex:param name="sortField" value="Name" />
										<apex:param name="sortOrder" value="{! IF(sortField == 'Name', IF (sortOrder == 'ASC', 'DESC', 'ASC'), 'ASC') }" />
									</apex:commandLink>
								</apex:facet>
								
								<apex:commandLink value="{! rec.record['Name'] }" action="{! doViewRecord }">
									<apex:param name="viewId" value="{! rec.record.Id }" />
								</apex:commandLink>
							</apex:column>
						
						<apex:repeat value="{! sortFields }" var="label" >
							<apex:column >
								<apex:facet name="header">
									<apex:commandLink action="{! doSort }" reRender="pageBlockTableId">
										<apex:outputText escape="false" value="{! label } {! 
												IF(sortableMap[fieldSetMap[label]], 
													IF(sortField == fieldSetMap[label], 
														IF (sortOrder == 'ASC', 
															'&#9650;', 
															IF (sortOrder == 'DESC', 
																'&#9660;', 
																''
															)
														), 
														''
													),
													''
												)}" />
										
										<apex:param name="sortField" value="{! fieldSetMap[label] }" />
										<apex:param name="sortOrder" value="{! IF(sortField == fieldSetMap[label], IF (sortOrder == 'ASC', 'DESC', 'ASC'), 'ASC') }" />
									</apex:commandLink>
								</apex:facet>
								
								<apex:outputLink value="/apex/ISA_AccountContacts?aId={! rec.record.Id }" rendered="{! IF(label == 'Insight Index', TRUE, FALSE) }">
									{! rec.record[fieldSetMap[label]] }
									<apex:outputPanel rendered="{! IF(NOT(ISNULL(rec.record[fieldSetMap[label]])), TRUE, FALSE) }">
										%
									</apex:outputPanel>
								</apex:outputLink>
								
								<apex:outputLink value="/apex/ISA_ContactNoData?aId={! rec.record.Id }" rendered="{! IF(label == 'No Data', TRUE, FALSE) }">
									{! rec.record[fieldSetMap[label]] }
								</apex:outputLink>
								
								<apex:outputLink value="/apex/ISA_ContactNoStrategy?aId={! rec.record.Id }" rendered="{! IF(label == 'No Strategy', TRUE, FALSE) }">
									{! rec.record[fieldSetMap[label]] }
								</apex:outputLink>
								
								<apex:commandLink value="{! rec.record['Last_Activity__c'] }" action="{! goLastActivity }" rendered="{! IF(label = 'Last Activity', TRUE, FALSE) }">
									<apex:param name="viewId" value="{! rec.record['Id'] }" />
								</apex:commandLink>

								<apex:outputLink value="/apex/ISA_AccountContacts?aId={! rec.record.Id }" rendered="{! IF(label == 'Contacts', TRUE, FALSE) }">
									{! rec.record[fieldSetMap[label]] }
								</apex:outputLink>
								
								<apex:outputField value="{! rec.record[fieldSetMap[label]] }" rendered="{! AND(label != 'Contacts', label != 'Insight Index', label != 'No Data', label != 'No Strategy', label != 'Last Activity') }"/>
							</apex:column>
						</apex:repeat>
					</apex:pageblockTable>
						
					<table class="pagination">
						<tr>
							<td>
								<apex:commandButton value="<<" styleClass="btn btn-default btn-xs" action="{! doFirstPage }" reRender="pageBlockId" />
							</td>
							<td>
								<apex:commandButton styleClass="btn btn-default btn-xs" value="<" action="{! doPreviousPage }" reRender="pageBlockId" />
							</td>
							<td>
								<apex:outputText value="{! pageIndex }" />
							</td>
							<td>
								<apex:commandButton styleClass="btn btn-default btn-xs" value=">" action="{! doNextPage }" reRender="pageBlockId" />
							</td>
							<td>
								<apex:commandButton styleClass="btn btn-default btn-xs" value=">>" action="{! doLastPage }" reRender="pageBlockId" />
							</td>
							<td>
								<apex:outputText value="{! countOfPage }" />
							</td>
						</tr>
					</table>
					
				</apex:pageBlock>
				
				<div id="dialogAssignTeamId" style="display:none" width="100%">
					<div width="100%" align="center">
						<input id="accountIds" type="hidden" />
						
						<label>Selected Accounts : </label>
						<br />
						
						<ul id="accountNames">
						</ul>
					</div>
					
					<br /> <br />
					
					<div width="100%" align="center">
						<apex:outputLabel for="usersId" value="Users: " />
						
						<apex:selectList id="usersId" value="{! selectedUser }" size="1" onchange="doSelectUser(this); return false;">
							<apex:selectOptions value="{! assignedUserOptions }" />
						</apex:selectList>
					</div>
					
					<div width="100%" align="center">
						<input id="userIds" type="hidden" />
						
						<apex:outputLabel for="selectedUsers" value="Selected Users: " />
						<ul id="selectedUsersId">
							<apex:repeat value="{! selectedUsers }" var="user">
								<li>
									<div style="min-height: 25px; min-width: 300px; max-width: 300px;">
										<span style="float: right;">{! user.name }</span>
										<a href="#" onclick="unassignUser(this); return false;" >delete</a>
									</div>
								</li>
							</apex:repeat>
						</ul>
					</div>
					
					<div width="100%" align="center">
						<apex:commandButton value="Assign" onclick="assign(); return false;" 
							reRender="formId" styleClass="btn btn-blue-spin btn-sm " />
						&nbsp;&nbsp;
						<apex:commandButton value="Cancel" styleClass="btn btn-blue-spin btn-sm"
							onclick="j$('#dialogAssignTeamId').dialog('destroy'); return false;" 
							reRender="formId" />
					</div>
				</div>
				
				<script>
					j$(function() {
						j$('.sObject').click(function() {
							j$(this).attr('checked', j$(this).is(':checked'));
						});
					});
					
					function showDialog() {
						if (j$('input:checked[class*="recordId"]').size() > 0) {
							j$('#dialogAssignTeamId').dialog({
								title: 'Assigned Team', 
								width: 350, 
								height: 400, 
								modal: true
							});
							
							var accountIds = [];
							var accountNames = [];
							
							j$('input:checked[class*="recordId"]').each(function(index, value) {
								var classes = j$(value).attr('class');
								accountIds.push(classes.split(' ')[1].substring(8));
								accountNames.push(j$(value).closest('tr').find('td').eq(1).find('a').eq(0).text());
							});
							
							var hidden = '';
							for (var i = 0; i < accountNames.length; i++) {
								j$('#dialogAssignTeamId').find('ul#accountNames').eq(0).append('<li>' + accountNames[i] + '</li>');
								hidden += ',' + accountIds[i];
							}
							hidden = hidden.substring(1);
							j$('#dialogAssignTeamId').find('input[type="hidden"][id="accountIds"]').val(hidden);
						} else {
							alert('Please, select at least one account record.');
						}
					}
					
					function doSelectUser(sel) {
						var userID = j$(sel).val();
						var userName = j$(sel).find(":selected").text();
						
						var ul = j$('#selectedUsersId');
						
						if (j$(ul).length) {
							ul.append('<li><div style="min-height: 25px; min-width: 300px; max-width: 300px;" userId="' + 
								userID + '"><span style="float:left;">' + userName + '</span>' + 
								'<a href="#" onclick="unassignUser(this); return false;">delete</a></div></li>');
						}
						
						j$(sel).find(":selected").remove();
					}
					
					function checkAll(ch) {
						j$('.sObject').each(function(index, value) {
							j$(value).val(j$(ch).is(":checked"));
						});
						
						var checkboxes = document.getElementsByClassName('sObject');
						
						for(var i = 0; i < checkboxes.length; i++) {
							checkboxes[i].checked = ch.checked;
						}
					}
					
					function unassignUser(li) {
						var userName = j$(li).prev('span').text();
						var userId = j$(li).closest('div').attr('userid');
						
						j$('select[id*=usersId]').append('<option value="' + userId + '">' + userName + '</option>');
						
						j$(li).closest('li').remove();
					}
					
					function deleteUser(a) {
						a.parentNode.style.display = 'none';
					}
					
					function deleteRecords() {
						if (j$('input:checked').size() > 0 && confirm('Are you sure delete selected records?')) {
							deleteR();
						}
					}
				</script>
				<div class="lightbox"></div>
			</apex:form>
		</apex:define>
	</apex:composition>
</apex:page>