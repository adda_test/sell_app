<apex:page controller="ISA_InsightListViewCtrl" showHeader="false" sidebar="false" standardstylesheets="false">
	<style>
		.topButtons {
			text-align: right;
		}
		
		.topActions {
			width: 100%;
		}
		
		.pagination {
			float: right;
		}
	</style>
	<apex:composition template="ISA_Composition" >
		<apex:define name="body">
			<script>
				function checkAll(ch) {
					var checkboxes = document.getElementsByName('sObject');
					
					for(var i = 0; i < checkboxes.length; i++) {
						checkboxes[i].checked = ch.checked;
					}
				}
			</script>
			
			<apex:form id="formId">
				<div class="topButtons">
					<apex:commandLink value="Create Insight" action="{! doCreateObject }" target="_blank" 
							style="margin: 5px;" styleClass="btn btn-blue-spin btn-sm" 
							rendered="{! IF(userRole != 'Company User', TRUE, FALSE) }"/>
				</div>
				<div style="background: #eee; width: 100%; height: 300px;color: white;font-size: 18px;">
					Chart
				</div>
				<table class="topActions">
					<tr>
						<td width="70px">
							Showing:
						</td>
						<td width="80px;">
							<apex:selectList styleClass="form-control" value="{! selectedCountRecords }" size="1">
								<apex:selectOptions value="{! countRecordOptions }" />
								
								<apex:actionSupport event="onchange" action="{! doChange }" reRender="pageBlockId" />
							</apex:selectList>
						</td>
						<td width="100px;" align="left" style="padding-left: 5px;">
							 / {! records.size }
						</td>
						<td>
							Bulk actions:
								<apex:commandButton value="Delete" action="{! doRemoveObject }" reRender="pageBlockTableId" 
										style="margin: 5px;" styleClass="btn btn-blue-spin btn-sm"/>
						</td>
						<td>
							<table class="pagination">
								<tr>
									<td>
										<apex:commandButton value="First" styleClass="btn btn-default btn-xs" action="{! doFirstPage }" reRender="pageBlockId" />
									</td>
									<td>
										<apex:commandButton styleClass="btn btn-default btn-xs" value="Previous" action="{! doPreviousPage }" reRender="pageBlockId" />
									</td>
									<td>
										<apex:outputText value="{! pageIndex }" />
									</td>
									<td>
										<apex:commandButton styleClass="btn btn-default btn-xs" value="Next" action="{! doNextPage }" reRender="pageBlockId" />
									</td>
									<td>
										<apex:commandButton styleClass="btn btn-default btn-xs" value="Last" action="{! doLastPage }" reRender="pageBlockId" />
									</td>
									<td>
										<apex:outputText value="{! countOfPage }" />
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				<apex:pageBlock id="pageBlockId">
					<apex:pageblockTable id="pageBlockTableId" value="{! records }" var="rec" 
						styleClass="table table-responsive table-bordered table-hover">
						<apex:column headerValue="Action" >
							<apex:facet name="header" >
								<input type="checkbox" id="allCheckboxId" onchange="checkAll(this);" />
								&nbsp;Action
							</apex:facet>
							
							<input type="checkbox" name="sObject" value="{! rec.checked }" />
							&nbsp;|&nbsp;
							<apex:commandButton value="edit" action="{! doEditRecord }" reRender="pageBlockId" 
									styleClass="btn btn-default btn-xs">
								<apex:param name="editRecordId" value="{! rec.record['Id'] }" />
							</apex:commandButton>
							&nbsp;|&nbsp;
							<apex:commandButton value="remove" action="{! doRemoveRecord }" reRender="pageBlockId" 
									styleClass="btn btn-default btn-xs">
								<apex:param name="removeRecordId" value="{! rec.record['Id'] }" />
							</apex:commandButton>
						</apex:column>
						
						<apex:column >
								<apex:facet name="header">
									<apex:commandLink action="{! doSort }" reRender="pageBlockTableId">
										<apex:outputText escape="false" value="Insight Name {! 
												IF(sortableMap['Name'], 
													IF(sortField == 'Name', 
														IF (sortOrder == 'ASC', 
															'&#9650;', 
															IF (sortOrder == 'DESC',																   '&#9660;', 
																''
															)
														), 
														''
													),
													''
												)}" />
										
										<apex:param name="sortField" value="Name" />
										<apex:param name="sortOrder" value="{! IF(sortField == 'Name', IF (sortOrder == 'ASC', 'DESC', 'ASC'), 'ASC') }" />
									</apex:commandLink>
								</apex:facet>
								
								<apex:outputLink value="/apex/ISA_ViewInsight?sId={! rec.record.Id }" >{! rec.record['Name'] }</apex:outputLink>
							</apex:column>
						
						<apex:repeat value="{! fieldSetMap }" var="label" >
							<apex:column >
								<apex:facet name="header">
									<apex:commandLink action="{! doSort }" reRender="pageBlockTableId">
										<apex:outputText escape="false" value="{! label } {! 
												IF(sortableMap[fieldSetMap[label]], 
													IF(sortField == fieldSetMap[label], 
														IF (sortOrder == 'ASC', 
															'&#9650;', 
															IF (sortOrder == 'DESC', 
																'&#9660;', 
																''
															)
														), 
														''
													),
													''
												)}" />
										
										<apex:param name="sortField" value="{! fieldSetMap[label] }" />
										<apex:param name="sortOrder" value="{! IF(sortField == fieldSetMap[label], IF (sortOrder == 'ASC', 'DESC', 'ASC'), 'ASC') }" />
									</apex:commandLink>
								</apex:facet>
								
								<apex:outputField value="{! rec.record[fieldSetMap[label]] }" />
							</apex:column>
						</apex:repeat>
					</apex:pageblockTable>
					
					<table class="pagination">
						<tr>
							<td>
								<apex:commandButton value="First" styleClass="btn btn-default btn-xs" action="{! doFirstPage }" reRender="pageBlockId" />
							</td>
							<td>
								<apex:commandButton styleClass="btn btn-default btn-xs" value="Previous" action="{! doPreviousPage }" reRender="pageBlockId" />
							</td>
							<td>
								<apex:outputText value="{! pageIndex }" />
							</td>
							<td>
								<apex:commandButton styleClass="btn btn-default btn-xs" value="Next" action="{! doNextPage }" reRender="pageBlockId" />
							</td>
							<td>
								<apex:commandButton styleClass="btn btn-default btn-xs" value="Last" action="{! doLastPage }" reRender="pageBlockId" />
							</td>
							<td>
								<apex:outputText value="{! countOfPage }" />
							</td>
						</tr>
					</table>
				</apex:pageBlock>
			</apex:form>
		</apex:define>
	</apex:composition>
</apex:page>